///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo.h"
#include "rheolef/geo_domain.h"
#include "rheolef/space_numbering.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"

#include "geo_header.h"

namespace rheolef {
using namespace std;

// =========================================================================
// cstors
// =========================================================================
template <class T, class M>
geo_base_rep<T,M>::geo_base_rep ()
 : geo_abstract_rep<T,M>(),
   _name("*nogeo*"),
   _version(0),
   _serial_number(0),
   _geo_element(),
   _gs(),
   _domains(),
   _have_connectivity(false),
   _have_neighbour(false),
   _node(),
   _dimension(0),
   _sys_coord(space_constant::cartesian),
   _xmin(),
   _xmax(),
   _hmin(),
   _hmax(),
   _piola_basis("P1"),
   _locator(),
   _tracer_ray_boundary(),
   _nearestor()
{
}
template <class T, class M>
geo_base_rep<T,M>::geo_base_rep (const geo_base_rep<T,M>& o)
 : geo_abstract_rep<T,M>(o),
   _name                (o._name),
   _version             (o._version),
   _serial_number       (o._serial_number),
   _geo_element         (o._geo_element),
   _gs                  (o._gs),
   _domains             (o._domains),
   _have_connectivity   (o._have_connectivity),
   _have_neighbour      (o._have_neighbour),
   _node                (o._node),
   _dimension           (o._dimension),
   _sys_coord           (o._sys_coord),
   _xmin                (o._xmin),
   _xmax                (o._xmax),
   _hmin		(o._hmin),
   _hmax                (o._hmax),
   _piola_basis         (o._piola_basis),
   _locator             (o._locator),
   _tracer_ray_boundary (o._tracer_ray_boundary),
   _nearestor           (o._nearestor)
{
  trace_macro ("*** PHYSICAL COPY OF GEO_BASE_REP ***");
}
template <class T>
geo_rep<T,sequential>::geo_rep ()
 : geo_base_rep<T,sequential>()
{
}
template <class T>
geo_rep<T,sequential>::geo_rep (const geo_rep<T,sequential>& omega)
 : geo_base_rep<T,sequential>(omega)
{
  trace_macro ("*** PHYSICAL COPY OF GEO_REP<seq> ***");
}
template <class T>
geo_abstract_rep<T,sequential>*
geo_rep<T,sequential>::clone() const 
{
  trace_macro ("*** CLONE OF GEO_REP<seq> ***");
  typedef geo_rep<T,sequential> rep;
  return new_macro (rep(*this));
}
template <class T>
const typename geo_rep<T,sequential>::geo_element_map_type&
geo_rep<T,sequential>::get_external_geo_element_map (size_type variant) const
{
  static const geo_element_map_type dummy;
  return dummy;
}
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
geo_rep<T,distributed>::geo_rep ()
 : geo_base_rep<T,distributed>(),
   _inod2ios_dis_inod(),
   _ios_inod2dis_inod(),
   _ios_ige2dis_ige(),
   _ios_gs(),
   _igev2ios_dis_igev(),
   _ios_igev2dis_igev()
{
}
template <class T>
geo_rep<T,distributed>::geo_rep (const geo_rep<T,distributed>& o)
 : geo_base_rep<T,distributed>(o),
   _inod2ios_dis_inod(o._inod2ios_dis_inod),
   _ios_inod2dis_inod(o._ios_inod2dis_inod),
   _ios_ige2dis_ige  (o._ios_ige2dis_ige),
   _ios_gs           (o._ios_gs),
   _igev2ios_dis_igev(o._igev2ios_dis_igev),
   _ios_igev2dis_igev(o._ios_igev2dis_igev)
{
  trace_macro ("*** PHYSICAL COPY OF GEO_REP<dis> ***");
}
template <class T>
geo_abstract_rep<T,distributed>*
geo_rep<T,distributed>::clone() const 
{
  trace_macro ("*** CLONE OF GEO_REP<dis> ***");
  typedef geo_rep<T,distributed> rep;
  return new_macro (rep(*this));
}
#endif // _RHEOLEF_HAVE_MPI
// =========================================================================
// global table of preloaded geos
// =========================================================================
template<class T, class M>
typename geo_base_rep<T,M>::loaded_map_t geo_base_rep<T,M>::_loaded_map;

template<class T, class M>
typename geo_base_rep<T,M>::loaded_map_t&
geo_base_rep<T,M>::loaded_map() 
{
  return _loaded_map;
}
// =========================================================================
// geo_load(name): handle name as "square[boundary]"
// =========================================================================
// geo(name) { base::operaor=(geo_load(name)); }
static
bool
is_domain (const std::string& name, std::string& bgd_name, std::string& dom_name)
{
  size_t n = name.length();
  if (n <= 2 || name[n-1] != ']') return false;
  size_t i = n-2;
  for (; i > 0 && name[i] != '[' && name[i] != '/'; i--) ;
  // ../../nfem/ptst/triangle_p10-v2 => name[i] == '/' : stop before "../../"
  if (i == 0 || i == n-2 || name[i] == '/') return false;
  // name[i] == '[' as "square[right]"
  bgd_name = name.substr(0,i); // range [0:i[
  dom_name = name.substr(i+1); // range [i+1:end[
  dom_name = dom_name.substr(0,dom_name.length()-1); // range [0:end-1[ : skip ']'
  return true;
}
template<class T, class M>
geo_basic<T,M>
geo_load (const std::string& filename)
{
  typedef typename geo_basic<T,M>::base base;
  std::string root_name = delete_suffix (delete_suffix(filename, "gz"), "geo");
  std::string bgd_name, dom_name;
  if (is_domain(root_name,bgd_name,dom_name)) {
    geo_basic<T,M> omega (bgd_name);
    return omega[dom_name];
  }
  std::string name = get_basename (root_name);

  // check for previously loaded geo:
  auto iter = geo_base_rep<T,M>::loaded_map().find (name);
  if (iter != geo_base_rep<T,M>::loaded_map().end()) {
    // geo(name) was already loaded: reuse it from table
#ifdef DEBUG_LOADED_TABLE_TRACE
    dis_warning_macro ("geo::geo_load("<<name<<"): REUSE");
#endif // DEBUG_LOADED_TABLE_TRACE
    geo_basic<T,M> omega;
    omega.base::operator= (base((*iter).second,typename base::internal()));
    return omega;
  }
  // allocate a new geo_rep<mpi> and load it from file
  geo_rep<T,M>* ptr = new_macro((geo_rep<T,M>));
  ptr->load (filename, communicator());
  geo_basic<T,M> omega;
  omega.geo_basic<T,M>::base::operator= (ptr);

  // store the new created geo in the loaded map: 
#ifdef DEBUG_LOADED_TABLE_TRACE
  dis_warning_macro ("geo::geo_load("<<name<<"): NEW");
#endif // DEBUG_LOADED_TABLE_TRACE
  geo_base_rep<T,M>::loaded_map().insert (std::make_pair(name, omega.base::get_count())); 
  return omega;
}
// ==============================================================================
// compact: convert a geo_domain_indirect to a geo_domain
// by compacting elements and node numbering to refer only to those used 
// by the domain: can then be used to solve a pb by FEM on this domain
// ==============================================================================
template <class T, class M>
geo_basic<T,M>
compact (const geo_basic<T,M>& gamma)
{
    if (gamma.variant() != geo_abstract_base_rep<T>::geo_domain_indirect) {
        return gamma;
    }
    /* allocate a new geo_rep object */
    const geo_domain_indirect_rep<T,M>* dom_ptr
     = dynamic_cast<const geo_domain_indirect_rep<T,M>*>(gamma.operator->());
    geo_domain_rep<T,M>* geo_ptr = new_macro((geo_domain_rep<T,M>)(*dom_ptr));
    geo_basic<T,M> new_gamma;
    new_gamma.geo_basic<T,M>::base::operator= (geo_ptr);
    return new_gamma;
}
// ==============================================================================
// geo interface for non-const members that are specific to geo_rep
// - check that pointer to geo_abstract_rep points to a geo_rep
// - as non-const, manage also entries in the table
// ==============================================================================

#define _RHEOLEF_save(M)	                                              	\
template <class T>								\
void										\
geo_basic<T,M>::save (std::string filename) const				\
{										\
  if (filename == "") filename = name();					\
  odiststream out (filename, "geo");						\
  put (out);									\
}
#define _RHEOLEF_set_name(M)		                                    	\
template <class T>								\
void										\
geo_basic<T,M>::set_name (std::string new_name)					\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot set_name on geo_domains");			\
  ptr->set_name(new_name);							\
  geo_base_rep<T,M>::loaded_map().insert (make_pair(name(), base::get_count()));\
}
#define _RHEOLEF_set_serial_number(M)	                                    	\
template <class T>								\
void										\
geo_basic<T,M>::set_serial_number (size_type i)					\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot set_serial_number on geo_domains");		\
  ptr->set_serial_number(i);							\
  geo_base_rep<T,M>::loaded_map().insert (make_pair(name(), base::get_count()));\
}
#define _RHEOLEF_reset_order(M)                                              	\
template <class T>								\
void										\
geo_basic<T,M>::reset_order (size_type order)					\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot reset_order on geo_domains");			\
  ptr->reset_order(order);							\
}
#define _RHEOLEF_set_nodes(M)                                              	\
template <class T>								\
void										\
geo_basic<T,M>::set_nodes (const disarray<node_type,M>& x)			\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot set_nodes on geo_domains");			\
  ptr->set_nodes(x);								\
}
#define _RHEOLEF_set_coordinate_system(M)                                    	\
template <class T>								\
void										\
geo_basic<T,M>::set_coordinate_system (coordinate_type sys_coord)		\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot set_coordinate_system on geo_domains");	\
  ptr->set_coordinate_system(sys_coord);					\
}
#define _RHEOLEF_set_dimension(M)	                                    	\
template <class T>								\
void										\
geo_basic<T,M>::set_dimension (size_type dim)					\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot set_dimension on geo_domains");		\
  ptr->set_dimension(dim);							\
}
#define _RHEOLEF_build_by_subdividing(M)		                        \
template <class T>								\
void										\
geo_basic<T,M>::build_by_subdividing (						\
  const geo_basic<T,M>& omega, 							\
  size_type k)									\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot build_by_subdividing on geo_domains");		\
  ptr->build_by_subdividing (omega, k);						\
}
#define _RHEOLEF_build_from_data(M)		                                \
template <class T>								\
void										\
geo_basic<T,M>::build_from_data (						\
  const geo_header&               hdr,						\
  const disarray<node_type, M>&   node,						\
        std::array<disarray<geo_element_auto<>,M>, reference_element::max_variant>& tmp_geo_element, \
  bool do_upgrade)								\
{										\
  geo_rep<T,M>* ptr = dynamic_cast<geo_rep<T,M>*>(base::pointer());		\
  check_macro (ptr != 0, "cannot build_from_data on geo_domains");		\
  ptr->build_from_data (hdr, node, tmp_geo_element, do_upgrade);		\
}

_RHEOLEF_save(sequential)
_RHEOLEF_set_name(sequential)
_RHEOLEF_set_serial_number(sequential)
_RHEOLEF_reset_order(sequential)
_RHEOLEF_set_nodes(sequential)
_RHEOLEF_set_coordinate_system(sequential)
_RHEOLEF_set_dimension(sequential)
_RHEOLEF_build_by_subdividing(sequential)
_RHEOLEF_build_from_data(sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_save(distributed)
_RHEOLEF_set_name(distributed)
_RHEOLEF_set_serial_number(distributed)
_RHEOLEF_reset_order(distributed)
_RHEOLEF_set_nodes(distributed)
_RHEOLEF_set_coordinate_system(distributed)
_RHEOLEF_set_dimension(distributed)
_RHEOLEF_build_by_subdividing(distributed)
#ifdef TODO
_RHEOLEF_build_from_data(distributed)
#endif // TODO
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_set_nodes
#undef _RHEOLEF_reset_order
#undef _RHEOLEF_set_coordinate_system
#undef _RHEOLEF_set_dimension
#undef _RHEOLEF_set_name
#undef _RHEOLEF_build_from_data
#undef _RHEOLEF_build_by_subdividing
// ----------------------------------------------------------------------------
// set order: resize node disarray ; internal node are not computed
// since they depend on the curved boundary information (cad data)
// that is not aivailable here
// ----------------------------------------------------------------------------
#define _RHEOLEF_reset_order(M)						\
template <class T>							\
void									\
geo_rep<T,M>::reset_order (size_type new_order)				\
{									\
  if (new_order == base::_piola_basis.degree()) return;			\
  base::_piola_basis.reset_family_index (new_order);			\
  size_type dis_nnod = space_numbering::dis_ndof (base::_piola_basis, base::_gs, base::_gs._map_dimension); \
  size_type     nnod = space_numbering::ndof     (base::_piola_basis, base::_gs, base::_gs._map_dimension); \
  base::_gs.node_ownership = distributor (nnod, base::comm(), nnod);	\
  disarray<point_basic<T>, M> new_node (base::_gs.node_ownership);	\
  for (size_type iv = 0, nv = base::_gs.ownership_by_dimension[0].size(); iv < nv; iv++) { \
    new_node [iv] = base::_node [iv];					\
  }									\
  base::_node = new_node;						\
  build_external_entities ();						\
}
_RHEOLEF_reset_order(sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_reset_order(distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_reset_order
// =========================================================================
// geo_base_rep destructor
// =========================================================================
template <class T, class M>
geo_base_rep<T,M>::~geo_base_rep()
{
  size_t n_erased = geo_base_rep<T,M>::loaded_map().erase (name());
  if (_name == "*nogeo*" || _name == "unnamed") {
    return;
  }
#ifdef DEBUG_LOADED_TABLE_TRACE
  if (n_erased != 1) { 
    dis_warning_macro ("geo::~geo(\""<<name()<<"\"): was not present in the preloaded table");
  }
  if (n_erased == 1) { 
    dis_warning_macro ("geo::~geo("<<name()<<"): ERASED from table");
  }
#endif // DEBUG_LOADED_TABLE_TRACE
}
// =========================================================================
// geo_base_rep members
// =========================================================================
template <class T, class M>
std::string
geo_base_rep<T,M>::name() const
{
  if (_serial_number == 0) return _name;
  char buffer [100];
  sprintf (buffer, "%.3d", int(_serial_number));
  return _name + "-" + buffer;
}
template <class T, class M>
bool
geo_base_rep<T,M>::have_domain_indirect (const std::string& name) const
{
  for (typename std::vector<domain_indirect_basic<M> >::const_iterator
        iter = _domains.begin(), last = _domains.end(); iter != last; iter++) {
    const domain_indirect_basic<M>& dom = *iter;
    if (name == dom.name()) return true;
  }
  return false;
}
template <class T, class M>
const domain_indirect_basic<M>&
geo_base_rep<T,M>::get_domain_indirect (const std::string& name) const
{
  for (typename std::vector<domain_indirect_basic<M> >::const_iterator
        iter = _domains.begin(), last = _domains.end(); iter != last; iter++) {
    const domain_indirect_basic<M>& dom = *iter;
    if (name == dom.name()) return dom;
  }
  error_macro ("undefined domain \""<<name<<"\" in mesh \"" << _name << "\"");
  return *(_domains.begin()); // not reached
}
template <class T, class M>
void
geo_base_rep<T,M>::insert_domain_indirect (const domain_indirect_basic<M>& dom) const
{
  for (typename std::vector<domain_indirect_basic<M> >::iterator
        iter = _domains.begin(), last = _domains.end(); iter != last; iter++) {
    domain_indirect_basic<M>& curr_dom = *iter;
    if (dom.name() == curr_dom.name()) {
      // a domain with the same name already exists: replace it
      curr_dom = dom;
      return;
    }
  }
  // insert a new domain:
  _domains.push_back (dom);
}
template <class T, class M>
typename geo_base_rep<T,M>::size_type
geo_base_rep<T,M>::size (size_type dim) const
{
  size_type sz = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(dim);
                 variant < reference_element:: last_variant_by_dimension(dim); variant++) {
    sz += _geo_element [variant].size();
  }
  return sz;
}
template <class T, class M>
typename geo_base_rep<T,M>::size_type
geo_base_rep<T,M>::dis_size (size_type dim) const
{
  size_type sz = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(dim);
                 variant < reference_element:: last_variant_by_dimension(dim); variant++) {
    sz += _geo_element [variant].dis_size();
  }
  return sz;
}
template <class T, class M>
void    
geo_base_rep<T,M>::dis_inod (
  const geo_element& K,
  std::vector<size_type>& dis_inod) const
{
  space_numbering::dis_idof (_piola_basis, _gs, K, dis_inod);
}
template <class T, class M>
void
geo_base_rep<T,M>::compute_bbox()
{
  // first, compute bbox sequentialy:
  for (size_type j = 0; j < _dimension; j++) {
    _xmin[j] =  std::numeric_limits<T>::max();
    _xmax[j] = -std::numeric_limits<T>::max();
  }
  for (size_type j = _dimension+1; j < 3; j++) {
    _xmin [j] = _xmax [j] = T(0);
  }
  for (size_type inod = 0, nnod = _node.size(); inod < nnod; inod++) {
    const point_basic<T>& x = _node [inod];
    for (size_type j = 0 ; j < _dimension; j++) {
      _xmin[j] = min(x[j], _xmin[j]);
      _xmax[j] = max(x[j], _xmax[j]);
    }
  }
  // distributed case: min & max are grouped across procs 
#ifdef _RHEOLEF_HAVE_MPI
  if (is_distributed<M>::value) {
    for (size_type j = 0 ; j < _dimension; j++) {
      _xmin[j] = mpi::all_reduce (comm(), _xmin[j], mpi::minimum<T>());
      _xmax[j] = mpi::all_reduce (comm(), _xmax[j], mpi::maximum<T>());
    }
  }
#endif // _RHEOLEF_HAVE_MPI

  // next, compute hmin & hmax
  _hmin = std::numeric_limits<T>::max();
  _hmax = T(0);
  for (size_t iedg = 0, nedg = geo_element_ownership(1).size(); iedg < nedg; ++iedg) {
    const geo_element& E = get_geo_element (1, iedg);
    const point_basic<T>& x0 = dis_node(E[0]);
    const point_basic<T>& x1 = dis_node(E[1]);
    T hloc = dist(x0,x1);
    _hmin = min(_hmin, hloc);
    _hmax = max(_hmax, hloc);
  }
#ifdef _RHEOLEF_HAVE_MPI
  if (is_distributed<M>::value) {
    _hmin = mpi::all_reduce (comm(), _hmin, mpi::minimum<T>());
    _hmax = mpi::all_reduce (comm(), _hmax, mpi::maximum<T>());
  }
#endif // _RHEOLEF_HAVE_MPI
}
template <class T, class M>
typename geo_base_rep<T,M>::const_reference
geo_base_rep<T,M>::get_geo_element (size_type dim, size_type ige) const
{
  size_type first_ige = 0, last_ige = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(dim);
                 variant < reference_element:: last_variant_by_dimension(dim); variant++) {
    last_ige += _geo_element [variant].size();
    if (ige < last_ige) return _geo_element [variant] [ige - first_ige];
    first_ige = last_ige;
  }
  error_macro ("geo_element index " << ige << " out of range [0:"<< last_ige << "[");
  return _geo_element [0][0]; // not reached
}
template <class T, class M>
typename geo_base_rep<T,M>::reference
geo_base_rep<T,M>::get_geo_element (size_type dim, size_type ige)
{
  size_type first_ige = 0, last_ige = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(dim);
                 variant < reference_element:: last_variant_by_dimension(dim); variant++) {
    last_ige += _geo_element [variant].size();
    if (ige < last_ige) return _geo_element [variant] [ige - first_ige];
    first_ige = last_ige;
  }
  error_macro ("geo_element index " << ige << " out of range [0:"<< last_ige << "[");
  return _geo_element [0][0]; // not reached
}
// =========================================================================
// utility: vertex ownership follows node ownership, but dis_numbering differ
// for high order > 1 meshes. This function converts numbering.
// =========================================================================
template <class T, class M>
typename geo_base_rep<T,M>::size_type
geo_base_rep<T,M>::dis_inod2dis_iv (size_type dis_inod) const
{
  if (is_sequential<M>::value || order() == 1) return dis_inod;
  return _gs.dis_inod2dis_iv (dis_inod);
}
template <class T, class M>
typename geo_base_rep<T,M>::size_type
geo_base_rep<T,M>::dis_iv2dis_inod (size_type dis_iv) const
{
  if (is_sequential<M>::value || order() == 1) return dis_iv;
  return _gs.dis_iv2dis_inod (dis_iv);
}
// --------------------------------------------------------------------------
// accessors to distributed data
// --------------------------------------------------------------------------
template <class T, class M>
typename geo_base_rep<T,M>::const_reference  
geo_base_rep<T,M>::dis_get_geo_element (size_type dim, size_type dis_ige) const
{
  if (is_sequential<M>::value) return get_geo_element (dim, dis_ige);
  if (_gs.ownership_by_dimension[dim].is_owned (dis_ige)) {
    size_type first_dis_ige = _gs.ownership_by_dimension[dim].first_index();
    size_type ige = dis_ige - first_dis_ige;
    return get_geo_element (dim, ige);
  }
  // element is owned by another proc ; get its variant and its index-variant igev
  size_type variant;
  size_type dis_igev = _gs.dis_ige2dis_igev_by_dimension (dim, dis_ige, variant);
  typename geo_base_rep<T,M>::const_reference res = _geo_element [variant].dis_at (dis_igev);
  return res;
}
// -------------------------------------------------------------------
// guards for omega.boundary() and omega.internal_sides()
// -------------------------------------------------------------------
template <class T, class M>
void boundary_guard (const geo_basic<T,M>& omega)
{
  typedef typename geo_basic<T,M>::size_type size_type;
  if (omega.have_domain_indirect ("boundary")) return;
  omega.neighbour_guard();
  size_type map_dim = omega.map_dimension();
  check_macro (map_dim > 0, "undefined boundary for 0D geometry");
  size_type sid_dim = map_dim-1;
  std::vector<size_type> isid_list;
  for (size_type isid = 0, nsid = omega.size(sid_dim); isid < nsid; isid++) {
    const geo_element& S = omega.get_geo_element (sid_dim, isid);
    if (S.master(1) != std::numeric_limits<size_type>::max()) continue; // S is an internal side
    isid_list.push_back (isid);
  }
  communicator comm = omega.sizes().ownership_by_dimension[sid_dim].comm();
  domain_indirect_basic<M> isid_dom (omega, "boundary", sid_dim, comm, isid_list);
  omega.insert_domain_indirect (isid_dom);
}
template <class T, class M>
void internal_sides_guard (const geo_basic<T,M>& omega)
{
  typedef typename geo_basic<T,M>::size_type size_type;
  if (omega.have_domain_indirect ("internal_sides")) return;
  omega.neighbour_guard();
  size_type map_dim = omega.map_dimension();
  check_macro (map_dim > 0, "undefined internal_sides for 0D geometry");
  size_type sid_dim = map_dim-1;
  std::vector<size_type> isid_list;
  for (size_type isid = 0, nsid = omega.size(sid_dim); isid < nsid; isid++) {
    const geo_element& S = omega.get_geo_element (sid_dim, isid);
    if (S.master(1) == std::numeric_limits<size_type>::max()) continue; // S is on boundary
    isid_list.push_back (isid);
  }
  communicator comm = omega.sizes().ownership_by_dimension[sid_dim].comm();
  domain_indirect_basic<M> isid_dom (omega, "internal_sides", sid_dim, comm, isid_list);
  isid_dom.set_broken (true);
  omega.insert_domain_indirect (isid_dom);
}
template <class T, class M>
void sides_guard (const geo_basic<T,M>& omega)
{
  typedef typename geo_basic<T,M>::size_type size_type;
  if (omega.have_domain_indirect ("sides")) return;
  omega.neighbour_guard();
  size_type map_dim = omega.map_dimension();
  check_macro (map_dim > 0, "undefined sides for 0D geometry");
  size_type sid_dim = map_dim-1;
  std::vector<size_type> isid_list;
  for (size_type isid = 0, nsid = omega.size(sid_dim); isid < nsid; isid++) {
    const geo_element& S = omega.get_geo_element (sid_dim, isid);
    isid_list.push_back (isid);
  }
  communicator comm = omega.sizes().ownership_by_dimension[sid_dim].comm();
  domain_indirect_basic<M> isid_dom (omega, "sides", sid_dim, comm, isid_list);
  isid_dom.set_broken (true);
  omega.insert_domain_indirect (isid_dom);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M) 				\
template class geo_base_rep<T,M>;				\
template class geo_rep<T,M>;					\
template class geo_basic<T,M>;					\
template geo_basic<T,M> geo_load (const std::string& name); 	\
template geo_basic<T,M> compact    (const geo_basic<T,M>&);    	\
template void boundary_guard       (const geo_basic<T,M>&);	\
template void internal_sides_guard (const geo_basic<T,M>&);	\
template void sides_guard          (const geo_basic<T,M>&);

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
