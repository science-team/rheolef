#
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# move a mesh: obtain a curved element
# from Pk reference triangle or tetra
BEGIN {
	have_rheolef = 0;
	have_3d      = 1;
      }
($1 != "") {
        if (have_rheolef) {
	  x = $1;
	  y = $2; 
	  z = $3;
        } else { # gmsh
	  i = $1;
	  x = $2;
	  y = $3; 
	  z = $4;
        }
        f = x*(1-x) + y*(1-y) + z*(1-z);
        if (! have_3d && z == 0) {
          a = 0.5;
          b = 0.25;
          c = 0;
        } else {
          a = 0.2;
          b = 0.15;
          c = 0.1;
        }
        u = a*f;
        v = b*f;
        w = c*f;
        x += u;
        y += v;
        z += w;
        if (have_rheolef) {
          printf("%.15g %.15g %.15g\n", x,y,z);
        } else {
          printf("%d %.15g %.15g %.15g\n", i, x,y,z);
        }
  }
