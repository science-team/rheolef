#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# utility
# ------------------------------------------
check_eval () {
  command="$*"
  echo $command 1>&2
  eval $command
  if test $? -ne 0; then
    echo "$0: error on command: $command"
    exit 1
  fi
}
# ------------------------------------------
# main
# ------------------------------------------
n="${1-"10"}"
quadrangle=false
if test $# -ge 2 && test $2 = "q" -o $2 = "uq"; then
  quadrangle=true
fi
boundary=true
file_basename="tmp-${n}"
file_bamgcad="${file_basename}.bamgcad"
file_bamg="${file_basename}.bamg"
file_dmn="${file_basename}.dmn"
file_geo_v1="${file_basename}-v1.geo"
file_geo_v2="${file_basename}-v2.geo"
if $boundary; then
  e2="101"; e3="101"; e4="101";
else
  e2="102"; e3="103"; e4="104";
fi
if $quadrangle; then
  iopt="-2q -thetaquad 0"
  h=`echo $n | gawk '{print 2.0/$1}'`
else
  iopt=""
  h=`echo $n | gawk '{print 1.0/$1}'`
fi
cat > ${file_bamgcad} << EOF1
MeshVersionFormatted
0
Dimension
2
Vertices
4
0  0     1
1  0     1
1  1     1
0  1     1
Edges
4
1  2     101
2  3     $e2
3  4     $e3
4  1     $e4
hVertices
$h $h $h $h
EOF1

check_eval "bamg $iopt -g ${file_bamgcad} -o ${file_bamg} 1>&2"

if $boundary; then
  cat > ${file_dmn} << EOF2
    EdgeDomainNames
           1
           boundary
EOF2
else
  cat > ${file_dmn} << EOF3
    EdgeDomainNames
           4
           bottom
           right
           top
           left
EOF3
fi

geo_bin="../../main/bin/geo"
geo_seq="../../main/tst/geo_seq_tst"

# aux files:
#check_eval "cat ${file_bamg} ${file_dmn} | ${geo_bin} -name toto -input-bamg -upgrade -geo -noverbose - > ${file_geo_v1}"
#check_eval "${geo_seq} < ${file_geo_v1} > ${file_geo_v2} 2>/dev/null"

check_eval "cat ${file_bamg} ${file_dmn} | ${geo_bin} -name toto -input-bamg -upgrade -geo -noverbose - | ${geo_seq} 2>/dev/null"


check_eval "rm -f ${file_bamgcad} ${file_bamg} ${file_dmn}"
