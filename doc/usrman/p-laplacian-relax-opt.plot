set terminal cairolatex pdf color standalone
set output "p-laplacian-relax-opt.tex"

set size square 
set colors classic
set xrange [1:8]
set yrange [0:2]
set xtics 1
set ytics 0.5
set xlabel '[c]{\large $p$}'
set  label '[r]{\large $\omega_\textrm{opt}$}' at graph -0.08,0.9 

f2(x) = a2*(1-1/x)+b2
a2  = -2
b2  =  2
#a2              = -2.03012 #       +/- 0.07938      (3.91%)
#b2              = 2.00722  #       +/- 0.04454      (2.219%)
#fit f2(x) 'p-laplacian-relax-opt.gdat' via a2,b2
#plot 'p-laplacian-relax-opt.gdat' u (1-1/$1):2 w lp, a2*x+b2

f2bis(p) = 2.0/p

plot  \
  'p-laplacian-relax-opt.gdat' t '[r]{computation: $\omega_\textrm{opt}$}' w p pt 7 lc 3 lw 2, \
  f2bis(x) t '[r]{fit: $\omega_\textrm{opt}(p)=2/p$}' w l lc 0 dt 3 lw 2

#pause -1 "<retour>"
