#ifndef _RHEO_DIS_MEMORY_USAGE_H
#define _RHEO_DIS_MEMORY_USAGE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   4 january 2018

namespace rheolef {
/**
@utilclassfile dis_memory_usage memory used in distributed environment

Synopsis
========
@snippet dis_memory_usage.h verbatim_dis_memory_usage

Description
===========
`seq_memory_usage`
>	Returns the current resident set size 
>	(physical memory use) measured in bytes, 
>	or zero if the value cannot be determined on this OS.

`dis_memory_usage`
>	Returns the accumulated memory usage
>	for all processed linked together via the default communicator.

`seq_peak_memory_usage`
>	Returns the peak (maximum so far)
>	resident set size (physical memory use) measured in bytes,
>	or zero if the value cannot be determined on this OS.

`dis_peak_memory_usage`
>	Returns the accumulated peak memory usage
>	for all processed linked together via the default communicator.

`memory_size`
>	Returns the size of physical memory (RAM) in bytes
>	associated to the given process.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/communicator.h"

namespace rheolef {

// [verbatim_dis_memory_usage]
size_t seq_memory_usage();
size_t dis_memory_usage();
size_t seq_peak_memory_usage();
size_t dis_peak_memory_usage();
size_t memory_size();
// [verbatim_dis_memory_usage]

} // namespace rheolef
#endif // _RHEO_DIS_MEMORY_USAGE_H
