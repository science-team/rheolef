%\chapter{Yield slip at the wall}
\newcommand{\eqdef}     {\stackrel {\mbox{\tiny def}}{=}}

% ---------------------------------- 
\subsection{Problem statement}
% ---------------------------------- 
\pbindex{yield slip}%
\cindex{geometry!pipe}%
  The problem of a Newtonian fluid with yield slip at the wall
  and flowing in a pipe \citep{RoqSar-2004} writes:\\
  \mbox{} \ \ $(P)$: find $u$, defined in $\Omega$, such that
\begin{subequations}
  \begin{eqnarray}
      -\Delta u = f
        \ \mbox{ in } \Omega
	\phantom{aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa}
        \label{eq-yield-slip-1}
        \\
	\left.\begin{array}{ll}
          \left| \Frac{\partial u}{\partial n} \right| \leq S
	     & \mbox{ when } u = 0
	     \\
          \Frac{\partial u}{\partial n} 
	   =
           -C_f |u|^{-1+n} u - S \Frac{u}{|u|}
	     & \mbox{ otherwise }
	\end{array}\right\}
        \ \mbox{ on } \partial\Omega
        \label{eq-yield-slip-2}
  \end{eqnarray}
\end{subequations}
  Here, $S\geq 0$ and $C_f>0$ are respectively the yield slip and the
  friction coefficient while $n>0$ is a power-law index.
  The computational domain $\Omega$ represents the cross-section
  of the pipe and $u$ is the velocity component along the axis of the pipe.
\pbindex{Poisson}%
\cindex{boundary condition!Robin}%
\cindex{boundary condition!Dirichlet}%
  The right-hand side $f$ is a given constant, and without loss
  of generality, we can suppose $f=1$~:
  the parameters are $S, C_f$ and $n$.
  When $S=0$ and $n=1$, the problem reduces to a Poisson
  problem with homogeneous Robin boundary condition that depend upon $C_f$.
% ------------------------------------------
\subsection{The augmented Lagrangian algorithm}
% ------------------------------------------
\subsubsection*{Principe of the algorithm}
\cindex{method!augmented Lagrangian}%
  The problem writes as a minimization one:
  \[	
	\min_{u\in H^1(\Omega)} J(u)
  \]
  where
  \[
	J(u) 
	=	
	\Frac{C_f}{1+n}
	\int_{\partial\Omega} |u|^{1+n} \, {\rm d}s
	+
	S
	\int_{\partial\Omega} |u| \, {\rm d}s
	+
	\Frac{1}{2}
	\int_\Omega |\bnabla u|^2 \, {\rm d}x
	-
	\int_\Omega f \, u\, {\rm d}x
  \]
  This problem is solved by using an augmented Lagrangian algorithm.
  The auxiliary variable $\gamma=u_{|\partial\Omega}$ is introduced
  together with the Lagrangian multiplier $\lambda$ associated
  to the constraints $u_{|\partial\Omega}-\gamma=0$.
  For all $r>0$, let:
  \begin{eqnarray*}
     L((u,\gamma);\lambda)
	&=&
	\Frac{C_f}{1+n}
	\int_{\partial\Omega} |\gamma|^{1+n} \, {\rm d}s
	+
	S
	\int_{\partial\Omega} |\gamma| \, {\rm d}s
	+
	\Frac{1}{2}
	\int_\Omega |\bnabla u|^2 \, {\rm d}x
	-
	\int_\Omega f \, u\, {\rm d}x
	\\
	&&
	+
	\int_{\partial\Omega} \lambda\,(u-\gamma) \, {\rm d}s
	+
	\Frac{r}{2}
	\int_{\partial\Omega} |u-\gamma|^2 \, {\rm d}s
  \end{eqnarray*}
  An Uzawa-like minimization algorithm writes:\\
  \begin{itemize}
  \item $k=0$: let $\lambda^{(0)}$ and $\gamma^{(0)}$ arbitrarily chosen.
  \item $k\geq 0$: let $\lambda^{(k)}$ and $\gamma^{(k)}$ being known.
  \begin{eqnarray*}
    u^{(k+1)} &:=& \argmin_{v\in H^1(\Omega)}
		 L((v,\gamma^{(k)});\lambda^{(k)})
	\\
    \gamma^{(k+1)} &:=& \argmin_{\delta\in L^{\infty}(\partial\Omega)}
		L((u^{(k+1)},\delta);\lambda^{(k)})
	\\
     \lambda^{(k+1)}
	&:=&
        \lambda^{(k)}
	+ \rho \left( u^{(k+1)} - \gamma^{(k+1)} \right)
        \ \mbox{ on } \partial\Omega
  \end{eqnarray*}
  \end{itemize}
  The descent step $\rho$ is chosen as $\rho=r$
  for sufficiently large $r$. 
  The Lagrangian $L$ is quadratic in $u$ and thus the computation
  of $u^{(k+1)}$ reduces to a linear problem.
  The non-linearity is treated when computing $\gamma^{(k+1)}$.
  This operation is performed point-by-point on $\partial\Omega$
  by minimizing:
  \[
	\gamma := \argmin_{\delta\in\mathbb{R}}
		\Frac{C_f|\delta|^{1+n}}{1+n}
		+ \Frac{r|\delta|^{2}}{2}
		+ S|\delta|
		- \xi\,\delta
  \]
  where $\xi=\lambda^{(k)}+r\,u^{(k+1)}$ is given.
  This problem is convex and its solution is unique.
  The solution has the form:
  \begin{equation}
     \gamma 
	=
	P_{n,r}(\xi)
	\eqdef
	\left\{ \begin{array}{cl}
          0
	    & \mbox{ when } |\xi| \leq S \\
	 \phi_{n,r}(|\xi|-S) \ {\rm sgn}(\xi)
	    & \mbox{ otherwise}
	\end{array}\right.
	\label{eq-def-proj}
  \end{equation}
  where $\phi_{n,r}(x)=f^{-1}_{n,r}(x)$
  and $f_{n,r}$ is defined for all $y>0$ by:
  \begin{equation}
      f_{n,r}(y) = C_f y^n + r\,y
	\label{eq-def-proj-f}
  \end{equation}

% ------------------------------------------------
\myexamplelicense{projection.h}
% ------------------------------------------------

  Observe that
  \mbox{$
      f_{n,r}'(y) = n C_f y^{-1+n} + r > 0
  $} 
  when $r >0$:
  the function is strictly increasing and is thus invertible.
  When $n=1$ then $f_{n,r}$ is linear and
  \mbox{$
	\phi_{1,r}(x)
	=
	x/(C_f+r) 
  $}.
  When $n=1/2$ this problem reduces to a second order polynomial equation
  and the solution is also explicit:
  \[
	\phi_{\frac{1}{2},r}(x)
	=
       \Frac{1}{4}
	\left(
	  \left\{ 1+\Frac{4r\,x}{C_f^2} \right\}^{1/2} - 1
        \right)^2
  \]
  When $r=0$, for any $n>0$ the solution is
  \mbox{$
	\phi_{n,0}(x)
	=
	(x/C_f)^{\frac{1}{n}} 
  $}.
  In general, when $n>0$ and $r>0$, the solution is no more explicit.
\cindex{method!newton}%
  We consider the Newton method:
  \begin{itemize}
   \item $i=0$: Let $y_0$ being given.
   \item $i\geq 0$: Suppose $y_i$ being known and compute
   \mbox{$
	y_{i+1} = y_i - (f_{n,r}(y_i)-x)/f'_{n,r}(y_i)
   $}.
  \end{itemize}

% ------------------------------------------------
\myexamplelicense{phi.h}
% ------------------------------------------------

In the present implementation, in order to avoid too large steps,
the Newton step
is damped when $y_{i+1}$ becomes negative.

  The Uzawa algorithm writes:
  \begin{itemize}
  \item $k=0$: let $\lambda^{(0)}$ and $\gamma^{(0)}$ arbitrarily chosen.
  \item $k\geq 0$: let $\lambda^{(k)}$ and $\gamma^{(k)}$ being known, find $u^{(k+1)}$,
	defined in $\Omega$, such that
  \begin{eqnarray*}
      -\Delta u^{(k+1)}
	&=&
	f
        \ \mbox{ in } \Omega
	\\
      \ \Frac{\partial u^{(k+1)}}{\partial n} 
        +
	r\,u^{(k+1)}
        &=&
	r\,\gamma^{(k)} - \lambda^{(k)} 
        \ \mbox{ on } \partial\Omega
  \end{eqnarray*}
  and then compute explicitely $\gamma^{(k+1)}$
  and $\lambda^{(k+1)}$:
  \begin{eqnarray*}
    \gamma^{(k+1)}
	&:=&
	P_{n,r}\left(\lambda^{(k)} + r\,u^{(k+1)}\right)
        \ \mbox{ on } \partial\Omega
        \\
    \lambda^{(k+1)}
	&:=&
        \lambda^{(k)}
	+ r \left( u^{(k+1)} - \gamma^{(k+1)} \right)
        \ \mbox{ on } \partial\Omega
  \end{eqnarray*}
  \end{itemize}
  This algorithm reduces the nonlinear problem to a sequence of
  linear and completely standard Poisson problems with a Robin boundary condition
  and some explicit computations.
  At convergence, $\lambda = -\Frac{\partial u}{\partial n}$ and
  $\gamma=u$ on $\partial\Omega$.

  Note that the solution satisfies the following variational formulation:
  \begin{eqnarray*}
    \int_\Omega \bnabla u\,.\bnabla v\,{\rm d}x
        +
        \int_{\partial\Omega} v\,\lambda\,{\rm d}s
        &=&
        \int_\Omega f\,v\,{\rm d}x
	,\ \ \forall v\in H^1(\Omega)
	\\
    \int_{\partial\Omega} u\,\gamma\,{\rm d}s
        -
	\int_{\partial\Omega} P_{n,0}(\lambda)\,\gamma\,{\rm d}s
        &=&
	0
	,\ \ \forall \gamma\in L^\infty(\partial\Omega)
  \end{eqnarray*}
  This formulation is the base of the computation of the
  residual test used for the stopping criteria.

% ------------------------------------------------
\myexamplelicense{yield_slip_augmented_lagrangian.cc}

\myexamplelicense{poisson_robin.icc}

\myexamplelicense{yield_slip_augmented_lagrangian.icc}
% ------------------------------------------------

Observe also that the stopping criterion for breaking the loop
bases on the max of the relative error for the $\lambda_h$ variable.
For this algorithm, this stopping criterion guaranties that all
residual terms of the initial problem are also converging to zero,
as it will be checked here.
Moreover, this stopping criterion is very fast to compute while
the full set of residual terms of the initial problem would take
more computational time inside the loop.

% --------------------------------------------- 
\subsubsection*{Running the program}
% --------------------------------------------- 
 
  \begin{figure}[htb]
     \begin{center}
     %\mbox{}\hspace{-2.7cm}
       \begin{tabular}{cccc}
       %  \includegraphics[height=4.3cm]{yield-slip-50-n-0,5-S-0,6.png} &
          \includegraphics[height=6.0cm]{yield-slip-50-n-1-S-0,6.png} &
       %  \includegraphics[height=4.3cm]{yield-slip-50-n-1,5-S-0,6.png} &
	  \stereoglasses
       \end{tabular}
     \end{center}
     \caption{The yield slip problem for $S=0.6$ and $n=1$.
	% (a) $n=0.5$; (b) $n=1$; (c) $n=1.5$.
       }
     \label{fig-yield-slip-visu}
  \end{figure}
  Assume that the previous code is contained in
  the file \file{yield-slip-augmented-lagrangian.cc}.
  Compile and run the program as usual:
\begin{verbatim}
  make yield_slip_augmented_lagrangian
  mkgeo_grid -a -1 -b 1 -c -1 -d 1 -t 50 > square.geo
  ./yield_slip_augmented_lagrangian square.geo P1 0.6 1 > square.field
\end{verbatim}
  Also you can replace \code{P1} by \code{P2}.
  The solution can be represented in elevation view
     (see Fig.~\ref{fig-yield-slip-visu}):
\begin{verbatim}
  field square.field -elevation -stereo
\end{verbatim}
  As analysed by \citep{RoqSar-2004}, when $S \leq 0.382$, the fluid
  slips at the wall, when $0.382 < S < 0.674$, the fluid partially
  sticks and when $S \geq 0.674$ the fluid completely sticks.
  Remark that the velocity is not zero along the boundary: there is a
  stick-slip transition point.
  \begin{figure}[htb]
     \mbox{}\hspace{-2.2cm}
     \begin{tabular}{ccc}
          \includegraphics[height=6cm]{yield-slip-50-S-0,6-n-cut0.pdf} &
          \includegraphics[height=6cm]{yield-slip-50-S-0,6-n-cut.pdf} &
          \includegraphics[height=6cm]{yield-slip-50-S-0,6-n-lambda.pdf}
     \end{tabular}
     \caption{The yield slip problem for $S=0.6$:
	cut of the velocity
	(left) along the $0x_0$ axis ;
	(center) along the boundary ;
	(right) cut of the normal stress $\lambda$ along the boundary.
       }
     \label{fig-yield-slip-cut}
  \end{figure}
  The velocity along along the $0x_0$ axis and the top boundary
  are available as (see Fig.~\ref{fig-yield-slip-cut}):
\pindexopt{field}{-normal}%
\pindexopt{field}{-origin}%
\pindexopt{field}{-cut}%
\pindexopt{field}{-gnuplot}%
\pindexopt{field}{-domain}%
\pindexopt{field}{-elevation}%
\begin{verbatim}
  field square.field -normal 0 1 -origin 0 0 -cut -gnuplot
  field square.field -domain top -elevation -gnuplot
\end{verbatim}
The corresponding Lagrange multiplier $\lambda$ on the boundary can also be viewed as:
\begin{verbatim}
  field square.field -mark lambda -elevation
\end{verbatim}

  \begin{figure}[htb]
     \begin{center}
        \includegraphics[height=7cm]{yield-slip-al-n-1-S-0,6-cvg.pdf} 
     \end{center}
     \caption{The convergence of the augmented Lagrangian
	algorithm for the yield slip problem with $S=0.6$ and $n=1$
	and $P_1$ polynomial approximation.
       }
     \label{fig-yield-slip-al-convergence}
  \end{figure}

% -------------------------------------
\myexamplenoinput{yield_slip_residue.cc}
% -------------------------------------
The file \file{yield_slip_residue.cc} implement the computation of the full set
of residual terms of the initial problem.
This file it is not listed here but is available in the \Rheolef\  example directory.
The computation of residual terms is obtained by:
\begin{verbatim}
  make yield_slip_residue
  ./yield_slip_residue < square.field
\end{verbatim}
Observe that the residual terms of the initial problem
are of about $10^{-10}$, as required by the stopping criterion.
Fig.~\ref{fig-yield-slip-al-convergence} plots the max of the relative error
for the $\lambda_h$ variable: this quantity is used as stopping criterion.
Observe that it behaves asymptotically as $1/k$ for larges meshes,
with a final acceleration to machine precision.
\cindex{method!newton}%
Note that these convergence properties could be dramatically improved
by using a Newton method, as shown in the next section.

\vfill\clearpage% flushfigures
% ---------------------------------- 
\subsection{Newton algorithm}
% ---------------------------------- 
\label{sec-yield-slip-newton}%
\subsubsection*{Reformulation of the problem}
\cindex{method!newton}%
  The idea of this algorithm first proposed by \citet{Ala-1997}
  in the context of contact and friction problems.
  At convergence, the augmented Lagrangian method
  solve the following problem:\\
  \mbox{} \ \ $(P)_{r}$: find $u$, defined in $\Omega$,
   and $\lambda$, defined on $\partial\Omega$, such that
  \begin{eqnarray*}
      -\Delta u &=& f
        \ \mbox{ in } \Omega
        \\
      \Frac{\partial u}{\partial n}
        + \lambda &=& 0
        \ \mbox{ on } \partial\Omega
        \\
      u - P_{n,r}(\lambda + r\, u)
	&=& 0
        \ \mbox{ on } \partial\Omega
  \end{eqnarray*}
  The solution is independent upon $r \in \mathbb{R}$
  and this problem is equivalent to the original one.
  In order to diagonalize the non-linearity in $P_{n,r}(.)$, let 
  us introduce $\beta=\lambda+r\,u$.
  The problem becomes: \\
  \mbox{} \ \ $(P)_{r}$: find $u$, defined in $\Omega$,
   and $\beta$, defined on $\partial\Omega$, such that
  \begin{eqnarray*}
      -\Delta u &=& f
        \ \mbox{ in } \Omega
        \\
      \Frac{\partial u}{\partial n}
        -r\, u + \beta  &=& 0
        \ \mbox{ on } \partial\Omega
        \\
      u - P_{n,r}(\beta)
	&=& 0
        \ \mbox{ on } \partial\Omega
  \end{eqnarray*}
\subsubsection*{Variational formulation}
  Consider the following forms:
  \begin{eqnarray*}
    m(u,v) &=&
	\int_\Omega u\, v \, {\rm d}x,
	,
	\ \ \forall u\,v\in L^2(\Omega)
	\\
    a(u,v) &=&
	\int_\Omega \bnabla u\, . \bnabla v \, {\rm d}x
	- r\ 
        \int_{\partial\Omega} u \, v \, {\rm d}s
	,
	\ \ \forall u,\,v\in H^1(\Omega)
	\\
    b(v,\gamma) &=& \int_\Omega v\,\gamma \, {\rm d}s
	,
	\ \ \forall \gamma\in L^{2}(\partial\Omega),
	\ \ \forall v\in H^1(\Omega)
	\\
    c(\beta,\gamma) &=&
	\int_\Omega P_{n,r}(\beta)\,\gamma \, {\rm d}s
	,
	\ \ \forall \beta\in L^{\infty}(\partial\Omega),
	    \forall \gamma \in L^{2}(\partial\Omega)
  \end{eqnarray*}
  Remark that, since $\Omega \subset \mathbb{R}^2$, from the Sobolev
  embedding theorem, if
  $u\in H^{1}(\Omega)$ then $u_{|\partial\Omega}\in L^{\infty}(\partial\Omega)$.
  Then, all integrals have sense.
  The variational formulation writes:\\
  \mbox{} \ \ $(FV)$: find $u\in H^1(\Omega)$ and $\beta\in L^{\infty}(\partial\Omega)$
   such that
  \begin{eqnarray*}
	a(u,v) + b(v,\beta) &=&  m(f,v)
	  , \ \ \forall v\in H^1(\Omega)
	  \\
	b(u,\gamma) - c(\beta,\gamma) &=&  0
	  ,\ \ \forall \gamma\in L^{2}(\partial\Omega)
  \end{eqnarray*}
  Let $M$, $A$ and $B$ the operators associated to forms $m$, $a$, $b$ and $c$.
  The problem writes also as:
  \[
    \left(\begin{array}{cc}
	A & B^* \\
        B & -C
    \end{array}\right)
    \left(\begin{array}{c}
	u \\ \beta
    \end{array}\right)
	=
    \left(\begin{array}{c}
	Mf \\ 0
    \end{array}\right)
  \]
\cindex{formal adjoint}%
  where $B^*$ denotes the formal adjoint of $B$.
  The bilinear form $a$ is symmetric positive definite when $r\in ]0,C_f[$.
  Then $A$ is non-singular and let $A^{-1}$ denotes its inverse.
  The unknown $u$ can be eliminated: $u=A^{-1}(Mf-B^T\beta)$ and the
  problem reduces to:
  \[
    \mbox{find $\beta\in L^{\infty}(\partial\Omega)$ such that }
	F(\beta) = 0
  \]
  where
  \[
	F(\beta)
	=
	C(\beta)
	+ BA^{-1}B^* \beta
	- BA^{-1}Mf 
  \]
  This problem is a good candidate for a Newton method:
  \[
	F'(\beta)\delta\beta
	=
	C'(\beta)\delta\beta
	+ BA^{-1}B^* \delta\beta
  \]
  where $C'(\beta)$ is associated to the bilinear form:
  \begin{eqnarray*}
    c_1(\beta;\gamma,\delta) &=&
	\int_\Omega P'_{n,r}(\beta)\,\gamma\,\delta \, {\rm d}s
	,
	\ \ \forall \beta\in L^{\infty}(\partial\Omega),
	    \forall \gamma,\delta \in L^{2}(\partial\Omega)
  \end{eqnarray*}
  where, for all $\xi\in\mathbb{R}$:
  \begin{equation}
	P'_{n,r}(\xi)
	=
	\left\{ \begin{array}{cl}
          0
	    & \mbox{ when } |\xi| \leq S \\
	 \phi'_{n,r}(|\xi|-S)
	    & \mbox{ otherwise}
	\end{array}\right.
	\label{eq-def-proj-derivee}
  \end{equation}
  Recall that, for all $\zeta>0$,
  $\phi_{n,r}(\zeta)=f^{-1}_{n,r}(\zeta)$
  where
  $f_{n,r}(y)=C_f y^n+r\,y$, for all $y>0$.
  Then
  \begin{equation}
    \phi'_{n,r}(\zeta)
    = \Frac{1}{f'_{n,r}(f^{-1}_{n,r}(\zeta))}
    = \Frac{1}{f'_{n,r}(\phi_{n,r}(\zeta))}
    = \Frac{1}{r + n\, C_f \,\left\{\phi_{n,r}(\zeta) \right\}^{-1+n}}
    \label{eq-phi-derivee}
  \end{equation}
  When $n=1$ we simply have:
  \mbox{$
    \phi'_{1,r}(\zeta) = \Frac{1}{C_f + r}
  $}.
  When $r = 0$, for any $n>0$ we have
  \mbox{$
    \phi'_{n,0}(\zeta) = \Frac{\zeta^{-1+1/n}}{n \,C_f^{1/n}}
  $}.

\myexamplelicense{d_projection_dx.h}

\myexamplelicense{yield_slip_damped_newton.cc}

\myexamplelicense{yield_slip.h}

\myexamplelicense{yield_slip1.icc}

\myexamplelicense{yield_slip2.icc}

% ------------------------------
\subsubsection*{Running the program}
% ------------------------------
  \begin{figure}[htb]
     \mbox{}\hspace{-2cm}
     \begin{tabular}{cc}
          \includegraphics[height=6cm]{yield-slip-n-0,5-S-0,6.pdf} &
          \includegraphics[height=6cm]{yield-slip-n-0,9-S-0,6.pdf} \\
          \includegraphics[height=6cm]{yield-slip-n-1-S-0,6.pdf} &
          \includegraphics[height=6cm]{yield-slip-n-1,5-S-0,6.pdf}
     \end{tabular}
     \caption{The convergence of the damped Newton
	algorithm for the yield slip problem ($S=0.6$):
	(top-left)    $n=0.5$; (top-right)    $n=0.9$;
	(bottom-left) $n=1$;   (bottom-right) $n=1.5$.
       }
     \label{fig-yield-slip-newton-rate}
  \end{figure}
\begin{verbatim}
  make ./yield_slip_damped_newton
  ./yield_slip_damped_newton square.geo P1 0.6 1 > square.field
  field square.field -elevation -stereo
  field square.field -mark lambda -elevation
\end{verbatim}
  Observe on Fig.~\ref{fig-yield-slip-newton-rate}.a
  and~\ref{fig-yield-slip-newton-rate}.b that
  the convergence is super-linear and mesh-independent
  when $n=1/2$ and $n=0.9$.
  For mesh-independent convergence of the Newton method,
  see e.g. the $p$-Laplacian example,
  Fig.~\ref{fig-p-laplacian-err}, page~\pageref{fig-p-laplacian-err}.
  When $n=0.9$, observe that
  the convergence depends slightly upon the mesh for rough meshes
  while it becomes asymptotically mesh independent for fine meshes.
  When $n\geq 1$, the convergence starts to depend also upon the mesh
  (Fig.~\ref{fig-yield-slip-newton-rate}.bottom-left
     and~\ref{fig-yield-slip-newton-rate}.bottom-right).
  Recall that when $n\geq 1$, the problem becomes 
  non-differentiable and the convergence of the Newton
  method is no more assured.
  Nevertheless, in that case, the convergence is clearly faster
  (about 100 times faster)
  than the corresponding one with the augmented Lagrangian algorithm
  on the same problem (see Fig.~\ref{fig-yield-slip-al-convergence},
  page~\pageref{fig-yield-slip-al-convergence}) and moreover there is
  no saturation of the residual terms on large meshes.

% ----------------------
\subsection{Error analysis}
% ----------------------
  \begin{figure}[htb]
     \mbox{}\hspace{-0cm}
     \begin{tabular}{cc}
          \includegraphics[height=7cm]{yield-slip-circle-S-0,25-n-0,5-err-l2.pdf} &
          \includegraphics[height=7cm]{yield-slip-circle-S-0,25-n-0,5-err-linf.pdf} \\
          \includegraphics[height=7cm]{yield-slip-circle-S-0,25-n-0,5-err-h1.pdf} &
          \includegraphics[height=7cm]{yield-slip-circle-S-0,25-n-0,5-err-lambda.pdf} 
     \end{tabular}
     \caption{The yield slip problem: error analysis.}
     \label{fig-yield-slip-err}
  \end{figure}
  Assume that the previous code is contained in
  the file \file{yield-slip-augmented-lagrangian.cc}.
  When $\Omega$ is the unit circle, the exact solution is known.
  In polar coordinates $(r,\theta$), as the solution $u$ depends only of $r$,
  equation~\eqref{eq-yield-slip-1} becomes:
  \[
     -\Frac{1}{r}\partial_r (r\partial_r u) = 1
  \]
  and then, from the symmetry, $u(r)=c-r^2/4$, where $c$ is a constant to determine
  from the boundary condition.
  On the boundary $r=1$ we have $\partial_n u=\partial_r u = -1/2$.
  When $S \geq 1/2$ the fluid sticks at the wall and when $S>1/2$ we have
  from~\eqref{eq-yield-slip-2}:
  \[
     -C_f u^n - S = \Frac{1}{2}
  \]
  From the previous expression $u(r)$, taken for $r=1$ we obtain
  the constant $c$ and then:
  \[
     u(r) = \Frac{1-r^2}{4} + \left( \Frac{\max(0,1/2-S)}{C_f} \right)^{1/n}
     , \ \ 0 \leq r \leq 1
  \]
% -------------------------------------
\myexamplenoinput{yield_slip_circle.h}

\myexamplenoinput{yield_slip_error.cc}
% -------------------------------------
\cindex{directory of example files}%
The error computation is implemented in
the files \file{yield_slip_error.cc}
and \file{yield_slip_circle.h}:
it is not listed here but is available in the \Rheolef\  example directory.

The error can be computed (see Fig.~\ref{fig-yield-slip-err}):
\begin{verbatim}
  make yield_slip_error
  mkgeo_ball -t 20 -order 2 > circle-20-P2.geo
  ./yield_slip_damped_newton circle-20-P2.geo P2 0.6 1 | ./yield_slip_error
\end{verbatim}
  It appears that the discrete formulation develops optimal 
  convergence properties versus mesh refinement for $k\geq 1$
  in $H^1$, $L^2$ and $L^\infty$ norms.

