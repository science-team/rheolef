#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"1"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "      not yet (skipped)"
exit 0

status=0

run "${SBINDIR}/mkgeo_grid_2d -v4 -t 10 2>/dev/null | ${BINDIR}/geo -upgrade - | ./geo_split_filter | ${BINDIR}/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

 lambda_min="0.58734"; # 10x10 grid
#lambda_min="0.591522324359204"; # 20x20 grid

tol="1e-4"

loop_mpirun "./oldroyd_cavity mesh-2d-t 0.88888 1 0.1 0.1 500 2>/dev/null | ./oldroyd_eigmin_b > tmp.log 2>/dev/null && cat tmp.log | gawk '{err = int((\$3-${lambda_min})/${tol}); have_value=1;} END { exit (have_value == 1 && err == 0)?0:1;}'"
if test $? -ne 0; then status=1; fi

run "rm -f mesh-2d-t.geo tmp.log"
exit $status
