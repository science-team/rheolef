///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile eta.h The p-Laplacian problem -- the eta function
struct eta {
  Float operator() (const Float& z) const {
   check_macro(z != 0 || p > 2, "eta: division by zero (HINT: check mesh)");
   return pow(z, (p-2)/2);
  }
  Float derivative (const Float& z) const {
   check_macro(z != 0 || p > 4, "eta': division by zero (HINT: check mesh)");
   return 0.5*(p-2)*pow(z, (p-4)/2);
  }
  eta (const Float& q) : p(q) {}
  Float p;
};
