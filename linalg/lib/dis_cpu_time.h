#ifndef _RHEO_DIS_CPU_TIME_H
#define _RHEO_DIS_CPU_TIME_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   5 november 2013

namespace rheolef {

/**
@utilclassfile dis_cpu_time cumulated cpu for distributed computations

Synopsis
========
@snippet dis_cpu_time.h verbatim_dis_cpu_time

Example
=======

        double t_start = dis_wall_time();
        ...stuff to be timed...
        double t_end   = dis_wall_time();
        derr << "That took " << t_end - t_start << " seconds" << endl

        double cpu_start = dis_cpu_time();
        ...stuff to be timed...
        double cpu_end   = dis_cpu_time();
        derr << "That needs " << cpu_end - cpu_start << " CPU seconds" << endl

See also @ref diststream_2 for `derr`.

Description
===========
`dis_cpu_time`
>	Returns the accumulated CPU
>	for all processed linked together via the default communicator.

`seq_cpu_time`
>	It is similar but returns the computing time per user for
>	the current process only.

`dis_wall_time`
>	Returns a floating-point number of seconds, 
>	representing elapsed wall-clock time since some time in the past.
>	The *time in the past* is guaranteed not to change during the life of the process.
>	The user is responsible for converting large numbers of seconds to other units if they are preferred.
>	This function is portable (it returns seconds, not *ticks*), it allows high resolution,
>	and carries no unnecessary baggage.
>	In a distributed environment,
>	`dis_wall_time` clocks are synchronized: different nodes return the same time
>	value at the same instant.

`seq_wall_time`
>	It is similar but the time returned is local to the node that called them
>	and clocks are not synchronized: in a distributed environment,
>	different nodes can return different local times,
>	at different instant when the call to `seq_wall_time` is reached.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/communicator.h"
namespace rheolef {

// [verbatim_dis_cpu_time]
double dis_cpu_time();
double dis_wall_time();
double seq_cpu_time();
double seq_wall_time();
// [verbatim_dis_cpu_time]

} // namespace rheolef

#endif // _RHEO_DIS_CPU_TIME_H
