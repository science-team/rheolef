%option noyywrap
%{
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// ----------------------------------------------------------------------------
//
// filter betweeen source files and doxygen
//
#include <iostream>
#include <fstream>
#include "config.h"
using namespace std;
static string progname, top_srcdir,
              full_inputfilename, inputfilename, strip_inputfilename,
              name, brief;

// my_getenv("name") --> result or ""
std::string my_getenv (std::string name) {
  char* s = std::getenv (name.c_str());
  return (s != 0) ? std::string(s) : "";
}
// get_basename("dir/toto") --> "toto"
static string get_basename (const string& name)
{
  string::size_type l = name.length();
  string::size_type i = name.find_last_of ('/');
  if (i >= l) return name;
  string b = string(name, i+1, l-i-1);
  return b;
}
string strip_srcdir (string full_inputfilename)
{
  if (top_srcdir == "") return get_basename (full_inputfilename);
  size_t i = full_inputfilename.find (top_srcdir);
  if (i == string::npos) return get_basename(full_inputfilename);
  string strip = full_inputfilename.substr (i + top_srcdir.size());
  size_t j = 0;
  for (; j < strip.size() && strip[j] == '/'; j++);
  return strip.substr (j);
}
// delete_any_suffix("toto.any_suffix") --> "toto"
static string delete_any_suffix (const string& name)
{
    size_t ln = name.length();
    if (ln == 0) return name;
    size_t i_dot = 0;
    for (size_t i = ln-1; i > 0 && name[i] != '/'; --i) {
        if (name [i] == '.') {
          i_dot = i;
          break;
        }
    }
    if (i_dot == 0) return name;
    return string(name, 0, i_dot);
}
typedef enum { man=0, html=1 } mode_type;
mode_type mode = man;
static mode_type invocation2mode (string invocation) { // invocation indicates the mode
  if (progname == "src2dox_filter_man")  return man;
  if (progname == "src2dox_filter_html") return html;
  cerr << progname << ": error: unexpected invocation name, expect src2dox_filter_man or src2dox_filter_html"<<endl;
  exit(1);
}
static string make_page_name (const string& name) {
  string page_name = name;
  for (size_t i = 0, n = page_name.size(); i < n; ++i) {
    switch (page_name[i]) {
      case '-': page_name[i] = '_'; break;
      default: true;
    }
  }
  return page_name;
}
static void get_name_brief (string line, string label) {
  size_t label_len = label.size();
  size_t n = line.size(), i = 0, j = 0, k = 0, m = 0;
  if (n > 0 && line[n-1] == '\n') n--;
  for (i = 0; i < n && (line[i] != '@' && line[i] != '\\'); ++i);
  if (i == n) { name = ""; brief = ""; return; }
  for (j = i+label_len; j < n && (line[j] == ' ' || line[j] == '\t'); ++j);
  if (j == n) { name = ""; brief = ""; return; }
  for (k = j+1; k < n && (line[k] != ' ' && line[k] != '\t'); ++k);
  name = line.substr(j,k-j);
  for (m = k+1; m < n && (line[m] == ' ' || line[m] == '\t'); ++m);
  brief    = line.substr(m,n-m);
}
void do_cmd_line (string yytext, string cmd, string section_cmd, size_t section_ident, string default_suffix) 
{
  get_name_brief (yytext, cmd);
  if (inputfilename != "" && name != delete_any_suffix (inputfilename)) {
    cerr << progname << ": error: name=\""<<name<<"\" after "<<cmd<<" unmatches filename=\""<<inputfilename<<"\"" <<endl;
    exit(1);
  }
  string filename = (inputfilename != "") ? inputfilename : name+default_suffix;
  string c = yytext[0] == '#' ? "#" : ""; // shell scripts auto-detected
  if (mode == html) {
    cout   << c << "@file "   << filename  << endl
           << c << "@brief `" << name << "` - " << brief << endl;
    if (section_cmd == "") { // unix command
      cout << c << "@page  "  << make_page_name(name) << "_" << section_ident << " `" << name << "`" << endl
           << c << "@brief " << brief << endl;
    } else { // class, func, etc
      cout << c << section_cmd << " "  << name << " " << filename << endl
           << c << "@brief see the @ref " << make_page_name(name) << "_" << section_ident
                << " page for the full documentation" << endl;
      cout << c << "@page  "  << make_page_name(name) << "_" << section_ident << " `" << name << "`" << endl
           << c << "@brief " << brief << endl;
    }
  } else { // mode == man: page label should be the command name
    cout   << c << "@file "   << filename  << endl
           << c << "@brief `" << name << "` - " << brief << endl
           << c << "@page  "  << name << endl
			// PORTAGE doxygen-1.8.13 (buster): @page requires a @brief for unix man
           << c << "@brief " << brief 
                << " (" << _RHEOLEF_PACKAGE << "-" << _RHEOLEF_VERSION << ")" << endl
           << c << " " << endl
           ;
  }
}
%}
%x verbatim
%%
%{
// --------------------------------------------------------------
// filter files from "doc/examples" directory:
// @classfile   BASENAME SHORT_DESCRIPTION
// @commandfile BASENAME SHORT_DESCRIPTION
// @examplefile FILENAME SHORT_DESCRIPTION  with FILENAME=BASENAME.cc
// TODO: support both //! and /**
// --------------------------------------------------------------
%}
<INITIAL>^"@verbatim"		{ BEGIN(verbatim); /* doxgen-1.8.13 requires two \\ for one in verbatim */ }
<verbatim>"\\"			{ cout << ((mode == man) ? "\\\\" : "\\"); }
<verbatim>^"@endverbatim"	{ BEGIN(INITIAL); }
<INITIAL>^([#]?)([ \t]*)[@\\]"commandfile"[^\n]*\n {
			  do_cmd_line (yytext, "@commandfile", "", 1, ".cc"); }
<INITIAL>^([ \t]*)[@\\]"classfile"[^\n]*\n {
			  do_cmd_line (yytext, "@classfile", "@class", 2, ".h"); }
<INITIAL>^([ \t]*)[@\\]"functionfile"[^\n]*\n {
			  do_cmd_line (yytext, "@functionfile", "@fn", 3, ".h"); }
<INITIAL>^([ \t]*)[@\\]"linalgclassfile"[^\n]*\n {
			  do_cmd_line (yytext, "@linalgclassfile", "@class", 4, ".h"); }
<INITIAL>^([ \t]*)[@\\]"linalgfunctionfile"[^\n]*\n {
			  do_cmd_line (yytext, "@linalgfunctionfile", "@fn", 5, ".h"); }
<INITIAL>^([ \t]*)[@\\]"femclassfile"[^\n]*\n {
			  do_cmd_line (yytext, "@femclassfile", "@class", 6, ".h"); }
<INITIAL>^([ \t]*)[@\\]"utilclassfile"[^\n]*\n {
			  do_cmd_line (yytext, "@utilclassfile", "@class", 7, ".h"); }
<INITIAL>^([ \t]*)[@\\]"internalclassfile"[^\n]*\n {
			  do_cmd_line (yytext, "@internalclassfile", "@class", 8, ".h"); }
<INITIAL>^([ \t]*)[@\\]"internalfunctionfile"[^\n]*\n {
			  do_cmd_line (yytext, "@internalfunctionfile", "@class", 9, ".h"); }
<INITIAL>^"//!"([ \t]*)[@\\]"examplefile"[^\n]*\n {
			  get_name_brief (yytext, "@examplefile");
			  string filename = (inputfilename != "") ? inputfilename : name;
			  cout << "//! @file "    << filename  << endl
			       << "//! @brief "   << brief << endl
			       << "//! @example " << name  << endl
			       << "//! @brief "   << brief << endl;
			}
<INITIAL>^([#]?)([ \t]*)[@\\]"showfromfile"[^\n]*\n {
  			  string c = yytext[0] == '#' ? "#" : ""; // shell scripts auto-detected
			  cout << c << "This documentation has been generated from file " << strip_inputfilename << endl  
                               << c << endl;
			}
%%
// --------------------------------------------------------------
int main(int argc, char** argv) {
// --------------------------------------------------------------
  // usage: src2dox_filter_man  < in > out
  //        src2dox_filter_html < in > out
  // note: invocation prog name indicates the mode: "man" or "html"
  //       since doxygen INPUT_FILTER do not supports option as flag
  progname = get_basename    (argv[0]);
  mode     = invocation2mode (argv[0]);
  //cerr << "! main: mode="<<mode<<endl;
  top_srcdir = my_getenv ("SRC2DOX_FILTER_TOP_SRCDIR");
  //cerr << "! top_srcdir=\""<<top_srcdir<<"\""<<endl;
  yyFlexLexer lexer;
  if (argc == 1) {
    full_inputfilename = my_getenv ("SRC2DOX_FILTER_INPUT_FILENAME");
    inputfilename = get_basename (full_inputfilename);
    strip_inputfilename = strip_srcdir (full_inputfilename);
    //cerr << "! input on stdin=\""<<full_inputfilename<<"\"" << endl;
    //cerr << "! strip_inputfilename=\""<<strip_inputfilename<<"\"" << endl;
    return lexer.yylex();
  } else {
    full_inputfilename = argv[1];
    inputfilename = get_basename (full_inputfilename);
    strip_inputfilename = strip_srcdir (full_inputfilename);
    //cerr << "! input on file=\""<<full_inputfilename<<"\"" << endl;
    //cerr << "! strip_inputfilename=\""<<strip_inputfilename<<"\"" << endl;
    ifstream in (argv[1]);
    lexer.yyrestart (in);
    return lexer.yylex();
  }
}
