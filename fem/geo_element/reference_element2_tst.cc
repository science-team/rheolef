///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/reference_element.h"
//#include "rheolef/geo_element.h"
using namespace rheolef;
using namespace std;

void
put_prism (ostream& out, size_t my_order)
{
  typedef reference_element::size_type size_type;
  typedef point_basic<size_type>       ilat;
  for (size_type k = 0; k <= my_order; k++) {
    for (size_type j = 0; j <= my_order; j++) {
      for (size_type i = 0; i+j <= my_order; i++) {
        size_type loc_inod = reference_element_P::ilat2loc_inod (my_order, ilat(i, j, k));
	out << "  loc_inod("<<i<<","<<j<<","<<k<<") = " << loc_inod << endl;
      }
    }
  }
}
int main() {
   typedef reference_element::size_type size_type;
   cout << "P1(P):"<<endl; put_prism (cout, 1);
   cout << "P2(P):"<<endl; put_prism (cout, 2);
   cout << "P3(P):"<<endl; put_prism (cout, 3);
   return 0;
}

