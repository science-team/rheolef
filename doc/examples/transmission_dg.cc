///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile transmission_dg.cc The transmission problem with discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1d";
  Float epsilon = (argc > 3) ? atof(argv[3]) : 1e-2;
  space Xh (omega, approx);
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  check_macro (k >= 1, "polynomial degree k="<<k<<" shoud be >= 1");
  Float beta = (k+1)*(k+d)/d;
  field eta_h(Xh);
  eta_h["west"] = epsilon;
  eta_h["east"] = 1;
  geo gamma_d = omega["left"] + omega["right"];
  geo Shd     = omega["internal_sides"] + gamma_d;
  trial u (Xh); test v (Xh);
  auto eta_s = 2/(1/inner(eta_h) + 1/outer(eta_h));
  auto eta_w_o = inner(eta_h)/(inner(eta_h) + outer(eta_h));
  auto eta_w_i = outer(eta_h)/(inner(eta_h) + outer(eta_h));
  auto average_w_u = eta_w_i*inner(eta_h*dot(grad_h(u),normal()))
                   + eta_w_o*outer(eta_h*dot(grad_h(u),normal()));
  auto average_w_v = eta_w_i*inner(eta_h*dot(grad_h(v),normal()))
                   + eta_w_o*outer(eta_h*dot(grad_h(v),normal()));
  form a = integrate (eta_h*dot(grad_h(u),grad_h(v)))
         + integrate (Shd, beta*penalty()*eta_s*jump(u)*jump(v)
	                - jump(u)*average_w_v
                        - jump(v)*average_w_u);
  field lh = integrate (v);
  solver_option sopt;
  sopt.iterative = false;
  problem p (a, sopt);
  field uh (Xh);
  p.solve (lh, uh);
  dout << catchmark("epsilon") << epsilon << endl
       << catchmark("u")       << uh;
}
