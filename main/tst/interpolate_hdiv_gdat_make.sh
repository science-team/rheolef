#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
e=${1-"t"}
case $e in
 e)   kmax=5; L="3 4 8 16 32 64 128 256 512 1024";;
 t|q) kmax=3; L="1 2 4 8 16 32 64 128 256";;
 *)   kmax=3; L="5 10 20 40";;
esac
k=0
while test $k -le $kmax; do
  echo "# RT${k}d $e"
  echo "# n err_l2 err_div_l2   err_linf err_div_linf  err_prl2_l2 err_prl2_div_l2   err_div_pi_h_sw_linf err_div_prl2_sw_linf"
  for n in $L; do
    command="mkgeo_grid -$e $n > tmp.geo"
    #echo "! $command" 1>&2
    eval $command
    command="./interpolate_hdiv_tst tmp.geo RT${k}d >/dev/null 2>tmp.txt"
    #echo "! $command" 1>&2
    eval $command
    err_l2=`grep np_err_l2 tmp.txt | gawk '{print $2}'`
    err_div_l2=`grep np_err_div_l2 tmp.txt | gawk '{print $2}'`
    err_linf=`grep np_err_linf tmp.txt | gawk '{print $2}'`
    err_div_linf=`grep np_err_div_linf tmp.txt | gawk '{print $2}'`
    err_prl2_l2=`grep np_err_prl2_l2 tmp.txt | gawk '{print $2}'`
    err_prl2_div_l2=`grep np_err_prl2_div_l2 tmp.txt | gawk '{print $2}'`
    err_div_pi_h_sw_linf=`grep np_err_div_pi_h_sw_linf tmp.txt | gawk '{print $2}'`
    err_div_prl2_sw_linf=`grep np_err_div_prl2_sw_linf tmp.txt | gawk '{print $2}'`
    echo "$n $err_l2 $err_div_l2   $err_linf $err_div_linf   $err_prl2_l2 $err_prl2_div_l2   $err_div_pi_h_sw_linf $err_div_prl2_sw_linf"
  done
  echo; echo
  k=`expr $k + 1`
done
