///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo.h"
#include "rheolef/geo_domain.h"

namespace rheolef {

// external graphic drivers:
template <class T> odiststream& visu_gnuplot      (odiststream&, const geo_basic<T,sequential>&);
template <class T> odiststream& visu_vtk_paraview (odiststream&, const geo_basic<T,sequential>&);
template <class T> odiststream& geo_put_gmsh      (odiststream&, const geo_basic<T,sequential>&);
template <class T> odiststream& geo_put_bamg      (odiststream&, const geo_basic<T,sequential>&);

template <class T>
odiststream&
geo_basic<T,sequential>::put (odiststream& ops)  const {
  iorheo::flag_type format = iorheo::flags(ops.os()) & iorheo::format_field;
  if (format [iorheo::gnuplot]) { return visu_gnuplot    (ops,*this); }
  if (format [iorheo::paraview]){ return visu_vtk_paraview (ops,*this); }
  if (format [iorheo::gmsh])    { return geo_put_gmsh    (ops,*this); }
  if (format [iorheo::bamg])    { return geo_put_bamg    (ops,*this); }
  return base::data().put (ops); // in .geo format
}
template <class T>
odiststream&
geo_rep<T,sequential>::put_geo (odiststream& ops)  const {
  using namespace std;
  size_type n_vert = base::_node.dis_size ();
  size_type n_elt  = base::_gs.ownership_by_dimension[base::_gs._map_dimension].dis_size ();
  //
  // put header
  //
  ops.os() << std::setprecision(numeric_limits<T>::digits10);
  ops << "#!geo" << endl
      << endl
      << "mesh" << endl
      << base::_version;

  if (base::_version == 4) {
    geo_header h;
    h.dimension = base::_dimension;
    h.sys_coord = base::_sys_coord;
    h.order     = base::order();
    h.dis_size_by_variant [0] = base::_node.dis_size();
    if (base::_have_connectivity) {
      // put all
      for (size_type variant = 1; variant < reference_element::max_variant; variant++) {
        h.dis_size_by_variant [variant] = base::_geo_element [variant].dis_size();
      }
    } else {
      // put only d-dimensional elements, as was the input
      for (size_type variant = reference_element::first_variant_by_dimension(base::_gs._map_dimension);
                     variant < reference_element:: last_variant_by_dimension(base::_gs._map_dimension); variant++) {
        h.dis_size_by_variant [variant] = base::_geo_element [variant].dis_size();
      }
    }
    ops << endl << h << endl;
  }
  //
  // put nodes
  //
  T rounding_prec = iorheo::getrounding_precision(ops.os());
  if (rounding_prec == 0) {
    base::_node.put_values (ops, _point_put<T>(base::_dimension));
  } else {
    base::_node.put_values (ops, _round_point_put<T>(base::_dimension, rounding_prec));
  }
  ops << endl;
  //
  // put elements
  //
  if (base::_gs._map_dimension > 0) {
    for (size_type variant = reference_element::first_variant_by_dimension(base::_gs._map_dimension);
                   variant < reference_element:: last_variant_by_dimension(base::_gs._map_dimension); variant++) {
      base::_geo_element [variant].put_values (ops);
    }
    ops << endl;
  }
  //
  // put faces & edges
  //
  if (base::_gs._map_dimension > 1 && base::_have_connectivity) {
    for (size_type side_dim = base::_gs._map_dimension - 1; side_dim >= 1; side_dim--) {
      for (size_type variant = reference_element::first_variant_by_dimension(side_dim);
                     variant < reference_element:: last_variant_by_dimension(side_dim); variant++) {
        base::_geo_element [variant].put_values (ops); 
      }
    }
  }
  //
  // put domains
  //
  for (typename std::vector<domain_indirect_basic<sequential> >::const_iterator
        iter = base::_domains.begin(), last = base::_domains.end();
	iter != last; ++iter) {
    ops << endl;
    (*iter).put (ops);
  }
  return ops;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_rep  <Float,sequential>;
template odiststream& geo_basic<Float,sequential>::put (odiststream&) const;

} // namespace rheolef
