///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito 
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/compiler.h"
#include "rheolef/gamma.h"
#include "rheolef/jacobi.h"
#include "rheolef/jacobi_roots.h"
#include <iterator>
namespace rheolef {
template <class Size, class OutputIterator1, class OutputIterator2>
void gauss_lobatto_jacobi (Size R,
    	typename std::iterator_traits<OutputIterator1>::value_type alpha,
    	typename std::iterator_traits<OutputIterator1>::value_type beta,
    	OutputIterator1 zeta, OutputIterator2 omega)
{
    	typedef typename std::iterator_traits<OutputIterator1>::value_type T;
	check_macro (R >= 2, "gauss_lobatto_jacobi: node number " << R << " may be >= 2");
	T num = pow(T(2), alpha+beta+1)/T(1.*R-1);
	T w0  = pow(T(2), alpha+beta+1)*(beta+1);
	T wf  = pow(T(2), alpha+beta+1)*(alpha+1);
	if (alpha == floor(alpha) && beta == floor(beta)) {
	  num *= T(1.*R)/((alpha+R)*(beta+R));
	  w0  *= 1/(T(R-1.)*(alpha+R));
	  wf  *= 1/(T(R-1.)*(beta+R));
	  for (Size k = 1; k <= size_t(static_cast<int>(beta)); k++) {
	    num *= T(1.*R+k)/(alpha+T(1.*R+k));
	    w0  *= T(sqr(int(k)))/(T(1.*R-1+k)*(alpha+R+k));
	  }
	  for (Size k = 1; k <= size_t(static_cast<int>(alpha)); k++) {
	    wf  *= T(sqr(int(k)))/(T(1.*R-1+k)*(beta+T(1.*R+k)));
	  }
        } else {
	  num *=  (my_gamma(alpha+R)/my_gamma(alpha+beta+R+1))
	         *(my_gamma(beta+R)/my_gamma(T(1.*R)));
	  w0  *=  (my_gamma(alpha+R)/my_gamma(alpha+beta+R+1))
	         *(sqr(my_gamma(beta+1))*my_gamma(T(1.*R-1))/my_gamma(beta+R));
	  wf  *=  (my_gamma(beta+R)/my_gamma(alpha+beta+R+1))
	         *(sqr(my_gamma(alpha+1))*my_gamma(T(1.*R-1))/my_gamma(alpha+R));
  	}
	zeta [0] = -1;
	omega[0] = w0;
	jacobi_roots (R-2, alpha+1, beta+1, zeta+1);
  	jacobi<T> P1 (R-1, alpha, beta);
  	for (Size r = 1; r < R-1; ++r)
      	    omega[r] = num/sqr(P1(zeta[r]));
	zeta [R-1] = 1;
	omega[R-1] = wf;
}
} // namespace rheolef
