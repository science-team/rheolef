#!geo

mesh
4
header
 dimension 2
 nodes	6
 quadrangles	2
 edges	7
end header

0 0
0 1
0.5 0
0.5 1
1 0
1 1

q	0 2 3 1
q	2 4 5 3

e	0 2
e	2 3
e	3 1
e	1 0
e	2 4
e	4 5
e	5 3

domain
boundary
2 1 6
0
4
5
2
6
3

domain
bottom
2 1 2
0
4

domain
right
2 1 1
5

domain
top
2 1 2
2
6

domain
left
2 1 1
3

domain
west
2 2 1
0

domain
east
2 2 1
1

domain
interface
2 1 1
1

domain
interface2
2 1 1
-1
