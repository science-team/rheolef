///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  space V1 (omega, "P1");
  space V2 (omega, "P2");
  space V3 (omega, "P3");
  space Vh = V1*V2*V3;
  field fh (Vh, 1.0);
  Float sum = 0;
  for (size_t i = 0, n = fh.ndof(); i < n; i++) {
    sum += fh.dof(i);
  }
#ifdef _RHEOLEF_HAVE_MPI
  sum = mpi::all_reduce (Vh.comm(), sum, std::plus<Float>());
#endif // _RHEOLEF_HAVE_MPI
  dout << fh;
  derr << "sum      = " << sum << endl
       << "dis_size = " << fh.dis_ndof() << endl;
  return (sum == fh.dis_ndof()) ? 0 : 1;
}
