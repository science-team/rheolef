///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// 
// check the Ds operator on some simple geometries
// 
// authors: Pierre Saramito
//
// usage:
//	prog mesh.geo Pk qorder tol u0 u1 u2 n0 n1 n2
//
// examples:
//
//	mkgeo_ball -s -t 10 > ball-P1.geo
// 	./form_2Ds_Ds_tst ball-P1.geo P1 18 0.050 z x^2 'x*y' x y z
//
//	mkgeo_ball -s -t 10 -order 2 > ball-P2.geo
// 	./form_2Ds_Ds_tst ball-P2.geo P2 18 0.002 z x^2 'x*y' x y z
//
//	mkgeo_grid -T 10 > cube-10.geo
//	./geo_domain_tst cube-10 right > cube-10.right.geo
// 	./form_2Ds_Ds_tst cube-10.right.geo P1 18 1e-14 z x^2 'x*y' 0 1 0
// 	./form_2Ds_Ds_tst cube-10.right.geo P2 18 1e-14 z x^2 'x*y' 0 1 0
//
// note: the banded level set is also supported: see the field_Ds_tst.sh for invocations.
//
#include "form_2Ds_Ds.icc"

// --------------------------------------------------------------
// compare two evaluations of 2Ds(u):Ds(v) :
//  symbolic : integrate_omega f(x) dx
//  		with f = 2Ds(u):Ds(v) and using a quadrature
//  numeric :  form 2Ds_Ds (uh,vh)
// arguments are sring expressions for u,v and n
// --------------------------------------------------------------
Float
compute_error_s (
  const geo&                      omega,
  const string&                   approx,
  const point_basic<std::string>& u_expr,
  const point_basic<std::string>& n_expr,
  const point_basic<symbol>&      x,
  const quadrature_option&   qopt)
{
  space Xh (omega, approx, "vector");
  size_t k = Xh.degree();
  string grad_approx = "P" + std::to_string(k-1) + "d";
  space Th (omega, grad_approx, "tensor");
  tensor_function tau = Ds (u_expr,n_expr,x);
  field tau_h = lazy_interpolate (Th, tau);
  vector_function u_exact = vector_function (u_expr,x);
  field uh    = lazy_interpolate (Xh, u_exact);
  field Ds_uh = lazy_interpolate (Th, Ds(uh));
  Float err_l2   = sqrt(integrate (omega, norm2(Ds_uh-tau_h), qopt));
  Float err_linf = field(Ds_uh-tau_h).max_abs();
  dout << setprecision(16)
       << "# nelt err_l2 err_linf" << endl
       << omega.dis_size() << " " << err_l2 << " " << err_linf << endl;
  return err_linf;
}
// the same with the banded level set method
// assume the unit sphere:
Float phi (const point& x) { return norm(x) - 1; }

Float
compute_error_band (
  const geo&                      lambda,
  const string&                   approx,
  const point_basic<std::string>& u_expr,
  const point_basic<std::string>& n_expr,
  const point_basic<symbol>&      x,
  const quadrature_option&   qopt)
{
warning_macro("compute_error_band...");
  space Xh  (lambda, approx);
warning_macro("compute_error_band(0)...");
  field phi_h = lazy_interpolate(Xh, phi);
warning_macro("compute_error_band(1)...");
  band gh (phi_h); // TODO: option with quadrangles
  size_t k = Xh.degree();
  string grad_approx = "P" + std::to_string(k-1) + "d";
warning_macro("compute_error_band(2)...");
  space Th (gh.level_set(), grad_approx, "tensor");
  tensor_function tau_exact = Ds (u_expr,n_expr,x);
warning_macro("compute_error_band(3)...");
  field tau_h = lazy_interpolate (Th, tau_exact);
warning_macro("compute_error_band(4)...");
  space Xbh (gh.band(), approx,      "vector");
  space Tbh (gh.band(), grad_approx, "tensor");
warning_macro("compute_error_band(5)...");
  trial u (Xbh), sigma (Tbh); test v (Xbh), tau (Tbh);
  integrate_option fopt (qopt);
  fopt.invert = true;
warning_macro("compute_error_band(6)...");
  form inv_m  = integrate (gh, ddot(sigma,tau), fopt);
warning_macro("compute_error_band(7)...");
  form ds     = integrate (gh, ddot(Ds(u), tau), qopt);
warning_macro("compute_error_band(8)...");
  field uh = lazy_interpolate (Xbh, vector_function(u_expr,x));
warning_macro("compute_error_band(9)...");
  field Ds_uh = inv_m*(ds*uh);
  // interpolate Ds_uh from band to Gamma for error comparison
warning_macro("compute_error_band(10)...");
  field Ds_uh_ls = lazy_interpolate (Th, Ds_uh);
warning_macro("compute_error_band(11)...");
  Float err_l2   = sqrt(integrate (gh.level_set(), norm2(Ds_uh_ls-tau_h), qopt));
  Float err_linf = field(Ds_uh_ls-tau_h).max_abs();
  dout << setprecision(16)
       << "# nelt err_l2 err_linf" << endl
       << gh.band().dis_size() << " " << err_l2 << " " << err_linf << endl;
warning_macro("compute_error_band done");
  return err_linf;
}
// --------------------------------------------------------------
// usage: prog mesh.geo Pk qorder tol u0 u1 u2 n0 n1 n2
// --------------------------------------------------------------
int main(int argc, char**argv) {
    environment rheolef (argc,argv);
    geo omega (argv[1]);
    string approx = (argc > 2) ?      argv[2]  : "P1";
    size_t q      = (argc > 3) ? atoi(argv[3]) : 12;
    Float tol     = (argc > 4) ? atof(argv[4]) : 1e-10;
    quadrature_option qopt;
    qopt.set_order  (q);
    qopt.set_family (quadrature_option::gauss);

    size_t d = omega.dimension();
    point_basic<symbol> x (symbol("x"),symbol("y"),symbol("z"));
    point_basic<std::string> u_expr ("0", "0", "0");
    point_basic<std::string> n_expr ("0", "0", "0");
    size_t next = 5;
    for (size_t i = 0; i < d; ++i) {
      u_expr [i] = (next+i < size_t(argc)) ? argv[next+i] : "0";
    }
    next += d;
    for (size_t i = 0; i < d; ++i) {
      n_expr [i] = (next+i < size_t(argc)) ? argv[next+i] : "0";
    }
    derr << "approx = "<<approx<<endl
         << "qorder = "<<qopt.get_order()<<endl
         << "u = "<<u_expr<<endl
         << "n = "<<n_expr<<endl;
    Float err = (omega.map_dimension() < omega.dimension()) ? 
        compute_error_s    (omega, approx, u_expr, n_expr, x, qopt) :
        compute_error_band (omega, approx, u_expr, n_expr, x, qopt);

    return (err < tol) ? 0 : 1;
}
