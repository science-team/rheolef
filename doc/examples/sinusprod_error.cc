///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusprod_error.cc The cosinus product function -- error analysis for the Poisson problem
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float err_u_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  field uh; din >> uh;
  geo omega = uh.get_geo();
  space Xh = uh.get_space();
  size_t k = Xh.degree();
  size_t d = Xh.get_geo().dimension();
  integrate_option iopt;
  iopt.set_order(min(3*(k+1)+4,size_t(17)));
  Float err_u_l2 = sqrt(integrate (omega, sqr(uh-u_exact(d)), iopt));
  string opts = Xh.get_basis().option().stamp();
  space Xh1 (omega, "P"+to_string(k+1)+"d"+opts);
  field euh = lazy_interpolate (Xh1, uh-u_exact(d));
  Float err_u_linf = euh.max_abs();
  Float err_u_h1  = sqrt(integrate (omega, norm2(grad_h(euh)), iopt));
  dout << "err_u_l2   " << err_u_l2 << endl
       << "err_u_linf " << err_u_linf   << endl
       << "err_u_h1   " << err_u_h1 << endl;
  return (err_u_linf <= err_u_linf_expected) ? 0 : 1;
}
