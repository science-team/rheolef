///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/config.h"
#ifdef _RHEOLEF_HAVE_MPI
#include "rheolef/geo.h"

namespace rheolef {

template <class T>
typename geo_rep<T,distributed>::size_type
geo_rep<T,distributed>::dis_ige2ios_dis_ige (size_type dim, size_type dis_ige) const
{
    const geo_element& K = base::dis_get_geo_element(dim,dis_ige);
    return K.ios_dis_ie();
}
// --------------------------------------------------------------------------
// access by geo_element(dim,idx)
// --------------------------------------------------------------------------
template <class T>
distributor
geo_rep<T,distributed>::geo_element_ios_ownership (size_type dim) const
{
  return _ios_ige2dis_ige[dim].ownership();
}
template <class T>
typename geo_rep<T,distributed>::size_type
geo_rep<T,distributed>::ios_ige2dis_ige (size_type dim, size_type ios_ige) const
{
  return _ios_ige2dis_ige[dim][ios_ige];
}
template <class T>
typename geo_rep<T,distributed>::size_type
geo_rep<T,distributed>::ige2ios_dis_ige (size_type dim, size_type ige) const
{
    const geo_element& K = get_geo_element(dim,ige);
    return K.ios_dis_ie();
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_rep<Float,distributed>;

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
