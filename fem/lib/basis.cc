///
/// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// the basis unix command
// author: Pierre.Saramito@imag.fr
// date: 11 september 2017
//

namespace rheolef {
/**
@commandfile basis plot a finite element basis 
@addindex command `basis`
@addindex command `gnuplot`
@addindex plotting

Synopsis
========
 
    basis name [element] [options]
  
Description
===========
View a finite element polynomial basis on a given
reference element (see @ref reference_element_6).
Alternatively, it could show the interpolation nodes.

Examples
========

      basis P5 t
      basis P5[warburton] t
      basis P5[warburton,monomial] t
      basis 'trace(P3d)' t
      
      basis P3 t -node
      basis P5 t -node-side 0
      basis P5 t -node-side 1
      basis P3 T -node
      basis B7 t
      basis S7 t
      basis RT0 t
      basis RT3 t -node
      basis "trace_n(RT5d)" t -node
      basis P3d T -node-side 1
      basis 'trace_n(RT5d)' T -node-side 1

Basis and element specifications
================================
The basis *name* argument is specified as for the `space`
and `basis` constructor class argument
(see @ref space_2 and @ref basis_2).
The *element* argument is one of `e`, `t`, `q`, `T`, `P`, `H`
(see @ref reference_element_6). 
The `gnuplot` render is used for visualization.

The raw basis
=============

`-raw` \n
`-fem`
>	A raw basis is used for computing the Vandermonde matrix and
>	building the Lagrange basis.
>	The raw basis is either `monomial`, `Dubiner` or `Bernstein`
>       while `Dubiner` is the default.
>	This raw basis is hiden by default:
>	it is represented by using the `-raw` option
>       while `-fem` switch back to the default finite element
>       basis representation.
>       When the basis family is either `M`, for `monomial`
>       or `D` for `Dubiner`, there is no ambiguity, and
>       the `-raw` option could be omitted.
>       Conversely, when the basis family is `B`, for `Bernstein`,
>       then, there is an ambiguity and this option should be used.

Here is an example of raw basis visualization:

    basis M4 t
    basis D5 e
    basis B5 e -raw

Here, the `-raw` option is used to disambiguate  
between the Bernstein basis as finite element basis
and the Bernstein basis as a raw basis.

Redering options
================

The `basis` command supports several modes of visualization:

`-ndof`
>	Print the basis size, i.e. the number of degrees of freedom (ndof).
`-poly`
>	Represents the polynomial functions in elevation, for 1D and 2D
>	elements (3D elements visualization not yet supported).
>	All basis polynomials are showed in an animation.
`-node`
>       Represents the node location, with distinct colors for 
>	each dimension associated to.
`-node-side`
>       Represents the node location, restricted on a specific side.

Others options
==============
`-[no]verbose`
>       Print messages related to graphic files created and
>       command system calls (this is the default).

`-[no]clean`
>       Clear temporary graphic files (this is the default).
    
`-[no]execute`
>       Execute graphic command (this is the default).
>       The `-noexecute` variant is useful
>       in conjunction with the `-verbose` and `-noclean` options
>       in order to modify some render options by hand.


Limitations
===========
Polynomial visualization in 3D are not yet supported:
future development will use paraview with volume mode
and animation for scanning all basis polynomials.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/basis.h"
#include "rheolef/basis_raw.h"
#include "rheolef/iorheo.h"
#include "rheolef/reference_element_face_transformation.h"
using namespace rheolef;
using namespace std;

void usage() {
      cerr << "basis: usage:" << endl
           << "basis "
           << "[approx=P1] "
           << "[element=t] "
           << "{-dubiner|-monomial} "
           << "{-fem|-raw} "
           << "{-ndof|-poly|-node|-node-side int} "
           << "[-subdivide int] "
           << "[-[no]clean] [-[no]execute] [-[no]verbose] "
           << endl;
      exit (1);
}
int main(int argc, char**argv) {
  // --------------------------------
  // scan command line
  // --------------------------------
  if (argc == 1) usage();
  typedef enum {
     show_poly         = 0,
     show_node         = 1,
     show_node_on_side = 2,
     show_ndof         = 3,
     show_nnod         = 4
  } show_type;
  show_type show = show_poly;
  string approx = "P1";
  char   t      = 't';
  reference_element hat_K;
  hat_K.set_name(t);
  bool   raw    = false;
  size_t nsub   = 0;
  side_information_type sid;
  for (int i = 1; i < argc; i++) {

    // general options:
         if (strcmp (argv[i], "-clean") == 0)     cout << clean;
    else if (strcmp (argv[i], "-noclean") == 0)   cout << noclean;
    else if (strcmp (argv[i], "-execute") == 0)   cout << execute;
    else if (strcmp (argv[i], "-noexecute") == 0) cout << noexecute;
    else if (strcmp (argv[i], "-verbose") == 0)   cout << verbose;
    else if (strcmp (argv[i], "-noverbose") == 0) cout << noverbose;

    // basis options:
    else if (strcmp (argv[i], "-fem") == 0)        raw = false;
    else if (strcmp (argv[i], "-raw") == 0)        raw = true;

    // view options:
    else if (strcmp (argv[i], "-ndof")  == 0)      show = show_ndof;
    else if (strcmp (argv[i], "-nnod")  == 0)      show = show_nnod;
    else if (strcmp (argv[i], "-poly")  == 0)      show = show_poly;
    else if (strcmp (argv[i], "-node")  == 0)      show = show_node;
    else if (strcmp (argv[i], "-node-side")  == 0) {
      show = show_node_on_side;
      if (i == argc-1) { cerr << "basis -node-side: option argument missing" << endl; usage(); }
      sid.loc_isid = atoi(argv[++i]);
    } else if (strcmp (argv[i], "-subdivide") == 0) {
      if (i == argc-1) { cerr << "basis -subdivide: option argument missing" << endl; usage(); }
      nsub = atoi(argv[++i]);
      cout << setsubdivide (nsub);

    // reference element spec:
    } else if (argv[i][0] != '-' && strlen(argv[i]) == 1) {
      t = argv[i][0];
      hat_K.set_name(t);
    // basis spec:
    } else if (argv[i][0] != '-' && strlen(argv[i]) >= 2) {
      approx = argv[i];
    } else {
      cerr << "unexpected option `" << argv[i]<< endl;
      usage();
    }
  }
  if (approx[0] == 'M' || approx[0] == 'D') {
    raw = true;
  }
  if (nsub == 0) {
    nsub = (hat_K.dimension() == 1) ? 1000 : 40;
    cout << setsubdivide (nsub);
  }
  // --------------------------------
  // show
  // --------------------------------
  if (raw) { // raw basis
    if (show != show_poly) {
      cerr << "basis: raw basis no not have nodes (HINT: use -poly option instead of -node)" << endl;
      exit (1);
    }
    basis_raw b (approx);
    b.put (cout, hat_K);
    return 0;
  }
  // fem basis
  basis b (approx);
  switch (show) {
    case show_ndof:
      cout << b.ndof (hat_K) << endl;
      break;
    case show_nnod:
      cout << b.nnod (hat_K) << endl;
      break;
    case show_poly:
      b.put (cout, hat_K);
      break;
    case show_node:
      b.put_hat_node (cout, hat_K);
      break;
    case show_node_on_side:
      b.put_hat_node_on_side (cout, hat_K, sid);
      break;
  }
}
