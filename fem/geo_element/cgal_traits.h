#ifndef _RHEO_CGAL_TRAITS_H
#define _RHEO_CGAL_TRAITS_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/cgal_kernel_float128.h"
#include "rheolef/cgal_kernel.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wignored-attributes"
#include <CGAL/Filtered_kernel.h>
#pragma GCC diagnostic pop

namespace rheolef {

template <class T, size_t D> struct geo_cgal_traits {};

// The following are equivalent:
//    typedef CGAL::Exact_predicates_inexact_constructions_kernel   Kernel;
//    typedef CGAL::Filtered_kernel<CGAL::Simple_cartesian<T> >     Kernel;
// Here we based on rheolef::point_basic<T> :
template <class T>
struct geo_cgal_traits<T,1> {
  typedef CGAL::Filtered_kernel_adaptor<custom_cgal::kernel_2d<T> >     Kernel;
};
template <class T>
struct geo_cgal_traits<T,2> {
  typedef CGAL::Filtered_kernel_adaptor<custom_cgal::kernel_2d<T> >     Kernel;
};
template <class T>
struct geo_cgal_traits<T,3> {
  typedef CGAL::Filtered_kernel_adaptor<custom_cgal::kernel_3d<T> >     Kernel;
};

} // namespace rheolef
#endif // _RHEO_CGAL_TRAITS_H
