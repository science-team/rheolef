#ifndef _RHEOLEF_REFERENCE_ELEMENT_H
#define _RHEOLEF_REFERENCE_ELEMENT_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   30 november 2003

namespace rheolef {
/**
@femclassfile reference_element reference element
@addindex  reference element

Description
===========
The reference_element class defines all supported types of 
geometrical elements in zero, one, two and three dimensions.
Each supported element is represented by a letter:

- `p`: @ref point_6  (dimension 0)
- `e`: @ref edge_6   (dimension 1)
- `t`: @ref triangle_6   (dimension 2)
- `q`: @ref quadrangle_6 (dimension 2)
- `T`: @ref tetrahedron_6 (dimension 3)
- `P`: @ref prism_6       (dimension 3)
- `H`: @ref hexahedron_6  (dimension 3)

Implementation
==============
@showfromfile
@snippet reference_element.h verbatim_reference_element
@snippet reference_element.h verbatim_reference_element_cont

*/
} // namespace rheolef

#include "rheolef/point.h"
#include <array>

namespace rheolef {

// [verbatim_reference_element]
class reference_element {
public:

// typedefs:

    typedef std::vector<int>::size_type size_type;
    typedef size_type                   variant_type; 

    static const variant_type
	p = 0, 
	e = 1, 
	t = 2, 
	q = 3, 
	T = 4, 
	P = 5, 
	H = 6, 
	max_variant = 7;

    static const size_type max_side_by_variant = 6;

// allocators/deallocators:

    reference_element (variant_type x = max_variant)
      : _x(x) { assert_macro (x >= 0 && x <= max_variant, "invalid type " << x); }

// initializers:

    void set_variant (variant_type x) { _x = x; }
    void set_variant (size_type n_vertex, size_type dim) { _x = variant (n_vertex, dim); }
    void set_name    (char name);

// accessors:

    variant_type variant() const { return _x; }
    char name() const { return _name[_x % max_variant]; }
    size_type dimension() const { return _dimension[_x]; }
    size_type size()     const { return _n_vertex[_x]; }
    size_type n_vertex() const { return _n_vertex[_x]; }
    size_type n_side() const { return dimension() > 0 ? n_subgeo (variant(), dimension()-1) : 0; }
    size_type n_edge() const { return n_subgeo(1); }
    size_type n_face() const { return n_subgeo(2); }
    size_type n_subgeo_by_variant (size_type subgeo_variant) const {
        return n_subgeo_by_variant(variant(), subgeo_variant); }

    size_type n_subgeo(size_type subgeo_dim) const { return n_subgeo (variant(), subgeo_dim); }
    reference_element subgeo (size_type subgeo_dim, size_type loc_isid) const { 
        if (dimension() == 0) return reference_element(reference_element::p);
        if (dimension() == subgeo_dim) return *this;
        reference_element hat_S;
        size_type subgeo_n_vertex = subgeo_size (subgeo_dim, loc_isid);
        hat_S.set_variant (subgeo_n_vertex, subgeo_dim);
	return hat_S;
    }
    reference_element side (size_type loc_isid) const { return subgeo (dimension()-1, loc_isid); }
    size_type subgeo_size (size_type subgeo_dim, size_type loc_isid) const { 
	return subgeo_n_node (_x, 1, subgeo_dim, loc_isid); }
    size_type subgeo_local_vertex(size_type subgeo_dim, size_type loc_isid, size_type loc_jsidvert) const {
        return subgeo_local_node (_x, 1, subgeo_dim, loc_isid, loc_jsidvert); }
    size_type local_subgeo_index2index_by_variant (size_type subgeo_variant, size_type i) const {
      return (variant() == P && subgeo_variant == q) ? i-2 : i; }

    // TODO: use template<class T> instead of Float
    const point_basic<Float>& vertex (size_type iloc) const;
    friend Float measure (reference_element hat_K);
    Float side_measure (size_type loc_isid) const;
    void side_normal (size_type loc_isid, point_basic<Float>& hat_n) const;

// helpers:

    static variant_type variant  (char name);
    static variant_type variant  (size_type n_vertex, size_type dim);
    static char         name     (variant_type variant) { return _name     [variant]; }
    static size_type dimension   (variant_type variant) { return _dimension[variant]; }
    static size_type n_vertex    (variant_type variant) { return _n_vertex [variant]; }
    static size_type n_node      (variant_type variant, size_type order);

    static size_type n_sub_edge    (variant_type variant);
    static size_type n_sub_face    (variant_type variant);
    static size_type n_subgeo      (variant_type variant, size_type subgeo_dim);
    static size_type subgeo_n_node (variant_type variant, size_type order, size_type subgeo_dim, size_type loc_isid);
    static size_type subgeo_local_node (variant_type variant, size_type order, size_type subgeo_dim, size_type loc_isid, size_type loc_jsidnod);

    static variant_type first_variant_by_dimension (size_type dim) {
    		return _first_variant_by_dimension[dim]; }
    static variant_type  last_variant_by_dimension (size_type dim) {
    		return _first_variant_by_dimension[dim+1]; }

    static size_type first_inod_by_variant (variant_type variant, size_type order, variant_type subgeo_variant);
    static size_type  last_inod_by_variant (variant_type variant, size_type order, variant_type subgeo_variant)
       { return first_inod_by_variant (variant, order, subgeo_variant+1); }
    static size_type first_inod (variant_type variant, size_type order, size_type subgeo_dim)
       { return first_inod_by_variant (variant, order, first_variant_by_dimension(subgeo_dim)); }
    static size_type  last_inod (variant_type variant, size_type order, size_type subgeo_dim)
       { return first_inod_by_variant (variant, order,  last_variant_by_dimension(subgeo_dim)); }
    static void init_local_nnode_by_variant (size_type order, std::array<size_type,reference_element::max_variant>& loc_nnod_by_variant);

    static size_type n_subgeo_by_variant (size_type variant, size_type subgeo_variant)
      { return _n_subgeo_by_variant [variant] [subgeo_variant]; } 
// [verbatim_reference_element]

protected:
// constants:

    static const char         _name [max_variant];
    static const size_type    _dimension [max_variant];
    static const size_type    _n_subgeo_by_variant [max_variant][max_variant];
    static const size_type    _n_vertex [max_variant];
    static const variant_type _first_variant_by_dimension[5];

// data:

    variant_type _x;
// [verbatim_reference_element_cont]
};
// [verbatim_reference_element_cont]

class reference_element_p {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

class reference_element_e {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

class reference_element_t {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

class reference_element_q {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

class reference_element_T {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static size_type face2edge         (size_type loc_iface, size_type loc_iface_jedg);
  static int       face2edge_orient  (size_type loc_iface, size_type loc_iface_jedg);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

class reference_element_P {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static size_type face2edge         (size_type loc_iface, size_type loc_iface_jedg);
  static int       face2edge_orient  (size_type loc_iface, size_type loc_iface_jedg);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

class reference_element_H {
public:

// typedefs:

  typedef point_basic<size_t>::size_type size_type;

// static functions:

  static size_type n_subgeo          (size_type side_dim);
  static size_type subgeo_n_node     (size_type order, size_type side_dim, size_type loc_isid);
  static size_type subgeo_local_node (size_type order, size_type side_dim, size_type loc_isid, size_type loc_jsidnod);
  static size_type ilat2loc_inod     (size_type order, const point_basic<size_type>& ilat);
  static size_type first_inod_by_variant (size_type order, size_type subgeo_variant);
  static size_type face2edge         (size_type loc_iface, size_type loc_iface_jedg);
  static int       face2edge_orient  (size_type loc_iface, size_type loc_iface_jedg);
  static const point_basic<Float>& vertex (size_type iloc);
  static Float side_measure (size_type loc_isid);
  static void side_normal (size_type loc_isid, point_basic<Float>& hat_n);
};

} // namespace rheolef
#endif // _RHEOLEF_REFERENCE_ELEMENT_H
