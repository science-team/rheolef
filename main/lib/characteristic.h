#ifndef _RHEO_CHARACTERISTIC_H
#define _RHEO_CHARACTERISTIC_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@classfile characteristic Lagrange-Galerkin method
@addindex  Lagrange-Galerkin method
@addindex  method of characteristic
@addindex  quadrature formulae

Description
===========
The class characteristic implements the Lagrange-Galerkin method.
It is the extension of the method of characteristic from the finite
difference to the finite element context.

Note that the Lagrange-Galerkin method applies to diffusion-convection
problems when the convection term is not dominant.
For more serious problems, please refer to the discontinuous
Galerkin method in the Rheolef library.

Example
=======
Consider the bilinear form `lh` defined by

            /
            |
    lh(x) = | uh(x+dh(x)) v(x) dx
            |
            / Omega

where `dh` is a deformation vector field.
The characteristic is defined by `X(x)=x+dh(x)`
for any x in Omega,
and the previous integral writes equivalently:

            /
            |
    lh(x) = | uh(X(x)) v(x) dx
            |
            / Omega

For instance, in Lagrange-Galerkin methods, 
the deformation field `dh(x)=-dt*uh(x)` 
where `uh` is the advection field and `dt` a time step.
The following code implements the computation of `lh`:

        field dh = ...;
        field uh = ...;
        characteristic X (dh);
        test v (Xh);
        field lh = integrate (compose(uh, X)*v, qopt);

The Gauss-Lobatto quadrature formula is recommended for 
the computation of integrals involving the characteristic
`X` of the Lagrange-Galerkin method.
The order equal to the polynomial order of `Xh`
(order 1: trapeze, order 2: simpson, etc).
Recall that this choice of quadrature formula
guaranties inconditionnal stability at any polynomial order.
Alternative quadrature formulae or order can be used by using the 
additional @ref integrate_option_3 argument to the @ref integrate_3
function.

Implementation
==============
@showfromfile
The `characteristic` class is simply an alias to the `characteristic_basic` class

@snippet characteristic.h verbatim_characteristic
@par

The `characteristic_basic` class provides an interface,
via the @ref smart_pointer_7 class family,
to a data container:

@snippet characteristic.h verbatim_characteristic_basic
@snippet characteristic.h verbatim_characteristic_basic_cont
*/
} // namespace rheolef

#include "rheolef/field.h"
#include "rheolef/piola_on_pointset.h"

namespace rheolef { 

// -------------------------------------------------------------------
// characteristic_on_quadrature: store the localization in mesh of the 
// quadrature point tacking: y = x + d(x) : that belongs in K as hat_y
// -------------------------------------------------------------------
template <class T, class M>
struct characteristic_on_quadrature_rep {
// allocators:
  characteristic_on_quadrature_rep (quadrature_option qopt1 = quadrature_option(quadrature_option::max_family,0))
  : _qopt         (qopt1),
    _quad         (),
    _piola_on_quad(),
    _ie2dis_ix    (),
    _hat_y        (),
    _yq           ()
  {}

  characteristic_on_quadrature_rep (const characteristic_on_quadrature_rep<T,M>& x)
  : _qopt         (x._qopt),
    _quad         (), // OK: DO NOT COPY !
    _piola_on_quad(),
    _ie2dis_ix    (),
    _hat_y        (),
    _yq           ()
  {
    error_macro ("physical copy of characteristic_on_quadrature_rep may not happened"); 
  }

public:
// data:
  quadrature_option        _qopt;
  quadrature<T>                 _quad;
  basis_on_pointset<T>          _piola_on_quad;
  disarray<index_set,M>         _ie2dis_ix;
  disarray<point_basic<T>,M>    _hat_y;
  disarray<point_basic<T>,M>    _yq;
};
template <class T, class M>
class characteristic_on_quadrature : public smart_pointer<characteristic_on_quadrature_rep<T,M> > {
public:
// typedefs:
  typedef characteristic_on_quadrature_rep<T,M> rep;
  typedef smart_pointer<rep>                    base;
// allocator:
  characteristic_on_quadrature
    (quadrature_option qopt = quadrature_option(quadrature_option::max_family,0))
  : base (new_macro(rep(qopt)))
  {}
// data:
};
// -------------------------------------------------------------------
// characteristic_rep: store a list of characteristic_on_quadrature(s)
// together with the displacement field dh
// -------------------------------------------------------------------
template<class T, class M>
class characteristic_rep {
public:
  typedef std::map<std::string,characteristic_on_quadrature<T,M> > map_type;

// allocator:

  characteristic_rep(const field_basic<T,M>& dh);

// accesor:

  const field_basic<T,M>& get_displacement() const { return _dh; }

  const characteristic_on_quadrature<T,M>&
  get_pre_computed (
    const space_basic<T,M>&       Xh,
    const field_basic<T,M>&       dh,
    const piola_on_pointset<T>&   pops) const;

// data:
protected:
  field_basic<T,M>  _dh;
  mutable map_type  _coq_map;
};


// [verbatim_characteristic_basic]
template<class T, class M = rheo_default_memory_model>
class characteristic_basic : public smart_pointer<characteristic_rep<T,M> > {
public:
  typedef characteristic_rep<T,M> rep;
  typedef smart_pointer<rep>      base;

// allocator:

  characteristic_basic(const field_basic<T,M>& dh);

// accesors:

  const field_basic<T,M>& get_displacement() const;
// [verbatim_characteristic_basic]

  const characteristic_on_quadrature<T,M>&
  get_pre_computed (
    const space_basic<T,M>&       Xh,
    const field_basic<T,M>&       dh,
    const piola_on_pointset<T>&   pops) const;
// [verbatim_characteristic_basic_cont]
};
// [verbatim_characteristic_basic_cont]

// [verbatim_characteristic]
typedef characteristic_basic<Float> characteristic;
// [verbatim_characteristic]

template<class T, class M>
inline
characteristic_basic<T,M>::characteristic_basic (const field_basic<T,M>& dh)
 : base (new_macro(rep(dh)))
{
}
template<class T, class M>
inline
const field_basic<T,M>&
characteristic_basic<T,M>::get_displacement() const
{
  return base::data().get_displacement();
}
template<class T, class M>
inline
const characteristic_on_quadrature<T,M>&
characteristic_basic<T,M>::get_pre_computed (
    const space_basic<T,M>&       Xh,
    const field_basic<T,M>&       dh,
    const piola_on_pointset<T>&   pops) const
{
  return base::data().get_pre_computed (Xh,dh,pops);
}
// ===========================================================================
// the temporary class returned by compose (phi_h,X)
// ===========================================================================
template<class T, class M>
class field_o_characteristic {
public:

// allocator:

  field_o_characteristic(const field_basic<T,M>& uh, const characteristic_basic<T,M>& X)
    : _uh(uh), _X(X) {}

// accesor:

  const field_basic<T,M>&           get_field() const { return _uh; }
  const characteristic_basic<T,M>&  get_characteristic()  const { return _X; }

// data:
protected:
  field_basic<T,M>          _uh;
  characteristic_basic<T,M> _X;
};

}// namespace rheolef
#endif // _RHEO_CHARACTERISTIC_H
