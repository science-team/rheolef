///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/quadrature.h"
using namespace rheolef;
using namespace std;

template <class Function>
typename Function::result_type
integrate (reference_element hat_K, 
	Function f, const quadrature<typename Function::result_type>& quad)
{
    typedef typename Function::result_type T;
    T sum = 0;
    typename quadrature<T>::const_iterator first = quad.begin (hat_K);
    typename quadrature<T>::const_iterator last  = quad.end   (hat_K);
    for (; first != last; first++) 
        sum += f((*first).x) * (*first).w;
    return sum;
}
struct monom1d : unary_function<point,Float> {
    monom1d(size_t _n0) : n0(_n0) {}
    Float operator() (const point& x) {
        return pow(x[0], n0);
    }
    size_t n0;
};
struct monom2d : unary_function<point,Float> {
    monom2d(size_t _n0, size_t _n1) : n0(_n0), n1(_n1) {}
    Float operator() (const point& x) {
        return pow(x[0], n0) * pow(x[1], n1);
    }
    size_t n0, n1;
};
struct monom3d : unary_function<point,Float> {
    monom3d(size_t _n0, size_t _n1, size_t _n2) : n0(_n0), n1(_n1), n2(_n2) {}
    Float operator() (const point& x) {
        return pow(x[0], n0) * pow(x[1], n1) * pow(x[2], n2);
    }
    size_t n0, n1, n2;
};
Float
factorial (size_t n) {
    Float f = 1;
    for (size_t i = 2; i <= n; i++) f *= Float(int(i));
    return f;
}
Float exact_integrate_e (monom1d m) {
    return Float(1)/Float(int(m.n0)+1);
}
Float exact_integrate_t (monom2d m) {
    return factorial(m.n0)*factorial(m.n1)/factorial(m.n0+m.n1+2);
}
Float exact_integrate_q (monom2d m) {
    if (m.n0 % 2 == 1 || m.n1 % 2 == 1) return 0;
    return Float(4) / (Float(int(m.n0+1))*Float(int(m.n1+1)));
}
Float exact_integrate_T (monom3d m) {
    return factorial(m.n0)*factorial(m.n1)*factorial(m.n2)/factorial(m.n0+m.n1+m.n2+3);
}
Float exact_integrate_P (monom3d m) {
    if (m.n2 % 2 == 1) return 0;
    return 2*factorial(m.n0)*factorial(m.n1)/(factorial(m.n0+m.n1+2)*Float(int(m.n2+1)));
}
Float exact_integrate_H (monom3d m) {
    if (m.n0 % 2 == 1 || m.n1 % 2 == 1 || m.n2 % 2 == 1) return 0;
    return Float(8) / Float(int((m.n0+1)*(m.n1+1)*(m.n2+1)));
}
int main(int argc, char**argv) {
    if (argc < 3) {
	cerr << "usage: quadrature_tst <hat_K> <r> [-put]" << endl
	     << "ex:" << endl
             << "       quadrature_tst t 2" << endl;
        exit (0);
    }
#if defined(_RHEOLEF_HAVE_FLOAT128)
    Float default_tol = 1e3*numeric_limits<Float>::epsilon();
#else // ! _RHEOLEF_HAVE_xxx
    Float default_tol = 10*sqrt(numeric_limits<Float>::epsilon());
#endif // _RHEOLEF_HAVE_xxx
    char   c  = argv[1][0];
    size_t r  = atoi(argv[2]);
    bool put  = (argc > 3) && (string(argv[3]) == "-put");
    Float tol = (argc > 3) && (string(argv[3]) != "-put") ? Float(atof(argv[3])) : default_tol;
    reference_element hat_K;
    hat_K.set_name (c);
    quadrature_option qopt;
    qopt.set_order(r);
    quadrature<Float> quad (qopt);
    switch (hat_K.variant()) {
      case reference_element::e: {
	for (size_t i = 0; i <= r; i++) {
            Float sq = integrate  (hat_K, monom1d(i), quad);
            Float se = exact_integrate_e (monom1d(i));
            if (fabs(sq-se) > tol) {
	        cerr << "Iq(" << r << ";" << i << ") = "
	             << sq << " != " << se << endl;
	        exit(1);
            }
        }
        break;
      }
      case reference_element::t: {
	for (size_t i = 0; i <= r; i++) {
          for (size_t j = 0; j <= r-i; j++) {
            Float sq = integrate  (hat_K, monom2d(i,j), quad);
            Float se = exact_integrate_t (monom2d(i,j));
	    trace_macro ("error = " << fabs(sq-se));
            if (fabs(sq-se) > tol) {
	        cerr << "Iq(" << r << ";" << i << "," << j << ") = "
	             << sq << " != " << se 
		     << " where error = " << fabs(sq-se) << " > tol = " << tol << endl;
	        exit(1);
            }
          }
        }
        break;
      }
      case reference_element::q: {
	for (size_t i = 0; i <= r; i++) {
          for (size_t j = 0; j <= r; j++) {
            Float sq = integrate  (hat_K, monom2d(i,j), quad);
            Float se = exact_integrate_q (monom2d(i,j));
            if (fabs(sq-se) > tol) {
	        cerr << "Iq(" << r << ";" << i << "," << j << ") = "
	             << sq << " != " << se
		     << " where error = " << fabs(sq-se) << " > tol = " << tol << endl;
	        exit(1);
            }
          }
        }
        break;
      }
      case reference_element::T: {
	for (size_t i = 0; i <= r; i++) {
          for (size_t j = 0; j <= r-i; j++) {
            for (size_t k = 0; k <= r-i-j; k++) {
              Float sq = integrate  (hat_K, monom3d(i,j,k), quad);
              Float se = exact_integrate_T (monom3d(i,j,k));
              if (fabs(sq-se) > tol) {
	        cerr << "Iq(" << r << ";" << i << "," << j << "," << k << ") = "
	             << sq << " != " << se << endl;
	        exit(1);
              }
            }
          }
        }
        break;
      }
      case reference_element::P: {
	for (size_t i = 0; i <= r; i++) {
          for (size_t j = 0; j <= r-i; j++) {
            for (size_t k = 0; k <= r; k++) {
              Float sq = integrate  (hat_K, monom3d(i,j,k), quad);
              Float se = exact_integrate_P (monom3d(i,j,k));
              if (fabs(sq-se) > tol) {
	        cerr << "Iq(" << r << ";" << i << "," << j << "," << k << ") = "
	             << sq << " != " << se << endl;
	        exit(1);
              }
            }
          }
        }
        break;
      }
      case reference_element::H: {
	for (size_t i = 0; i <= r; i++) {
          for (size_t j = 0; j <= r; j++) {
            for (size_t k = 0; k <= r; k++) {
              Float sq = integrate  (hat_K, monom3d(i,j,k), quad);
              Float se = exact_integrate_H (monom3d(i,j,k));
              if (fabs(sq-se) > tol) {
	        cerr << "Iq(" << r << ";" << i << "," << j << "," << k << ") = "
	             << sq << " != " << se 
		     << " where error = " << fabs(sq-se) << " > tol = " << tol << endl;
	        exit(1);
              }
            }
          }
        }
        break;
      }
      default: {
	error_macro ("unsuported test on element type `" << hat_K.name() << "'");
      }
    }
    if (!put) return 0;
    size_t d = hat_K.dimension();
    cout << setprecision(numeric_limits<Float>::digits10)
         << quad.size(hat_K) << endl;
    for (quadrature<Float>::const_iterator first = quad.begin(hat_K), last = quad.end(hat_K);
		first != last; first++) {
      const point& xq = (*first).x;
      const Float& wq = (*first).w;
      cout << wq << "\t";
      xq.put (cout, d);
      cout << endl;
    }
}
