# ci_gitlab

Configuration files used for continuous integration on gricad-gitlab.


* dockerfiles : contains a directory for each system you want to test, with a Dockerfile describing what must be installed on each Docker image.

 e.g. : [Dockerfile for Debian buster](dockerfiles/debian_buster/Dockerfile).

* [ci-rheolef-templates.yml](ci-rheolef-templates.yml) : definition of templates used to create CI jobs.



Everything is driven by the jobs defined in the file [.gitlab-ci.yml](../.gitlab-ci.yml).
