///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_upw_dg.icc The Navier-Stokes equations with the discontinuous Galerkin method and upwinding -- class body
#include "inertia_upw.icc"
navier_stokes_upw_dg::navier_stokes_upw_dg (
 Float Re1, const geo& omega, string approx)
 : navier_stokes_dg (Re1, omega, approx) {}

navier_stokes_upw_dg::value_type
navier_stokes_upw_dg::residue (const value_type& xh) const {
  trial u (Xh); test v (Xh);
  form a = a0 + Re*(  inertia     (xh[0], u, v, iopt) 
                    + inertia_upw (xh[0], u, v, iopt));
  value_type mrh(2);
  mrh[0] = a*xh[0] + b.trans_mult(xh[1]) - lh;
  mrh[1] = b*xh[0] -            c*xh[1]  - kh;
  return mrh;
}
void navier_stokes_upw_dg::update_derivative (const value_type& xh) const {
  trial du (Xh); test v (Xh);
  a1 = a0 + Re*(      inertia         (xh[0], du,    v, iopt) 
                 + inertia_upw        (xh[0], du,    v, iopt)
                 +    inertia         (du,    xh[0], v, iopt)  
               + d_inertia_upw (xh[0], du,    xh[0], v, iopt));
  stokes1 = problem_mixed (a1, b, c);
  stokes1.set_metric (mp);
}
