///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile p_laplacian_newton.cc The p-Laplacian problem by the Newton method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "p_laplacian.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  Float  eps      = std::numeric_limits<Float>::epsilon();
  string approx   = (argc > 2) ?      argv[2]  : "P1";
  Float  p        = (argc > 3) ? atof(argv[3]) : 1.5;
  Float  tol      = (argc > 4) ? atof(argv[4]) : 1e5*eps;
  size_t max_iter = (argc > 5) ? atoi(argv[5]) : 500;
  derr << "# P-Laplacian problem by Newton:" << endl
       << "# geo = " << omega.name() << endl
       << "# approx = " << approx << endl
       << "# p = " << p << endl
       << "# tol = " << tol << endl
       << "# max_iter = " << max_iter << endl;
  p_laplacian F (p, omega, approx);
  field uh = F.initial ();
  int status = newton (F, uh, tol, max_iter, &derr);
  dout << setprecision(numeric_limits<Float>::digits10)
       << catchmark("p") << p << endl
       << catchmark("u") << uh;
  return status;
}
