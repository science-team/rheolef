#ifndef _RHEO_RANGE_H
#define _RHEO_RANGE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   29 feb 2018

namespace rheolef {
/**
@linalgclassfile range index range for a subset of a vector

Description
===========
This class represents a range of index for 
addressing a subset of the @ref vec_4 container.

Example
=======

        vec<double> x (100, 3.14);
        vec<double> y = x(range(0,50));
        dout << y << endl;

Implementation
==============
@showfromfile
@snippet range.h verbatim_range
@snippet range.h verbatim_range_cont
*/
} // namespace rheolef

# include "rheolef/compiler.h"

namespace rheolef {

// [verbatim_range]
struct range {
  using size_type = size_t;
  range (size_type start=0, size_type stop=0);
  size_type start() const;
  size_type size() const;
  static range all();
// [verbatim_range]
protected:
  size_type _start, _size;
// [verbatim_range_cont]
};
// [verbatim_range_cont]

// -------------------------------
// inlined
// -------------------------------

inline
range::range (size_type start, size_type stop)
 : _start (start),
   _size (stop - start)
{
  check_macro (start <= stop, "invalid range ["<<_start<<":"<<stop<<"[");
}
inline range::size_type range::start() const { return _start; }
inline range::size_type range::size() const { return _size; } 
inline range            range::all() { return range(0, size_type (-1)); }

} // namespace rheolef
#endif // _RHEO_RANGE_H
