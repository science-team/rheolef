set terminal cairolatex pdf color standalone
set output "dirichlet_dg-1d-elevation2.tex"

set sample 1000
set colors classic
set size square 
set xrange [0:1]
set yrange [-1.1:1.1]
set xtics (0,0.5,1)
set ytics (-1,0,1)
set xlabel '[c]{\large $x$}'
set  label '[r]{$h=1/20$}' at graph 0.80,0.70
plot \
  cos(pi*x) \
	title '[r]{$u(x)$}' \
	w l lw 1 lc 0 lt 1, \
  "dirichlet_dg-1d-elevation2.gdat" \
	title '[r]{$u_h(x)$}' \
	with linespoints lc 1 lw 3 lt 1

#pause -1
