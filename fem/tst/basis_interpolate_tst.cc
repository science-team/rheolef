///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "field_element.h"
using namespace rheolef;
using namespace std;

Float dx = 1;
Float u (const point& x) { return exp(sqr((x[0]+x[1]+x[2])/dx)); }

int main(int argc, char**argv) {
  string approx = (argc > 1) ?      argv[1]    : "P3";
  char   t      = (argc > 2) ?      argv[2][0] : 't';
  size_t nsub   = (argc > 3) ? atoi(argv[3])   : 0;
  basis b (approx);
  reference_element hat_K;
  hat_K.set_name(t);
  if (t == 'q' || t == 'P' || t == 'H') dx = 2; else dx = 1;
  space_element Vk (b, hat_K);
  field_element uk (Vk);
  uk.interpolate (u); // TODO: uk = interpolate(Vk,u)
  if (nsub == 0) nsub = max(size_t(5), 2*b.degree() + 2);
  Float err_l2   = error_l2   (uk, u, nsub);
  Float err_linf = error_linf (uk, u, nsub);
  cout << "approx   " << b.name() << endl
       << "degree   " << b.degree() << endl
       << "node     " << b.option().get_node_name() << endl
       << "raw_poly " << b.option().get_raw_polynomial_name() << endl
       << "nsub     " << nsub << endl
       << "err_l2   " << err_l2 << endl
       << "err_linf " << err_linf << endl
    ;
}
