#ifndef _RHEOLEF_FORM_EXPR_VARIATIONAL_H
#define _RHEOLEF_FORM_EXPR_VARIATIONAL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// variational expressions are used for form assembly
//
// author: Pierre.Saramito@imag.fr
//
// date: 23 september 2015 
//
// Notes: use template expressions and SFINAE techniques
//
// SUMMARY:
// 1. concept
// 2. unary function 
//    2.1. unary node
//    2.2. unary calls
// 3. binary operators +-  between two variational forms
//    3.1. binary node
//    3.2. binary calls
// 4. binary operators * between two variational fields that returns a form
//    4.1. unary node
//    4.2. unary calls
// 5. binary operators */ between a variational form and a nonlinear expr
//    5.1. binary node
//    5.2. binary calls
// 6. binary operators */ between a variational form and a constant
//
#include "rheolef/field_expr_variational.h"

namespace rheolef {

// -------------------------------------------------------------------
// 1. concept
// -------------------------------------------------------------------
namespace details {

// Define a trait type for detecting form expression valid arguments
template<class T> struct is_form_expr_v2_variational_arg : std::false_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 2. unary function
// example: -(u*v), 2*(u*v), (u*v)/2
// ---------------------------------------------------------------------------
// 2.1. unary node
// ---------------------------------------------------------------------------
namespace details {

template<class UnaryFunction, class Expr>
class form_expr_v2_variational_unary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename details::generic_unary_traits<UnaryFunction>::template result_hint<
          typename Expr::value_type>::type              result_hint;
  typedef typename details::generic_unary_traits<UnaryFunction>::template hint<
	  typename Expr::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_v2_variational_unary<UnaryFunction,Expr>           self_type;
  typedef form_expr_v2_variational_unary<UnaryFunction, typename Expr::dual_self_type>
                                                        dual_self_type;
  typedef typename Expr::maybe_symmetric::type          maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  form_expr_v2_variational_unary (const UnaryFunction& f, const Expr& expr)
    : _f(f), _expr(expr) {}

  form_expr_v2_variational_unary (const form_expr_v2_variational_unary<UnaryFunction,Expr>& x)
    : _f(x._f), _expr(x._expr) {}

  form_expr_v2_variational_unary<UnaryFunction,Expr>&
  operator= (const form_expr_v2_variational_unary<UnaryFunction,Expr>& x) {
    _f = x._f; _expr = x._expr; return *this; }

// accessors:

  const space_type&  get_trial_space() const { return _expr.get_trial_space(); }
  const space_type&  get_test_space()  const { return _expr.get_test_space(); }
  size_type n_derivative() const             { return _expr.n_derivative(); }

// mutable modifiers:

  void initialize (const piola_on_pointset<float_type>& pops, const integrate_option& iopt) { 
    _expr.initialize (pops, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const piola_on_pointset<float_type>& pops, const integrate_option& iopt) {  
    _expr.initialize (gh, pops, iopt);
  }
  template<class Result>
  void evaluate (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K,
    Eigen::Tensor<Result,3>&                 value) const
  {
    // Arg=Result=float_type for f in general: elementary matrix,
    // so use Result-valued "value" for arg of f:
    _expr.evaluate (omega_K, K, value);
    for (size_type i = 0, ni = value.dimension(0); i < ni; ++i) {
    for (size_type j = 0, nj = value.dimension(1); j < nj; ++j) {
    for (size_type k = 0, nk = value.dimension(2); k < nk; ++k) {
      value(i,j,k) = _f (value(i,j,k));
    }}}
  }
  template<class Result>
  bool valued_check() const {
    typedef Result A1;
    if (! is_undeterminated<A1>::value) return _expr.template valued_check<A1>();
    return true;
  }
protected:
// data:
  UnaryFunction  _f;
  Expr           _expr;
};
template<class F, class Expr> struct is_form_expr_v2_variational_arg    <form_expr_v2_variational_unary<F,Expr> > : std::true_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 2.2. unary calls
// ---------------------------------------------------------------------------

#define _RHEOLEF_make_form_expr_v2_variational_unary(FUNCTION,FUNCTOR)	\
template<class Expr>								\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_v2_variational_arg<Expr>::value				\
 ,details::form_expr_v2_variational_unary<					\
    FUNCTOR									\
   ,Expr									\
  >										\
>::type										\
FUNCTION (const Expr& expr)							\
{										\
  return details::form_expr_v2_variational_unary <FUNCTOR,Expr> (FUNCTOR(), expr); \
}

_RHEOLEF_make_form_expr_v2_variational_unary (operator+, details::unary_plus)
_RHEOLEF_make_form_expr_v2_variational_unary (operator-, details::negate)
#undef _RHEOLEF_make_form_expr_v2_variational_unary

// ---------------------------------------------------------------------------
// 3. binary operators +-  between two variational forms
// ---------------------------------------------------------------------------
// 3.1. binary node
// ---------------------------------------------------------------------------
// example: operator+ between two forms as in
//	    (u*v) + dot(grad(u),grad(v))

namespace details {

template<class BinaryFunction, class Expr1, class Expr2>
class form_expr_v2_variational_binary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename Expr1::memory_type,typename Expr2::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename Expr1::value_type
         ,typename Expr2::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type; // TODO: deduce from Exprs
  typedef typename details::bf_vf_tag<BinaryFunction,
	typename Expr1::vf_tag_type,
	typename Expr2::vf_tag_type>::type              vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_v2_variational_binary<BinaryFunction,Expr1,Expr2>         self_type;
  typedef form_expr_v2_variational_binary<BinaryFunction,typename Expr1::dual_self_type,
                                        typename Expr2::dual_self_type>
                                                        dual_self_type;
  typedef typename and_type<typename Expr1::maybe_symmetric::type,
		            typename Expr2::maybe_symmetric::type>::type
	                                               maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  form_expr_v2_variational_binary (const BinaryFunction& f, 
		    const Expr1&    expr1,
                    const Expr2&    expr2)
    : _f(f), _expr1(expr1), _expr2(expr2) {}

  form_expr_v2_variational_binary (const form_expr_v2_variational_binary<BinaryFunction,Expr1,Expr2>& x)
    : _f(x._f), _expr1(x._expr1), _expr2(x._expr2) {}

  form_expr_v2_variational_binary<BinaryFunction,Expr1,Expr2>&
  operator= (const form_expr_v2_variational_binary<BinaryFunction,Expr1,Expr2>& x)
    { _f = x._f; _expr1 = x._expr1; _expr2 = x._expr2; return *this; }

// accessors:

  const space_type&  get_trial_space() const { return _expr1.get_trial_space(); }
  const space_type&  get_test_space()  const { return _expr1.get_test_space(); }
  size_type n_derivative() const             { return std::min(_expr1.n_derivative(), _expr2.n_derivative()); }

// mutable modifiers:

  // TODO: at init, check that exp1 & expr2 has the same test & trial spaces
  void initialize (const piola_on_pointset<float_type>& pops, const integrate_option& iopt) { 
    _expr1.initialize (pops, iopt);
    _expr2.initialize (pops, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const piola_on_pointset<float_type>& pops, const integrate_option& iopt) {  
    _expr1.initialize (gh, pops, iopt);
    _expr2.initialize (gh, pops, iopt);
  }
  // Result is float_type in general: elementary matrix
  template<class Result>
  void evaluate (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K, 
    Eigen::Tensor<Result,3>&                 value) const
  {
    // for f=operator+ => sum of two elementary matrix of the same type
    // TODO: otherwise Result and 2 could be obtained from the hint<> helper
    typedef Result A1;
    typedef Result A2;
    Eigen::Tensor<float_type,3> value1; _expr1.evaluate (omega_K, K, value1);
    Eigen::Tensor<float_type,3> value2; _expr2.evaluate (omega_K, K, value2);
    value.resize (value1.dimension(0), value1.dimension(1), value1.dimension(2));
    check_macro (value1.dimension(0) == value2.dimension(0) &&
                 value1.dimension(1) == value2.dimension(1) &&
                 value1.dimension(2) == value2.dimension(2),
      "invalid sizes value1("
      << value1.dimension(0) <<","<< value1.dimension(1) <<","<< value1.dimension(2) << ") and value2("
      << value2.dimension(0) <<","<< value2.dimension(1) <<","<< value2.dimension(2) <<")");
    for (size_type i = 0, ni = value.dimension(0); i < ni; ++i) {
    for (size_type j = 0, nj = value.dimension(1); j < nj; ++j) {
    for (size_type k = 0, nk = value.dimension(2); k < nk; ++k) {
      value(i,j,k) = _f (value1(i,j,k), value2(i,j,k));
    }}}
  }
  template<class Result>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K, 
    const side_information_type&             sid,
    Eigen::Tensor<Result,3>&                 value) const
  {
    // for f=operator+ => sum of two elementary matrix of the same type
    // TODO: otherwise Result and 2 could be obtained from the hint<> helper
    typedef Result A1;
    typedef Result A2;
    Eigen::Tensor<float_type,3> value1; _expr1.evaluate_on_side (omega_K, K, sid, value1);
    Eigen::Tensor<float_type,3> value2; _expr2.evaluate_on_side (omega_K, K, sid, value2);
    value.resize (value1.dimension(0), value1.dimension(1), value1.dimension(2));
    check_macro (value1.dimension(0) == value2.dimension(0) &&
                 value1.dimension(1) == value2.dimension(1) &&
                 value1.dimension(2) == value2.dimension(2),
      "invalid sizes value1("
      << value1.dimension(0) <<","<< value1.dimension(1) <<","<< value1.dimension(2) << ") and value2("
      << value2.dimension(0) <<","<< value2.dimension(1) <<","<< value2.dimension(2) <<")");
    for (size_type i = 0, ni = value.dimension(0); i < ni; ++i) {
    for (size_type j = 0, nj = value.dimension(1); j < nj; ++j) {
    for (size_type k = 0, nk = value.dimension(2); k < nk; ++k) {
      value(i,j,k) = _f (value1(i,j,k), value2(i,j,k));
    }}}
  }
  template<class Result>
  bool valued_check() const {
    typedef Result A1;
    typedef Result A2;
    bool status = true;
    if (! is_undeterminated<A1>::value)  status &= _expr1.template valued_check<A1>();
    if (! is_undeterminated<A2>::value)  status &= _expr2.template valued_check<A2>();
    return status;
  }
protected:
// data:
  BinaryFunction  _f;
  Expr1           _expr1;
  Expr2           _expr2;
};
template<class F, class Expr1, class Expr2> struct is_form_expr_v2_variational_arg    <form_expr_v2_variational_binary<F,Expr1,Expr2> > : std::true_type {};

} // namespace details

// ---------------------------------------------------------------------------
// 3.2. binary calls
// ---------------------------------------------------------------------------

#define _RHEOLEF_form_expr_v2_variational_binary(FUNCTION,FUNCTOR)		\
template <class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
     details::is_form_expr_v2_variational_arg <Expr1>::value			\
  && details::is_form_expr_v2_variational_arg <Expr2>::value			\
 ,details::form_expr_v2_variational_binary<					\
    FUNCTOR									\
   ,Expr1									\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_v2_variational_binary				\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), expr1, expr2);							\
}

_RHEOLEF_form_expr_v2_variational_binary (operator+, details::plus)
_RHEOLEF_form_expr_v2_variational_binary (operator-, details::minus)

#undef _RHEOLEF_form_expr_v2_variational_binary

// ---------------------------------------------------------------------------
// 4. binary operators * between two variational fields that returns a form
//    example: integrate(u*v)
// ---------------------------------------------------------------------------
// 4.1. binary node
// ---------------------------------------------------------------------------
namespace details {

template<class BinaryFunction, class Expr1, class Expr2>
class form_expr_v2_variational_binary_field {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename Expr1::memory_type,typename Expr2::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename Expr1::value_type
         ,typename Expr2::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type; // TODO: deduce from Exprs
  typedef typename details::bf_vf_tag<BinaryFunction,
	typename Expr1::vf_tag_type,
	typename Expr2::vf_tag_type>::type              vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_v2_variational_binary_field<BinaryFunction,Expr1,Expr2>   self_type;
  typedef form_expr_v2_variational_binary_field<BinaryFunction,typename Expr1::dual_self_type,typename Expr2::dual_self_type>
                                                        dual_self_type;
  typedef typename and_type<typename details::generic_binary_traits<BinaryFunction>::is_symmetric::type,
		            typename details::is_equal<
				Expr1,
				typename Expr2::dual_self_type>::type>::type 
	                                                maybe_symmetric;

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  form_expr_v2_variational_binary_field (const BinaryFunction& f, 
		    const Expr1&    expr1,
                    const Expr2&    expr2)
    : _f(f), _expr1(expr1), _expr2(expr2) {}
  form_expr_v2_variational_binary_field<BinaryFunction,Expr1,Expr2>&
  operator= (const form_expr_v2_variational_binary_field<BinaryFunction,Expr1,Expr2>& x) {
    _f = x._f; _expr1 = x._expr1; _expr2 = x._expr2; return *this; }

// accessors:

  const space_type&  get_test_space()  const { 
    return (details::is_equal<typename Expr1::vf_tag_type, details::vf_tag_01>::value) ?
	_expr1.get_vf_space() : _expr2.get_vf_space();
  }
  const space_type&  get_trial_space() const {
    return (details::is_equal<typename Expr1::vf_tag_type, details::vf_tag_10>::value) ?
	_expr1.get_vf_space() : _expr2.get_vf_space();
  }

  size_type n_derivative() const             { return _expr1.n_derivative() + _expr2.n_derivative(); }

// mutable modifiers:

  void initialize (
    const piola_on_pointset<float_type>&             pops,
    const integrate_option&                          iopt)
  {
    _expr1.initialize (pops, iopt);
    _expr2.initialize (pops, iopt);
  }
  void initialize (
    const band_basic<float_type,memory_type>&        gh,
    const piola_on_pointset<float_type>&             pops,
    const integrate_option&                          iopt)
  {  
    _expr1.initialize (gh, pops, iopt);
    _expr2.initialize (gh, pops, iopt);
  }
  // TODO: DVT_EXPR_SPARSE_MATRIX return array[q] of SparseMatrix(i,j)
  template<class ValueType, class Arg1, class Arg2>
  void evaluate_internal (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K,
    Eigen::Tensor<ValueType,3>&              value) const
  {
    typedef long int eig_idx_t;
    if (details::is_equal<typename Expr2::vf_tag_type, details::vf_tag_01>::value) {
      // expr1 is trial and expr2 is test
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> v_test;  _expr2.evaluate (omega_K, K, v_test);
      Eigen::Matrix<Arg1,Eigen::Dynamic,Eigen::Dynamic> u_trial; _expr1.evaluate (omega_K, K, u_trial);
      check_macro(u_trial.rows() == v_test.rows(), "mult: invalid sizes u_trial("
        <<u_trial.rows()<<","<<u_trial.cols() <<") and v_test("
        <<v_test.rows() <<","<<v_test.cols()<<")");
      eig_idx_t nq = u_trial.rows();
      eig_idx_t ni = v_test.cols(); 
      eig_idx_t nj = u_trial.cols();
      value.resize(nq,ni,nj);
      for (eig_idx_t q = 0; q < nq; ++q) {
      for (eig_idx_t i = 0; i < ni; ++i) {
      for (eig_idx_t j = 0; j < nj; ++j) {
        value(q,i,j) = _f (u_trial(q,j), v_test(q,i));
      }}}
    } else {
      // expr2 is trial and expr1 is test
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> v_test;  _expr1.evaluate (omega_K, K, v_test);
      Eigen::Matrix<Arg1,Eigen::Dynamic,Eigen::Dynamic> u_trial; _expr2.evaluate (omega_K, K, u_trial);
      check_macro(u_trial.rows() == v_test.rows(), "binary: invalid sizes");
      eig_idx_t nq = u_trial.rows();
      eig_idx_t ni = v_test.cols(); 
      eig_idx_t nj = u_trial.cols();
      value.resize(nq,ni,nj);
      for (eig_idx_t q = 0; q < nq; ++q) {
      for (eig_idx_t i = 0; i < ni; ++i) {
      for (eig_idx_t j = 0; j < nj; ++j) {
        value(q,i,j) = _f (u_trial(q,j), v_test(q,i));
      }}}
    }
  }
  template<class ValueType, class Arg1, class Arg2>
  void evaluate_on_side_internal (
    const geo_basic<float_type,memory_type>& omega_K,
    const geo_element&                       K,
    const side_information_type&             sid,
    Eigen::Tensor<ValueType,3>&              value) const
  {
    typedef long int eig_idx_t;
    bool do_local_component_assembly = true;
    if (details::is_equal<typename Expr2::vf_tag_type, details::vf_tag_01>::value) {
      // expr1 is trial and expr2 is test
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> v_test;  _expr2.evaluate_on_side (omega_K, K, sid, v_test,  do_local_component_assembly);
      Eigen::Matrix<Arg1,Eigen::Dynamic,Eigen::Dynamic> u_trial; _expr1.evaluate_on_side (omega_K, K, sid, u_trial, do_local_component_assembly);
      check_macro(u_trial.rows() == v_test.rows(), "invalid sizes u_trial("
        <<u_trial.rows()<<","<<u_trial.cols() <<") and v_test("
        <<v_test.rows() <<","<<v_test.cols()<<")");
      eig_idx_t nq = u_trial.rows();
      eig_idx_t ni = v_test.cols(); 
      eig_idx_t nj = u_trial.cols();
      value.resize(nq,ni,nj);
      for (eig_idx_t q = 0; q < nq; ++q) {
      for (eig_idx_t i = 0; i < ni; ++i) {
      for (eig_idx_t j = 0; j < nj; ++j) {
        value(q,i,j) = _f (u_trial(q,j), v_test(q,i));
      }}}
    } else {
      // expr2 is trial and expr1 is test
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> v_test;  _expr1.evaluate_on_side (omega_K, K, sid, v_test,  do_local_component_assembly);
      Eigen::Matrix<Arg1,Eigen::Dynamic,Eigen::Dynamic> u_trial; _expr2.evaluate_on_side (omega_K, K, sid, u_trial, do_local_component_assembly);
      check_macro(u_trial.rows() == v_test.rows(), "binary: invalid sizes");
      eig_idx_t nq = u_trial.rows();
      eig_idx_t ni = v_test.cols(); 
      eig_idx_t nj = u_trial.cols();
      value.resize(nq,ni,nj);
      for (eig_idx_t q = 0; q < nq; ++q) {
      for (eig_idx_t i = 0; i < ni; ++i) {
      for (eig_idx_t j = 0; j < nj; ++j) {
        value(q,i,j) = _f (u_trial(q,j), v_test(q,i));
      }}}
    }
  }
  // when both args are defined at compile time:
  template<class This, class ValueType,
        class Arg1,        space_constant::valued_type Arg1Tag,
        class Arg2,        space_constant::valued_type Arg2Tag>
  struct evaluate_switch {
    typedef typename This::float_type  float_type;
    typedef typename This::memory_type memory_type;
    void operator() (
        const This&                              obj,
	const geo_basic<float_type,memory_type>& omega_K,
	const geo_element&                       K,
 	Eigen::Tensor<ValueType,3>&              value) const
    {
      obj.template evaluate_internal<ValueType, Arg1, Arg2> (omega_K, K, value);
    }
    void operator() (
        const This&                              obj,
	const geo_basic<float_type,memory_type>& omega_K,
	const geo_element&                       K,
        const side_information_type&             sid,
 	Eigen::Tensor<ValueType,3>&              value) const
    {
      obj.template evaluate_on_side_internal<ValueType, Arg1, Arg2> (omega_K, K, sid, value);
    }
  };
  template<class ValueType>
  void evaluate (
	const geo_basic<float_type,memory_type>& omega_K,
	const geo_element&                       K,
 	Eigen::Tensor<ValueType,3>&              value) const
  {
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,ValueType>::first_argument_type   first_argument_type;
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,ValueType>::second_argument_type second_argument_type;
    static const space_constant::valued_type  first_argument_tag = space_constant::valued_tag_traits<first_argument_type>::value;
    static const space_constant::valued_type second_argument_tag = space_constant::valued_tag_traits<second_argument_type>::value;
    evaluate_switch <self_type, ValueType,
        first_argument_type,   first_argument_tag,
        second_argument_type, second_argument_tag>   eval;
    eval (*this, omega_K, K, value);
  }
  template<class ValueType>
  void evaluate_on_side (
	const geo_basic<float_type,memory_type>& omega_K,
	const geo_element&                       K,
        const side_information_type&             sid,
 	Eigen::Tensor<ValueType,3>&              value) const
  {
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,ValueType>::first_argument_type   first_argument_type;
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,ValueType>::second_argument_type second_argument_type;
    static const space_constant::valued_type  first_argument_tag = space_constant::valued_tag_traits<first_argument_type>::value;
    static const space_constant::valued_type second_argument_tag = space_constant::valued_tag_traits<second_argument_type>::value;
    evaluate_switch <self_type, ValueType,
        first_argument_type,   first_argument_tag,
        second_argument_type, second_argument_tag>   eval;
    eval (*this, omega_K, K, sid, value);
  }
  template<class ValueType>
  bool valued_check() const {
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,ValueType>::first_argument_type   A1;
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,ValueType>::second_argument_type A2;
    if (! is_undeterminated<A1>::value)  _expr1.template valued_check<A1>();
    if (! is_undeterminated<A2>::value)  _expr2.template valued_check<A2>();
    return true;
  }
protected:
// data:
  BinaryFunction  _f;
  Expr1           _expr1;
  Expr2           _expr2;
};
template<class F, class Expr1, class Expr2> struct is_form_expr_v2_variational_arg    <form_expr_v2_variational_binary_field<F,Expr1,Expr2> > : std::true_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 4.2. binary calls
// ---------------------------------------------------------------------------
namespace details {

template<class Expr1, class Expr2, class Sfinae = void>
struct is_form_expr_v2_variational_binary_field : std::false_type {};

template <class Expr1, class Expr2>
struct is_form_expr_v2_variational_binary_field <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
       is_field_expr_v2_variational_arg<Expr1>::value
    && is_field_expr_v2_variational_arg<Expr2>::value
  >::type
>
: and_type<
    is_field_expr_v2_variational_arg<Expr1>
   ,is_field_expr_v2_variational_arg<Expr2>
   ,std::is_same <
      typename Expr1::vf_tag_type
     ,typename dual_vf_tag<typename Expr2::vf_tag_type>::type
    >
  >
{};

} // namespace details

#define _RHEOLEF_form_expr_v2_variational_binary_field(FUNCTION,FUNCTOR)	\
template <class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_v2_variational_binary_field <Expr1,Expr2>::value	\
 ,details::form_expr_v2_variational_binary_field<				\
    FUNCTOR									\
   ,Expr1									\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_v2_variational_binary_field				\
    <FUNCTOR,   Expr1, Expr2>							\
    (FUNCTOR(), expr1, expr2);							\
}

_RHEOLEF_form_expr_v2_variational_binary_field (operator*, details::multiplies)
_RHEOLEF_form_expr_v2_variational_binary_field (dot,       details::dot_)
_RHEOLEF_form_expr_v2_variational_binary_field (ddot,      details::ddot_)
_RHEOLEF_form_expr_v2_variational_binary_field (dddot,     details::dddot_)

#undef _RHEOLEF_form_expr_v2_variational_binary_field 

// ---------------------------------------------------------------------------
// 5. binary operators */ between a variational form and a nonlinear expr
// ---------------------------------------------------------------------------
// 5.1. binary node
// ---------------------------------------------------------------------------
// example: integrate(eta_h*(u*v))

namespace details {

template<class BinaryFunction, class NLExpr, class VFExpr>
class form_expr_v2_variational_binary_binded {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename NLExpr::memory_type,typename VFExpr::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename NLExpr::value_type
         ,typename VFExpr::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename NLExpr::value_type
	 ,typename VFExpr::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type; // TODO: deduce from Exprs
  typedef typename details::bf_vf_tag<BinaryFunction,
	details::vf_tag_00,
	typename VFExpr::vf_tag_type>::type             vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef form_expr_v2_variational_binary_binded <BinaryFunction,NLExpr,VFExpr>         self_type;
  typedef form_expr_v2_variational_binary_binded <BinaryFunction,NLExpr,typename VFExpr::dual_self_type>
                                                        dual_self_type;
  typedef typename VFExpr::maybe_symmetric::type	maybe_symmetric;
  // TODO: symmetry: works only when eta_h is scalar
  // TODO: problem when ddot(eta_h,otimes(u,v)) when eta_h is unsymmetric tensor 
  // and "unsymmetric tensor" is not known at compile time

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;

// alocators:

  form_expr_v2_variational_binary_binded (const BinaryFunction& f, 
		    const NLExpr&    nl_expr,
                    const VFExpr&    vf_expr)
    : _f(f), 
      _nl_expr(nl_expr),
      _vf_expr(vf_expr)
    {}

// accessors:

  const space_type&  get_trial_space() const { return _vf_expr.get_trial_space(); }
  const space_type&  get_test_space()  const { return _vf_expr.get_test_space(); }
  size_type n_derivative() const             { return _vf_expr.n_derivative(); }

// mutable modifiers:

  void initialize (const piola_on_pointset<float_type>& pops, const integrate_option& iopt) { 
    _nl_expr.initialize (pops, iopt);
    _vf_expr.initialize (pops, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const piola_on_pointset<float_type>& pops, const integrate_option& iopt) {  
    _nl_expr.initialize (    pops, iopt);
    _vf_expr.initialize (gh, pops, iopt);
  }
  // ---------------------------------------------
  // element initialize: evaluate nl_expr
  // ---------------------------------------------
  template<class Result>
  void evaluate (
    const geo_basic<float_type,memory_type>&  omega_K, 
    const geo_element&                        K, 
    Eigen::Tensor<Result,3>&                  value) const
  {
    typedef Result Arg1; // TODO: switch
    typedef Result Arg2; // TODO: switch ; is float_type in general, as elementary matrix
    Eigen::Matrix<Arg1,Eigen::Dynamic,1> value1; _nl_expr.evaluate (omega_K, K, value1);
    Eigen::Tensor<float_type,3>          value2; _vf_expr.evaluate (omega_K, K, value2);
    check_macro (value1.size() == value2.dimension(0), "invalid sizes");
    size_type loc_nnod  = value2.dimension(0);
    size_type loc_ndof1 = value2.dimension(1);
    size_type loc_ndof2 = value2.dimension(2);
    value.resize(loc_nnod,loc_ndof1,loc_ndof2);
    for (size_type loc_inod = 0; loc_inod < loc_nnod;  ++loc_inod) {
    for (size_type loc_jdof = 0; loc_jdof < loc_ndof1; ++loc_jdof) {
    for (size_type loc_kdof = 0; loc_kdof < loc_ndof2; ++loc_kdof) {
      value(loc_inod,loc_jdof,loc_kdof)	
	= _f (value1(loc_inod,loc_jdof), value2(loc_inod,loc_jdof,loc_kdof));
    }}}
  }
  template<class Result>
  bool valued_check() const {
    typedef Result A1;
    typedef Result A2;
    bool status = true;
    if (! is_undeterminated<A1>::value)  status &= _nl_expr.template valued_check<A1>();
    if (! is_undeterminated<A2>::value)  status &= _vf_expr.template valued_check<A2>();
    return status;
  }
//protected:
// data:
  BinaryFunction  _f;
  NLExpr          _nl_expr;
  VFExpr          _vf_expr;
};
template<class F, class Expr1, class Expr2> struct is_form_expr_v2_variational_arg    <form_expr_v2_variational_binary_binded<F,Expr1,Expr2> > : std::true_type {};

} // namespace details

// ---------------------------------------------------------------------------
// 5.2. binary calls
// ---------------------------------------------------------------------------
namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_form_expr_v2_variational_binary_multiplies_divides_left : std::false_type {};

template<class Expr1, class Expr2>
struct is_form_expr_v2_variational_binary_multiplies_divides_left <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
         is_field_expr_v2_nonlinear_arg <Expr1>::value
    && ! is_rheolef_arithmetic          <Expr1>::value
    &&   is_form_expr_v2_variational_arg<Expr2>::value
  >::type
>
: std::true_type
{};

template<class Expr1, class Expr2>
struct is_form_expr_v2_variational_binary_multiplies_divides_right
:      is_form_expr_v2_variational_binary_multiplies_divides_left <Expr2,Expr1> {};

} // namespace details

#define _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_left(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_v2_variational_binary_multiplies_divides_left <Expr1,Expr2>::value \
 ,details::form_expr_v2_variational_binary_binded<				\
    FUNCTOR									\
   ,typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr1>::type \
   ,Expr2 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  typedef typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr1>::type wrap1_t; \
  return details::form_expr_v2_variational_binary_binded 			\
	<FUNCTOR,   wrap1_t,        Expr2> 					\
	(FUNCTOR(), wrap1_t(expr1), expr2); 					\
}

#define _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_right(FUNCTION,FUNCTOR) \
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_v2_variational_binary_multiplies_divides_right <Expr1,Expr2>::value \
 ,details::form_expr_v2_variational_binary_binded<				\
    details::swapper<FUNCTOR>							\
   , typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr2>::type \
   ,Expr1 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  typedef typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr2>::type wrap2_t; \
  return details::form_expr_v2_variational_binary_binded 			\
	<details::swapper<FUNCTOR>,            wrap2_t,        Expr1> 		\
	(details::swapper<FUNCTOR>(FUNCTOR()), wrap2_t(expr2), expr1); 		\
}
#define _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides(FUNCTION,FUNCTOR) \
        _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_left  (FUNCTION,FUNCTOR) \
        _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_right (FUNCTION,FUNCTOR)

_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides       (operator*, details::multiplies)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_right (operator/, details::divides)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides       (dot,       details::dot_)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides       (ddot,      details::ddot_)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides       (dddot,     details::dddot_)
#undef _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_left
#undef _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_right
#undef _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides

// ---------------------------------------------------------------------------
// 6. binary operators */ between a variational form and a constant
// ---------------------------------------------------------------------------
namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_form_expr_v2_variational_binary_multiplies_divides_constant_left : std::false_type {};

template<class Expr1, class Expr2>
struct is_form_expr_v2_variational_binary_multiplies_divides_constant_left <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
       is_rheolef_arithmetic          <Expr1>::value
    && is_form_expr_v2_variational_arg<Expr2>::value
  >::type
>
: std::true_type
{};

template<class Expr1, class Expr2>
struct is_form_expr_v2_variational_binary_multiplies_divides_constant_right
:      is_form_expr_v2_variational_binary_multiplies_divides_constant_left <Expr2,Expr1> {};

} // namespace details

#define _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_left(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_v2_variational_binary_multiplies_divides_constant_left <Expr1,Expr2>::value \
 ,details::form_expr_v2_variational_unary<				\
    details::binder_first <FUNCTOR, Expr1> 					\
   ,Expr2 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_v2_variational_unary 				\
	<details::binder_first <FUNCTOR,Expr1>,                    Expr2> 	\
	(details::binder_first <FUNCTOR,Expr1> (FUNCTOR(), expr1), expr2); 	\
}

#define _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_right(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_form_expr_v2_variational_binary_multiplies_divides_constant_right <Expr1,Expr2>::value \
 ,details::form_expr_v2_variational_unary<				\
    details::binder_second <FUNCTOR, Expr2> 					\
   ,Expr1 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::form_expr_v2_variational_unary 				\
	<details::binder_second <FUNCTOR,Expr2>,                    Expr1> 	\
	(details::binder_second <FUNCTOR,Expr2> (FUNCTOR(), expr2), expr1); 	\
}

#define _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant(FUNCTION,FUNCTOR)		\
        _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_left  (FUNCTION,FUNCTOR) 	\
        _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_right (FUNCTION,FUNCTOR)


_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant       (operator*, details::multiplies)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_right (operator/, details::divides)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant       (dot,       details::dot_)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant       (ddot,      details::ddot_)
_RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant       (dddot,     details::dddot_)

#undef _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_right
#undef _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant_left
#undef _RHEOLEF_make_form_expr_v2_variational_binary_operator_multiplies_divides_constant

} // namespace rheolef
#endif // _RHEOLEF_FORM_EXPR_VARIATIONAL_H
