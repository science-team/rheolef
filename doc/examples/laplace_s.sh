#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
DATADIR=${TOP_SRCDIR}/main/tst
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      no yet (skipped)"
#exit 0

status=0

# --------------------------------------------------------
# using direct surface mesh
# --------------------------------------------------------

# geo			err_P1	err_P2
L="
torus_s-5		0.17	0.015
"
# torus-q-5		2e-3	5e-7 TODO

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./laplace_s $DATADIR/$geo-$Pk $Pk 2>/dev/null | RHEOPATH=$DATADIR \$RUN ./laplace_s_error $err >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

exit $status
