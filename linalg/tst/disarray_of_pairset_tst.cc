///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
# include "rheolef/config.h"

#ifndef _RHEOLEF_HAVE_MPI

int main() { return 0; }

#else // _RHEOLEF_HAVE_MPI

#include "rheolef/pair_util.h"
#include "rheolef/mpi_pair_datatype.h"
#include "rheolef/disarray.h"
#include "rheolef/pair_set.h"
#include "rheolef/pretty_name.h"
#include "rheolef/environment.h"
using namespace rheolef;
using std::make_pair;
// ======================================================
int main(int argc, char**argv) {
// ======================================================
    environment rheolef(argc, argv);
    communicator comm;
    if (comm.size() != 3) {
	warning_macro ("expect np=3 processors, find np="<<comm.size());
	return 0;
    }
    size_t n = 3;
    disarray<pair_set<Float> > x(n);
    switch (comm.rank()) {
      case 0: {
	x[0] += make_pair(100, 1.0);
	x.dis_entry(1) += make_pair(101, 1.1);
	x.dis_entry(1) += make_pair(201, 2.1);
	x.dis_entry(1) += make_pair(301, 3.1);
	break;
      }
      case 1: {
	x[0] += make_pair(101, 1.1);
	x.dis_entry(1) += make_pair(200, 2.0);
	break;
      }
      case 2: {
	x[0] += make_pair(102, 1.2);
	x.dis_entry(1) += make_pair(101, 1.1);
	x.dis_entry(1) += make_pair(401, 4.1);
	break;
      }
    }
    x.dis_entry_assembly();
    dout << x.dis_size() << std::endl
         << x;
}
#endif // _RHEOLEF_HAVE_MPI
