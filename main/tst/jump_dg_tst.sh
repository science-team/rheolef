#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATADIR=${SRCDIR}
BINDIR="../bin"
SBINDIR="../sbin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skiped)"
#exit 0

status=0

run "$SBINDIR/mkgeo_grid_1d -v4 -e 10 -region 2>/dev/null | $BINDIR/geo -upgrade - > mesh-1d-r.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_2d -v4 -t 10 -region 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-r.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_3d -v4 -T  4 -region 2>/dev/null | $BINDIR/geo -upgrade - > mesh-3d-r.geo 2>/dev/null"

L="
mesh-1d-r
mesh-2d-r
mesh-3d-r
"
eps="1e-14"
# Pkd, k=0,1,2...
err_list="
    0.06 1e-3
    $eps 2e-3
    $eps 2e-7
    $eps 9e-8
    $eps 7e-12
    $eps 4e-12

    0.07 2e-3
    2e-3 4e-3
    2e-7 3e-7
    8e-8 3e-7
    6e-12 2e-11
    4e-12 3e-11

    0.17 2e-2
    0.04 0.05
    8e-6 7e-5
    1e-5 4e-5
    3e-9 7e-8
    3e-9 5e-8
"

for geo in $L; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for k in 0 1 2 3 4 5; do
    err_jump_valid=`echo $err_list | gawk '{print $1}'`
    err_mass_valid=`echo $err_list | gawk '{print $2}'`
    err_list=`echo $err_list | gawk '{for (i=3; i <= NF; i++) print $i}'`
    loop_mpirun "./jump_dg_tst $geo P${k}d $err_jump_valid $err_mass_valid 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi 
  done
done

run "rm -f mesh-1d-r.geo mesh-2d-r.geo mesh-3d-r.geo"

exit $status
