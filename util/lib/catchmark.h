# ifndef _RHEO_CATCHMARK_H
# define _RHEO_CATCHMARK_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@functionfile catchmark input/output manipulator

Description
===========
The `catchmark` input/output manipulator 
is used for defining labels associated to @ref field_2
when saving them on files:

    	dout << catchmark("u")   << uh
    	     << catchmark("w")   << wh
    	     << catchmark("psi") << psih;

Then, the read is performed similarly:

    	din  >> catchmark("u")   >> uh
    	     >> catchmark("w")   >> wh
    	     >> catchmark("psi") >> psih;

See also @ref diststream_2.

Implementation
==============
@showfromfile
@snippet catchmark.h verbatim_catchmark
@snippet catchmark.h verbatim_catchmark_cont
*/
} // namespace rheolef

#include "rheolef/iorheo.h"
#include "rheolef/rheostream.h"

namespace rheolef { 

// [verbatim_catchmark]
//! @brief see the @ref catchmark_3 page for the full documentation
class catchmark {
    public:
	catchmark(const std::string& x);
	const std::string& mark() const { return _mark; }
	friend std::istream& operator >> (std::istream& is, const catchmark& m);
	friend std::ostream& operator << (std::ostream& os, const catchmark& m);
// [verbatim_catchmark]
    protected:
	std::string _mark;
// [verbatim_catchmark_cont]
};
// [verbatim_catchmark_cont]

// ================== [ inlined ] ==================================================
inline
catchmark::catchmark(const std::string& x)
: _mark(x)
{
}
inline
std::istream&
operator >> (std::istream& is, const catchmark& m)
{
    is >> setmark(m._mark);
    std::string label = "#"+m._mark;
    if (!scatch(is,label)) {
       	warning_macro ("catchmark: label `"<< label <<"' not found on input");
    }
    return is;
}
inline
std::ostream&
operator << (std::ostream& os, const catchmark& m)
{
    os << setmark(m._mark);
    os << "#" << m._mark << std::endl;
    return os;
}
}// namespace rheolef
# endif // _RHEO_CATCHMARK_H
