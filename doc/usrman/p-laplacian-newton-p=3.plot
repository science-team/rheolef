set terminal cairolatex pdf color standalone
set output "p-laplacian-newton-p=3.tex"

set logscale y
set colors classic
set colors classic
set size square 

set xrange [0:25]
set xtics (0,5,10,15,20,25)
set yrange [1e-15:1e5]
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )
set xlabel '[c]{\large $n$}'
set  label '[r]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph -0.01,0.92
set  label '[c]{$p=3$}' at graph 0.5,0.75

plot \
  "p-laplacian-square-opt-p=3.gdat"  \
	u 1:2 title '[r]{fixed point $\omega=2/3$}' w lp lt 1 lc rgb '#008800' lw 2, \
  "p-laplacian-newton-p=3.gdat" \
	u 1:2          title '[r]{Newton}'    w lp lt 1 lc 1 lw 2
#pause -1 "<retour>"
