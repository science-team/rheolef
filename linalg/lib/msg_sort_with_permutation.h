#ifndef _RHEOLEF_MSG_SORT_WITH_PERMUTATION_H
#define _RHEOLEF_MSG_SORT_WITH_PERMUTATION_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
/*
   sort_with_permutation - Computes the permutation of values that gives 
   a sorted sequence.

   Input Parameters:
       v - values to sort. Unchanged
       p - permutation array.  Must be initialized to 0:n-1 on input.
       n - number of values to sort

   Notes:
       inspirated from petsc-2.0.22/sortip.c
       with a bug fix in quicksort as in http://iulib.googlecode.com/hg/colib/quicksort.h
*/

#include "rheolef/compiler.h"
namespace rheolef {

template<class RandomIterator, class SizeRandomIterator, class Size>
void 
quick_sort_with_permutation(
  RandomIterator     v,
  SizeRandomIterator p,
  Size               start,
  Size               end)
{
  if (start + 1 >= end) return;
  typedef typename std::iterator_traits<RandomIterator>::value_type T;
  const T& pivot = v[p[(start+end-1)/2]];
  Size lo = start, hi = end;
  for(;;) {
    while (lo < hi && v[p[lo]]   <  pivot) lo++;
    while (lo < hi && v[p[hi-1]] >= pivot) hi--;
    if (lo == hi || lo+1 == hi) break;
    std::swap (p[lo], p[hi-1]);
    lo++; hi--;
  }
  Size split1 = lo;
  hi = end;
  for(;;) {
    while (lo < hi && v[p[lo]]   == pivot) lo++;
    while (lo < hi && v[p[hi-1]] >  pivot) hi--;
    if (lo == hi || lo+1 == hi) break;
    std::swap (p[lo], p[hi-1]);
    lo++; hi--;
  }
  Size split2 = lo;
  quick_sort_with_permutation (v, p, start,  split1);
  quick_sort_with_permutation (v, p, split2, end);
}
template<class RandomIterator, class SizeRandomIterator, class Size>
void
bubble_sort_with_permutation(
  RandomIterator     v,
  SizeRandomIterator p,
  Size               n)
{
  for (Size k = 0; k < n; k++) {
    Size vk = v [p [k]];
    for (Size j = k+1; j < n; j++) {
      if (vk > v [p [j]]) {
        std::swap (p[k], p[j]);
	vk = v [p [k]];
      }
    }
  }
}
template<class RandomIterator, class SizeRandomIterator, class Size>
void
sort_with_permutation(
  RandomIterator     v,
  SizeRandomIterator p,
  Size               n)
{
  const Size n_qsort_min = 8;
  if (n >= n_qsort_min) {
    quick_sort_with_permutation  (v, p, Size(0), n);
  } else {
    bubble_sort_with_permutation (v, p, n);
  }
}

} // namespace rheolef
#endif // _RHEOLEF_MSG_SORT_WITH_PERMUTATION_H
