///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// P0 approximation
//
#include "basis_symbolic.h"
using namespace rheolef;
using namespace std;
using namespace GiNaC;

class P0_symbolic : public basis_symbolic_nodal
{
public:
    P0_symbolic ();
};
P0_symbolic::P0_symbolic ()
: basis_symbolic_nodal("P0",0)
{
  basis_symbolic_nodal::_family_name = "P";
  basis_symbolic_nodal::set_degree_parameter();
  on('p') << node(0) 
	  << poly (1)
	  << end;
  on('e') << node (0.5)
	  << poly (1)
	  << end;
  on('t') << node (ex(1)/3, ex(1)/3)
          << poly (1)
	  << end;
  on('q') << node (0, 0)
          << poly (1)
	  << end;
  on('T') << node (1./4, 1./4, 1./4)
          << poly (1)
          << end;
  on('P') << node (ex(1)/3, ex(1)/3, 0)
          << poly (1)
          << end;
  on('H') << node (0, 0, 0)
          << poly (1)
          << end;
}
int main (int argc, char **argv) {
	P0_symbolic P0;
	P0.put_cxx_main (argc,argv);
}
