#ifndef _RHEOLEF_SPACE_CONSTANT_H
#define _RHEOLEF_SPACE_CONSTANT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// constants

#include "rheolef/undeterminated.h"
#include "rheolef/field_expr_utilities.h"
#include <type_traits>

// ---------------------------------------------------------------------------
// utility
// ---------------------------------------------------------------------------

namespace rheolef { namespace details {

// is_equal : used by type resolution and by is_symmetric 
template <class T1, class T2> struct is_equal      : std::false_type {};
template <class T>            struct is_equal<T,T> : std::true_type {};

// switch for a constant, or a pointer-to-function or a class-function:
template<class T> struct is_scalar            : std::false_type {};
template<>        struct is_scalar<int>       : std::true_type {};
template<>        struct is_scalar<const int> : std::true_type {};
template<>        struct is_scalar<size_t>    : std::true_type {};
template<>        struct is_scalar<double>    : std::true_type {};
#ifdef _RHEOLEF_HAVE_FLOAT128
template<>        struct is_scalar<float128>  : std::true_type {};
#endif // _RHEOLEF_HAVE_FLOAT128

template<class T> struct is_point                  : std::false_type {};
template<class T> struct is_point<point_basic<T> > : std::true_type {};

template<class T> struct is_tensor                   : std::false_type {};
template<class T> struct is_tensor<tensor_basic<T> > : std::true_type {};

template<class T> struct is_tensor3                    : std::false_type {};
template<class T> struct is_tensor3<tensor3_basic<T> > : std::true_type {};

template<class T> struct is_tensor4                    : std::false_type {};
template<class T> struct is_tensor4<tensor4_basic<T> > : std::true_type {};

} // namespace details 
// ---------------------------------------------------------------------------
// argument promote, for template expression (binders, etc)
// ---------------------------------------------------------------------------
namespace details {
template <class T1, class T2, class Sfinae = void>
struct field_promote_first_argument:  promote<T1,T2> {};
template <class T1, class T2, class Sfinae = void>
struct field_promote_second_argument: promote<T1,T2> {};

#define _RHEOLEF_field_promote_argument(tensor)						\
template<class T1, class T2>							\
struct field_promote_first_argument<tensor##_basic<T1>, T2,				\
  typename std::enable_if<							\
	is_rheolef_arithmetic<T1>::value					\
     &&	is_rheolef_arithmetic<T2>::value					\
  >::type									\
> {										\
	using type = tensor##_basic<typename promote<T1,T2>::type>; };		\
template <class T1, class T2>							\
struct field_promote_first_argument<T1,tensor##_basic<T2>,				\
  typename std::enable_if<							\
	is_rheolef_arithmetic<T1>::value					\
     &&	is_rheolef_arithmetic<T2>::value					\
  >::type									\
> {										\
	using type = typename promote<T1,T2>::type; };				\
template <class T1, class T2>							\
struct field_promote_second_argument<tensor##_basic<T1>, T2,				\
  typename std::enable_if<							\
	is_rheolef_arithmetic<T1>::value					\
     &&	is_rheolef_arithmetic<T2>::value					\
  >::type									\
> {										\
	using type = typename promote<T1,T2>::type; };				\
template <class T1, class T2>							\
struct field_promote_second_argument<T1,tensor##_basic<T2>,				\
  typename std::enable_if<							\
	is_rheolef_arithmetic<T1>::value					\
     &&	is_rheolef_arithmetic<T2>::value					\
  >::type									\
> {										\
	using type = tensor##_basic<typename promote<T1,T2>::type>; };

_RHEOLEF_field_promote_argument(point)
_RHEOLEF_field_promote_argument(tensor)
_RHEOLEF_field_promote_argument(tensor3)
_RHEOLEF_field_promote_argument(tensor4)
#undef _RHEOLEF_field_promote_argument

} // namespace details 
// -------------------------------------------------------------
// coordinate system helper
// -------------------------------------------------------------
namespace space_constant {

typedef size_t size_type;

typedef enum {
        cartesian       = 0,
        axisymmetric_rz = 1,
        axisymmetric_zr = 2,
        last_coord_sys  = 3
} coordinate_type;

coordinate_type coordinate_system      (std::string sys_coord);
std::string     coordinate_system_name (coordinate_type);
void check_coord_sys_and_dimension     (coordinate_type, size_type d);

// -------------------------------------------------------------
// multi-component field support
// -------------------------------------------------------------
typedef enum {
	scalar             = 0,
	vector             = 1,
	tensor             = 2, // symmetric, D_ij
        unsymmetric_tensor = 3,
        tensor3            = 4, // unsymmetric, G_ijk
        tensor4            = 5, // symmetric, A_ijkl
	mixed              = 6,
	last_valued        = 7
} valued_type;

const std::string& valued_name (valued_type tag);
valued_type valued_tag  (const std::string& name);

size_type n_component (
    valued_type        valued_tag,
    size_type          d,
    coordinate_type    sys_coord);

size_type n_component (
    const std::string& valued,
    size_type          d,
    coordinate_type    sys_coord);

// convert a type to the enum valued_tag:
// 	size_t tag = valued_tag_traits<T>::value;
template<class T> struct valued_tag_traits                           { static const valued_type value = scalar; };
template<class T> struct valued_tag_traits<point_basic<T> >          { static const valued_type value = vector; };
template<class T> struct valued_tag_traits<tensor_basic<T> >         { static const valued_type value = tensor; };
template<class T> struct valued_tag_traits<tensor3_basic<T> >        { static const valued_type value = tensor3; };
template<class T> struct valued_tag_traits<tensor4_basic<T> >        { static const valued_type value = tensor4; };
template<class T> struct valued_tag_traits<undeterminated_basic<T> > { static const valued_type value = last_valued; };

// convert an enum valued_tag to the type based on T:
// 	typedef typename valued_type_traits<tag,T>::type valued_t;
//
template<int Tag, class T> struct valued_type_traits                { typedef undeterminated_basic<T> type; };
template<class T>          struct valued_type_traits<scalar,T>      { typedef T                       type; };
template<class T>          struct valued_type_traits<vector,T>      { typedef point_basic<T>          type; };
template<class T>          struct valued_type_traits<tensor,T>      { typedef tensor_basic<T>         type; };
template<class T>          struct valued_type_traits<tensor3,T>     { typedef tensor3_basic<T>        type; };
template<class T>          struct valued_type_traits<tensor4,T>     { typedef tensor4_basic<T>        type; };
template<class T>          struct valued_type_traits<last_valued,T> { typedef undeterminated_basic<T> type; };

// tensorial up and down helpers (for grad(expr))
template<class T> struct rank_down                    { typedef undeterminated_basic<typename scalar_traits<T>::type>  type; };
template<class T> struct rank_down<point_basic<T> >   { typedef T                   type; };
template<class T> struct rank_down<tensor_basic<T> >  { typedef point_basic<T>      type; };
template<class T> struct rank_down<tensor3_basic<T> > { typedef tensor_basic<T>     type; };
template<class T> struct rank_down<tensor4_basic<T> > { typedef tensor3_basic<T>    type; };

template<class T> struct rank_up                           { typedef point_basic<typename scalar_traits<T>::type>  type; };
template<class T> struct rank_up<point_basic<T> >          { typedef tensor_basic<T> type; };
template<class T> struct rank_up<tensor_basic<T> >         { typedef tensor3_basic<T> type; };
template<class T> struct rank_up<tensor3_basic<T> >        { typedef tensor4_basic<T> type; };
template<class T> struct rank_up<undeterminated_basic<T> > { typedef undeterminated_basic<typename scalar_traits<T>::type>  type; };

template<class T>
T contract_product (const T& a, const T& b) { return a*b; }
template<class T>
T contract_product (const point_basic<T>& a, const point_basic<T>& b) { return dot(a,b); }
template<class T>
T contract_product (const tensor_basic<T>& a, const tensor_basic<T>& b) { return ddot(a,b); }

// -------------------------------------------------------------
// 2-tensor support
// -------------------------------------------------------------
size_type tensor_index (
    valued_type       valued_tag,
    coordinate_type   sys_coord,
    size_type         i,
    size_type         j);

size_type tensor_index (
    std::string      valued,
    std::string      sys_coord, 
    size_type   i, 
    size_type   j);

std::pair<size_type,size_type>
tensor_subscript (
    valued_type       valued_tag,
    coordinate_type   sys_coord,
    size_type         i_comp);

std::string
tensor_subscript_name (
    valued_type       valued_tag,
    coordinate_type   sys_coord,
    size_type         i_comp);

std::pair<size_type,size_type>
tensor_subscript (
    std::string     valued,
    std::string     sys_coord,
    size_type       i_comp);

std::string
tensor_subscript_name (
    std::string     valued,
    std::string     sys_coord,
    size_type       i_comp);

// -------------------------------------------------------------
// 4-tensor support
// -------------------------------------------------------------
size_type
tensor4_index (
    valued_type       valued,
    coordinate_type   sys_coord,
    size_type         i,
    size_type         j,
    size_type         k,
    size_type         l);

size_type
tensor4_index (
    std::string valued,
    std::string sys_coord, 
    size_type   i,
    size_type   j,
    size_type   k,
    size_type   l);

std::pair<std::pair<size_type,size_type>, std::pair<size_type,size_type> >
tensor4_subscript (
    valued_type       valued,
    coordinate_type   sys_coord,
    size_type         i_comp);

std::string
tensor4_subscript_name (
    valued_type       valued,
    coordinate_type   sys_coord,
    size_type         i_comp);

std::pair<std::pair<size_type,size_type>, std::pair<size_type,size_type> >
tensor4_subscript (
    std::string     valued,
    std::string     sys_coord,
    size_type       i_comp);

std::string
tensor4_subscript_name (
    std::string     valued,
    std::string     sys_coord,
    size_type       i_comp);

// -------------------------------------------------------------
// field*field & field/field valued_type computed at run time
// -------------------------------------------------------------
valued_type multiplies_result_tag (valued_type tag1, valued_type tag2);
valued_type    divides_result_tag (valued_type tag1, valued_type tag2);


} // namespace space_constant
// --------------------------------------------------------------------------
// utility to determine whether a template arg is a function or a constant
// --------------------------------------------------------------------------
namespace details {
  // build a function that returns a constant
  template<class T1, class T2>
  struct f_constant {
    T2 operator() (const T1& x) const { return c; }
    f_constant (const T2& c0) : c(c0) {}
    const T2 c;
  };
  template<class F> struct is_vector_function :
    and_type <
        std::is_class<F>
       ,is_point<typename F::result_type>
    > {};
  template<class T> struct is_vector_function<point_basic<T> (const point_basic<T>)>
  	: std::true_type {};

  template<class E> class field_nonlinear_expr;
  template<class E> struct is_expr                           : std::false_type {};
  template<class E> struct is_expr<field_nonlinear_expr<E> > : std::true_type {};

  template<class C>
  struct is_constant :
        or_type <
          is_scalar<C>
         ,is_point<C>
        > {};

  template<class F>
  struct is_function :
    and_type <
      or_type <
        std::is_class<F>
       ,std::is_pointer<F>
       ,std::is_function<F>
      >
     ,not_type <
        or_type <
          is_scalar<F>
         ,is_point<F>
         ,is_expr<F>
        >
      >
    >
  {};
  template<class F>
  struct result_type {
    typedef typename function_traits<F>::result_type type;
  };
  template<class T, class R>
  struct result_type<R(const point_basic<T>&)> {
    typedef R type;
  };
  template<class T, class R>
  struct result_type<R (*)(const point_basic<T>&)> {
    typedef R type;
  };
  template<class Constant>
  struct constant_promote { typedef Constant type; };
  template<>        struct constant_promote<int>    { typedef Float type; };
  template<>        struct constant_promote<size_t> { typedef Float type; };
#ifdef _RHEOLEF_HAVE_FLOAT128
  template<>        struct constant_promote<double> { typedef Float type; };
#endif // _RHEOLEF_HAVE_FLOAT128

// constants in field expressions
template <class Value>
struct is_field_expr_v2_constant
: std::disjunction<
     is_rheolef_arithmetic<Value>
    ,is_point<Value>
    ,is_tensor<Value>
    ,is_tensor3<Value>
    ,is_tensor4<Value>
  >
{};

}} // namespace rheolef::details
#endif // _RHEOLEF_SPACE_CONSTITUTION_H
