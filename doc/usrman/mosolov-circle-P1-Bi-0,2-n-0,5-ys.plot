set terminal cairolatex pdf color standalone
set output "mosolov-circle-P1-Bi-0,2-n-0,5-ys.tex"

r0=0.2
rmax=0.21
set size ratio -1  # equal scales
set colors classic
set xrange [-rmax:rmax]
set yrange [-rmax:rmax]
set key left Right at graph 1,.9
set parametric
set noxtics
set noytics
set noborder
plot \
"mosolov-circle-10-P1-Bi-0,2-n-0,5-ys.gdat" \
  title '[r]{$h=1/10$}' \
  with l lc 1 lw 3, \
"mosolov-circle-20-P1-Bi-0,2-n-0,5-ys.gdat" \
  title '[r]{$h=1/20$}' \
  with l lc rgb '#008800' lw 3, \
"mosolov-circle-40-P1-Bi-0,2-n-0,5-ys.gdat" \
  title '[r]{$h=1/40$}' \
  with l lc 3 lw 4, \
r0*cos(t), r0*sin(t) \
  t 'exact' \
  w l lc 0 lw 4

#pause -1
