set terminal cairolatex pdf color standalone
set output "mosolov-sector-P2-Bi-0-5-n-1-geo-adapt-loop-hmin.tex"

set size square
set colors classic
set log y 
set xlabel '[c]{adaptation iteration}'
set  label '[l]{$h_\textrm{min}$}' at graph 0.05, 0.93
set xtics 5
set yrange [1e-5:1e-1]
set ytics add ( \
	'[r]{$10^{-1}$}' 1e-1, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$10^{-5}$}' 1e-5)
plot 'mosolov-sector-P2-Bi-0.5-n-1-geo-adapt-loop.gdat' u 1:3 notitle w lp lc 0 lw 4
#pause -1
