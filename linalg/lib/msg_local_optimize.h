#ifndef RHEO_MSG_LOCAL_OPTIMIZE_H
#define RHEO_MSG_LOCAL_OPTIMIZE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

# include "rheolef/msg_util.h"
namespace rheolef {

/*F:
NAME: msg_local_optimize -- local scatter optimize (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Optimize local exchanges during gatter/scatter.
ALGORITHM:
  msg_local_optimize
  
  "input": the send and receive local contexts (to, from)
  |   to_loc_idx(0:n_local-1), from_loc_idy(0:n_local-1)
  "output": the boolean, true when optimization is possible
  |   has_opt
  begin
  |     if n_local = 0 then
  |       has_opt := false
  |     else
  |       to_start   := to_loc_idx(0)
  |       from_start := from_loc_idy(0)
  |       has_opt := true
  |       i := 1
  |       while i < n_local and has_opt do
  |         to_start   := to_start + 1
  |         from_start := from_start + 1
  |         if to_loc_idx(i)   <> to_start 
  |         or from_loc_idy(i) <> from_start then
  |           has_opt := false
  |         endif
  |         i := i + 1
  |       endwhile
  |     endif
  end

COMPLEXITY:
  Memory and time complexity is O(receive_total_size).
SEE ALSO: "msg_to_context"(5)

METHODS: @msg_local_optimize 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   23 march 1999
END:
*/

//<msg_local_optimize:
template <
    class InputIterator1,
    class InputIterator2>
bool
msg_local_optimize (
    InputIterator1 		to_loc_idx,	// n_local
    InputIterator1 		last_to_loc_idx,	
    InputIterator2 		from_loc_idy)	// n_local
{
    typedef typename std::iterator_traits<InputIterator1>::value_type Size;
    if (to_loc_idx == last_to_loc_idx) {
        return false;
    }
    Size to_start   = *to_loc_idx++;
    Size from_start = *from_loc_idy++;
    bool has_opt = true;
    while (to_loc_idx != last_to_loc_idx && has_opt) {
	to_start++;
	from_start++;
	if ((*to_loc_idx++)   != to_start ||
	    (*from_loc_idy++) != from_start) {
	    has_opt = false;
	}
    }
    return has_opt;
}
//>msg_local_optimize:
} // namespace rheolef
#endif // RHEO_MSG_LOCAL_OPTIMIZE_H
