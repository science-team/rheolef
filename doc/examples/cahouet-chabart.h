///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile cahouet-chabart.h The Cahouet-Chabart preconditioner for the Navier-Stokes equations
#include "neumann-laplace-assembly.h"
struct cahouet_chabart {
  cahouet_chabart (const space& Qh, Float lambda_1)
    : fact_m(), fact_c(), lambda(lambda_1) {
    form m (Qh, Qh, "mass");
    fact_m = ldlt(m.uu);
    trial p (Qh); test q (Qh);
    form a = integrate (dot(grad(p),grad(q)));
    form_diag dm (Qh, "mass");
    field mh (dm);
    csr<Float> c = neumann_laplace_assembly (a.uu, mh.u);
    fact_c = ldlt(c);
  }
  vec<Float> solve (const vec<Float>& Mp) const {
    vec<Float> q1 = fact_m.solve(Mp);
    vec<Float> Mp_e (Mp.size()+1);
    for (size_t i = 0; i < Mp.size(); i++) Mp_e.at(i) = Mp.at(i);
    Mp_e.at(Mp.size()) = 0;
    vec<Float> q2_e = fact_c.solve(Mp_e);
    vec<Float> q2 (Mp.size());
    for (size_t i = 0; i < q2.size(); i++) q2.at(i) = q2_e.at(i);
    vec<Float> q = q1 + lambda*q2;
    return q;
  }
  ssk<Float> fact_m;
  ssk<Float> fact_c;
  Float lambda;
};
