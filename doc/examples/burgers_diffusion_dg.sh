#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skipped)"
#exit 0

# epsilon=1 k=1 h=2/400 nmax_t=100
# 	p=1	p=2	p=3
L="
	2e-2	3e-4	3e-5
"

status=0

run "$SBINDIR/mkgeo_grid_1d -v4 -e 400 -a -1 -b 1 2>/dev/null | $BINDIR/geo -upgrade - > tmp.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

for p in 1 2 3; do
  err_linf_l2_P1d=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  loop_mpirun "./burgers_diffusion_dg tmp P1d 0.1 100 1 $p 2>/dev/null | ./burgers_diffusion_error $err_linf_l2_P1d >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

run "rm -f tmp.geo"

exit $status
