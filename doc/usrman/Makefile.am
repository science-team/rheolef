## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ${top_builddir}/config/config.mk

docdir = $(prefix)/share/doc/@doc_dir@
exampledir = $(docdir)/examples

# -----------------------------------------------------------------------------
# the file set
# -----------------------------------------------------------------------------

MAIN_TEX = 							\
	rheolef.tex			

TEX_INCLUDED =							\
	rheolef.sty						\
	notations.tex						\
	fdl.tex							\
	rheolef.tex						\
	introduction.tex					\
	dirichlet.tex						\
	dirichlet-nh.tex					\
	neumann-nh.tex						\
	robin.tex						\
	dirichlet-nh-error.tex					\
	neumann-laplace.tex					\
	surface.tex						\
	transmission.tex					\
	green.tex						\
	elasticity.tex						\
	stokes.tex						\
	incompressible-elasticity.tex				\
	stabilized-stokes.tex					\
	slip.tex						\
	axisymmetric.tex					\
	heat.tex						\
	navier-stokes.tex					\
	convect.tex						\
	p_laplacian.tex						\
	continuation.tex					\
	transport_dg.tex					\
	zalesak_dg.tex						\
	leveque_dg.tex						\
	hyperbolic_nonlinear_dg.tex				\
	limiter.tex						\
	burgers_dg.tex						\
	dirichlet_dg.tex					\
	neumann_dg.tex						\
	transmission_dg.tex					\
	burgers_diffusion.tex					\
	elasticity_dg.tex					\
	stokes_dg.tex						\
	navier_stokes_statio_dg.tex				\
	dirichlet_hdg.tex					\
	dirichlet_hho.tex					\
	convect_hdg.tex						\
	hyperbolic_nonlinear_hdg.tex				\
	transport_hdg.tex					\
	introduction-cfma.tex					\
	yield_slip.tex						\
	mosolov.tex						\
	transport_tensor_dg.tex					\
	diffusion_transport_tensor_dg.tex			\
	oldroyd.tex						\
	raviart_thomas.tex					\
	how-to-mesh.tex						\
	how-to-mesh-bamg.tex					\
	how-to-mesh-gmsh.tex					\
	migrate-to-v6.tex					

PNG_SRC =							\
	lunettes-stereo.png					\
	dirichlet-2d-P1-gnuplot.png				\
	dirichlet-2d-P1-paraview.png				\
	dirichlet-2d-elevation-gnuplot.png			\
	dirichlet-2d-elevation.png				\
	red-cyan-glasses.png					\
	cube-3d-stereo-fig.png					\
	dirichlet-3d-mayavi-mix-fig.png				\
	embankment-3d-fig.png					\
	embankment-3d-fill-fig.png				\
	embankment-2d-tensor-fig.png				\
	embankment-3d-tensor-fig.png				\
	embankment-2d-s01-P1d-elevation-fig.png			\
	embankment-3d-s01-P0-fig.png           			\
	embankment-3d-s01-P1d-fig.png 				\
	embankment-adapt-2d-P1-fig.png				\
	embankment-adapt-2d-P2-fig.png				\
	stokes-cavity-2d-fig.png				\
	stokes-cavity-3d-fig.png				\
	vorticity-cavity-2d-fig.png				\
	vorticity-cavity-3d-fig.png				\
	streamf-cavity-3d-fig.png				\
	incompressible-elasticity-cube-fig.png			\
	contraction-bubble-u.png				\
	cube-gmsh-geo.png					\
	contraction-cmp-P2-psi.png				\
	contraction-zr-P2-psi.png				\
	heat-fig.png						\
	convect-fig.png						\
	p-laplacian-square-p=1,15-elevation.png			\
	p-laplacian-square-p=1,25-elevation.png			\
	p-laplacian-square-p=2-elevation.png			\
	p-laplacian-square-p=2,5-elevation.png			\
	p-laplacian-square-p=7-elevation.png			\
	sphere_s_P1_geo.png					\
	sphere_s_P1_field.png					\
	sphere_s_P3_field.png					\
	torus_s_P1_geo.png					\
	torus_s_P1_field.png					\
	torus_s_P2_field.png					\
	level-set-sphere-geo.png				\
	level-set-torus-geo.png					\
	level-set-circle-field.png				\
	level-set-sphere-field.png				\
	level-set-torus-field.png				\
	banded-level-set-circle-geo.png				\
	banded-level-set-sphere-geo.png				\
	banded-level-set-torus-geo.png				\
	banded-level-set-circle-field.png			\
	banded-level-set-sphere-field.png			\
	banded-level-set-torus-field.png			\
	zalesak-100-P2d-6000.jpg				\
	zalesak-100-P2d-6000-zoom.jpg				\
	zalesak-100-P2d-5000-elevation.jpg			\
	leveque-100-P2d-6000-half-subdiv3.jpg			\
	leveque-100-P2d-6000-last.jpg				\
	yield-slip-50-n-0,5-S-0,6.png				\
	yield-slip-50-n-1,5-S-0,6.png				\
	yield-slip-50-n-1-S-0,6.png				\
	mosolov-square-30-Bi-0,4-n-1.png			\
	mosolov-sector-P2-Bi-0.5-n-1.png			\
	mosolov-sector-P2-Bi-0.5-n-1-zoom.png			\
	oldroyd-cavity-10-We-0,2-alpha-0,8888-a-1-lambda-min.png \
	oldroyd-cavity-20-We-0,2-alpha-0,8888-a-1-lambda-min.png \
	oldroyd-cavity-40-We-0,2-alpha-0,8888-a-1-lambda-min.png \
	oldroyd-contraction3-zr-We-0.png			\
	oldroyd-contraction3-zr-We-0.1.png			\
	oldroyd-contraction3-zr-We-0.3.png			\
	oldroyd-contraction3-zr-We-0.5.png			\
	oldroyd-contraction3-zr-We-0.7.png			\
	obstacle-slip-streamf.jpg				\
	obstacle-zr-slip-streamf.jpg

PDF_SRC =							\
	dirichlet-2d-bw-gnuplot.pdf				\
	square-ut.pdf						\
	square-P3-disto.pdf					\
	ball-t-P1.pdf						\
	ball-t-P4.pdf						\
	ball-q-P1.pdf						\
	ball-q-P3.pdf						\
	square-region-fig.pdf					\
	embankment-2d-fig.pdf					\
	embankment-2d-fill-fig.pdf				\
	embankment-2d-s01-P0-fig.pdf 				\
	streamf-cavity-2d-fig.pdf				\
	incompressible-elasticity-square-fig.pdf		\
	contraction-geo.pdf					\
	contraction-bubble-psi.pdf				\
	contraction-P2-psi.pdf					\
	square-gmsh-geo.pdf					\
	navier-stokes-Re=100-geo.pdf				\
	navier-stokes-Re=100-psi.pdf				\
	navier-stokes-Re=400-geo.pdf				\
	navier-stokes-Re=400-psi.pdf				\
	navier-stokes-Re=1000-geo.pdf				\
	navier-stokes-Re=1000-psi.pdf				\
	sphere_s_P3_geo.pdf					\
	torus_s_P2_geo.pdf					\
	level-set-circle-geo.pdf				\
	banded-level-set-cc-geo.pdf				\
	navier-stokes-dg-cavity-80-P1d-Re=25000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=20000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=15000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=10000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=7500-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=5000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=3200-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=2000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=1000-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=400-upw-psi.pdf	\
	navier-stokes-dg-cavity-80-P1d-Re=0-upw-psi.pdf		\
	mosolov-augmented-lagrangian-square-P1-Bi-0,4-cvg.pdf	\
	oldroyd-cavity-40-We-0,2-alpha-0,8888-a-1-psi.pdf	

FIGSRC =                                                        \
        square-cube-fig.fig					\
        synthese-fig.fig					\
	upwinding-triangle-2-fig.fig				\
        limiter-fig.fig						\
	contraction-fig.fig					\
	fig-obstacle-slip-quarter.fig				\
	fig-obstacle-slip-half.fig

SVGSRC = 							\
	zalesak-h-1-P2d.svg					\
	zalesak-h-2-P1d.svg					\
	zalesak-h-2-P2d.svg					\
	zalesak-h-2-P3d.svg					\
	zalesak-h-3-P2d.svg

PLOTSRC =							\
	cvge-dirichlet-nh-l2.plot				\
	cvge-dirichlet-nh-linf.plot				\
	cvge-dirichlet-nh-h1.plot				\
	cvge-dirichlet-nh-disto-l2.plot				\
	cvge-dirichlet-nh-disto-linf.plot			\
	cvge-dirichlet-nh-disto-h1.plot				\
	cvge-dirichlet-nh-ball-t-l2.plot			\
	cvge-dirichlet-nh-ball-t-linf.plot			\
	cvge-dirichlet-nh-ball-t-h1.plot			\
	cvge-dirichlet-nh-ball-q-l2.plot			\
	cvge-dirichlet-nh-ball-q-linf.plot			\
	cvge-dirichlet-nh-ball-q-h1.plot			\
	cvge-helmholtz-s-sphere-l2.plot				\
	cvge-helmholtz-s-sphere-linf.plot			\
	cvge-helmholtz-s-sphere-h1.plot				\
	speedup-2d-assembly.plot				\
	speedup-3d-assembly.plot				\
	speedup-2d-solve-direct.plot				\
	speedup-3d-solve-direct.plot				\
	speedup-2d-solve-iter.plot				\
	speedup-3d-solve-iter.plot				\
	timing_cmp_direct_iter_2d.plot				\
	timing_cmp_direct_iter_3d.plot				\
	transmission-1d-fig.plot				\
	transmission-dg-1d-fig.plot				\
	contraction-zr-P2-cut.plot				\
	contraction-zr-P1-tau22-cut.plot			\
	convect_cvge_Pk_err_l2_l2.plot				\
	convect_cvge_Pk_err_linf_linf.plot			\
	navier-stokes-cut-u0.plot				\
	navier-stokes-cut-u1.plot				\
	p-laplacian-fixed-point-p=1,5-res.plot 			\
	p-laplacian-fixed-point-p=1,5-Pk.plot 			\
	p-laplacian-square-r1.plot 				\
	p-laplacian-square-r2.plot 				\
	p-laplacian-rate.plot 					\
	p-laplacian-rate-log.plot 				\
	p-laplacian-relax-1.plot 				\
	p-laplacian-relax-2.plot 				\
	p-laplacian-relax-opt.plot 				\
	p-laplacian-relax-opt-rate.plot 			\
	p-laplacian-newton-p=1,5-n.plot 			\
	p-laplacian-newton-p=3.plot 				\
	p-laplacian-newton-square-r2.plot 			\
	p-laplacian-square-newton-p=3-n.plot 			\
	p-laplacian-square-newton-p=3-Pk.plot 			\
	p-laplacian-damped-newton-p=1,5-n.plot 			\
	p-laplacian-damped-newton-p=1,5-Pk.plot 		\
	p-laplacian-damped-newton-n=50-P1-p1.plot 		\
	p-laplacian-damped-newton-n=50-P1-p2.plot		\
	p-laplacian-fixed-point-p=1,5-err-w1p.plot		\
	p-laplacian-fixed-point-p=1,5-err-lp.plot		\
	p-laplacian-fixed-point-p=1,5-err-linf.plot		\
	transport_dg-cv-l2-2d-t-grid.plot			\
	transport_dg-cv-linf-2d-t-grid.plot			\
	transport_dg-1d-elevation.plot				\
	transport_dg-1d-elevation2.plot				\
	zalesak-error-l1.plot					\
	zalesak-error-l1-circle.plot				\
	zalesak-error-l1-P1d-time.plot				\
	zalesak-error-l1-P2d-time.plot				\
	square-100-P2d-nmax-error-mass-time.plot		\
	zalesak-error-mass-P1d.plot				\
	zalesak-error-mass-P2d.plot				\
	zalesak-error-mass-P3d.plot				\
	zalesak-error-mass-P4d.plot				\
	zalesak-100-P2d-5000-equispaced-error.plot		\
	zalesak-100-P2d-5000-equispaced-mass.plot		\
	zalesak-25-P2d-96000-adapt-error-subdivide.plot \
	zalesak-25-P3d-96000-adapt-error-subdivide.plot \
	zalesak-25-P4d-96000-adapt-error-subdivide.plot \
	zalesak-25-P5d-96000-adapt-error-subdivide.plot \
	harten-anim-t-0.plot					\
	harten-anim-t-0,5.plot					\
	harten-anim-tcrit.plot					\
	harten-anim-t-0,75.plot					\
	harten-anim-t-1.plot					\
	harten-anim-t-1,75.plot					\
	harten-Pk-err-before-choc.plot				\
	harten-Pk-err.plot					\
	dirichlet_dg-1d-elevation.plot				\
	dirichlet_dg-1d-elevation2.plot				\
	dirichlet_dg-cv-l2-2d-t-grid.plot			\
	dirichlet_dg-cv-linf-2d-t-grid.plot			\
	dirichlet_dg-cv-h1-2d-t-grid.plot			\
	burgers_diffusion_exact.plot				\
	burgers_diffusion_semiimplicit_error-h-50-dt.plot	\
	burgers_diffusion_rk_semiimplicit_error-dt.plot		\
	burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3.plot \
	burgers-diffusion-rk-semiimplicit-limiter-line2-400-eps-1e-3.plot \
	navier-stokes-dg-cavity-40-P1d-residu.plot \
	dirichlet-hdg-n-00-error-l2-t.plot \
	dirichlet-hdg-n-p1-error-l2-t.plot \
	dirichlet-hdg-n-m1-error-l2-t.plot \
	dirichlet-hdg-n-00-error-sigma-l2-t.plot \
	dirichlet-hdg-n-p1-error-sigma-l2-t.plot \
	dirichlet-hdg-n-m1-error-sigma-l2-t.plot \
	dirichlet-hdg-n-00-error-lambda-l2-t.plot \
	dirichlet-hdg-n-p1-error-lambda-l2-t.plot \
	dirichlet-hdg-n-m1-error-lambda-l2-t.plot \
	dirichlet-hdg-n-00-average-error-l2-t.plot \
	dirichlet-hdg-n-p1-average-error-l2-t.plot \
	dirichlet-hdg-n-m1-average-error-l2-t.plot \
	dirichlet-hdg-n-00-post-error-us-l2-t.plot \
	dirichlet-hdg-n-p1-post-error-us-l2-t.plot \
	dirichlet-hdg-n-m1-post-error-us-l2-t.plot \
	dirichlet-hdg-n-00-post-error-sigmat-hdiv-t.plot \
	dirichlet-hdg-n-p1-post-error-sigmat-hdiv-t.plot \
	dirichlet-hdg-n-m1-post-error-sigmat-hdiv-t.plot \
	reconstruction-hho-error-l2-t.plot \
	reconstruction-hho-error-h1-t.plot \
	dirichlet-hho-k-k-t-us-l2.plot	\
	dirichlet-hho-k-k-t-us-h1.plot	\
	dirichlet-hho-k-k-t-us-cpu.plot \
	combustion-a-0,5.plot \
	combustion-ac.plot \
	combustion_umax.plot \
	lambda_c_cv.plot \
	combustion_error_h1.plot \
	combustion_det_P1.plot \
	combustion_arc_umax.plot \
	combustion_upper.plot \
	yield-slip-al-n-1-S-0,6-cvg.plot \
	yield-slip-50-S-0,6-n-cut0.plot \
	yield-slip-50-S-0,6-n-cut.plot \
	yield-slip-50-S-0,6-n-lambda.plot \
	yield-slip-n-0,5-S-0,6.plot \
	yield-slip-n-0,9-S-0,6.plot \
	yield-slip-n-1-S-0,6.plot \
	yield-slip-n-1,5-S-0,6.plot \
	yield-slip-circle-S-0,25-n-0,5-err-l2.plot \
	yield-slip-circle-S-0,25-n-0,5-err-linf.plot \
	yield-slip-circle-S-0,25-n-0,5-err-h1.plot \
	yield-slip-circle-S-0,25-n-0,5-err-lambda.plot  \
	mosolov-square-Bi-0,4-cut-bisector.plot \
	mosolov-circle-Bi-0,2-n-0,5-error-u-h1.plot \
	mosolov-circle-Bi-0,2-n-0,5-error-u-l2.plot \
	mosolov-circle-Bi-0,2-n-0,5-error-u-linf.plot \
	mosolov-circle-Bi-0,2-n-0,5-error-sigma-l2.plot \
	mosolov-circle-Bi-0,2-n-0,5-error-sigma-linf.plot \
	mosolov-circle-Bi-0,2-n-0,5-error-ys.plot \
	mosolov-circle-P1-Bi-0,2-n-0,5-ys.plot \
	mosolov-sector-P2-Bi-0-5-n-1-cut-bisector.plot	\
	mosolov-sector-P2-Bi-0-5-n-1-geo-adapt-loop-size.plot \
	mosolov-sector-P2-Bi-0-5-n-1-geo-adapt-loop-hmin.plot \
	transport-tensor-err.plot \
	oldroyd-cavity-We-0,2-alpha-0,8888-a-1-cut-tau-log.plot \
	oldroyd-cavity-We-0,2-alpha-0,8888-a-1-cut-tau.plot \
	oldroyd-cavity-We-0,2-alpha-0,8888-a-1-cut-u.plot \
	oldroyd-contraction-zr-psi-max.plot \
	oldroyd-contraction3-zr-u0-axis.plot \
	oldroyd-contraction3-zr-tau00-axis.plot \
	oldroyd-contraction3-zr-tau11-axis.plot \
	oldroyd-contraction3-zr-tau22-axis.plot \
	commute-rtd-t-p-err-l2.plot \
	commute-rtd-t-i-err-l2.plot \
	commute-rtd-t-p-err-div-l2.plot \
	commute-rtd-t-i-err-div-l2.plot \
	obstacle-slip-ux-xaxis.plot \
	obstacle-slip-ux-yaxis.plot \
	diffusion-tensor-err.plot \
	diffusion-transport-tensor-err.plot

PLOTDATA =							\
	cvge-dirichlet-nh-P1-ut.gdat				\
	cvge-dirichlet-nh-P2-ut.gdat				\
	cvge-dirichlet-nh-P3-ut.gdat				\
	cvge-dirichlet-nh-disto-P1-t.gdat			\
	cvge-dirichlet-nh-disto-P2-t.gdat			\
	cvge-dirichlet-nh-disto-P3-t.gdat			\
	cvge-dirichlet-nh-disto-P4-t.gdat			\
	cvge-dirichlet-nh-ball-P1-t.gdat			\
	cvge-dirichlet-nh-ball-P2-t.gdat			\
	cvge-dirichlet-nh-ball-P3-t.gdat			\
	cvge-dirichlet-nh-ball-P4-t.gdat			\
	cvge-dirichlet-nh-ball-P1-q.gdat			\
	cvge-dirichlet-nh-ball-P2-q.gdat			\
	cvge-dirichlet-nh-ball-P3-q.gdat			\
	cvge-dirichlet-nh-ball-P4-q.gdat			\
	cvge-helmholtz_s_sphere-P1-t.gdat			\
	cvge-helmholtz_s_sphere-P2-t.gdat			\
	cvge-helmholtz_s_sphere-P3-t.gdat			\
	speedup-dirichlet-square-40-P1-direct.gdat	\
	speedup-dirichlet-square-80-P1-direct.gdat	\
	speedup-dirichlet-square-160-P1-direct.gdat	\
	speedup-dirichlet-square-320-P1-direct.gdat	\
	speedup-dirichlet-square-640-P1-direct.gdat	\
	speedup-dirichlet-square-1280-P1-direct.gdat	\
	speedup-dirichlet-square-40-P1-iter.gdat	\
	speedup-dirichlet-square-80-P1-iter.gdat	\
	speedup-dirichlet-square-160-P1-iter.gdat	\
	speedup-dirichlet-square-320-P1-iter.gdat	\
	speedup-dirichlet-square-640-P1-iter.gdat	\
	speedup-dirichlet-square-1280-P1-iter.gdat	\
	speedup-dirichlet-cube-10-P1-direct.gdat	\
	speedup-dirichlet-cube-20-P1-direct.gdat	\
	speedup-dirichlet-cube-40-P1-direct.gdat	\
	speedup-dirichlet-cube-60-P1-direct.gdat	\
	speedup-dirichlet-cube-10-P1-iter.gdat	\
	speedup-dirichlet-cube-20-P1-iter.gdat	\
	speedup-dirichlet-cube-40-P1-iter.gdat	\
	speedup-dirichlet-cube-60-P1-iter.gdat	\
	timing_cmp_direct_iter_2d.gdat				\
	timing_cmp_direct_iter_3d.gdat				\
	transmission-line-10.gdat				\
	transmission-line-14.gdat				\
	transmission-line-6.gdat				\
	transmission-dg-line-6.gdat				\
	transmission-dg-line-10.gdat				\
	transmission-dg-line-14.gdat				\
	contraction-P2-cut.gdat					\
	contraction-zr-P2-cut.gdat				\
	contraction-zr-P1-tau22-cut.gdat			\
	convect_cvge_Pk.gdat					\
	navier-stokes-bench-cut-u0.gdat				\
	navier-stokes-bench-cut-u1.gdat				\
	navier-stokes-Re=100-dt=0.05-t2-2-cut-u0.gdat		\
	navier-stokes-Re=100-dt=0.05-t2-2-cut-u1.gdat		\
	navier-stokes-Re=400-dt=0.05-t2-2-cut-u0.gdat		\
	navier-stokes-Re=400-dt=0.05-t2-2-cut-u1.gdat		\
	navier-stokes-Re=1000-dt=0.02-t2-2-cut-u0.gdat		\
	navier-stokes-Re=1000-dt=0.02-t2-2-cut-u1.gdat		\
	p-laplacian-fixed-point-n=10-p=1.5.gdat  \
	p-laplacian-fixed-point-n=20-p=1.5.gdat  \
	p-laplacian-fixed-point-n=30-p=1.5.gdat  \
	p-laplacian-fixed-point-n=40-p=1.5.gdat  \
	p-laplacian-fixed-point-n=50-p=1.5.gdat  \
	p-laplacian-fixed-point-p=1,5-P1.gdat  \
	p-laplacian-fixed-point-p=1,5-P2.gdat  \
	p-laplacian-fixed-point-p=1,5-P3.gdat  \
	p-laplacian-fixed-point-p=1,5-P4.gdat  \
	p-laplacian-fixed-point-p=1,5-P5.gdat  \
	p-laplacian-rate.gdat  \
	p-laplacian-rate-opt.gdat  \
	p-laplacian-relax-opt.gdat  \
	p-laplacian-relax-p=1.2.gdat  \
	p-laplacian-relax-p=1.3.gdat  \
	p-laplacian-relax-p=1.4.gdat  \
	p-laplacian-relax-p=1.5.gdat  \
	p-laplacian-relax-p=3.gdat  \
	p-laplacian-relax-p=4.gdat  \
	p-laplacian-relax-p=5.gdat  \
	p-laplacian-relax-p=6.gdat  \
	p-laplacian-square-newton-p=3-n=10.gdat  \
	p-laplacian-square-newton-p=3-n=20.gdat  \
	p-laplacian-square-newton-p=3-n=30.gdat  \
	p-laplacian-square-newton-p=3-n=40.gdat  \
	p-laplacian-square-newton-p=3-n=50.gdat  \
	p-laplacian-square-newton-p=3-P1.gdat  \
	p-laplacian-square-newton-p=3-P2.gdat  \
	p-laplacian-square-newton-p=3-P3.gdat  \
	p-laplacian-square-newton-p=3-P4.gdat  \
	p-laplacian-square-newton-p=3-P5.gdat  \
	p-laplacian-square-opt-p=3.gdat  \
	p-laplacian-square-p=1.15.gdat  \
	p-laplacian-square-p=1.25.gdat  \
	p-laplacian-square-p=1.5.gdat  \
	p-laplacian-square-p=2.5.gdat  \
	p-laplacian-square-p=2.95.gdat  \
	p-laplacian-square-p=2.9.gdat  \
	p-laplacian-newton-p=1.5-n=10.gdat  \
	p-laplacian-newton-p=1.5-n=20.gdat  \
	p-laplacian-newton-p=1.5-n=30.gdat  \
	p-laplacian-newton-p=1.5-n=40.gdat  \
	p-laplacian-newton-p=1.5-n=50.gdat  \
	p-laplacian-newton-p=3.gdat  \
	p-laplacian-newton-square-p=2.5.gdat  \
	p-laplacian-newton-square-p=3.0.gdat  \
	p-laplacian-newton-square-p=3.5.gdat  \
	p-laplacian-damped-newton-p=1.2-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=1.3-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=1.4-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=1.5-n=10.gdat  \
	p-laplacian-damped-newton-p=1.5-n=20.gdat  \
	p-laplacian-damped-newton-p=1.5-n=30.gdat  \
	p-laplacian-damped-newton-p=1.5-n=40.gdat  \
	p-laplacian-damped-newton-p=1.5-n=50.gdat  \
	p-laplacian-damped-newton-p=1.5-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=1.5-P1.gdat  \
	p-laplacian-damped-newton-p=1.5-P2.gdat  \
	p-laplacian-damped-newton-p=1.5-P3.gdat  \
	p-laplacian-damped-newton-p=1.5-P4.gdat  \
	p-laplacian-damped-newton-p=1.5-P5.gdat  \
	p-laplacian-damped-newton-p=3-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=4-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=5-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=6-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=7-n=50-P1.gdat  \
	p-laplacian-damped-newton-p=8-n=50-P1.gdat	\
	p-laplacian-fixed-point-p=1.5-P1-err.gdat	\
	p-laplacian-fixed-point-p=1.5-P2-err.gdat	\
	p-laplacian-fixed-point-p=1.5-P3-err.gdat	\
	p-laplacian-fixed-point-p=1.5-P4-err.gdat	\
	transport_dg-cv-2d-t-grid.gdat			\
	transport_dg-1d-elevation.gdat			\
	transport_dg-1d-elevation2.gdat			\
	zalesak-error-l1.gdat					\
	zalesak-error-l1-circle.gdat				\
	zalesak-error-l1-P1d-time.gdat				\
	zalesak-error-l1-P2d-time.gdat				\
	zalesak-error-l1-P1d-time-mass.gdat			\
	zalesak-error-l1-P2d-time-mass.gdat			\
	zalesak-25-Pkd-96000-adapt-error-subdivide.gdat 	\
	zalesak-50-Pkd-96000-adapt-error-subdivide.gdat 	\
	square-100-P2d-nmax-error-mass-time.gdat		\
	zalesak-error-mass-P1d.gdat				\
	zalesak-error-mass-P2d.gdat				\
	zalesak-error-mass-P3d.gdat				\
	zalesak-error-mass-P4d.gdat				\
	zalesak-100-P2d-5000-equispaced.gdat			\
	harten-anim-t-0.gdat					\
	harten-anim-t-0,5.gdat					\
	harten-anim-t-0,5-P0-h-200-n-1000.gdat			\
	harten-anim-tcrit.gdat					\
	harten-anim-tcrit-P0-h-200-n-1000.gdat			\
	harten-anim-t-0,75.gdat					\
	harten-anim-t-0,75-P0-h-200-n-1000.gdat			\
	harten-anim-t-1.gdat					\
	harten-anim-t-1-P0-h-200-n-1000.gdat			\
	harten-anim-t-1,75.gdat					\
	harten-anim-t-1,75-P0-h-200-n-1000.gdat			\
	harten-P0-err-before-choc.gdat				\
	harten-P1d-err-before-choc.gdat				\
	harten-P0-err.gdat					\
	harten-P1d-err.gdat					\
	dirichlet_dg-1d-elevation.gdat			\
	dirichlet_dg-1d-elevation2.gdat			\
	dirichlet_dg-cv-2d-t-grid.gdat			\
	dirichlet-hdg-n-00-error-t.gdat			\
	dirichlet-hdg-n-p1-error-t.gdat			\
	dirichlet-hdg-n-m1-error-t.gdat			\
	dirichlet-hdg-n-00-average-error-t.gdat		\
	dirichlet-hdg-n-p1-average-error-t.gdat		\
	dirichlet-hdg-n-m1-average-error-t.gdat		\
	dirichlet-hdg-n-00-post-error-t.gdat		\
	dirichlet-hdg-n-p1-post-error-t.gdat		\
	dirichlet-hdg-n-m1-post-error-t.gdat		\
	reconstruction-hho-error-t.gdat 		\
	dirichlet-hho-k-k-e.gdat			\
	dirichlet-hho-k-k-t.gdat			\
	dirichlet2-t.gdat				\
	reconstruction-hho-cond-k-k-e.gdat		\
	burgers_diffusion_rk_semiimplicit_error-P1d-dt.gdat \
	burgers_diffusion_semiimplicit_error-P1d-h-50-dt.gdat \
	burgers_diffusion_semiimplicit_error-P2d-h-50-dt.gdat \
	burgers-diffusion-rk-semiimplicit-limiter-line2-400-eps-1e-3-init.gdat \
	burgers-diffusion-rk-semiimplicit-limiter-line2-400-eps-1e-3-final.gdat \
	burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3-init.gdat \
	burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3-final.gdat \
	navier-stokes-dg-cavity-40-P1d-Re=100-residu.gdat \
	navier-stokes-dg-cavity-40-P1d-Re=400-residu.gdat \
	navier-stokes-dg-cavity-40-P1d-Re=500-residu.gdat \
	navier-stokes-dg-cavity-40-P1d-Re=1000-residu.gdat \
	combustion-10-a-0,5.gdat \
	combustion-10-ac.gdat \
	combustion-100-ac.gdat \
	combustion_umax.gdat \
	combustion_umax.gdat \
	lambda_c_cv.gdat \
	lambda_c_cv.gdat \
	lambda_c_cv.gdat \
	combustion_error.gdat \
	combustion_error.gdat \
	combustion_error.gdat \
	combustion_det_P1.gdat \
	combustion_det_P1.gdat \
	combustion_det_P1.gdat \
	combustion_det_P1.gdat \
	combustion_det_P1.gdat \
	combustion_det_P1.gdat \
	line-10-arc-umax.gdat \
	line-80-arc-umax.gdat \
	line-20-upper.gdat \
	yield-slip-50-n-1,5-S-0,6-cut0.gdat \
	yield-slip-50-n-1-S-0,6-cut0.gdat \
	yield-slip-50-n-0,5-S-0,6-cut0.gdat \
	yield-slip-50-n-1,5-S-0,6-cut.gdat \
	yield-slip-50-n-1-S-0,6-cut.gdat \
	yield-slip-50-n-0,5-S-0,6-cut.gdat \
	yield-slip-50-n-1,5-S-0,6-lambda.gdat \
	yield-slip-50-n-1-S-0,6-lambda.gdat \
	yield-slip-50-n-0,5-S-0,6-lambda.gdat \
	yield-slip-al-n-10-n-1-S-0.6.gdat \
	yield-slip-al-n-20-n-1-S-0.6.gdat \
	yield-slip-al-n-30-n-1-S-0.6.gdat \
	yield-slip-al-n-40-n-1-S-0.6.gdat \
	yield-slip-al-n-50-n-1-S-0.6.gdat \
	yield-slip-circle-S-0,25-n-0,5-P1-sym.gdat \
	yield-slip-circle-S-0,25-n-0,5-P1-unsym.gdat \
	yield-slip-circle-S-0,25-n-0,5-P2-sym.gdat \
	yield-slip-circle-S-0,25-n-0,5-P2-unsym.gdat \
	yield-slip-circle-S-0,25-n-0,5-P3-sym.gdat \
	yield-slip-circle-S-0,25-n-0,5-P3-unsym.gdat \
	yield-slip-n-10-n-0.5-S-0.6.gdat \
	yield-slip-n-10-n-0.9-S-0.6.gdat \
	yield-slip-n-10-n-1.5-S-0.6.gdat \
	yield-slip-n-10-n-1-S-0.6.gdat \
	yield-slip-n-20-n-0.5-S-0.6.gdat \
	yield-slip-n-20-n-0.9-S-0.6.gdat \
	yield-slip-n-20-n-1.5-S-0.6.gdat \
	yield-slip-n-20-n-1-S-0.6.gdat \
	yield-slip-n-30-n-0.5-S-0.6.gdat \
	yield-slip-n-30-n-0.9-S-0.6.gdat \
	yield-slip-n-30-n-1.5-S-0.6.gdat \
	yield-slip-n-30-n-1-S-0.6.gdat \
	yield-slip-n-40-n-0.5-S-0.6.gdat \
	yield-slip-n-40-n-0.9-S-0.6.gdat \
	yield-slip-n-40-n-1.5-S-0.6.gdat \
	yield-slip-n-40-n-1-S-0.6.gdat \
	yield-slip-n-50-n-0.5-S-0.6.gdat \
	yield-slip-n-50-n-0.9-S-0.6.gdat \
	yield-slip-n-50-n-1.5-S-0.6.gdat \
	yield-slip-n-50-n-1-S-0.6.gdat  \
	mosolov-square-10-Bi-0,4-cut-bisector.gdat \
	mosolov-square-20-Bi-0,4-cut-bisector.gdat \
	mosolov-square-30-Bi-0,4-cut-bisector.gdat \
	mosolov-square-40-Bi-0,4-cut-bisector.gdat \
	mosolov-square-50-Bi-0,4-cut-bisector.gdat \
	mosolov-circle-Bi-0,2-n-0,5-error.gdat \
	mosolov-circle-Bi-0,2-n-0,5-error-ys.gdat \
	mosolov-circle-10-P1-Bi-0,2-n-0,5-ys.gdat \
	mosolov-circle-20-P1-Bi-0,2-n-0,5-ys.gdat \
	mosolov-circle-40-P1-Bi-0,2-n-0,5-ys.gdat \
	mosolov-sector-P2-Bi-0-5-n-1-cut-bisector.gdat	\
	mosolov-sector-P2-Bi-0.5-n-1-geo-adapt-loop.gdat \
	transport-tensor-err.gdat \
	oldroyd-cavity-10-We-0,2-alpha-0,8888-a-1-cut-tau.gdat \
	oldroyd-cavity-10-We-0,2-alpha-0,8888-a-1-cut-u.gdat \
	oldroyd-cavity-20-We-0,2-alpha-0,8888-a-1-cut-tau.gdat \
	oldroyd-cavity-20-We-0,2-alpha-0,8888-a-1-cut-u.gdat \
	oldroyd-cavity-40-We-0,2-alpha-0,8888-a-1-cut-tau.gdat \
	oldroyd-cavity-40-We-0,2-alpha-0,8888-a-1-cut-u.gdat  \
	oldroyd-contraction-zr-psi-max.gdat \
	oldroyd-contraction3-zr-u0-axis.gdat \
	oldroyd-contraction3-zr-tau00-axis.gdat \
	oldroyd-contraction3-zr-tau11-axis.gdat \
	oldroyd-contraction3-zr-tau22-axis.gdat \
	commute-rtd-t.gdat \
	obstacle-slip-ux-xaxis.gdat \
	obstacle-slip-ux-yaxis.gdat \
	obstacle-zr-slip-ux-xaxis.gdat \
	obstacle-zr-slip-ux-yaxis.gdat \
	diffusion-tensor-err.gdat \
	diffusion-transport-tensor-err.gdat

# requires gnuplot and fig2dev, or cairosvg, so put it in tar.gz distrib
# but not in cvs distrib:
SPECIAL_GENERATED =						\
	${FIGSRC:.fig=.pdf} 					\
	${SVGSRC:.svg=.pdf} 					\
	${PLOTSRC:.plot=.pdf}

EXTRA_DIST_WITHOUT_LICENSE = 					\
	synthese.tex						\
	rheolef.bib						\
	${MAIN_TEX:.tex=-autobib.bib}				\
	convect-1d.mac						\
	convect-2d.mac						

noinst_DATA =							\
	$(PNG_SRC)						\
	${PDF_SRC} 						\
	${FIGSRC} 						\
	${SVGSRC} 						\
	$(PLOTDATA)						

EXTRA_DIST = 							\
	Makefile.am						\
	$(EXTRA_DIST_WITHOUT_LICENSE)				\
	${MAIN_TEX}	 					\
	$(TEX_INCLUDED)						\
	$(PLOTSRC) 						\
	${noinst_DATA} 						\
	${FIGSRC:.fig=.pdf} 					\
	${SVGSRC:.svg=.pdf} 					\
	${PLOTSRC:.plot=.pdf}					\
	install-check-examples.sh				\
	mosolov-circle-Bi-0,2-n-0,5-error-gdat-make.sh		\
	mosolov-circle-Bi-0,2-n-0,5-error-ys-gdat-make.sh	\
	commute-rtd-gdat-make.sh				\
	dirichlet-hdg-error-gdat-make.sh			\
	dirichlet-hdg-average-error-gdat-make.sh		\
	dirichlet-hdg-post-error-gdat-make.sh			\
	reconstruction-hho-error-gdat-make.sh			\
	reconstruction-hho-cond-gdat-make.sh			\
	dirichlet-hho-error-gdat-make.sh			\
	dirichlet-hho-cond-gdat-make.sh				\
	dirichlet-error-gdat-make.sh				\
	oldroyd-contraction-zr-psi-max-make.sh			\
	oldroyd-contraction-cut-make.sh				\
	zalesak-error-l1-Pkd-time-gdat-make.sh			\
	zalesak-size-Pkd-nmax-adapt-error-gdat-make.sh		\
	zalesak-size-Pkd-nmax-error-vs-nmax-gdat-make.sh 	\
	zalesak-video-make.sh

CVSIGNORE = 							\
	Makefile.in 						\
	${SPECIAL_GENERATED}


WCIGNORE = 							\
	${MAIN_TEX:.tex=-autobib.bib}				\
	$(noinst_DATA)

LICENSEIGNORE = 						\
	$(noinst_DATA)						\
	$(EXTRA_DIST_WITHOUT_LICENSE)				\
	$(PLOTSRC) 						\
	$(TEX_INCLUDED)	

CLEANFILES = 							\
	$(MAIN_TEX:.tex=.pdf)					\
	synthese.pdf						\
	version.tex						\
	*.aux *.dvi *.idx 					\
	*.ilg *.ind *.log  					\
	*.bbl *.blg *.biblog *.lbs *.toc *.lof			\
	texput.log *.log 					\
	*.mtc1 *.mtc						\
	*.cnx *.fox *.exx *.prx					\
	*.cnd *.fod *.exd *.prd					\
	*.out *.brf

# -----------------------------------------------------------------------------
# extra rules
# -----------------------------------------------------------------------------
if HAVE_TEX_DOCUMENTATION
dvi-local: $(MAIN_TEX:.tex=.pdf) synthese.pdf
else
dvi-local:
endif

clean-local:
	rm -f ${PLOTSRC:.plot=.pdf} ${PLOTSRC:.plot=.tex} ${PLOTSRC:.plot=-inc.eps}
	rm -f ${FIGSRC:.fig=.pdf} ${SVGSRC:.svg=.pdf}
	rm -f *-warnings.log

real-clean: clean
	cd ${srcdir}; rm -f ${SPECIAL_GENERATED}


SUFFIXES = .plot .fig .tex .dvi .ps .cc .ctex .pdf .png

synthese.pdf: synthese.tex synthese-fig.pdf
	if test x"${PDFLATEX}" != x""; then		\
	     TEXINPUTS=":.:$(srcdir)/:$$TEXINPUTS" ${PDFLATEX} $(srcdir)/synthese.tex; 	\
	fi

AUTOBIB = autobib

# note
# pdf compression with ghoscript is suspended since it removed hyperlinks
# in pdf, due to a bug: still wait for a fix 
# see http://www.documentsnap.com/reduce-pdf-file-size-ghostscript
USE_GS = false
GSFLAGS = 								\
	-sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPrinted=false 	\
	-dPDFSETTINGS=/prepress 					\
	-dColorImageDownsampleType=/Bicubic  -dColorImageResolution=150 \
	-dNOPAUSE -dQUIET -dBATCH


%.pdf %.aux %.idx %.ind: 					\
		%.tex 						\
		rheolef.bib 					\
		${FIGSRC:.fig=.pdf}				\
		${SVGSRC:.svg=.pdf}				\
		${PLOTSRC:.plot=.pdf}				\
		$(PNG_SRC)	  				\
		synthese.pdf					\
		version.tex
	rm -f $*.aux $*.idx $*.ind $*.out
	rm -f $*.toc $*.lot $*.lof
	if test x"${PDFLATEX}" != x"" -a x"${BIBTEX}" != x"" 	\
	     -a x"${MAKEINDEX}" != x""; then 			\
	     TEXINPUTS=":.:$(srcdir)/:$(top_srcdir)/doc/examples/:$$TEXINPUTS"; \
	     BIBINPUTS=":.:$(srcdir)/:$(top_srcdir)/doc/examples/:$$BIBINPUTS"; \
	     export TEXINPUTS BIBINPUTS; 	\
	     rm -f $*.idx; 			\
	     rm -f $*.bbl $*.aux $*.toc $*.lof; \
	     rm -f $*.cnd $*.exd $*.fod $*.prd; \
	     rm -f $*.cnx $*.exx $*.fox $*.prx; \
	     ${PDFLATEX} $*			\
	  && (x=$*; \
	  for f in `ls $${x}.*aux`; do \
	    g=`expr $$f : '\(.*\)\.aux' `; \
	    ncite=`grep '\\citation{' $$f | wc -l`; \
	    nrefs=`grep 'bibcite' $$f | wc -l`; \
	    echo "ncite=$$ncite nrefs=$$nrefs"; \
	    if test $$ncite -gt 0 -a $$nrefs -eq 0; then \
	      status=0; \
	      echo "$(AUTOBIB) $$g"; \
	      mv $(srcdir)/$*-autobib.bib $(srcdir)/$*-autobib.bib.old 2>/dev/null || \
		 touch $(srcdir)/$*-autobib.bib.old; \
              $(AUTOBIB) $$g; status=$$?; \
	      if test -f autobib.bib; then mv autobib.bib $(srcdir)/$*-autobib.bib; fi; \
	      if test $$status -ne 0; then \
		echo "autobib failed"; \
	        mv $(srcdir)/$*-autobib.bib.old $(srcdir)/$*-autobib.bib 2>/dev/null; \
	      fi; \
	      echo "strip bibtex url=..."; \
	      sed -e 's/^[    ]*url[  ]*=/omit_url=/' < $(srcdir)/$*-autobib.bib >  $(srcdir)/$*-autobib.bib.new && \
	      mv $(srcdir)/$*-autobib.bib.new $(srcdir)/$*-autobib.bib; \
	      echo "bibtex $$g"; \
	      bibtex $$g | tee $$g.biblog; \
	      if grep -i warning $$g.biblog >/dev/null; then exit 1; else true; fi; \
	      if grep -i error   $$g.biblog >/dev/null; then exit 1; else true; fi; \
	    fi; \
	  done) \
	  && ${MAKEINDEX} -o $*.cnd $*.cnx 	\
	  && ${MAKEINDEX} -o $*.exd $*.exx 	\
	  && ${MAKEINDEX} -o $*.prd $*.prx 	\
	  && ${PDFLATEX} $* 		\
	  && ${PDFLATEX} $* 		\
	  && ${PDFLATEX} $* 		\
	  && (cat < $*.log 2>/dev/null | grep "LaTeX Warning:" |	\
	      grep -aiv 						\
		-e 'has changed' 	 			 	\
		-e 'marginpar' 		 			 	\
	     | tee $*-warnings.log || true)				\
	  && test `cat $*-warnings.log | wc -l` -eq 0			\
	  && if test x"${GS}" != x"" -a x"${USE_GS}" = x"true"; then	\
	       command="${GS} ${GSFLAGS} -sOutputFile=$*-default.pdf $*.pdf" \
	       && echo $$command 					\
	       && $$command 						\
	       && mv $*-default.pdf $*.pdf; 				\
	     fi  							\
	  && if test x"${PDFFONTS}" != x""; then			\
	       command="test \`${PDFFONTS} $*.pdf | grep 'Type 3' | wc -l\` -eq 0"; \
	          echo "$${command}"; 					\
	          eval "$${command}";					\
	     fi; 							\
	else								\
	  true;								\
	fi

$(MAIN_TEX:.tex=.pdf): ${TEX_INCLUDED}

# uncomment: flags depend upon latex2html version
L2H_FLAGS = -white -image_type gif -noinfo -noaddress # -verbosity 3

version.tex: $(top_srcdir)/configure.ac $(srcdir)/rheolef.tex ${top_srcdir}/VERSION
	@sh ${top_srcdir}/config/mk_version.sh $(srcdir) $(top_srcdir) `cat $(top_srcdir)/VERSION` rheolef $(exampledir)

.plot.pdf:
	GNUPLOT_LIB=$(srcdir) bash $(top_srcdir)/config/plot2pdf.sh -gnuplot5 $<
	rm -f $*.glog $(srcdir)/$*.glog
.svg.pdf:
	if test x"${CAIROSVG}" != x""; then				\
	  ${CAIROSVG} -o $(srcdir)/$*.pdf $(srcdir)/$*.svg;		\
	elif test x"${INKSCAPE}" != x"" -a x"${PDFCROP}" != x""; then 	\
	  ${INKSCAPE} --export-pdf=$(srcdir)/$*.pdf $(srcdir)/$*.svg; 	\
	  ${PDFCROP} $(srcdir)/$*.pdf $(srcdir)/$*-crop.pdf; 		\
	  mv $(srcdir)/$*-crop.pdf $(srcdir)/$*.pdf;			\
	else								\
	 touch $(srcdir)/$*.pdf;					\
	fi
lib:
	cd ../../config; ${MAKE}
	cd ../../util; ${MAKE}
	cd ../../linalg; ${MAKE}
	cd ../../fem; ${MAKE}
	cd ../../main; ${MAKE}

install-data-local: dvi
	$(MKDIRHIER) $(DESTDIR)$(docdir)
	for x in ${MAIN_TEX:.tex=.pdf}; do 			\
	  if test -f $${x}; then 				\
	    $(INSTALL_DATA) $${x} $(DESTDIR)$(docdir)/$${x};	\
	  fi;							\
	done

uninstall-local:
	rm -rf $(DESTDIR)$(docdir)/*

web-install: rheolef.pdf
	$(MKDIRHIER) $(WEB_DIR)
	$(INSTALL_DATA) rheolef.pdf $(WEB_DIR)/rheolef.pdf

number-of-pages: rheolef.ps
	@grep Pages rheolef.ps | sed -e '2,$$d' | awk '{print $$2}'

poid:
	du -s $(PDF_SRC) $(PNG_SRC) | sort -n

show-examples:
	@cd ../examples && $(MAKE) -s show-examples

install-check-examples: $(MAIN_TEX:.tex=.pdf)
	@ $(MAKE) -s show-examples >/dev/null ; # avoid trace of Makefile rebuilt
	- @bash $(srcdir)/install-check-examples.sh `$(MAKE) -s show-examples` 

check-local: install-check-examples

# personal tools
plots: ${PLOTSRC:.plot=.pdf}

maintainer-clean-local:
	cd ${srcdir}; rm -f ${SPECIAL_GENERATED}

