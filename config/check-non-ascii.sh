#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
echo "checking for non-ascii sequences in source files..."
check_prog_full_path=$1
top_srcdir=${2-"."}

cd $top_srcdir

h=`find . -name "*.h"`
c=`find . -name "*.cc"`
ic=`find . -name "*.icc"`
a=`find . -name "*.am"`
d=`find . -name "*.ac"`
m=`find . -name "*.m4"`
t=`find . -name "*.tex"`
b=`find . -name "*.bib"`
p=`find . -name "*.plot"`
f=`find . -name "*.fig"`
s=`find . -name "*.sh"`
L="$h $c $ic $a $d $m $t $b $p $f $s"
status=0
for x in $L; do
 if test -h $x; then continue; fi ; # symb link
 if test `basename $x` = "check_non_ascii.cc"; then continue; fi ; # skip
 $check_prog_full_path $x
 st=$?
 if test $st -ne 0; then
  echo "$x: ascii check failed"
  status=1
 fi
done 
exit $status
