set terminal cairolatex pdf color standalone
set output "p-laplacian-rate.tex"

set size square 1.0
set colors classic
set xlabel "[c]{$p$}"
set  label "[r]{\\Large $\\bar{v}$}" at graph -0.08,0.95

set xtics (1,2,3)
set ytics (0,1,2)

plot  [1:3][0:3] \
"p-laplacian-rate.gdat" i 0 \
	title '[r]{computation}' w l lc 1 lt 1 lw 2, \
"p-laplacian-rate.gdat" i 1 \
	notitle                  w l lc 1 lt 1 lw 2

#pause -1 "<retour>"
