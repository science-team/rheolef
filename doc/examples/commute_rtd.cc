///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile commute_rtd.cc Discontinuous Raviart-Thomas L2 projection
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cosinus_vector.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  size_t d = omega.dimension(),
         k = (argc > 2) ? atoi(argv[2]) : 0;
  space Vh (omega, "RT"+to_string(k)+"d"),
        Lh (omega, "P" +to_string(k)+"d"),
        Xh = Vh*Lh;
  trial x (Xh); test y (Xh);
  auto u = x[0], lambda = x[1];
  auto v = y[0], mu     = y[1];
  integrate_option iopt;
  iopt.invert = true;
  form  inv_a = integrate (dot(u,v) + div_h(v)*lambda + div_h(u)*mu, iopt);
  field lh = integrate (dot(u_exact(d),v) + div_u_exact(d)*mu),
        xh = inv_a*lh,
        p_Vh_u = xh[0],
        pi_h_u = lazy_interpolate(Vh,u_exact(d));
  dout << catchmark("p_Vh_u") << p_Vh_u
       << catchmark("pi_h_u") << pi_h_u;
}
