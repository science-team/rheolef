///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile mosolov_error.cc The Mossolov problem for a circular pipe -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "mosolov_exact_circle.h"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  Float tol_u = (argc > 1) ? atof(argv[1]) : 1e-15;
  Float tol_s = (argc > 2) ? atof(argv[2]) : 1e-15;
  Float Bi, n;
  field sigma_h, uh;
  din >> catchmark("Bi")    >> Bi 
      >> catchmark("n")     >> n 
      >> catchmark("sigma") >> sigma_h
      >> catchmark("u")     >> uh;
  const geo& omega = uh.get_geo();
  Float meas_omega = integrate(uh.get_geo());
  geo boundary = omega["boundary"];
  const space& Xh = uh.get_space();
  const space& Th = sigma_h.get_space();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(3*Xh.degree());
  Float err_u_l2 = sqrt(integrate (omega, sqr(uh - u(Bi,n)), iopt)/meas_omega);
  Float err_u_h1 = sqrt(integrate (omega, norm2(grad(uh) - grad_u(Bi,n)), iopt)/meas_omega);
  Float err_s_l2 = sqrt(integrate (omega, norm2(sigma_h - sigma(Bi,n)), iopt)/meas_omega);
  space Xh1 (omega, "P" + to_string(2*Xh.degree()));
  space Th1 (omega, "P" + to_string(2*Xh.degree()) + "d", "vector");
  field euh = lazy_interpolate (Xh1, uh - u(Bi,n));
  field esh = lazy_interpolate (Th1, sigma_h - sigma(Bi,n));
  Float err_u_linf = euh.max_abs();
  Float err_s_linf = esh.max_abs();
  dout << "err_u_linf = " << err_u_linf << endl
       << "err_u_l2   = " << err_u_l2 << endl
       << "err_u_h1   = " << err_u_h1 << endl
       << "err_s_linf = " << err_s_linf << endl
       << "err_s_l2   = " << err_s_l2 << endl;
  return (err_u_linf < tol_u) && (err_s_l2 < tol_s) ? 0 : 1;
}
