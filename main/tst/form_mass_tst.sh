#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

GEODIR=$SRCDIR
# -------------------------------------------
#  form(Pk,Pk,mass)
#   P1, P2 : all polynoms in [0,1]^d, d=1,2,3
#  with geo elements [e,t,q,T,P,H]
#  and cartesian coordinate system
# -------------------------------------------
status=0
for approx in P3 P0 P1 P1d P2; do
  for d in 1 2 3; do
    case $approx in
      P0) Poly="1";;
      P1 | P1d) case $d in
         1) Poly="1 x";;
         2) Poly="1 x y";;
         3) Poly="1 x y z";;
         esac;;
      P2) case $d in
	 1) Poly="1 x x2";;
	 2) Poly="1 x x2 y xy y2";;
	 3) Poly="1 x x2 y xy y2 xz yz z2";;
         esac;;
      P3) case $d in
	 1) Poly="1 x x2 x3";;
	 2) Poly="1 x x2 y xy y2 x3 y3";;
	 3) Poly="1 x x2 y xy y2 xz yz z2 x3 y3 z3 x2y xy2 x2z xz2 y2z yz2 xyz";;
         esac;;
    esac
    case $d in
        1) input="${GEODIR}/line-bdry-v2";;
        2) input="${GEODIR}/carre-bamg-v2 ${GEODIR}/carre-q-10-dom-v2";
           L_MIXED="${GEODIR}/carre-tq-10-dom-v2.geo";
           input="${input} ${GEODIR}/carre-bamg-q-dom-v2 ${L_MIXED} ";
           ;;
        3) input="${GEODIR}/cube-dom-v2  ${GEODIR}/cube-H-6-dom-v2"
           if test $approx != "P3"; then
	     # TODO: Pk prism
             input="$input ${GEODIR}/cube-P-5-dom-v2"
           fi 
           ;;
    esac 
    for geo in $input; do
      for w in $Poly; do
        loop_mpirun "./form_mass_tst -app ${approx} -weight ${w} ${geo} >/dev/null 2>/dev/null"
        if test $? -ne 0; then status=1; fi
      done
    done
  done
done
# -------------------------------------------
#  axisymmetry:
#
#  form(Pk,Pk,mass)
#   P1, P2 : all polynoms in [0,1]^2
#  with simplicial geo elements [t]
#  and axisymmetric coordinate system
# -------------------------------------------
for approx in P1 P2; do
  input="${GEODIR}/carre-bamg-v2 ${GEODIR}/carre-q-10-dom-v2"
  for geo in $input; do
    case $approx in
      P1|P1d) Poly="1 x";;
      P2|P2d) Poly="1 x x2 y xy y2";;
    esac
    for w in $Poly; do
        loop_mpirun "./form_mass_tst -app ${approx} -weight ${w} -rz ${geo} >/dev/null 2>/dev/null"
        if test $? -ne 0; then status=1; fi
    done
  done
done
# -------------------------------------------
#  projections:
#
#  form(Pk,Pl,mass), k!=l
#  with simplicial geo elements [e,t,T]
#  and cartesian coordinate system
# -------------------------------------------

input="${GEODIR}/carre-v2"

projections="
P0_P1 P0_P1d  P0_P2
      P1_P1d  P1_P2
             P1d_P2"
#TODO 
TODO_projections="
P0_P1 P0_P1d  P0_P2  P0_P2d
      P1_P1d  P1_P2  P1_P2d
             P1d_P2 P1d_P2d
                     P2_P2d"

for d in 1 2 3; do
  case $d in
  1) input="${GEODIR}/line-bdry-v2";;
  2) input="${GEODIR}/carre-bamg-v2 ${GEODIR}/carre-q-10-dom-v2";
     L_MIXED="${GEODIR}/carre-tq-10-dom-v2.geo";
     input="${input} ${GEODIR}/carre-bamg-q-dom-v2 ${L_MIXED}";
     ;;
  3) input="${GEODIR}/cube-dom-v2  ${GEODIR}/cube-H-6-dom-v2 ${GEODIR}/cube-P-5-dom-v2";;
  esac
  for pair in $projections; do
    approx1=`echo $pair | gawk -v FS="_" '{print $1}'`
    approx2=`echo $pair | gawk -v FS="_" '{print $2}'`
    case ${approx1} in
      P0) Poly="1";;
      P1|P1d) case $d in
         1) Poly="1 x";;
         2) Poly="1 x y";;
         3) Poly="1 x y z";;
         esac;;
      P2|P2d) case $d in
	 1) Poly="1 x x2";;
	 2) Poly="1 x x2 y xy y2";;
	 3) Poly="1 x x2 y xy y2 xz yz z2";;
         esac;;
    esac
    for w in $Poly; do
      for geo in $input; do
        loop_mpirun "./form_mass_tst -app ${approx1} -proj ${approx2} -weight ${w} ${geo} >/dev/null 2>/dev/null"
        if test $? -ne 0; then status=1; fi
      done
    done
  done
done

exit $status

