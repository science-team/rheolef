///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
//    compute :
//    f = u - div_s(2Ds(u)) on Gamma=ball(0,1)
//    via discrete operators 
//    and compare it with direct interpolate of exact f
//
//    => do not converge in L2 and Linf
//       perhaps do in H^{-1} norm ?
//
#include "rheolef.h"
// -------------------------------------------------
// sphere.h : Analytic solutions 
// -------------------------------------------------
/*
u_r     = 0;
u_theta = sqr(sin(theta))*(sqr(cos(phi)) - sqr(sin(phi)));                                                       
u_phi   = -3*sqr(sin(theta))*cos(theta)*sin(phi)*cos(phi);
g = u - div_s(2D_s(u))  
g_r     = 0;
g_theta = (20*sqr(cos(phi)) - 10)*sqr(cos(theta)) - 10*sqr(cos(phi)) + 5;
g_phi   = 30*cos(phi)*sin(phi)*cos(theta)*sqr(sin(theta)) - 5*cos(phi)*sin(phi)*cos(theta);
*/
namespace rheolef {
  Float phi (const point& x) { return norm(x) - 1; }
  void get_spherical_coordinates (const point& x, Float& rho, Float& theta, Float& phi) {
    static const Float pi = acos(Float(-1));
    // refs. Bird, vol1, appendix A,; p. 580                                                                                                           
    rho = norm(x);
    Float r_xy = sqrt(sqr(x[0])+sqr(x[1]));
    theta = atan2 (r_xy, x[2]);
    phi   = atan2 (x[1], x[0]);
  }

  struct u_exact : std::unary_function<point,point> {
    point operator() (const point& x) const {
      Float rho, theta, phi;
      get_spherical_coordinates (x, rho, theta, phi);
      // refs. Bird, vol1, appendix A,; p. 580                                                                                                         
      point  e_r     = point(
                             sin(theta)*cos(phi),
                             sin(theta)*sin(phi),
                             cos(theta));
      point  e_theta = point(
                             cos(theta)*cos(phi),
                             cos(theta)*sin(phi),
                             -sin(theta));
      point  e_phi =   point(
                             -sin(phi),
                             cos(phi),
                             0);      
      // div_s(u) = 0                                                                                                                     
      Float u_r     = 0;
      Float u_theta = sqr(sin(theta))*(sqr(cos(phi)) - sqr(sin(phi))); 
      Float u_phi   = -3*sqr(sin(theta))*cos(theta)*sin(phi)*cos(phi);
      return u_r*e_r + u_theta*e_theta + u_phi*e_phi;
    }
    u_exact ()  {}
  };
  // f = u - div_s(2*Ds(u))                                                                                                              
  struct f : std::unary_function<point,point> {
    point operator() (const point& x) const {
      Float rho, theta, phi;
      get_spherical_coordinates (x, rho, theta, phi);
      point  e_r     = point(
                             sin(theta)*cos(phi),
                             sin(theta)*sin(phi),
                             cos(theta));
      point  e_theta = point(
                             cos(theta)*cos(phi),
                             cos(theta)*sin(phi),
                             -sin(theta));
      point  e_phi = point(
                           -sin(phi),
                           cos(phi),
                           0);
      // g = div_s(2*Ds(u))
      Float g_r     = 0;
      Float g_theta = (20*sqr(cos(phi)) - 10)*sqr(cos(theta)) - 10*sqr(cos(phi)) + 5;
      Float g_phi   = 30*cos(phi)*sin(phi)*cos(theta)*sqr(sin(theta)) - 5*cos(phi)*sin(phi)*cos(theta);
      point g = g_r*e_r + g_phi*e_phi + g_theta*e_theta;
      return _u(x)- g;
    }
    f () : _u() {}
  protected: u_exact _u;
  };
 struct u0 : std::unary_function<point,Float> {
     Float operator() (const point& x) {
       return _u(x)[0];
     }
     u0 () : _u() {}
  protected: u_exact _u;
 };
 struct u1 : std::unary_function<point,Float> {
     Float operator() (const point& x) {
       return _u(x)[1];
     }
     u1 () : _u() {}
  protected: u_exact _u;
 };
 struct u2 : std::unary_function<point,Float> {
     Float operator() (const point& x) {
       return _u(x)[2];
     }
     u2 () : _u() {}
  protected: u_exact _u;
 };
 struct f0 : std::unary_function<point,Float> {
     Float operator() (const point& x) {
       return _f(x)[0];
     }
     f0 () : _f() {}
  protected: f _f;
 };
 struct f1 : std::unary_function<point,Float> {
     Float operator() (const point& x) {
       return _f(x)[1];
     }
     f1 () : _f() {}
  protected: f _f;
 };
 struct f2 : std::unary_function<point,Float> {
     Float operator() (const point& x) {
       return _f(x)[2];
     }
     f2 () : _f() {}
  protected: f _f;
 };

} // namespace rheolef          
// -------------------------------------------------
// function.h
// -------------------------------------------------
namespace rheolef {
  void get_spherical_coordinates (const point& x, Float& theta, Float& phi) {
    static const Float pi = acos(Float(-1));
    // refs. Bird, vol1, appendix A,; p. 580                                                                                                           
    Float r_xy = sqrt(sqr(x[0])+sqr(x[1]));
    theta = atan2 (r_xy, x[2]);
    phi   = atan2 (x[1], x[0]);
  };
  ////////////////////////////////////////////////////////////
  // Vecteurs de bases cartesiens en coordonnees spheriques //
  ////////////////////////////////////////////////////////////
  point e_x (const point& x) {
     Float theta, phi;
     get_spherical_coordinates (x, theta, phi);
     return  point( sin(theta)*cos(phi),
                    cos(theta)*cos(phi),
                    -sin(phi)
                  );
  };
  point e_y (const point& x) {
     Float theta, phi;
     get_spherical_coordinates (x, theta, phi);
     return point( sin(theta)*sin(phi),
                   cos(theta)*sin(phi),
                   cos(phi)
                 );
  };
  point e_z (const point& x) {
     Float theta, phi;
     get_spherical_coordinates (x, theta, phi);
     return point(  cos(theta),
		    -sin(theta), 
                    0
                 );
  };
  ////////////////////////////////////////////////////////////
  // Vecteurs de bases spheriques en coordonnees cartesiennes //
  ////////////////////////////////////////////////////////////
  point e_r (const point& x) {
     Float theta, phi;
     get_spherical_coordinates (x, theta, phi);
     return point( sin(theta)*cos(phi),
                   sin(theta)*sin(phi),
                   cos(theta)
                 );
  };
  point e_theta  (const point& x) {
     Float theta, phi;
     get_spherical_coordinates (x, theta, phi);
     return point( cos(theta)*cos(phi),
                   cos(theta)*sin(phi),
                   -sin(theta)
                 );
  };
  point e_phi (const point& x) {
     Float theta, phi;
     get_spherical_coordinates (x, theta, phi);
     return point( -sin(phi),
                    cos(phi),
                    0
                 );
  };
   ////////////////////////////////////////////////////////////
  // produit de deux fields //
  ////////////////////////////////////////////////////////////
  field  scalar_product_field (const field& fh, const field& gh) {
     const space & Xh = fh.get_space();
     space Ph = Xh[0];
     field ph = lazy_interpolate(Ph, fh[0]*gh[0]+fh[1]*gh[1]+fh[2]*gh[2]); 
     return ph;
  };
  ////////////////////////////////////////////////////////////
  // fields  en coordonnees spheriques //
  ////////////////////////////////////////////////////////////
  field  spherical_field (const field& fh) {
     const space & Xh = fh.get_space();
     space X0h = Xh[0];
     field e_r_h = lazy_interpolate(Xh, e_r);
     field e_theta_h = lazy_interpolate(Xh, e_theta);
     field e_phi_h = lazy_interpolate(Xh, e_phi);
     field gh(Xh); 
     gh[0] = scalar_product_field(fh, e_r_h); 
     gh[1] = scalar_product_field(fh, e_theta_h); 
     gh[2] = scalar_product_field(fh, e_phi_h); 
     return gh;
  };  
} // namespace rheolef          
// -------------------------------------------------
// main
// -------------------------------------------------
using namespace std;
using namespace rheolef;
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1";
  space Xh (omega, approx, "vector");
  size_t k = Xh.degree();
  size_t q  = (argc > 3) ? atoi(argv[3]) : 4*k+2;
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(q);
  trial u (Xh); test v (Xh);
  form a = integrate (dot(u,v) + 2*ddot(Ds(u),Ds(v)), qopt);
  form m = integrate (dot(u,v), qopt);
  field pi_h_u = lazy_interpolate(Xh, u_exact());
  field pi_h_f = lazy_interpolate(Xh, f());
  field mfh = a*pi_h_u;
  solver sm (m.uu());
  field fh (Xh);
  fh.set_u() = sm.solve (mfh.u());
  field eh = fh - pi_h_f; 
  Float err_f_l2 = sqrt(m(eh,eh)); 
  Float err_f_linf = eh.max_abs();
  derr << " |eh|l2        = " << err_f_l2   << endl;
  derr << " |eh|linf      = " << err_f_linf << endl;
  field fh_s = spherical_field(fh);
  field pi_h_f_s = spherical_field(pi_h_f);
  field pi_h_u_s = spherical_field(pi_h_u);
  field eh_s = spherical_field(eh);
  dout << setprecision(numeric_limits<Float>::digits10)
       << catchmark("fh") << fh
       << catchmark("pi_h_f") << pi_h_f
       << catchmark("pi_h_u") << pi_h_u
       << catchmark("eh") << eh                                                                                              
       << catchmark("fh_s") << fh_s                                                                                         
       << catchmark("pi_h_f_s") << pi_h_f_s                                                                                 
       << catchmark("pi_h_u_s") << pi_h_u_s                                                                                 
       << catchmark("eh_s") << eh_s;                                        
}
