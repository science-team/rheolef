///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet_hdg_post.cc The Poisson problem by the hybrid discontinuous Galerkin method -- post-treatment
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod_dirichlet.h"
#include "dirichlet_hdg_average.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float n, beta;
  field uh, lambda_h, sigma_h;
  din >> catchmark("n")      >> n
      >> catchmark("beta")   >> beta
      >> catchmark("u")      >> uh
      >> catchmark("lambda") >> lambda_h
      >> catchmark("sigma")  >> sigma_h;
  field bar_uh = dirichlet_hdg_average (uh, lambda_h);
  const geo& omega = uh.get_geo();
  size_t d = omega.dimension();
  size_t k = uh.get_space().degree();
  space Xhs (omega, "P"+to_string(k+1)+"d"),
        Zhs (omega, "P0"),
        Yhs = Xhs*Zhs;
  trial x(Yhs); test y(Yhs);
  auto us = x[0], zeta = x[1];
  auto vs = y[0], xi   = y[1];
  integrate_option iopt;
  iopt.invert = true;
  form inv_ahs = integrate(dot(grad_h(us),grad_h(vs)) + zeta*vs + xi*us, iopt);
  field lhs = integrate (f(d)*vs + xi*bar_uh
                         + on_local_sides(dot(sigma_h,normal())*vs));
  field xhs = inv_ahs*lhs;
  dout << catchmark("n")      << n << endl
       << catchmark("beta")   << beta << endl
       << catchmark("u")      << xhs[0]
       << catchmark("lambda") << lambda_h
       << catchmark("sigma")  << sigma_h
       << catchmark("zeta")   << xhs[1];
}
