#ifndef _RHEOLEF_PIOLA_FEM_H
#define _RHEOLEF_PIOLA_FEM_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/piola.h"
#include "rheolef/space_constant.h"
#include "rheolef/smart_pointer.h"
namespace rheolef {

/*Class:piola_fem
NAME: @code{piola_fem} - maps a finite element method from a reference element
@cindex  piola transformation
@cindex  finite element method
SYNOPSIS:
  @noindent
  The @code{piola_fem} is an abstract class that defines how a finite element
  method could be map from a corresponding method defined on a reference element.
  See also the @ref{piola_fem_lagrange class} 
  and the the @ref{piola_fem_hdiv class} for concrete transformations
  and the @code{basis} class for finite element methods definied on a
  reference element.
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
   | Pierre.Saramito@imag.fr
DATE: 26 january 2019
End:
*/

// ---------------------------------------------------------------------------
// polymorphic abstract class
// ---------------------------------------------------------------------------
template<class T>
class piola_fem_rep {
public:
  typedef T                                   value_type;
  typedef typename point_basic<T>::size_type  size_type;
  piola_fem_rep() {}
  virtual ~piola_fem_rep() {}
  virtual std::string name() const = 0;

  virtual bool transform_need_piola() const { return true; }

  // scalar-valued:
  virtual void transform         (const piola<T>& p, const T& hat_u, T&     u) const;
  virtual void inv_transform     (const piola<T>& p, const T&     u, T& hat_u) const;
  virtual void grad_transform    (
          const piola<T>&                            p, 
          const T&                               hat_u, 
          const point_basic<T>&             hat_grad_u, 
	  const details::differentiate_option&    gopt,
                point_basic<T>&                 grad_u) const;

  // vector-valued:
  virtual void transform         (const piola<T>& p, const point_basic<T>& hat_u,       point_basic<T>&     u) const;
  virtual void inv_transform     (const piola<T>& p, const point_basic<T>&     u,       point_basic<T>& hat_u) const;
  virtual void grad_transform    (
          const piola<T>&                            p, 
          const point_basic<T>&                  hat_u, 
          const tensor_basic<T>&            hat_grad_u, 
	  const details::differentiate_option&    gopt,
                tensor_basic<T>&                grad_u) const;

  // tensor-valued:
  virtual void transform         (const piola<T>& p, const tensor_basic<T>& hat_u,       tensor_basic<T>&     u) const;
  virtual void inv_transform     (const piola<T>& p, const tensor_basic<T>&     u,       tensor_basic<T>& hat_u) const;
  virtual void grad_transform    (
          const piola<T>&                            p, 
          const tensor_basic<T>&                 hat_u, 
          const tensor3_basic<T>&           hat_grad_u, 
	  const details::differentiate_option&    gopt,
                tensor3_basic<T>&               grad_u) const;

  // tensor3-valued:
  virtual void transform         (const piola<T>& p, const tensor3_basic<T>& hat_u,      tensor3_basic<T>&     u) const;
  virtual void inv_transform     (const piola<T>& p, const tensor3_basic<T>&     u,      tensor3_basic<T>& hat_u) const;
  virtual void grad_transform    (
          const piola<T>&                            p, 
          const tensor3_basic<T>&                hat_u, 
          const tensor4_basic<T>&           hat_grad_u, 
	  const details::differentiate_option&    gopt,
                tensor4_basic<T>&               grad_u) const;

  // tensor4-valued:
  virtual void transform         (const piola<T>& p, const tensor4_basic<T>& hat_u,      tensor4_basic<T>&     u) const;
  virtual void inv_transform     (const piola<T>& p, const tensor4_basic<T>&     u,      tensor4_basic<T>& hat_u) const;
};

#define _RHEOLEF_inline_value_s(ValueName,Value)		 						\
template<class T>												\
void														\
piola_fem_rep<T>::transform (const piola<T>& p, const Value& hat_u, Value& u) const				\
{														\
  error_macro (ValueName<<"-valued "<<name()<<" transfom not implemented");					\
}														\
template<class T>												\
void														\
piola_fem_rep<T>::inv_transform (const piola<T>& p, const Value& u, Value& hat_u) const				\
{														\
  error_macro (ValueName<<"-valued "<<name()<<" inverse-transfom not implemented");				\
}

#define _RHEOLEF_inline_value_g(ValueName,Value,GradValue)	 						\
        _RHEOLEF_inline_value_s(ValueName,Value)		 						\
template<class T>												\
void														\
piola_fem_rep<T>::grad_transform (										\
          const piola<T>&                  p, 									\
          const Value&                     hat_u, 								\
          const GradValue&                 hat_grad_u, 								\
	  const details::differentiate_option& gopt,								\
                GradValue&                 grad_u) const							\
{														\
  error_macro (ValueName<<"-valued "<<name()<<" grad-transfom not implemented");				\
}

_RHEOLEF_inline_value_g("scalar",T,point_basic<T>)
_RHEOLEF_inline_value_g("vector",point_basic<T>,tensor_basic<T>)
_RHEOLEF_inline_value_g("tensor",tensor_basic<T>,tensor3_basic<T>)
_RHEOLEF_inline_value_g("tensor3",tensor3_basic<T>,tensor4_basic<T>)
_RHEOLEF_inline_value_s("tensor4",tensor4_basic<T>)
#undef _RHEOLEF_inline_value_s
#undef _RHEOLEF_inline_value_g

// ---------------------------------------------------------------------------
// smart pointer interface class
// ---------------------------------------------------------------------------
//<verbatim:
template<class T>
class piola_fem: public smart_pointer_nocopy<piola_fem_rep<T> > {
public:

// typedefs:

  typedef piola_fem_rep<T>              rep;
  typedef smart_pointer_nocopy<rep>     base;
  typedef typename rep::value_type      value_type;
  typedef typename rep::size_type       size_type;

// allocators:

  piola_fem(rep *p = 0);

// accessors:

  bool transform_need_piola() const;
  std::string name() const;

  template<class Value>
  void transform         (const piola<T>& p, const Value& hat_u, Value&     u) const;
  template<class Value>
  void inv_transform     (const piola<T>& p, const Value&     u, Value& hat_u) const;
  template<class Value, class GradValue>
  void grad_transform    (
          const piola<T>&                  p, 
          const Value&                     hat_u, 
          const GradValue&                 hat_grad_u, 
	  const details::differentiate_option& gopt,
                GradValue&                 grad_u) const;
};
//>verbatim:
// -----------------------------------------------------------
// inlined
// -----------------------------------------------------------
template<class T>
inline
piola_fem<T>::piola_fem(rep* p)
 : base(p)
{
}
template<class T>
inline
std::string
piola_fem<T>::name() const
{
  return base::data().name();
}
template<class T>
inline
bool
piola_fem<T>::transform_need_piola() const
{
  return base::data().transform_need_piola();
}
template<class T>
template<class Value>
inline
void
piola_fem<T>::transform (const piola<T>& p, const Value& hat_u, Value& u) const
{
  base::data().transform (p, hat_u, u);
}
template<class T>
template<class Value>
inline
void
piola_fem<T>::inv_transform (const piola<T>& p, const Value& u, Value& hat_u) const
{
  base::data().inv_transform (p, u, hat_u);
}
template<class T>
template<class Value, class GradValue>
inline
void
piola_fem<T>::grad_transform (
          const piola<T>&                  p,
          const Value&                     hat_u, 
          const GradValue&                 hat_grad_u, 
	  const details::differentiate_option& gopt,
                GradValue&                 grad_u) const
{
  base::data().grad_transform (p, hat_u, hat_grad_u, gopt, grad_u);
}

}// namespace rheolef
#endif // _RHEOLEF_PIOLA_FEM_H
