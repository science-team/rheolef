#ifndef _RHEOLEF_FUNCTOR_H
#define _RHEOLEF_FUNCTOR_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// ==========================================================================
// 
// utiities for field expressions
//
// author: Pierre.Saramito@imag.fr
//
// date: 4 september 2015
//

/*Class:functor
NAME:  @code{functor} - a function wrapper suitable for field expressions
DESCRIPTION:       
  @noindent
  A @emph{functor} is a class-function, i.e. a class that defines
  the @code{operator()}: 
  it can be used in place of an usual function.
  Moreover, functors can be used in Rheolef field expressions, mixed
  with fields (see @ref{field class}).
  For instance, assuming that @code{uh} is a field and @code{u_ex} is a functor:
  @example
    Float err_l1 = integrate (omega, abs(uh - uh_ex), qopt);
  @end example
  where @code{omega} denotes a mesh
  (see @ref{geo class})
  and @code{qopt} a quadrature formula
  (see @ref{quadrature_option class}).
  See also the @ref{integrate algorithm} function.
  An usual function @code{u_ex_f} cannot always be mixed so nicely
  in expressions, due to c++ language rules.
  For instance, the following exprtession is valid:
  @example
    Float err_l1 = integrate (omega, abs(uh - u_ex_f), qopt);
  @end example
  In some case, the compiler cannot build a field expression using 
  usual functionsn e.g.
  @example
    Float I = integrate (omega, 0.5*u_ex_f), qopt);
  @end example
  because @code{0.5*u_ex_f} is a direct algebraic operation between
  usual functions and flmoating points, that is not defined in the 
  c++ language.
  A way to circumvent this difficulty is to convert the usual function
  into a functor, as in:
  @example
    Float I = integrate (omega, 0.5*functor(u_ex_f)), qopt);
  @end example
AUTHOR: Pierre.Saramito@imag.fr
DATE:   12 march 2013
End:
*/
#include "rheolef/compiler.h"

namespace rheolef { 

//<doc:
template<class R, class... Args>
std::function<R(Args...)>
functor (R(*f)(Args...)) {
  return std::function<R(Args...)>(f); 
}
//>doc:
} // namespace rheolef
#endif // _RHEOLEF_FUNCTOR_H
