#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
Pk="P2"
nt=200
nu="1e-2"
echo "# Pk=$Pk nt=$nt nu=$nu"
echo "# n	err_l2_l2	err_linf_linf"
for n in 6 12 25 50 100 200 400 800 1600; do
  mkgeo_grid -e $n -a -4 -b 4  > line2.geo
  (./convect-Pk line2 $Pk $nu $nt 2>/dev/null | ./convect_error > /dev/null) 2> tmp.gdat
  err_l2_l2=`grep error_l2_l2 tmp.gdat | gawk '{print $4}'`
  err_linf_linf=`grep error_linf_linf tmp.gdat | gawk '{print $4}'`
  echo "$n	$err_l2_l2	$err_linf_linf"
done
