///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile diffusion_tensor.cc The tensor diffusion benchmark
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "diffusion_tensor_exact.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega            , argv[2], "tensor");
  space Wh (omega["boundary"], argv[2], "tensor");
  Xh.block("boundary");
  trial sigma (Xh); test tau (Xh);
  form  ah = integrate (ddot(sigma,tau) + dddot(grad(sigma), grad(tau)));
  field lh = integrate (ddot(chi(),tau));
  field sigma_h(Xh);
  sigma_h["boundary"] = interpolate(Wh, sigma_g());
  problem p (ah);
  p.solve (lh, sigma_h);
  dout << catchmark("sigma") << sigma_h;
}
