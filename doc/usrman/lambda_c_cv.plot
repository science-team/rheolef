set terminal cairolatex pdf color standalone
set output "lambda_c_cv.tex"

lambda_c = 3.51383071912516
fabs(x) = (x > 0) ? x : -x

set size square
set colors classic
set log xy
set xrange[1e-3:1]
set yrange[1e-10:1]
set key left at graph 0.07,0.95
set xlabel '[c]{\large $h$}'
set  label '[r]{\large $|\lambda_{c,h}-\lambda_c|$}' at graph -0.02,0.82

set xtics (\
        "[c]{$10^{-3}$}" 1e-3, \
        "[c]{$10^{-2}$}" 1e-2, \
        "[c]{$10^{-1}$}" 1e-1, \
        "[c]{$1$}" 1)
set ytics (\
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}"       1 )

graph_ratio = 3.0/10.0

# triangle a droite
slope_A = graph_ratio*2.0
xA =  0.27
yA =  0.62
dxA = 0.10
dyA = dxA*slope_A
set label "[l]{\\scriptsize $2$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a gauche
slope_B = graph_ratio*4.0
xB =  0.27
yB =  0.14
dxB = 0.10
dyB = dxB*slope_B
set label "[r]{\\scriptsize $4$}" at graph xB-0.02, yB+0.5*dyB right
set arrow from graph xB,     yB     to graph xB,     yB+dyB nohead
set arrow from graph xB,     yB+dyB to graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
slope_C = graph_ratio*6.0
xC =  0.65
yC =  0.26
dxC = 0.10
dyC = dxC*slope_C
set label "[l]{\\scriptsize $6=2k$}" at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

plot \
'lambda_c_cv.gdat' u (1./$1):(fabs($2-lambda_c)) t '[r]{$k=1$}' w lp lt 1 lc 1 lw 2, \
'lambda_c_cv.gdat' u (1./$1):(fabs($3-lambda_c)) t '[r]{$k=2$}' w lp lt 1 lc 3 lw 2, \
'lambda_c_cv.gdat' u (1./$1):(fabs($4-lambda_c)) t '[r]{$k=3$}' w lp lt 1 lc 4 lw 2

#pause -1
