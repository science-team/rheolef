#ifndef _CSR_TO_ASR_H
#define _CSR_TO_ASR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

namespace rheolef {
/*F:
NAME: csr_to_asr -- sequential sparse matrix convertion (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Convert sequential "csr" to sequential "asr" sparse matrix format.
ALGORITHM:
  csr_build_from_asr

  "input": the sparse asr matrix
  |   ia(0:nrow-1), a(0:nnz-1)
  |   ib(0:2*nrow+1), op(.)
  "ouput": the sparse csr matrix
  |   b(0:nnz-1)
  begin
  |   q := 0
  |   for i := 0 to nrow-1 do
  |     q := ib(2*i)
  |     for p := ia(i) to ia(i+1)-1 do
  |       b(q) := op(a(p))
  |       q := q + 1
  |     endfor
  |   endfor
  end
COMPLEXITY:
  Time and memory complexity is O(nnz).
NOTE:
  The output arrays are supposed to be coorectly allocated,
  i.e. large enough.

  The pointer array "ib" is taken as input parameter,
  since practical C++ "asr" implementation uses STL "map"
  for data storage. Thus "ib(.)" is an array of STL map,
  and this algorithm is only formal.
  METHODS: @csr_to_asr 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   22 march 1999
END:
*/
//<csr_to_asr:
template <
    class InputPtrIterator,
    class InputDataIterator,
    class UnaryOperation,
    class OutputPtrIterator>
OutputPtrIterator
csr_to_asr(
    InputPtrIterator  iter_ptr_a,
    InputPtrIterator  last_ptr_a,
    InputDataIterator iter_data_a,
    UnaryOperation    op,
    OutputPtrIterator iter_ptr_b)
{
    InputPtrIterator first_ptr_a = iter_ptr_a;
    InputDataIterator first_data_a = iter_data_a;
    InputPtrIterator next_ptr_a = iter_ptr_a;
    next_ptr_a++;
    while (next_ptr_a != last_ptr_a) {
	InputDataIterator last_data_a = (*next_ptr_a);
	while (iter_data_a != last_data_a) {
	    (*iter_ptr_b).insert(op(*iter_data_a));
	    iter_data_a++;
        }
	next_ptr_a++;
        iter_ptr_b++;
    }
    return iter_ptr_b;
}
//>csr_to_asr:
} // namespace rheolef
#endif // _CSR_TO_ASR_H
