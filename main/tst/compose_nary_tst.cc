///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compose supports n-ary functions and class-functions
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct f {
  typedef Float result_type;
  Float operator() (const Float& u, const Float& v, const Float& w) const {
    return u+v-w;
  }
};
Float u_f (const point& x) { return 1; }
struct u_s2: unary_function<point,Float> {
  Float operator() (const point& x) const { return 1; }
};
struct u_s {
  Float operator() (const point& x) const { return 1; }
};
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float tol = (argc > 2) ? atof(argv[2]) : 1e-10;
  geo omega (argv[1]);
  space Vh (omega, "P1");
  field uh (Vh, 1.0);
  field vh (Vh, 2.0);
  field wh (Vh, 4.0);
  field rh_valid (Vh, -1.0);
  field rh = lazy_interpolate(Vh, compose (f(),uh,vh,wh));
  Float err1 = field(rh-rh_valid).max_abs();
  dout << "err1 = " << err1 << endl;
  test v (Vh);
  field lh =  integrate(compose (f(),uh,vh,wh)*v);
  Float meas_omega =  integrate(omega);
  Float err2 = abs(dual(lh,uh) + meas_omega);
  dout << "err2 = " << err2 << endl;
  // DG: only compile-time test here
  space DGh (omega, "P1d");
  field uh_dg (DGh, 1.0);
  test v_dg (DGh);
  field lh2 = integrate("internal_sides",
	compose(f(), uh, inner(uh_dg), outer(uh_dg))*jump(v_dg));

  return (max(err1,err2) < tol) ? 0 : 1;
}
