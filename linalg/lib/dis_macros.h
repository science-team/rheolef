# ifndef _RHEO_DIS_MACRO_H
# define _RHEO_DIS_MACRO_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

#include "rheolef/diststream.h"
#include "rheolef/communicator.h"
namespace rheolef {

# ifndef _RHEOLEF_HAVE_MPI
# define dis_warning_macro(message) warning_macro(message) 
# define dis_trace_macro(message)   trace_macro(message) 
# else // _RHEOLEF_HAVE_MPI
# undef fatal_macro
# define fatal_macro(message) 						\
	{ 								\
	  if (boost::mpi::environment::initialized() && !boost::mpi::environment::finalized()) { \
	    std::cerr << "fatal{" << boost::mpi::communicator().rank() << "}(" \
	       << __FILE__ << "," << __LINE__ << "): " 			\
	       << message << std::endl << std::flush; 			\
	    boost::mpi::environment::abort (1); 				\
	  } else { 							 \
	    std::cerr << "fatal(" 					 \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	    exit(1);							\
	  }								\
	}

# undef  error_macro
# define error_macro(message) \
        fatal_macro(message)

# undef  warning_macro
# define warning_macro(message) 					 \
	{ 								 \
	  if (boost::mpi::environment::initialized() && !boost::mpi::environment::finalized()) { \
	    std::cerr << "warning{"<< boost::mpi::communicator().rank() << "}(" \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	  } else { 							 \
	    std::cerr << "warning(" 					 \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	  }								 \
	}

# define dis_warning_macro(message) 					\
	{ 								\
	  if (boost::mpi::environment::initialized() && !boost::mpi::environment::finalized()) { \
	    boost::mpi::communicator().barrier(); 				\
	    derr << "warning(" << __FILE__ << "," << __LINE__ 		\
		  << "): " << message << std::endl << std::flush; 	\
	    boost::mpi::communicator().barrier(); 				\
	  } else { 							 \
	    std::cerr << "warning(" 					 \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	  }								 \
	}

# undef assert_macro
# undef trace_macro
# ifdef _RHEOLEF_PARANO
#  define trace_macro(message) \
	{ 								 \
	  if (boost::mpi::environment::initialized() && !boost::mpi::environment::finalized()) { \
	    std::cerr << "trace{"<< boost::mpi::communicator().rank() << "}(" \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	  } else { 							 \
	    std::cerr << "trace(" 					 \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	  }								 \
	}
#  define dis_trace_macro(message) \
	{ 								\
	  if (boost::mpi::environment::initialized() && !boost::mpi::environment::finalized()) { \
	    boost::mpi::communicator().barrier(); 				\
	    derr << "trace(" << __FILE__ << "," << __LINE__ 		\
		  << "): " << message << std::endl << std::flush; 	\
	    boost::mpi::communicator().barrier(); 				\
	  } else { 							 \
	    std::cerr << "trace(" 					 \
	       << __FILE__ << "," << __LINE__ << "): " 			 \
	       << message << std::endl << std::flush; 			 \
	  }								 \
	}
#  define assert_macro(ok_condition, message) \
        { if (!(ok_condition)) fatal_macro(message); }
# else // _RHEOLEF_PARANO
#  define trace_macro(message)
#  define dis_trace_macro(message)
#  define assert_macro(ok_condition, message)
# endif // _RHEOLEF_PARANO
#ifdef TODO
#endif // TODO

# endif // _RHEOLEF_HAVE_MPI
} // namespace rheolef
# endif // _RHEO_DIS_MACRO_H
