#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
GEODIR=${SRCDIR}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# -------------------------
# 2d & 3d curved side
# -------------------------
L="
triangle_p10-v2
quadrangle_p9-v2
tetra_p9-v2
"
for geo in $L; do
 run "./geo_element_curved_ball_tst $GEODIR/$geo 2>/dev/null | diff -Bw $SRCDIR/geo_element_curved_ball-${geo}.plot.valid - >/dev/null"
 if test $? -ne 0; then status=1; fi
done
# ---------------------------
# 3d element with curved edge
# ---------------------------
L="
tetra_p9-v2
"
for geo in $L; do
 run "./geo_element_curved_ball_tst $GEODIR/$geo 9 -edge 2>/dev/null | diff -Bw $SRCDIR/geo_element_curved_ball-edge-${geo}.plot.valid - >/dev/null"
 if test $? -ne 0; then status=1; fi
done

exit $status
