namespace rheolef {
/** @mainpage notitle

Rheolef is a programming environment for solving partial differential equations (PDE)
by the finite element method (FEM).

Rheolef is both a **C++** library and a set of **unix** commands.

Rheolef expressions bases on the **variational formulation** of PDEs:
<p>

<table border="0">
  <caption></caption>
  <tr>
    <th>
      <table border="0">
        <tr>
          <th><img src="images/dirichlet-2d-P2.png" width=150 align=MIDDLE border=0></th>
        </tr>
        <tr>
          <th><img src="images/dirichlet-3d-mayavi-mix-fig.png" width=220 align=MIDDLE border=0></th>
        </tr>
      </table> 
    </th>
    <th>
      <img src="images/synthese-fig.png" width="90%" align=MIDDLE border=0>
    </th>
  <tr>
</table>
<p>
<center> 
      More?
      See the 
       <a href="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef.pdf">user's guide</a>
      (<a href="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef.pdf">pdf</A>, 15 Mo).
</center> 
<p>
Polynomial **space**, **field**, and bilinear **form**
are C++ types for variables.

They are combined in expressions, as you write it on the paper.

Combined together, as a Lego game, these bricks allow the user to solve
most complex nonlinear problems.

The **concision** and **readability** of codes written
with Rheolef is certainly a major keypoint of this environment. 
*/
} // namespace rheolef
