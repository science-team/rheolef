///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#undef  BUG
#ifndef BUG
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  space Vh (omega, argv[2]);
  field fh (Vh, 1.0);
  dout << fh;
}
#else // BUG
#include "rheolef/field.h"
#include "rheolef/field_expr_terminal.h"
namespace rheolef {
template<class T, class M, class Expr>
typename std::enable_if<
       details::is_field_expr_v2_nonlinear_arg<Expr>::value
  && ! details::has_field_rdof_interface<Expr>::value
  && ! details::is_field_function<Expr>::value
 ,void
>::type
my_interpolate (const space_basic<T,M>& Xh, const Expr& expr)
{
  std::cout << "case 1" << std::endl;
}
// 2.2. re-interpolation of fields and linear field expressions
//      for change of mesh, of approx, ect
//! @brief see the @ref interpolate_3 page for the full documentation
template <class T, class M, class Expr>
inline
typename std::enable_if<
       details::has_field_rdof_interface<Expr>::value
  && ! details::is_field<Expr>::value
 ,void
>::type
my_interpolate (const space_basic<T,M>& Xh, const Expr& expr)
{
  std::cout << "case 2" << std::endl;
}
//! @brief see the @ref interpolate_3 page for the full documentation
template<class T, class M>
void
my_interpolate (const space_basic<T,M>& X2h, const field_basic<T,M>& u1h)
{
  std::cout << "case 3" << std::endl;
}

// 2.3. function & functor
//! @brief see the @ref interpolate_3 page for the full documentation
template <class T, class M, class Expr>
inline
typename std::enable_if<
  details::is_field_function<Expr>::value
 ,void
>::type
my_interpolate (const space_basic<T,M>& Xh, const Expr& expr)
{
  std::cout << "case 4" << std::endl;
}
}//namespace rheolef

using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  derr << "is_field="<< details::is_field<field>::value << endl;
  derr << "is_field_wdof="<< details::is_field_wdof<field>::value << endl;
  derr << "is_field_rdof="<< details::is_field_rdof<field>::value << endl;
  derr << "has_field_wdof_interface="<< details::has_field_wdof_interface<field>::value << endl;
  derr << "has_field_rdof_interface="<< details::has_field_rdof_interface<field>::value << endl;
  derr << "is_field_expr_v2_nonlinear_arg="<< details::is_field_expr_v2_nonlinear_arg<field>::value << endl;
  derr << "is_field_expr_v2_nonlinear_arg<point>="<< details::is_field_expr_v2_nonlinear_arg<point>::value << endl;
  derr << "is_field_expr_v2_constant<point>="<< details::is_field_expr_v2_constant<point>::value << endl;
  my_interpolate (space(), field());
  using wt = typename details::field_expr_v2_nonlinear_terminal_wrapper_traits <field>::type;
  derr << "wt="<< pretty_typename_macro(wt) << endl;
#ifdef TODO
  using st = decltype(field()+field());
  derr << "st="<< pretty_typename_macro(st) << endl;
#endif // TODO
}
#endif // BUG
