\subsection{Non-homogeneous Dirichlet conditions}
\label{dirichlet-nh}%
\pbindex{Poisson}%
\cindex{boundary condition!Dirichlet}%

\subsubsection*{Formulation}
  We turn now to the case of a non-homogeneous 
  Dirichlet boundary conditions.
  Let $f \in H^{-1}(\Omega)$ and
  $g \in H^{\frac{1}{2}}(\partial \Omega)$.
  The problem writes:

  $(P_2)$ {\it find $u$, defined in $\Omega$ such that:}
  \begin{eqnarray*}
      -\Delta u &=& f \ {\rm in}\ \Omega \\
      u &=& g \ {\rm on}\ \partial \Omega
  \end{eqnarray*}
  The variational formulation of this problem expresses:

  $(VF_2)$ {\it find $u \in V$ such that:}
  $$
    a(u,v) = l(v), \ \forall v \in V_0
  $$
  where 
  \begin{eqnarray*}
    a(u,v) &=& \int_\Omega \nabla u . \nabla v \, {\rm d}x \\
    l(v) &=& \int_\Omega f\, v \, {\rm d}x \\
    V &=& \{ v \in H^1(\Omega); \ v_{|\partial \Omega} = g \} \\
    V_0 &=& H^1_0(\Omega)
  \end{eqnarray*}

\subsubsection*{Approximation}
  As usual, we introduce a mesh ${\cal T}_h$ of $\Omega$
  and the finite dimensional space $X_h$:
  $$
      X_h = \{ v \in H^1(\Omega); \
          v_{/K} \in P_k, \
          \forall K \in {\cal T}_h \}
  $$
  Then, we introduce:
  \begin{eqnarray*}
	V_h &=& \{ v \in X_h; \ v_{|\partial\Omega} = \pi_h(g) \} \\ 
	V_{0,h} &=& \{ v \in X_h; \ v_{|\partial\Omega} = 0 \} \\ 
  \end{eqnarray*}
\cindex{Lagrange!interpolation}%
  where $\pi_h$ denotes the Lagrange interpolation operator.
  The approximate problem writes:

  {\it $(VF_2)_h$: find $u_h\in V_h$ such that:}
  $$
      a(u_h,v_h) = l(v_h),
      \ \forall v_h \in V_{0,h}
  $$
% The computation of the right-hand side may be considered with
% attention since $f$ is not {\em a priori} piecewise polynomial.
  The following C++ code implement this problem in the \Rheolef\  environment.

% --------------------------
\myexamplelicense{dirichlet-nh.cc}
% --------------------------

  Let us choose $\Omega \subset \mathbb{R}^d$, $d=1,2,3$ with
  \begin{equation}
      f(x) = d\,\pi^2 {\displaystyle \prod_{i=0}^{d-1} \cos(\pi x_i)}
	\ \ \mbox{ and } \ \ 
      g(x) = {\displaystyle \prod_{i=0}^{d-1} \cos(\pi x_i)}
      \label{eq-cosinusprod}
  \end{equation}
\cindex{coordinate system!Cartesian}%
  Remarks the notation $x=(x_0,\ldots,x_{d-1})$ for the Cartesian coordinates in $\mathbb{R}^d$:
  since all arrays start at index zero in \code{C++} codes, and in order to avoid any
  mistakes between the code and the mathematical formulation, we also adopt
  this convention here.
  This choice of $f$ and $g$ is convenient, since the exact solution is known:
  \[
     u(x) = {\displaystyle \prod_{i=0}^{d-1} \cos(\pi x_i)}
  \]
\cindex{function!class-function object}%
  The following C++ code implement this problem by using 
  the concept of {\em function object},
  also called {\em class-function} (see e.g.~\citealp{MusSai-1996-1ed}).
  A convenient feature is the ability for function objects to store auxiliary parameters,
  such as the space dimension $d$ for $f$ here, or some constants, as $\pi$ for $f$ and $g$.

% ----------------------------------
\myexamplelicense{cosinusprod_laplace.h}
% ----------------------------------

\subsubsection*{Comments}
\clindex{Float}%
\clindex{point}%
  The class {\color{rheoleftypcolor} \code{point}} describes the coordinates 
  of a point $(x_0, \ldots, x_{d-1}) \in \mathbb{R}^d$ as a $d$-uplet 
  of {\color{rheoleftypcolor} \code{Float}}.
  The \code{Float} type is usually a \code{double}.
  This type depends upon the \Rheolef\ 
  configuration (see~\citealp{rheolef-home-page}, installation instructions),
  and could also represent some high precision floating point class.
  The {\tt dirichlet-nh.cc} code looks like the previous one
      {\tt dirichlet.cc} related to homogeneous boundary conditions.
  Let us comments the changes.
  The dimension $d$ comes from the geometry $\Omega$:
\begin{lstlisting}[numbers=none,frame=none]
  size_t d = omega.dimension();
\end{lstlisting}
  The linear form $l(.)$ is associated to the right-hand side $f$ and writes:
\begin{lstlisting}[numbers=none,frame=none]
  field lh = integrate (f(d)*v);
\end{lstlisting}
\cindex{functor}%
  Note that the function $f$ that depends upon the dimension $d$ parameter,
  is implemented by a \emph{functor}, i.e. a C++ \code{class} that possesses
  the \code{operator()} member function.
  The space $W_h$ of piecewise $P_k$ functions defined on the boundary $\partial\Omega$ 
  is defined by:
\begin{lstlisting}[numbers=none,frame=none]
  space Wh (omega["boundary"], argv[2]);
\end{lstlisting}
  where $P_k$ is defined via the second command line argument \code{argv[2]}.
\cindex{Lagrange!interpolation}%
\findex{interpolate}%
  This space is suitable for the Lagrange interpolation of $g$ on the boundary: 
\begin{lstlisting}[numbers=none,frame=none]
  uh ["boundary"] = interpolate(Wh, g(d));
\end{lstlisting}
  The values of the degrees of freedom
  related to the boundary are stored into the field \code{uh.b}, where 
  non-homogeneous Dirichlet conditions applies.
  The rest of the code is similar to the homogeneous Dirichlet case.

\subsubsection*{How to run the program}

  First, compile the program:
\begin{verbatim}
  make dirichlet-nh
\end{verbatim}
  Running the program is obtained from the homogeneous Dirichlet case,
  by replacing \code{dirichlet} by \code{dirichlet-nh}:
\begin{verbatim}
  mkgeo_grid -e 10 > line.geo
  ./dirichlet-nh line.geo P1 > line.field
  field line.field
\end{verbatim}
for the bidimensional case:
\begin{verbatim}
  mkgeo_grid -t 10 > square.geo
  ./dirichlet-nh square.geo P1 > square.field
  field square.field
\end{verbatim}
and for the tridimensional case:
\pindexopt{field}{-volume}%
\begin{verbatim}
  mkgeo_grid -T 10 > box.geo
  ./dirichlet-nh box.geo P1 > box.field
  field box.field -volume
\end{verbatim}
The optional \code{-volume} allows a 3D color light volume graphical rendering.
  Here, the \code{P1} approximation can be replaced by 
  \code{P2}, \code{P3}, etc, by modifying the command-line argument.

%\vfill
