///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
# include <rheolef/compiler.h>
#include <cstring>
using namespace rheolef;
using namespace std;
// 
// generate a mesh for a line
//
# define no(i) (i)

int nx = 10;
Float A = 0;
Float B = 1;

void usage (const char *prog)
{
        cerr << "uniform n-grid for the [a,b] interval" << endl
             << "usage: " << endl
             << prog << " "
	     << "[n="  << nx <<"] " 
	     << "[-a float="<< A <<"] "
	     << "[-b float="<< B <<"] "
	     << "[-[no]boundary] " 
	     << "[-[no]sides] " 
	     << "[-[no]region] " 
	     << "[-[no]corner] " 
	     << "[-v4] " 
	     << "[-[dis]ordered] " 
             << endl
             << "example: " << endl
             << prog << " 10" << endl;
	exit (1);
}
void missing_arg(const char* prog, const char* arg)
{
  cerr << prog << ": missing argument for option `" << arg << "'" << endl;
  usage(prog);
}
int main (int argc, char**argv)
{
    char* prog = argv[0];
    bool sides  = true;
    bool boundary = true;
    bool region = false;
    bool v4     = false;
    bool disordered = false;

    if (argc == 1) usage(prog);

    // machine-dependent precision
    int digits10 = numeric_limits<Float>::digits10;
    cout << setprecision(digits10);

    // default nb edge on x axis

    // parse command line 
    if (argc == 1) usage(prog);
    for (int i = 1; i < argc; i++) {
	     if (strcmp ("-a", argv[i]) == 0)       { if (i+1 >= argc) missing_arg(prog,argv[i]); A = atof(argv[++i]); }
	else if (strcmp ("-b", argv[i]) == 0)       { if (i+1 >= argc) missing_arg(prog,argv[i]); B = atof(argv[++i]); }

	else if (strcmp ("-sides", argv[i]) == 0)      { sides = true; }
	else if (strcmp ("-nosides", argv[i]) == 0)    { sides = false; }
	else if (strcmp ("-boundary", argv[i]) == 0)   { boundary = true; }
	else if (strcmp ("-noboundary", argv[i]) == 0) { boundary = false; }
	else if (strcmp ("-region", argv[i]) == 0)     { region = true; }
	else if (strcmp ("-noregion", argv[i]) == 0)   { region = false; }
	else if (strcmp ("-corner", argv[i]) == 0)     { sides = true; }
	else if (strcmp ("-nocorner", argv[i]) == 0)   { sides = false; }

	else if (strcmp ("-v4", argv[i]) == 0)     { v4 = true; }
	else if (strcmp ("-disordered", argv[i]) == 0) { disordered = true; }
	else if (strcmp ("-ordered", argv[i]) == 0) { disordered = false; }
	else                                  { nx = atoi(argv[i]); }
    }
    if (region && nx % 2 != 0) {
        cerr << prog << ": region: nx be an even number.\n";
        exit (1);
    }
    int np = nx+1;
    int ne = nx;

    // header
    cout << "#!geo" << endl 
         << endl
         << "mesh" << endl;
    if (!v4) {
      cout << "2 1 " << np << " " << ne << endl;
    } else {
      cout << "4" << endl
           << "header" << endl
           << " dimension 1" << endl
           << " nodes     " << np << endl
           << " edges     " << ne << endl
           << "end header" << endl;
    }
    cout << endl;
    
    // geometry
    for (int i = 0; i <= nx; i++)
   	cout << A + (B-A)*Float(i)/Float(nx) << endl;
    cout << endl;

    // connectivity
    for (int i = 0; i < nx; i++)
      if (!disordered || i%3==0)
	cout << "e " << no(i) << " " << no(i+1) << endl;
      else 
	cout << "e " << no(i+1) << " " << no(i) << endl;
    cout << endl;

    // domains = 2 points
    size_t dom_version = (v4 ? 2 : 1);
    if (boundary) {
        cout << "domain" << endl
             << "boundary" << endl
	     << dom_version << " 0 2" << endl
             << no(0) << endl
             << no(nx) << endl
	     ;
    }
    if (sides) {
        cout << "domain" << endl
             << "left" << endl
	     << dom_version << " 0 1" << endl
             << no(0) << endl
             << endl
	     << "domain" << endl
             << "right" << endl
	     << dom_version << " 0 1" << endl
             << no(nx) << endl
	     ;
    }
    cout << endl;

    if (region) {
	    cout << "domain" << endl
	         << "west"   << endl
		 << "1 1 " << nx/2 << endl;
	    for (int i = 0; i < nx/2; i++)
		cout << "e " << no(i) << " " << no(i+1) << endl;
	    cout << endl
	         << "domain" << endl
	         << "east"   << endl
		 << "1 1 " << nx/2 << endl;
	    for (int i = nx/2; i < nx; i++)
		cout << "e " << no(i) << " " << no(i+1) << endl;
	    cout << endl
	         << "domain" << endl
	         << "interface"   << endl
		 << dom_version << " 0 1" << endl
		 << no(nx/2) << endl
                 << endl;
    }
}
