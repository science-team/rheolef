#ifndef _RHEOLEF_EXPR_UTILITIES_H
#define _RHEOLEF_EXPR_UTILITIES_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// utilities for expressions: used by vec<T,M> and field_basic<T,M>
//
// author: Pierre.Saramito@imag.fr
//
// date: 14 september 2015
//
#include <functional>

namespace rheolef { namespace details {

// -------------------------------------------
// operators as functors
// -------------------------------------------

#define _RHEOLEF_generic_unary_syntax_functor(OP,NAME)			\
template <typename T = void> 						\
struct NAME;								\
									\
template <typename T>							\
struct NAME: public std::unary_function<T, T> {				\
  T operator() (const T& x) const { return OP x; }			\
};									\
									\
template<>								\
struct NAME<void> {							\
  template <typename T>							\
  auto operator() (T&& x) const						\
  noexcept (noexcept (OP std::forward<T>(x)))				\
          -> decltype(OP std::forward<T>(x))				\
  {            return OP std::forward<T>(x); }				\
};

#define _RHEOLEF_generic_binary_syntax_functor(OP,NAME)			\
template <typename T = void> 						\
struct NAME;								\
									\
template<typename T>							\
struct NAME: public std::binary_function<T, T, T> {			\
  T operator() (const T& x, const T& y) const { return x OP y; } 	\
};									\
									\
template<>								\
struct NAME<void> {							\
  template <typename T, typename U>					\
  auto operator() (T&& t, U&& y) const					\
  noexcept (noexcept (std::forward<T>(t) OP std::forward<U>(y)))	\
          -> decltype(std::forward<T>(t) OP std::forward<U>(y))		\
  {            return std::forward<T>(t) OP std::forward<U>(y); }	\
};

_RHEOLEF_generic_unary_syntax_functor  (+,generic_unary_plus)
_RHEOLEF_generic_unary_syntax_functor  (-,generic_negate)
_RHEOLEF_generic_binary_syntax_functor (+,generic_plus)
_RHEOLEF_generic_binary_syntax_functor (-,generic_minus)
_RHEOLEF_generic_binary_syntax_functor (*,generic_multiplies)
_RHEOLEF_generic_binary_syntax_functor (/,generic_divides)
#undef  _RHEOLEF_generic_unary_syntax_functor
#undef  _RHEOLEF_generic_binary_syntax_functor

template<class BinaryFunction, class A1>
struct generic_binder1st {
  generic_binder1st (const BinaryFunction& f, const A1& x1)
    : _f(f), _x1(x1) {}
  template<class A2>
  auto operator() (A2&& x2) const 
  // TODO: assume that both BinaryFunction and A1 have a default constructor...
  noexcept (noexcept (BinaryFunction() (A1(), std::forward<A2>(x2))))
          -> decltype(BinaryFunction() (A1(), std::forward<A2>(x2)))
  {                         return _f (_x1,   std::forward<A2>(x2)); }
protected:
  BinaryFunction  _f;
  A1              _x1;
};

template<class BinaryFunction, class A2>
struct generic_binder2nd {
  generic_binder2nd (const BinaryFunction& f, const A2& x2)
    : _f(f), _x2(x2) {}
  template<class A1>
  auto operator() (A1&& x1) const 
  // TODO: assume that both BinaryFunction and A2 have a default constructor...
  noexcept (noexcept (BinaryFunction() (std::forward<A1>(x1), A2())))
          -> decltype(BinaryFunction() (std::forward<A1>(x1), A2()))
  {                          return _f (std::forward<A1>(x1),_x2); }
protected:
  BinaryFunction  _f;
  A2              _x2;
};
// -------------------------------------------
// computed assignement as functors
// -------------------------------------------
// for = += -= *= /= etc
struct assign_op {
  template<class T, class U>
  void operator() (T &t, const U &u) const { t = u; }
};
struct plus_assign {
  template<class T, class U>
  void operator() (T &t, const U &u) const { t += u; }
};
struct minus_assign {
  template<class T, class U>
  void operator() (T &t, const U &u) const { t -= u; }
};
struct multiplies_assign {
  template<class T, class U>
  void operator() (T &t, const U &u) const { t *= u; }
};
struct divides_assign {
  template<class T, class U>
  void operator() (T &t, const U &u) const { t /= u; }
};
template<class ForwardIterator, class InputIterator, class OpAssign>
void
assign_with_operator (ForwardIterator first, ForwardIterator last, InputIterator iter_rhs, OpAssign op_assign) {
  for (; first != last; ++first, ++iter_rhs) {
    op_assign (*first, *iter_rhs);
  }
}
// -------------------------------------------
// misc
// -------------------------------------------
// for convenince: a dummy forward iterator that returns a constant
template <class T>
struct iterator_on_constant {

  typedef T  value_type;
  iterator_on_constant (const T& c) : _c(c) {}
  iterator_on_constant<T>& operator++ () { return *this; }
  value_type operator* () const { return _c; }
protected:
  T _c;
};

}} // namespace rheolef::details
#endif // _RHEOLEF_EXPR_UTILITIES_H
