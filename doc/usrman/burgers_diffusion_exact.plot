set terminal cairolatex pdf color standalone
set output "burgers_diffusion_exact.tex"

set size square
set colors classic
set sample 1000
#set key left bottom
#set key at graph 0.1,0.1
set xrange [-1:1]
set yrange [-0.1:2.5]
set xtics (-1,0,1)
set ytics (0,1,2)
set arrow from -1,0 to 1,0 nohead lc 0 lw 1
set xlabel '[c]{$x$}'
set  label '[l]{$u(t,x)$}' at graph 0.02,0.95

eps = 1e-1
x0=-0.5
u(t,x) = 1 - tanh((x-x0-t)/(2*eps))

plot \
 u(0,x) \
   t '[r]{$t=0$}' \
   w l ls 1 lc 0 lw 1, \
 u(1,x) \
   t '[r]{$t=1$}'\
   w l lc rgb "#990000" lw 2

#pause -1
