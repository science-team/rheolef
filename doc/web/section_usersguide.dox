namespace rheolef { /**
@page usersguide_page User's guide

The user's guide is an exhaustive presentation of @ref example_page :


> P. Saramito, Efficient C++ finite element computing with Rheolef, CNRS-CCSD ed., 2020. \n
> <a href="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef.pdf">https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef.pdf</a>

- <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/rheolef.pdf">pdf</A> (15 mb, 267 pages)

When using Rheolef in a publication, please cite:

    @book{rheolef,
      author = {Pierre Saramito},
      title = {Efficient C++ finite element computing with Rheolef},
      publisher = "CNRS-CCSD ed.",
      year = 2020,
      note = {\url{http://hal.archives-ouvertes.fr/cel-00573970}}
    }

For more about the mathematical and physical background, see also:

> P. Saramito,  <a href="https://membres-ljk.imag.fr/Pierre.Saramito/books/liste-books.html>Complex fluids: modeling and algorithms</a>, Springer, 2016. \n
> ISBN 978-3-319-44362-1

- <A HREF="http://www.springer.com/gp/book/9783319443614">paper</A> (57 euros, 276 pages, in English)
- <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/books/Sar-2016-cfma.pdf">pdf</A> (12 mb)
- <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/books/Sar-2016-cfma.epub">epub</A> (7 mb)
- <A HREF="https://www-ljk.imag.fr/membres/Pierre.Saramito/video_fnn/fnn.html">MOOC video</A> (21 hours, in French)

*/ } // namespace rheolef
