#ifndef PAIR_WITH_LINEAR_ALGEBRA_H
#define PAIR_WITH_LINEAR_ALGEBRA_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

namespace rheolef { namespace details {

template<class T1, class T2>
struct pair_with_linear_algebra: std::pair<T1,T2> {
  typedef std::pair<T1,T2> base;
  pair_with_linear_algebra (const T1& f=T1(), const T2& s=T2());
  pair_with_linear_algebra (const pair_with_linear_algebra<T1,T2>&);
  template<class T0>
  pair_with_linear_algebra<T1,T2>& operator*= (const T0& k);
};
template<class T1, class T2>
inline
pair_with_linear_algebra<T1,T2>::pair_with_linear_algebra (const T1& f, const T2& s)
 : base(f,s)
{
}
template<class T1, class T2>
inline
pair_with_linear_algebra<T1,T2>::pair_with_linear_algebra (const pair_with_linear_algebra<T1,T2>& x)
 : base(x)
{
}
template<class T1, class T2>
template<class T0>
inline
pair_with_linear_algebra<T1, T2>&
pair_with_linear_algebra<T1, T2>::operator*= (const T0& k) {
  base::first  *= k;
  base::second *= k;
  return *this;
}
template<class T1, class T2>
inline
pair_with_linear_algebra<T1, T2>
operator- (const pair_with_linear_algebra<T1,T2>& x) {
  return pair_with_linear_algebra<T1,T2> (-x.first, -x.second);
}
template<class T0, class T1, class T2>
inline
pair_with_linear_algebra<T1, T2>
operator* (const T0& k, const pair_with_linear_algebra<T1,T2>& x) {
  return pair_with_linear_algebra<T1,T2> (k*x.first, k*x.second);
}
template<class T1, class T2>
inline
pair_with_linear_algebra<T1, T2>
operator+ (const pair_with_linear_algebra<T1,T2>& x, const pair_with_linear_algebra<T1,T2>& y) {
  return pair_with_linear_algebra<T1,T2> (x.first + y.first, x.second + y.second);
}
template<class T1, class T2>
inline
pair_with_linear_algebra<T1, T2>
operator- (const pair_with_linear_algebra<T1,T2>& x, const pair_with_linear_algebra<T1,T2>& y) {
  return pair_with_linear_algebra<T1,T2> (x.first - y.first, x.second - y.second);
}

}} // namespace rheolef::details
#endif // PAIR_WITH_LINEAR_ALGEBRA_H
