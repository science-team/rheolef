///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_cavity.cc The Navier-Stokes equations on the driven cavity benchmark with the method of characteristics
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "navier_stokes_solve.icc"
#include "navier_stokes_criterion.icc"
#include "cavity.h"
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  if (argc < 2) {
    cerr << "usage: " << argv[0] << " <geo> <Re> <err> <n_adapt>" << endl;
    exit (1);
  }
  geo    omega (argv[1]);
  adapt_option options;
  Float  Re      = (argc > 2) ? atof(argv[2]) : 100;
  options.err    = (argc > 3) ? atof(argv[3]) : 1e-2;
  size_t n_adapt = (argc > 4) ? atoi(argv[4]) : 5;
  Float  delta_t = 0.05;
  options.hmin   = 0.004;
  options.hmax   = 0.1;
  space Xh = cavity::velocity_space (omega, "P2");
  space Qh (omega, "P1");
  field uh = cavity::velocity_field (Xh, 1.0);
  field ph (Qh, 0);
  field fh (Xh, 0);
  for (size_t i = 0; true; i++) {
    size_t max_iter = 1000;
    Float tol = 1e-5;
    navier_stokes_solve (Re, delta_t, fh, uh, ph, max_iter, tol, &derr);
    odiststream o (omega.name(), "field");
    o << catchmark("Re") << Re << endl
      << catchmark("delta_t") << delta_t << endl
      << catchmark("u")  << uh
      << catchmark("p")  << ph;
    o.close();
    if (i >= n_adapt) break;
    field ch = navier_stokes_criterion (Re,uh);
    omega = adapt (ch, options);
    o.open (omega.name(), "geo");
    o << omega;
    o.close();
    Xh = cavity::velocity_space (omega, "P2");
    Qh = space (omega, "P1");
    uh = cavity::velocity_field (Xh, 1.0);
    ph = field (Qh, 0);
    fh = field (Xh, 0);
  }
}
