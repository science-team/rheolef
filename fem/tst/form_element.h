#ifndef _RHEO_FORM_ELEMENT_H
#define _RHEO_FORM_ELEMENT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// for checking the spectral convergence exp(-ak)
// on the reference element
//
// author: Pierre.Saramito@imag.fr
//
// date: 1 october 2017
//
#include "rheolef/quadrature.h"
#include "rheolef/basis.h"

namespace rheolef {

// -----------------------------------------------------------------------------
// mass
// -----------------------------------------------------------------------------
template<class Value>
typename scalar_traits<Value>::type
product(const Value& a, const Value& b) { return a*b; }
template<class T>
T
product(const point_basic<T>& a, const point_basic<T>& b) { return dot(a,b); }

template<class T, class Value, class Basis>
void
build_mass_generic (
  const Basis&                            b,
  reference_element                       hat_K,
  Eigen::SparseMatrix<T,Eigen::RowMajor>& mass)
{
  typedef reference_element::size_type size_type;
  const T eps = 1e3*std::numeric_limits<T>::epsilon();
  quadrature_option qopt;
  qopt.set_order(2*b.degree());
  quadrature<Float> quad (qopt);
  size_type loc_ndof = b.ndof (hat_K);
  Eigen::Matrix<Value,Eigen::Dynamic,1> phi (loc_ndof);
  mass.resize (loc_ndof, loc_ndof);
  for (typename quadrature<T>::const_iterator iter_q = quad.begin(hat_K),
	last_q = quad.end(hat_K); iter_q != last_q; iter_q++) {	
    b.evaluate (hat_K, (*iter_q).x, phi);
    for (size_type loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
    for (size_type loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
      T coeff_w = (*iter_q).w*product(phi[loc_idof],phi[loc_jdof]);
      if (fabs(coeff_w) < eps) continue;
      mass.coeffRef(loc_idof,loc_jdof) += coeff_w;
    }}
  }
  // TODO: some zeros could appear as sum upon quadrature (e.g. Pk_sherwin):
  // => do a second pass to eliminate it
  mass.makeCompressed();
}
template<class T, class Basis>
void
build_mass (
  const Basis&                            b,
  reference_element                       hat_K,
  Eigen::SparseMatrix<T,Eigen::RowMajor>& mass)
{
  switch (b.valued_tag()) {
    case space_constant::scalar: build_mass_generic<T,T,Basis>              (b, hat_K, mass); break;
    case space_constant::vector: build_mass_generic<T,point_basic<T>,Basis> (b, hat_K, mass); break;
    default: error_macro("tensor-like mass_element test: not yet");
  }
}
// -----------------------------------------------------------------------------
// mass summed on element boundary, for "Pkd[sides]" tests
// -----------------------------------------------------------------------------
template<class T, class Basis>
void
build_mass_bdr (
  const Basis&                            b,
  reference_element                       tilde_K,
  Eigen::SparseMatrix<T,Eigen::RowMajor>& mass_bdr)
{
warning_macro("build_mass_bdr(0)...");
  typedef reference_element::size_type size_type;
  const T eps = 1e3*std::numeric_limits<T>::epsilon();
  quadrature_option qopt;
  qopt.set_order(2*b.degree());
  quadrature<Float> quad (qopt);
  size_type loc_ndof = b.ndof (tilde_K);
warning_macro("build_mass_bdr(1): loc_ndof="<<loc_ndof);
  Eigen::Matrix<T,Eigen::Dynamic,1> phi (loc_ndof);
  mass_bdr.resize (loc_ndof, loc_ndof);
  for (size_type loc_isid = 0, loc_nsid = tilde_K.n_side(); loc_isid < loc_nsid; ++loc_isid) {
    side_information_type sid (tilde_K, loc_isid);
    reference_element hat_S = sid.hat;
    T tilde_Js = tilde_K.side_measure(loc_isid)/measure(hat_S);
    for (typename quadrature<T>::const_iterator iter_q = quad.begin(hat_S),
	last_q = quad.end(hat_S); iter_q != last_q; iter_q++) {	
      T tilde_wq = tilde_Js*(*iter_q).w;
      if (b.option().is_trace_n()) {
        b.evaluate_on_side (tilde_K, sid, (*iter_q).x, phi);
      } else {
        b.evaluate         (tilde_K, (*iter_q).x, phi);
      }
      for (size_type loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
      for (size_type loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
        T coeff_w = tilde_wq*product(phi[loc_idof],phi[loc_jdof]);
        if (fabs(coeff_w) < eps) continue;
        mass_bdr.coeffRef(loc_idof,loc_jdof) += coeff_w;
      }}
    }
  }
warning_macro("build_mass_bdr done");
}
// -----------------------------------------------------------------------------
// grad_grad
// -----------------------------------------------------------------------------
template<class T, class Basis>
void
build_grad_grad (
  const Basis&                            b,
  reference_element                       hat_K,
  Eigen::SparseMatrix<T,Eigen::RowMajor>& grad_grad)
{
  typedef reference_element::size_type size_type;
  const T eps = 1e3*std::numeric_limits<T>::epsilon();
  quadrature_option qopt;
  qopt.set_order(2*b.degree());
  quadrature<Float> quad (qopt);
  size_type loc_ndof = b.ndof (hat_K);
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> grad_phi (loc_ndof);
  grad_grad.resize (loc_ndof, loc_ndof);
  for (typename quadrature<T>::const_iterator iter_q = quad.begin(hat_K),
	last_q = quad.end(hat_K); iter_q != last_q; iter_q++) {	
    b.grad_evaluate (hat_K, (*iter_q).x, grad_phi);
    for (size_type loc_idof = 0; loc_idof < loc_ndof; ++loc_idof) {
    for (size_type loc_jdof = 0; loc_jdof < loc_ndof; ++loc_jdof) {
      T coeff_w = (*iter_q).w*dot(grad_phi[loc_idof],grad_phi[loc_jdof]);
      if (fabs(coeff_w) < eps) continue; 
      grad_grad.coeffRef (loc_idof,loc_jdof) += coeff_w;
    }}
  }
}

} // namespace rheolef
#endif // _RHEO_FORM_ELEMENT_H
