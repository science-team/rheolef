#ifndef _RHEOLEF_FIELD_EXPR_H
#define _RHEOLEF_FIELD_EXPR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// field assignement from expresions, e.g.
//
//    field xh = expr;
//
// author: Pierre.Saramito@imag.fr
//
// date: 13 september 2015
//
// Notes:
//  check at compile time that the expression is affine
//  check at run time that it is also homogneous in terms of approximation space
//
// 1. field assignments
//    1.1. field assignment members
//    1.2. computed assignment
// 2. misc
//    2.1. duality product
//    2.2. form d = diag (expr)
//    2.3. output a linear expession

#include "rheolef/field_expr_recursive.h"

namespace rheolef {

// -------------------------------------------
// 1. field assignments
// -------------------------------------------
// 1.1. field assignment members
// -------------------------------------------
// 1.1.1 field
// -------------------------------------------
#ifdef TO_CLEAN
// uh = expr;
template<class T, class M>
template<class Expr>
inline
typename std::enable_if<     
       details::is_field_expr_affine_homogeneous<Expr>::value
  && ! details::is_field_expr_v2_constant       <Expr>::value
  && ! details::is_field                        <Expr>::value
 ,field_basic<T, M>&
>::type
field_basic<T,M>::operator= (const Expr& expr)
{
  space_basic<T,M> Xh;
  check_macro (expr.have_homogeneous_space (Xh),
    "field = expr; expr should have homogeneous space. HINT: use field = interpolate(Xh, expr)");
  if (get_space().name() != Xh.name()) {
    resize (Xh);
  }
  details::assign_with_operator (begin_dof(), end_dof(), expr.begin_dof(), details::assign_op());
  return *this;
}
// field uh = expr;
template<class T, class M>
template<class Expr, class Sfinae>
inline
field_basic<T,M>::field_basic (const Expr& expr)
 : _V (),
   _u (),
   _b (),
   _dis_dof_indexes_requires_update(true),
   _dis_dof_assembly_requires_update(false)
{
    operator= (expr);
}
#endif // TO_CLEAN
// ---------------------------------------------------------------------------
// 1.2. computed assignment
// ---------------------------------------------------------------------------

// uh -+= expr
// uh [i_comp] -+= expr; // note: requires a move &&
// uh [domain] -+= expr;
#define _RHEOLEF_field_expr_v2_op_assign_field(OP, FUNCTOR)			\
template<class T, class M, class Expr>						\
inline										\
typename std::enable_if<							\
  details::is_field_expr_affine_homogeneous<Expr>::value,			\
  field_basic<T,M>&								\
>::type										\
operator OP (field_basic<T,M>& uh, const Expr& expr)				\
{										\
  space_basic<T,M> Xh;								\
  check_macro (expr.have_homogeneous_space (Xh),				\
    "field [domain] " << #OP << " expr; expr should have homogeneous space. "	\
	<< "HINT: use field [domain] " << #OP << " interpolate(Xh, expr)");	\
  check_macro (uh.get_space().name() == Xh.name(), "field " << #OP << " field_expression : incompatible spaces " \
            << uh.get_space().name() << " and " << Xh.name());				\
  details::assign_with_operator (uh.begin_dof(), uh.end_dof(), expr.begin_dof(), FUNCTOR()); \
  return uh;									\
}

#define _RHEOLEF_field_expr_v2_op_assign_auxil(OP, FUNCTOR, NAME, IDX)		\
template<class FieldWdof, class FieldRdof>					\
inline										\
typename std::enable_if<							\
  details::is_field_expr_affine_homogeneous<FieldRdof>::value,			\
  NAME<FieldWdof>&								\
>::type										\
operator OP (NAME<FieldWdof>&& uh, const FieldRdof& expr)			\
{										\
  using space_type = typename FieldWdof::space_type;				\
  space_type Xh;								\
  check_macro (expr.have_homogeneous_space (Xh),				\
    "field [" << #IDX << "] " << #OP << " expr; expr should have homogeneous space. "	\
	<< "HINT: use field [" << #IDX << "] " << #OP << " interpolate(Xh, expr)");	\
  check_macro (uh.get_space().name() == Xh.name(), "field [" << #IDX << "] " << #OP << " field_expression : incompatible spaces " \
            << uh.get_space().name() << " and " << Xh.name());				\
  details::assign_with_operator (uh.begin_dof(), uh.end_dof(), expr.begin_dof(), FUNCTOR()); \
  return uh;									\
}

#define _RHEOLEF_field_expr_v2_op_assign(OP, FUNCTOR) 				\
        _RHEOLEF_field_expr_v2_op_assign_field(OP, FUNCTOR) 			\
        _RHEOLEF_field_expr_v2_op_assign_auxil(OP, FUNCTOR, details::field_wdof_sliced,   "i_comp")	\
        _RHEOLEF_field_expr_v2_op_assign_auxil(OP, FUNCTOR, details::field_wdof_indirect, "domain")

_RHEOLEF_field_expr_v2_op_assign (+=, details::plus_assign)
_RHEOLEF_field_expr_v2_op_assign (-=, details::minus_assign)
#undef _RHEOLEF_field_expr_v2_op_assign_field
#undef _RHEOLEF_field_expr_v2_op_assign_auxil
#undef _RHEOLEF_field_expr_v2_op_assign

// uh -+*/= c
// uh [i_comp] -+*/= c; // requires a move &&
// uh [domain] -+*/= c; // TODO
#define _RHEOLEF_field_expr_v2_op_assign_constant_field(OP, FUNCTOR)		\
template<class T, class M, class Expr>						\
inline										\
typename std::enable_if<							\
  details::is_field_expr_v2_constant<Expr>::value				\
 ,field_basic<T,M>&								\
>::type										\
operator OP (field_basic<T,M>& uh, const Expr& expr)				\
{										\
  details::assign_with_operator (uh.begin_dof(), uh.end_dof(), details::iterator_on_constant<Expr>(expr), FUNCTOR()); \
  return uh;									\
}

#define _RHEOLEF_field_expr_v2_op_assign_constant_auxil(OP, FUNCTOR, NAME, IDX)	\
template<class FieldWdof, class Expr>						\
inline										\
typename std::enable_if<							\
  details::is_field_expr_v2_constant<Expr>::value				\
 ,NAME<FieldWdof>&									\
>::type										\
operator OP (NAME<FieldWdof>&& uh, const Expr& expr)					\
{										\
  details::assign_with_operator (uh.begin_dof(), uh.end_dof(), details::iterator_on_constant<Expr>(expr), FUNCTOR()); \
  return uh;									\
}

#define _RHEOLEF_field_expr_v2_op_assign_constant(OP, FUNCTOR) 				\
        _RHEOLEF_field_expr_v2_op_assign_constant_field(OP, FUNCTOR) 			\
        _RHEOLEF_field_expr_v2_op_assign_constant_auxil(OP, FUNCTOR, details::field_wdof_sliced,   "i_comp")	\
        _RHEOLEF_field_expr_v2_op_assign_constant_auxil(OP, FUNCTOR, details::field_wdof_indirect, "domain")

_RHEOLEF_field_expr_v2_op_assign_constant (+=, details::plus_assign)
_RHEOLEF_field_expr_v2_op_assign_constant (-=, details::minus_assign)
_RHEOLEF_field_expr_v2_op_assign_constant (*=, details::multiplies_assign)
_RHEOLEF_field_expr_v2_op_assign_constant (/=, details::divides_assign)
#undef _RHEOLEF_field_expr_v2_op_assign_constant_field
#undef _RHEOLEF_field_expr_v2_op_assign_constant_auxil
#undef _RHEOLEF_field_expr_v2_op_assign_constant_auxil_old
#undef _RHEOLEF_field_expr_v2_op_assign_constant

// ---------------------------------------------------------------------------
// 2. misc
// ---------------------------------------------------------------------------
// 2.1. duality product
// ---------------------------------------------------------------------------
// dual (uh,vh)
template <class Expr1, class Expr2>
inline
typename
std::enable_if<
  details::is_field_expr_affine_homogeneous<Expr1>::value &&
  details::is_field_expr_affine_homogeneous<Expr2>::value,
  typename promote<
    typename Expr1::float_type,
    typename Expr2::float_type>::type
>::type
dual (const Expr1& expr1, const Expr2& expr2)
{
  typedef typename Expr1::float_type  T;
  typedef typename Expr1::memory_type M;
  space_basic<T,M> Xh1, Xh2;
  check_macro (expr1.have_homogeneous_space (Xh1),
    "dual(expr1,expr2); expr1 should have homogeneous space. HINT: use dual(interpolate(Xh, expr1),expr2)");
  check_macro (expr2.have_homogeneous_space (Xh2),
    "dual(expr1,expr2); expr2 should have homogeneous space. HINT: use dual(expr1,interpolate(Xh, expr2))");
  check_macro (Xh1.name() == Xh2.name(),
    "dual(expr1,expr2); incompatible \""<<Xh1.name()<<"\" and \""<<Xh2.name()<<" spaces for expr1 and expr2");
  return dis_inner_product (expr1.begin_dof(), expr2.begin_dof(), Xh1.ndof(), Xh1.ownership().comm(), M());
}
// dual (c,uh)
template <class Expr1, class Expr2>
inline
typename
std::enable_if<
  details::is_field_expr_v2_constant       <Expr1>::value &&
  details::is_field_expr_affine_homogeneous<Expr2>::value
 ,typename Expr2::float_type
>::type
dual (const Expr1& expr1, const Expr2& expr2)
{
  typedef typename Expr2::float_type  T;
  typedef typename Expr2::memory_type M;
  space_basic<T,M> Xh2;
  check_macro (expr2.have_homogeneous_space (Xh2),
    "dual(cte,expr2); expr2 should have homogeneous space. HINT: use dual(cte,interpolate(Xh, expr2))");
  return expr1*dis_accumulate (expr2.begin_dof(), Xh2.ndof(), Xh2.ownership().comm(), M());
}
// dual (uh,c) 
template <class Expr1, class Expr2>
inline
typename
std::enable_if<
  details::is_field_expr_affine_homogeneous<Expr1>::value &&
  details::is_field_expr_v2_constant       <Expr2>::value
 ,typename Expr1::float_type
>::type
dual (const Expr1& expr1, const Expr2& expr2)
{
  typedef typename Expr1::float_type  T;
  typedef typename Expr1::memory_type M;
  space_basic<T,M> Xh1;
  check_macro (expr1.have_homogeneous_space (Xh1),
    "dual(expr1,cte); expr1 should have homogeneous space. HINT: use dual(interpolate(Xh, expr1),cte)");
  return dis_accumulate (expr1.begin_dof(), Xh1.ndof(), Xh1.ownership().comm(), M())*expr2;
}
// ---------------------------------------------------------------------------
// 2.2. form d = diag (expr)
// ---------------------------------------------------------------------------
template<class Expr>
inline
typename
std::enable_if<
       details::has_field_rdof_interface<Expr>::value
  && ! details::is_field<Expr>::value
 ,form_basic <typename Expr::value_type, typename Expr::memory_type>
>::type
diag (const Expr& expr)
{
  typedef typename Expr::value_type  T;
  typedef typename Expr::memory_type M;
  return diag (field_basic<T,M>(expr));
}

} // namespace rheolef
#endif // _RHEOLEF_FIELD_EXPR_H
