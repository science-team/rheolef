#ifndef _RHEO_VEC_RANGE_H
#define _RHEO_VEC_RANGE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// example :
//   x[range(0,n)] = y;
//   x = y[range(0,m)];

# include "rheolef/vec.h"

namespace rheolef {

template <class T, class M = rheo_default_memory_model>
class vec_range {
public:

// typedef:

  typedef typename vec<T,M>::size_type           size_type;
  typedef typename vec<T,M>::difference_type	 difference_type;
  typedef range                                  range_type;
  typedef T			                 value_type;
  typedef typename vec<T,M>::iterator            iterator;
  typedef typename vec<T,M>::const_iterator      const_iterator;

// allocator/deallocator:

  vec_range (const vec<T,M>& u, const range_type& r) : _u(u), _r(r) {}
  vec_range (const vec_range_const<T,M>& ur) : _u(ur._u), _r(ur._r) {}

// accessors:

  size_type size() const { return _r.size(); }
  iterator begin() { return _u.begin() + _r.start(); }
  iterator end() { return begin() + size(); }
  const_iterator begin() const { return _u.begin() + _r.start(); }
  const_iterator end() const { return begin() + size(); }

protected:
  friend class vec<T,M>;
  friend class vec_range_const<T,M>;
// data:
  vec<T,M> _u;
  range    _r;
};
template <class T, class M = rheo_default_memory_model>
class vec_range_const {
public:

// typedef:

  typedef typename vec<T,M>::size_type           size_type;
  typedef typename vec<T,M>::difference_type	 difference_type;
  typedef range                                  range_type;
  typedef T			                 value_type;
  typedef typename vec<T,M>::const_iterator      const_iterator;

// allocator/deallocator:

  vec_range_const (const vec<T,M>& u, const range_type& r) : _u(u), _r(r) {}
  vec_range_const (const vec_range_const<T,M>& ur) : _u(ur._u), _r(ur._r) {}
  vec_range_const (const vec_range<T,M>& ur)       : _u(ur._u), _r(ur._r) {}

// accessors:

  size_type size() const { return _r.size(); }
  const_iterator begin() const { return _u.begin() + _r.start(); }
  const_iterator end() const { return begin() + size(); }

protected:
  friend class vec<T,M>;
// data:
  vec<T,M> _u;
  range    _r;
};

} // namespace rheolef
#endif // _RHEO_VEC_RANGE_H
