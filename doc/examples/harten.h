///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile harten.h The Burgers problem: the Harten exact solution
#include "harten0.h"
struct harten {
  Float operator() (const point& x) const {
    Float x0 = x[0]-a*t+c;
    Float shift = -2*floor((x0+1)/2);
    Float xs = x0 + shift;
    check_macro (xs >= -1 && xs <= 1, "invalid xs="<<xs);
    return a + b*h0 (point(xs));
  }
  harten (Float t1=0, Float a1=1, Float b1=0.5, Float c1=0):
    h0(b1*t1), t(t1), a(a1), b(b1), c(c1) {}
  Float M() const { Float pi = acos(-1.0); return sqr(pi)*b; }
  Float min() const { return a-b; }
  Float max() const { return a+b; }
protected:
  harten0 h0;
  Float t, a, b, c;
};
using u_init = harten;
using g = harten;
