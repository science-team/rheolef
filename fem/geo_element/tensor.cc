///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
//    2-tensor
//
//    author: Pierre.Saramito@imag.fr
//
#include "rheolef/tensor.h"
#include "rheolef/compiler_eigen.h"

using namespace rheolef;
using namespace std;
namespace rheolef { 


// output
template<class T>
ostream& 
tensor_basic<T>::put (ostream& out, size_type d) const
{
    switch (d) {
    case 0 : return out;
    case 1 : return out << _x[0][0];
    case 2 : return out << "[" << _x[0][0] << ", " << _x[0][1] << ";\n" 
	                << " " << _x[1][0] << ", " << _x[1][1] << "]";
    default: return out << "[" << _x[0][0] << ", " << _x[0][1] << ", " << _x[0][2] << ";\n" 
	                << " " << _x[1][0] << ", " << _x[1][1] << ", " << _x[1][2] << ";\n"
	                << " " << _x[2][0] << ", " << _x[2][1] << ", " << _x[2][2] << "]";
    }
}
template<class T>
istream&
tensor_basic<T>::get (istream& in)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        _x[i][j] = 0.;
    char c;
    if (!in.good()) return in;
    in >> ws >> c;
    // no tensor specifier: 
    // 1d case: "3.14"
    if (c != '[') {
        in.unget();
        return in >> _x[0][0];
    }
    // has a tensor specifier, as:
    //  	[  23, 17; 25, 2]
    in >> _x[0][0] >> ws >> c;
    if (c == ']') return in; // 1d case
    if (c != ',') error_macro ("invalid tensor input: expect `,'");
    in >> _x[0][1];
    in >> ws >> c;
    size_type d = 3;
    if (c == ',') d = 3;
    else if (c == ';') d = 2;
    else error_macro ("invalid tensor input: expect `,' or `;'");
    if (d == 3) {
        in >> _x[0][2] >> ws >> c;
        if (c != ';') error_macro ("invalid tensor input: expect `;'");
    }
    in >> _x[1][0] >> ws >> c;
    if (c != ',') error_macro ("invalid tensor input: expect `,'");
    in >> _x[1][1] >> ws >> c;
    if (d == 2) {
        if (c != ']') error_macro ("invalid tensor input: expect `]'");
	return in;
    }
    // d == 3
    if (c != ',') error_macro ("invalid tensor input: expect `,'");
    in >> _x[1][2] >> ws >> c;
    if (c != ';') error_macro ("invalid tensor input: expect `;'");
    in >> _x[2][0] >> ws >> c;
    if (c != ',') error_macro ("invalid tensor input: expect `,'");
    in >> _x[2][1] >> ws >> c;
    if (c != ',') error_macro ("invalid tensor input: expect `,'");
    in >> _x[2][2] >> ws >> c;
    if (c != ']') error_macro ("invalid tensor input: expect `]'");
    return in;
}
template<class T>
bool
tensor_basic<T>::operator== (const tensor_basic<T>& b) const
{
  for (size_type i = 0; i < 3; i++)
  for (size_type j = 0; j < 3; j++)
    if (_x[i][j] != b._x[i][j]) return false;
  return true;
}
template<class T>
tensor_basic<T> 
tensor_basic<T>::operator- () const
{
    tensor_basic<T> b;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        b._x[i][j] = - _x[i][j];
    return b;
}
template<class T>
tensor_basic<T> 
tensor_basic<T>::operator+ (const tensor_basic<T>& b) const
{
    tensor_basic<T> c;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        c._x[i][j] = _x[i][j] + b._x[i][j];
    return c;
}
template<class T>
tensor_basic<T>
tensor_basic<T>::operator- (const tensor_basic<T>& b) const
{
    tensor_basic<T> c;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        c._x[i][j] = _x[i][j] - b._x[i][j];
    return c;
}
template<class T>
tensor_basic<T>
tensor_basic<T>::operator* (const T& k) const
{
    tensor_basic<T> b;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        b._x[i][j] = _x[i][j] * k;
    return b;
}
template<class T>
point_basic<T>
tensor_basic<T>::operator* (const point_basic<T>& x) const
{
    point_basic<T> y;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        y [i] += _x[i][j] * x[j];
    return y;
}
template<class T>
point_basic<T> operator* (const point_basic<T>& x, const tensor_basic<T>& a)
{
    point_basic<T> y;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        y [i] += a._x[j][i] * x[j];
    return y;
}
template<class T>
tensor_basic<T>&
tensor_basic<T>::operator+= (const tensor_basic<T>& b)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        _x[i][j] += b._x[i][j];
    return *this;
}
template<class T>
tensor_basic<T>&
tensor_basic<T>::operator-= (const tensor_basic<T>& b)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        _x[i][j] -= b._x[i][j];
    return *this;
}
template<class T>
tensor_basic<T>&
tensor_basic<T>::operator*= (const T& k)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        _x[i][j] *= k;
    return *this;
}
template<class T>
tensor_basic<T>&
tensor_basic<T>::operator/= (const T& k)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
        _x[i][j] /= k;
    return *this;
}
template<class T>
tensor_basic<T> trans (const tensor_basic<T>& a, size_t d)
{
    tensor_basic<T> b;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < d; i++)
    for (size_type j = 0; j < d; j++)
        b._x[i][j] = a._x[j][i];
    return b;
}
template<class T>
tensor_basic<T> inv (const tensor_basic<T>& a, size_t d)
{
  tensor_basic<T> b;
  typedef typename tensor_basic<T>::size_type size_type;
  switch (d) {
    case 0: 
      break;
    case 1: 
      b(0,0) = 1/a(0,0);
      break;
    case 2: {
      T det = a.determinant(2);
      b(0,0) =   a(1,1)/det;
      b(1,1) =   a(0,0)/det;
      b(0,1) = - a(0,1)/det;
      b(1,0) = - a(1,0)/det;
      break;
    }
    case 3:
    default: {
      T det = a.determinant(3);
      b(0,0) = (a(1,1)*a(2,2) - a(1,2)*a(2,1))/det;
      b(0,1) = (a(0,2)*a(2,1) - a(0,1)*a(2,2))/det;
      b(0,2) = (a(0,1)*a(1,2) - a(0,2)*a(1,1))/det;
      b(1,0) = (a(1,2)*a(2,0) - a(1,0)*a(2,2))/det;
      b(1,1) = (a(0,0)*a(2,2) - a(0,2)*a(2,0))/det;
      b(1,2) = (a(0,2)*a(1,0) - a(0,0)*a(1,2))/det;
      b(2,0) = (a(1,0)*a(2,1) - a(1,1)*a(2,0))/det;
      b(2,1) = (a(0,1)*a(2,0) - a(0,0)*a(2,1))/det;
      b(2,2) = (a(0,0)*a(1,1) - a(0,1)*a(1,0))/det;
      break;
    }
  }
  return b;
}
template<class T>
void
prod (const tensor_basic<T>& a, const tensor_basic<T>& b, tensor_basic<T>& result,
                size_t di, size_t dj, size_t dk)
{
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < di; i++)
    for (size_type j = 0; j < dj; j++) {
      T sum = 0;
      for (size_type k = 0; k < dk; k++)
        sum += a(i,k) * b(k,j);
      result(i,j) = sum;
    }
}
template<class T>
tensor_basic<T>
tensor_basic<T>::operator* (const tensor_basic<T>& b) const
{
    tensor_basic<T> c;
    prod (*this,b,c,3,3,3);
    return c;
}
//! @brief ddot(x,y): see the @ref expression_3 page for the full documentation
template<class T>
T ddot (const tensor_basic<T>& a, const tensor_basic<T> & b)
{
    T r = 0;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++) 
    for (size_type j = 0; j < 3; j++)
        r += a._x[i][j] * b._x[i][j];
    return r;
}
template<class T>
T tensor_basic<T>::determinant (size_type d) const
{
    switch (d) {
      case 0 : return 1;
      case 1 : return _x[0][0];
      case 2 : return _x[0][0]*_x[1][1] - _x[0][1]*_x[1][0];
      case 3 : return _x[0][0]*(_x[1][1]*_x[2][2] - _x[1][2]*_x[2][1])
	            - _x[0][1]*(_x[1][0]*_x[2][2] - _x[1][2]*_x[2][0])
	            + _x[0][2]*(_x[1][0]*_x[2][1] - _x[1][1]*_x[2][0]);
      default: {
	error_macro ("determinant: unexpected dimension " << d);
      }
    }
}
// t += a otimes b
template<class T>
void
cumul_otimes (tensor_basic<T>& t, const point_basic<T>& a, const point_basic<T>& b, size_t na, size_t nb)
{
  for (size_t i = 0; i < na; i++)
    for (size_t j = 0; j < nb; j++)
      t(i,j) += a[i] * b[j];
}
template<class T>
point_basic<T>
tensor_basic<T>::row(size_type i) const
{
    point_basic<T> r;
    r[0] = _x[i][0];
    r[1] = _x[i][1];
    r[2] = _x[i][2];
    return r;
}
template<class T>
point_basic<T>
tensor_basic<T>::col(size_type j) const
{
    point_basic<T> c;
    c[0] = _x[0][j];
    c[1] = _x[1][j];
    c[2] = _x[2][j];
    return c;
}
template<class T>
bool
invert_3x3 (const tensor_basic<T>& A, tensor_basic<T>& result)
{
  T det = determinant(A,3);
  if (1+det == 1) return false;
  T invdet = 1.0/det;
  result(0,0) =  (A(1,1)*A(2,2)-A(2,1)*A(1,2))*invdet;
  result(0,1) = -(A(0,1)*A(2,2)-A(0,2)*A(2,1))*invdet;
  result(0,2) =  (A(0,1)*A(1,2)-A(0,2)*A(1,1))*invdet;
  result(1,0) = -(A(1,0)*A(2,2)-A(1,2)*A(2,0))*invdet;
  result(1,1) =  (A(0,0)*A(2,2)-A(0,2)*A(2,0))*invdet;
  result(1,2) = -(A(0,0)*A(1,2)-A(1,0)*A(0,2))*invdet;
  result(2,0) =  (A(1,0)*A(2,1)-A(2,0)*A(1,1))*invdet;
  result(2,1) = -(A(0,0)*A(2,1)-A(2,0)*A(0,1))*invdet;
  result(2,2) =  (A(0,0)*A(1,1)-A(1,0)*A(0,1))*invdet;
  return true;
}
template<class T>
bool
tensor_basic<T>::is_symmetric (size_t d) const {
  if (d <= 1) return true;
  static T tol = 1e3*std::numeric_limits<T>::epsilon();
  if (d <= 2) return fabs (_x[0][1] - _x[1][0]) < tol;
  return (fabs (_x[0][1] - _x[1][0]) < tol)
      && (fabs (_x[0][2] - _x[2][0]) < tol)
      && (fabs (_x[1][2] - _x[2][1]) < tol);
}
// ------------------------------------------
// eigenvalues: the 3D case
// ------------------------------------------
template<class T>
static point_basic<T> eig3x3 (const tensor_basic<T>& a, tensor_basic<T>& q)
{
  using namespace Eigen;
  Matrix<T,3,3> a1;
  for (size_t i = 0; i < 3; ++i)
  for (size_t j = 0; j < 3; ++j) a1(i,j) = a(i,j);
  SelfAdjointEigenSolver<Matrix<T,3,3> > es (a1);
  point lambda;
  for (size_t i = 0; i < 3; ++i) {
    lambda[i] = es.eigenvalues()(i);
    for (size_t j = 0; j < 3; ++j) {
      q(i,j) = es.eigenvectors()(i,j);
    }
  }
  return lambda;
}
// ------------------------------------------
// eigenvalues: the 2D case
// ------------------------------------------
template<class T>
static point_basic<T> eig2x2 (const tensor_basic<T>& a, tensor_basic<T>& q)
{
    point_basic<T> d (0,0,0);
    q.fill (0);
    if (a(0,1) == 0) {
      // a is already diagonal
      if (a(0,0) >= a(1,1)) {
        d[0] = a(0,0);
        d[1] = a(1,1);
        q(0,0) = q(1,1) = 1.;
        q(0,1) = q(1,0) = 0.;
      } else { // swap column 0 & 1:
        d[1] = a(0,0);
        d[0] = a(1,1);
        q(0,0) = q(1,1) = 0.;
        q(0,1) = q(1,0) = 1.;
      }
      return d;
    }
    // here a(0,1) != 0
    T discr = sqr(a(1,1) - a(0,0)) + 4*sqr(a(0,1));
    T trace = a(0,0) + a(1,1);
    d[0] = (trace + sqrt(discr))/2; 
    d[1] = (trace - sqrt(discr))/2;
    T lost_precision = 1e-6;
    if (fabs(d[0]) + lost_precision*fabs(d[1]) == fabs(d[0])) { // d[1] == 0 up to machine precision
	d[1] = 0.;
    }
    T c0 = (d[0]-a(0,0))/a(0,1);
    T n0 = sqrt(1+sqr(c0));
    q(0,0) =  1/n0;
    q(1,0) = c0/n0;
    T c1 = (d[1]-a(0,0))/a(0,1);
    T n1 = sqrt(1+sqr(c1));
    q(0,1) =  1/n1;
    q(1,1) = c1/n1;
    return d;
}
// ------------------------------------------
// eigenvalues: general case
// ------------------------------------------
template<class T>
point_basic<T>
tensor_basic<T>::eig (tensor_basic<T>& q, size_t d) const
{
  check_macro (is_symmetric(d), "eig: tensor should be symmetric");
  switch (d) {
    case 1 : {
      point_basic<T> d;
      q(0,0) = 1; d[0] = operator()(0,0); return d;
    }
    case 2 : return eig2x2 (*this, q);
    default: return eig3x3 (*this, q);
  }
}
template<class T>
point_basic<T> 
tensor_basic<T>::eig (size_t d) const
{
  tensor_basic<T> q;
  return eig (q, d);
}
// ------------------------------------------
// svd: the 3D case
// ------------------------------------------
template<class T, int N>
static
point_basic<T>
eigen_svd (const tensor_basic<T>& a, tensor_basic<T>& u, tensor_basic<T>& v)
{
  using namespace Eigen;
  Matrix<T,N,N> a1;
  for (size_t i = 0; i < N; ++i)
  for (size_t j = 0; j < N; ++j) a1(i,j) = a(i,j);
  JacobiSVD<Matrix<T,N,N> > svd (a1, ComputeFullU | ComputeFullV);
  point s;
  for (size_t i = 0; i < N; ++i) {
    s[i] = svd.singularValues()(i);
    for (size_t j = 0; j < N; ++j) {
      u(i,j) = svd.matrixU()(i,j);
      v(i,j) = svd.matrixV()(i,j);
    }
  }
  return s;
}
template<class T>
point_basic<T>
tensor_basic<T>::svd (tensor_basic<T>& u, tensor_basic<T>& v, size_t d) const
{
  if (d == 1) {
    u(0,0) = v(0,0) = 1;
    return point(operator()(0,0));
  }
  if (d == 2) return eigen_svd<T,2> (*this, u, v);
  else        return eigen_svd<T,3> (*this, u, v);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template class tensor_basic<T>;							\
template point_basic<T>  operator* (const point_basic<T>& x, const tensor_basic<T>& a);		\
template tensor_basic<T> trans (const tensor_basic<T>&, size_t);			 	\
template tensor_basic<T> inv (const tensor_basic<T>&, size_t);					\
template void prod (const tensor_basic<T>& a, const tensor_basic<T>& b, tensor_basic<T>& result, \
		    size_t di, size_t dj, size_t dk);						\
template T ddot (const tensor_basic<T>& a, const tensor_basic<T> & b);			\
template void cumul_otimes (tensor_basic<T>& t, const point_basic<T>& a, const point_basic<T>& b, size_t na, size_t nb); \
template bool invert_3x3 (const tensor_basic<T>& A, tensor_basic<T>& result);			\

_RHEOLEF_instanciation(Float)

}// namespace rheolef
