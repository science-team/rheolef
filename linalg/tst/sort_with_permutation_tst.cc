///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// check quick sort with permutation
#include "rheolef/msg_sort_with_permutation.h"
using namespace rheolef;
#include <vector>
#include <iostream>
using namespace std;
int main () {
  size_t n;
  cin >> n;
  std::vector<double> v(n,0);
  for (size_t i = 0; i < n; ++i) cin >> v[i];
  std::vector<size_t> p1(n,0), p2(n,0);
  for (size_t i = 0; i < n; ++i) p2[i] = p1[i] = i;
  bubble_sort_with_permutation  (v.begin(), p1.begin(), n);
   quick_sort_with_permutation  (v.begin(), p2.begin(), size_t(0), n);
  for (size_t i = 0; i < n; ++i) {
    if (p1[i] != p2[i]) {
      cerr << "sort: error when i="<<i<<endl;
      return 1;
    }
  }
  return 0;
}
