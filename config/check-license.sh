#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
echo "checking for copyright mention in source files..." 1>&2
dir="."
if test $# -eq 0; then
  L=`/bin/ls`
else
  if test x"$1" = x"-d"; then
    dir=$2
    shift; shift
  fi
  L=$*
fi
status=0
for x in $L; do
  if grep -i "copyright[ \t][ \t]*(c)" $dir/$x >/dev/null; then
    true;
  else 
    if test $status -eq 0; then
      echo "$0: the copyright notice is missing for the following files:" 1>&2
    fi
    echo $x;
    status=1
  fi
done
exit $status
