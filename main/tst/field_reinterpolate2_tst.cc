///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// mkgeo_grid -t 5 > square5.geo
// mkgeo_grid -t 7 > square7.geo
// mpirun -np 2 ./bug_noe square5.geo square7.geo 
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) {
  environment rheolef(argc,argv);
  geo omega1 (argv[1]);
  space Xh1 (omega1, "P1");
  field fh1 (Xh1,1.);
  geo omega2 (argv[2]);
  space Xh2 (omega2, "P1");
  trial u2(Xh2);
  test  v2(Xh2);
  field fh2 = lazy_interpolate(Xh2,1.*fh1);
  form m2 = integrate(u2*v2);
}
