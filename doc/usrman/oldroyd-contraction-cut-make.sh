#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
input=${1-"oldroyd-contraction3-zr.branch.gz"}
mark="u"; comp="0"
#mark="tau"; comp="00"
domain="axis"
output="output"

rm -f $output.gdat $output.plot

echo "# $input" >> $output.gdat
if test $domain = "axis"; then
  echo "set xrange [-4:8]"  >> $output.plot
fi
echo "plot \\"  >> $output.plot

branch -toc $input 2>/dev/null| tail --lines=+2 > tmp-toc.txt
n=`cat tmp-toc.txt | wc -l`
We_list=`cat tmp-toc.txt | gawk '{print $2}'`
i=0
while test $i -lt $n; do
  We=`echo $We_list | gawk '{print $1}'`
  We_list=`echo $We_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
  branch $input -extract $i 2>/dev/null | field - -field -name tmp -gnuplot -noclean -noexecute -domain $domain -mark $mark -comp $comp 2>/dev/null
  echo "# We $We" >> $output.gdat
  cat tmp.gdat >> $output.gdat
  (echo; echo) >> $output.gdat
  echo "'output.gdat' i $i u 1:3 t 'We=$We' w lp, \\" >> $output.plot
  i=`expr $i + 1`
done
echo >> $output.plot
echo "pause -1" >> $output.plot
echo "! file $output.gdat created"
echo "! file $output.plot created"

command="gnuplot output.plot"
echo "! $command"
eval   "$command"

