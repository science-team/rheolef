set terminal cairolatex pdf color standalone
set output "navier-stokes-cut-u1.tex"
#
# data NS cavite avec [GhiGhiShi-1982] Fig. 1, page 5
# USAGE:
# ./new-navier-stokes-cavity-cahcha square-Re\=100.geo 100 0.2 5 
# mfield square-Re\=100-5.mfield.gz -u1 |field -cut -normal 0 1 -origin 0 0.5 -noclean -gnuplot -

set size square
set colors classic
set  label '[l]{\Large $u_1(x_0,0.5)$}' at graph 0.04,0.91
set xlabel '[c]{\Large $x_0$}'
set xtics (0,0.5,1)
set ytics (-0.5,0,0.5)
set key left bottom Left reverse at graph 0.01,0.05

plot [0:1][-0.65:0.65] \
  "navier-stokes-Re\=100-dt=0.05-t2-2-cut-u1.gdat" \
	title "[l]{$Re=100$}" w l dt 1 lc 1 lw 2, \
  "navier-stokes-bench-cut-u1.gdat" u 2:3 \
	title "[l]{$Re=100$, data}" w p ps 0.5 pt 7 lc 1, \
  "navier-stokes-Re\=400-dt\=0.05-t2-2-cut-u1.gdat" \
	title "[l]{$Re=400$}" w l dt 2 lc rgb '#008800' lw 2, \
  "navier-stokes-bench-cut-u1.gdat" u 2:4 \
	title "[l]{$Re=400$, data}" w p ps 0.5 pt 9 lc rgb '#008800', \
  "navier-stokes-Re\=1000-dt\=0.02-t2-2-cut-u1.gdat" \
	title "[l]{$Re=1000$}" w l dt 3 lc 3 lw 2, \
  "navier-stokes-bench-cut-u1.gdat" u 2:5 \
	title "[l]{$Re=1000$, data}" w p ps 0.5 pt 9 lc 3

#pause -1 "<return>"


