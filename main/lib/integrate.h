#ifndef _RHEO_INTEGRATE_H
#define _RHEO_INTEGRATE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@functionfile integrate expression integration

Synopsis
========

        template <typename Expression>
        Value integrate (geo domain, Expression, integrate_option iopt);

Description
===========
This overloaded function is able to return either a scalar constant,
a @ref field_2 or a bilinear @ref form_2, depending upon its arguments.

-# When the expression involves both `trial` and @ref test_2 functions,
   the result is a bilinear @ref form_2
-# When the expression involves either a `trial` or a @ref test_2 function,
   the result is a linear form, represented by the @ref field_2 class
-# When the expression involves neither a `trial` nor a @ref test_2 function,
   the result is a scalar constant

The general call involves three arguments:

-# the @ref geo_2 domain of integration
-# the expression to integrate
-# the @ref integrate_option_3 

Here is the overloaded synopsis:

        Float integrate (geo domain, Expression, integrate_option iopt);
        field integrate (geo domain, Expression, integrate_option iopt);
        form  integrate (geo domain, Expression, integrate_option iopt);

Omitted arguments
=================
Some argument could be omitted when the expression
involves a @ref test_2 function:

- when the domain of integration is omitted, then
  it is taken as those of the @ref test_2 function

The reduced synopsis is:

 	field integrate (Expression, integrate_option iopt);
 	form  integrate (Expression, integrate_option iopt);

- when the @ref integrate_option_3 is omitted, then
  a Gauss quadrature formula is considered
  such that it integrates exactly `2*k+1` polynomials
  where `k` is the polynomial degree of the @ref test_2 function.
  When a `trial` function is also involved, then
  this degree is `k1+k2+1` where `k1` and `k2` are
  the polynomial degree of the @ref test_2 and `trial` functions.

The reduced synopsis is:

 	field integrate (geo domain, Expression);
 	form  integrate (geo domain, Expression);

Both arguments could be omitted an the synopsis becomes:

	field integrate (Expression);
 	form  integrate (Expression);

Integration over a subdomain
============================
Let `omega` be a finite element mesh of a geometric domain,
as described by the @ref geo_2 class.
A subdomain is defined by indexation, e.g. `omega["left"]`
and, when a @ref test_2 function is involved, the `omega`
could be omitted, and only the string `"left"` has to be present
e.g.

	test v (Xh);
	field lh = integrate ("left", 2*v);

is equivalent to

	field lh = integrate (omega["left"], 2*v);

Measure of a domain
===================
Finally, when only the domain argument is provided,
the `integrate` function returns its measure:

        Float integrate (geo domain);

Examples
========
The computation of the measure of a domain:

        Float meas_omega = integrate (omega);
        Float meas_left  = integrate (omega["left"]);

The integral of a function:

        Float f (const point& x) { return exp(x[0]+x[1]); }
        ...
        integrate_option iopt;
        iopt.set_order (3);
        Float int_f = integrate (omega, f, iopt);

The function can be replaced by any expression combining
functions, class-functions and @ref field_2.

The right-hand-side involved by the variational formulation

        space Xh (omega, "P1");
        test v (Xh);
        field lh = integrate (f*v);

For a bilinear form:

        trial u (Xh);
        form m = integrate (u*v);
        form a = integrate (dot(grad(u),grad(v)));

The expression can also combine
functions, class-functions and @ref field_2.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

// Implementation note
// -------------------
// SUMMARY:
// 1. scalar-result integration
//    1.1. general integration of a nonlinear expression
//    1.2. measure of the domain
//    1.3. when the valued result type is undetermined
// 2. field-result integration of a variational expression
//    2.1. general call
//    2.2. missing domain
//    2.3. subdomain by its name
// 3. form-result integration of a variational expression
//    3.1. general call
//    3.2. missing domain
//    3.3. subdomain by its name
// 4. variational integration: on a band
// 5. lazy-field-result integration of a variational expression
//    5.1. general call
//    5.2. missing domain
//    5.3. subdomain by its name
// 6. lazy-form-result integration of a variational expression
//    6.1. general call
//    6.2. missing domain
//    6.3. subdomain by its name
//
#include "rheolef/field_expr.h"
#include "rheolef/field_expr_variational.h"
#include "rheolef/form_expr_variational.h"

#include "rheolef/field_expr_value_assembly.h"
#include "rheolef/field_vf_assembly.h"
#include "rheolef/form_vf_assembly.h"
#include "rheolef/form_expr_quadrature.h"
#include "rheolef/field_expr_quadrature.h"
#include "rheolef/form_lazy_expr.h"

#include "rheolef/functor.h" // used to convert functions to functors

namespace rheolef { 

// ---------------------------------------------------
// 1. scalar-result integration
// ---------------------------------------------------
// 1.1. general integration of a nonlinear expression
// ---------------------------------------------------
template <class T, class M, class Expr,
    class Result = typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr>::type::value_type>
inline
typename std::enable_if<
     details::is_field_expr_v2_nonlinear_arg<Expr>::value
  && ! is_undeterminated<Result>::value,
  Result
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (const geo_basic<T,M>& omega, const Expr& expr, const integrate_option& iopt,
	   Result dummy = Result())
{
  typedef typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr>::type  wrap_t;
  if (omega.map_dimension() < omega.get_background_geo().map_dimension()) {
    omega.get_background_geo().neighbour_guard();
  }
  Result result(0);
  field_expr_v2_value_assembly (omega, wrap_t(expr), iopt, result);
  return result;
}
// ---------------------------------------------------
// 1.2. measure of the domain
// ---------------------------------------------------
template <class T, class M>
T
//! @brief see the @ref integrate_3 page for the full documentation
integrate (const geo_basic<T,M>& omega, integrate_option&& iopt = integrate_option())
{
  if (iopt.get_order() == std::numeric_limits<integrate_option::size_type>::max()) {
    iopt.set_order(0);
  }
  details::f_constant <point_basic<T>,T> one(1);
  return integrate (omega, one, iopt);
}
// ---------------------------------------------------
// 1.3. when the valued result type is undetermined
// ---------------------------------------------------
// TODO: return a overdetermined<T> value that is an union of all possibilities with a valued_tag
template<class T, class M, class Expr>
inline
typename std::enable_if<
     details::is_field_expr_v2_nonlinear_arg<Expr>::value
  && is_undeterminated<typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr>::type::value_type>::value,
  typename scalar_traits<typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr>::type::value_type>::type
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (const geo_basic<T,M>& omega, const Expr& expr, const integrate_option& iopt)
{
  typedef typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr>::type::value_type undef_t;
  typedef typename scalar_traits<undef_t>::type scalar_type;
  switch (expr.valued_tag()) {
    case space_constant::scalar: {
        return integrate (omega, expr, iopt, scalar_type());
    }
    // others type: problem on how to return a run-type type ?
    // TODO: return an overdetermined union type that convert to one of scalar, point, tensor, etc ?
    default:
        warning_macro ("Expr="<<pretty_typename_macro(Expr));
        error_macro ("integrate: not yet for `"
	  << space_constant::valued_name (expr.valued_tag())
          << "' valued expression");
        return 0;
  }
}
// -------------------------------------------------------
// 2. field-result integration of a variational expression
// -------------------------------------------------------
// 2.1. general call
// -------------------------------------------------------
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,field_basic<T,M>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const geo_basic<T,M>& domain, 
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  field_basic<T,M> lh;
  lh.do_integrate (domain, expr, iopt);
  return lh;
}
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,field_basic<T,M>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const geo_basic<T,M>& domain, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (domain, expr_quad, fopt);
}
// ----------------------------------------------
// 2.2. missing domain
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,field_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  field_basic <typename Expr::scalar_type, typename Expr::memory_type> lh;
  const geo_basic <typename Expr::scalar_type, typename Expr::memory_type>&
    dom = expr.get_vf_space().get_constitution().get_geo();
  lh.do_integrate (dom, expr, iopt);
  return lh;
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,field_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (expr_quad, fopt);
}
// ----------------------------------------------
// 2.3. subdomain by its name
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,field_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  field_basic <typename Expr::scalar_type, typename Expr::memory_type> lh;
  const geo_basic <typename Expr::scalar_type, typename Expr::memory_type>&
    dom = expr.get_vf_space().get_constitution().get_geo() [domname];
  lh.do_integrate (dom, expr, iopt);
  return lh;
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,field_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (domname, expr_quad, fopt);
}
// -------------------------------------------------------
// 3. form-result integration of a variational expression
// -------------------------------------------------------
// 3.1. general call
// -------------------------------------------------------
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const geo_basic<T,M>& domain, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  form_basic<T,M> a;
  a.do_integrate (domain, expr, fopt);
  return a;
}
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const geo_basic<T,M>& domain, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (domain, expr_quad, fopt);
}
// ----------------------------------------------
// 3.2. missing domain
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  form_basic <typename Expr::scalar_type, typename Expr::memory_type> a;
  geo_basic <typename Expr::scalar_type, typename Expr::memory_type>
    dom_trial = expr.get_trial_space().get_constitution().get_geo(),
    dom_test  = expr.get_test_space().get_constitution().get_geo(),
    dom;
  // dom = intersection of trial & test domain definition
  if (dom_trial.is_broken() && dom_test.is_broken() &&
      expr.get_trial_space().get_constitution().is_hierarchical() &&
       expr.get_test_space().get_constitution().is_hierarchical() ) {
    dom = dom_test.get_background_geo();
  } else if (dom_trial.name() == dom_test.name() ||
      dom_trial.name() == dom_test.get_background_geo().name() ||
      dom_trial.is_broken()) {
    dom = dom_test;
  } else if (dom_test.name() == dom_trial.get_background_geo().name() ||
             dom_test.is_broken()) {
    dom = dom_trial;
  } else {
    error_macro("integrate: incompatible domains: trial \""<<dom_trial.name()
	<< "\" and \"" << dom_test.name() << "\"");
  }
  trace_macro ("dom_trial="<<dom_trial.name()<<" dom_test="<<dom_test.name()<<" -> dom="<<dom.name());
  a.do_integrate (dom, expr, fopt);
  return a;
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (expr_quad, fopt);
}
// ----------------------------------------------
// 3.3. subdomain by its name
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  form_basic <typename Expr::scalar_type, typename Expr::memory_type> a;
  const geo_basic <typename Expr::scalar_type, typename Expr::memory_type>&
    dom = expr.get_trial_space().get_constitution().get_background_geo()[domname];
  a.do_integrate (dom, expr, fopt);
  return a;
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (domname, expr_quad, fopt);
}
// ----------------------------------------------
// 4. variational integration: on a band
// ----------------------------------------------
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,field_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const band_basic<T,M>& gh, 
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  field_basic <typename Expr::scalar_type, typename Expr::memory_type> lh;
  lh.do_integrate (gh, expr, iopt);
  return lh;
}
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,field_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const band_basic<T,M>& gh,
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{

  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (gh, expr_quad, iopt);
}
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const band_basic<T,M>& gh, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  form_basic <typename Expr::scalar_type, typename Expr::memory_type> a;
  a.do_integrate (gh, expr, fopt);
  return a;
}
template <class T, class M, class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,form_basic <typename Expr::scalar_type, typename Expr::memory_type>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
integrate (
  const band_basic<T,M>& gh, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return integrate (gh, expr_quad, fopt);
}
#ifdef TO_CLEAN
// -------------------------------------------------------
// 5. field-result integration of a variational expression
// -------------------------------------------------------
// 5.1. general call
// -------------------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,details::field_lazy_terminal_integrate <Expr>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const typename Expr::geo_type& domain, 
  const Expr&                    expr,
  const integrate_option&        iopt = integrate_option())
{
  return details::field_lazy_terminal_integrate<Expr> (domain, expr, iopt);
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,details::field_lazy_terminal_integrate <details::field_expr_quadrature_on_element<Expr>>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const geo_basic<typename Expr::scalar_type, typename Expr::memory_type>& domain, 
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return lazy_integrate (domain, expr_quad, iopt);
}
// ----------------------------------------------
// 5.2. missing domain
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,details::field_lazy_terminal_integrate <Expr>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  return details::field_lazy_terminal_integrate<Expr> (expr, iopt);
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,details::field_lazy_terminal_integrate <details::field_expr_quadrature_on_element<Expr>>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return lazy_integrate (expr_quad, fopt);
}
// ----------------------------------------------
// 5.3. subdomain by its name
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_quadrature_arg<Expr>::value
 ,details::field_lazy_terminal_integrate <Expr>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  return details::field_lazy_terminal_integrate<Expr> (domname, expr, iopt);
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_field_expr_v2_variational_arg<Expr>::value
 ,details::field_lazy_terminal_integrate <details::field_expr_quadrature_on_element<Expr>>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::field_expr_quadrature_on_element<Expr> expr_quad(expr);
  return lazy_integrate (domname, expr_quad, fopt);
}
#endif // TO_CLEAN
// -------------------------------------------------------
// 6. lazy_form-result integration of a variational expression
// -------------------------------------------------------
// 6.1. general call
// -------------------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,details::form_lazy_terminal_integrate <Expr>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const geo_basic<typename Expr::scalar_type, typename Expr::memory_type>& domain, 
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  return details::form_lazy_terminal_integrate<Expr> (domain, expr, iopt);
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,details::form_lazy_terminal_integrate <details::form_expr_quadrature_on_element<Expr>>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const geo_basic<typename Expr::scalar_type, typename Expr::memory_type>& domain, 
  const Expr& expr,
  const integrate_option& iopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return lazy_integrate (domain, expr_quad, iopt);
}
// ----------------------------------------------
// 6.2. missing domain
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,details::form_lazy_terminal_integrate <Expr>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  // TODO: improve the automatic determination of the domain
  //       by an union that operates recrusively
  constexpr bool has_on_local_sides = details::is_form_expr_quadrature_on_side_arg<Expr>::value;
  geo_basic <typename Expr::scalar_type, typename Expr::memory_type>
    dom_trial = expr.get_trial_space().get_constitution().get_geo(),
    dom_test  = expr. get_test_space().get_constitution().get_geo(),
    dom;
  // dom = intersection of trial & test domain definition
  if (dom_trial.is_broken() && dom_test.is_broken() &&
      expr.get_trial_space().get_constitution().is_hierarchical() &&
      expr. get_test_space().get_constitution().is_hierarchical() ) {
    dom = dom_test.get_background_geo();
  } else if (dom_trial.name() == dom_test.name() ||
      dom_trial.name() == dom_test.get_background_geo().name() ||
      dom_trial.is_broken()) {
    dom = has_on_local_sides ? dom_trial : dom_test;
  } else if (dom_test.name() == dom_trial.get_background_geo().name() ||
             dom_test.is_broken()) {
    dom = has_on_local_sides ? dom_test : dom_trial;
  } else {
    error_macro("integrate: incompatible domains: trial \""<<dom_trial.name()
	<< "\" and \"" << dom_test.name() << "\"");
  }
  trace_macro("Expr="<<pretty_typename_macro(Expr));
  trace_macro ("space_trial="<<expr.get_trial_space().name()<<" space_test="<<expr. get_test_space().name());
  trace_macro ("dom_trial="<<dom_trial.name()<<" dom_test="<<dom_test.name()<<" -> dom="<<dom.name());
  return lazy_integrate (dom, expr, fopt);
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,details::form_lazy_terminal_integrate <details::form_expr_quadrature_on_element<Expr>>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return lazy_integrate (expr_quad, fopt);
}
// ----------------------------------------------
// 6.3. subdomain by its name
// ----------------------------------------------
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_quadrature_arg<Expr>::value
 ,details::form_lazy_terminal_integrate <Expr>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  form_basic <typename Expr::scalar_type, typename Expr::memory_type> a;
  const geo_basic <typename Expr::scalar_type, typename Expr::memory_type>&
    dom = expr.get_trial_space().get_constitution().get_background_geo()[domname];
  return lazy_integrate (dom, expr, fopt);
}
template <class Expr>
inline
typename
std::enable_if<
  details::is_form_expr_v2_variational_arg<Expr>::value
 ,details::form_lazy_terminal_integrate <details::form_expr_quadrature_on_element<Expr>>
>::type
//! @brief see the @ref integrate_3 page for the full documentation
lazy_integrate (
  const std::string& domname, 
  const Expr& expr,
  const integrate_option& fopt = integrate_option())
{
  details::form_expr_quadrature_on_element<Expr> expr_quad(expr);
  return lazy_integrate (domname, expr_quad, fopt);
}

}// namespace rheolef
#endif // _RHEO_INTEGRATE_H
