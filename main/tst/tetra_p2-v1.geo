#!geo

mesh
4
header
 dimension	3
 order		2
 nodes		10
 tetrahedra	1
end header

0 0 -0.1
1 0 0
0 1 0
0.1 0.1 1
0.5 -0.1 0
0.6 0.5 0
-0.1 0.4 0
-0.1 0 0.5
-0.1 0.5 0.5
0.5 0 0.6

T	p2 0 1 2 3 4 5 6 7 9 8

