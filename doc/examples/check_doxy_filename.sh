#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# TODO: 
# - error when no file name
# - ommit files that are no installed in exampledir
#
# -------------------------------------------------------------------------
# command line arguments
# -------------------------------------------------------------------------
#echo "! $0 $*" 1>&2; exit 0
srcdir=$1; shift
if test $# -ne 0; then
  L=$*
else
  L=`/bin/ls $srcdir/*.cc $srcdir/*.h $srcdir/*.icc 2>/dev/null`  
  exit 0
fi
#echo "! L=$L" 1>&2
echo "checking for a doxygen comment in example files..." 1>&2
# -------------------------------------------------------------------------
# loop on arguments
# -------------------------------------------------------------------------
status=0
for y in $L; do
  suffix=`expr $y : '.*\.\(.*\)'`
  if test "$suffix" != "cc" -a "$suffix" != "h" -a "$suffix" != "icc"; then
    # skip e.g. .mshcad file
    # echo "suffix=$suffix" 1>&2
    # echo "skip $y" 1>&2
    continue
  fi
  x=$srcdir/$y
  name=`grep '[@\\]example' $x 2>/dev/null| awk '{print $3}'`
  if test "$name" = ""; then
    echo "`basename $x` : missing @examplefile for doxygen" 1>&2
    status=1
    continue;
  fi
  # -------------------------------------------------------------------------
  # check that filename matches with @examplefile FILENAME
  # -------------------------------------------------------------------------
  if test "$name" = "`basename $x`"; then
    true
  else
    echo "`basename $x` : doxygen example name \"$name\" differs" 1>&2
    status=1
    continue
  fi
  # -------------------------------------------------------------------------
  # check that [1:22] lines are comments
  # -------------------------------------------------------------------------
  if test `head -21 $x | grep -v '^//' | wc -l` -ne 0; then
    # note: a better solution would be
    #       to filter all comments before inclusion in latex 
    echo "`basename $x` : code starts before the 21-th line" 1>&2
    status=1
    continue
  fi
done
exit $status
