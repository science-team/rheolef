set terminal cairolatex pdf color standalone
set output "burgers_diffusion_semiimplicit_error-h-50-dt.tex"

set size square 
set colors classic
set log
set key bottom
set xrange [1e-6:1e-1]
set yrange [1e-5:1e-1]
graph_ratio = 5.0/4.0
set xtics (\
	'[c]{$10^{-6}$}' 1e-6, \
	'[c]{$10^{-5}$}' 1e-5, \
	'[c]{$10^{-4}$}' 1e-4, \
	'[c]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[c]{$10^{-1}$}' 1e-1)
set ytics (\
	'[r]{$10^{-5}$}' 1e-5, \
	'[r]{$10^{-4}$}' 1e-4, \
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1)
set xlabel '[c]{$\Delta t$}'
set  label '[l]{$\|u-u_h\|_{L^{\infty}(0,T;L^2)}$}' at graph 0.02, 0.95
set  label '[l]{(a) $k=1$, $h=2/50$}' at graph 0.08, 0.85

# triangle a droite
slope_A = graph_ratio*1
xA =  0.31
yA =  0.20
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%.2g$}",1) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

plot \
  "burgers_diffusion_semiimplicit_error-P1d-h-50-dt.gdat" \
        using (1./$2):3 \
	t '[r]{$k=1$}' \
	w lp lt 1 lw 2 lc 0, \
  "burgers_diffusion_semiimplicit_error-P2d-h-50-dt.gdat" \
        using (1./$2):3 \
	t '[r]{$k=2$}' \
	w lp lt 1 lw 2 lc 1

#pause -1
