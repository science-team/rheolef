#ifndef _RHEO_MSG_BOTH_PERMUTATION_APPLY_H
#define _RHEO_MSG_BOTH_PERMUTATION_APPLY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
namespace rheolef {
/*F:
NAME: msg_both_permutation_apply -- sequentail apply (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Applies permutations when copying an array.
ALGORITHM:
  msg_both_permutation_apply

  "input": the length array
  |   px(0:n-1), x(0:nx-1), py(0:n-1)
  "output": the pointer array and the total size
  |   y(0:ny)
  begin
  |   for i := 0 to n-1 do
  |     y(py(i)) := x(px(i))
  |   endfor
  end
COMPLEXITY:
  Time and memory complexity is O(n).
METHODS: @msg_both_permutation_apply 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   6 january 1999
END:
*/

//<msg_both_permutation_apply:
template <
    class InputIterator1,
    class InputIterator2,
    class InputRandomIterator,
    class SetOp,
    class OutputRandomIterator>
void
msg_both_permutation_apply (
    InputIterator1		px,
    InputIterator1		last_px,
    InputRandomIterator		x,
    SetOp			set_op,
    InputIterator2		py,
    OutputRandomIterator	y)
{
    while (px != last_px)
	set_op(y[*py++], x[*px++]);
}
} // namespace rheolef
//>msg_both_permutation_apply:
#endif // _RHEO_MSG_BOTH_PERMUTATION_APPLY_H
