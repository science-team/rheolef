#ifndef _RHEOLEF_SPACE_MULT_H
#define _RHEOLEF_SPACE_MULT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/space.h"

namespace rheolef {

template <class T, class M>
struct space_mult_list_rep {
    typedef space_basic<T,M>                                      value_type;
    typedef typename std::list<space_basic<T,M> >::size_type      size_type;
    typedef typename std::list<space_basic<T,M> >::const_iterator const_iterator;

// allocators:

    space_mult_list_rep() : _l() {}

// accessors:

    size_type      size()  const { return _l.size(); }
    const_iterator begin() const { return _l.begin(); }
    const_iterator end()   const { return _l.end(); }

// modifiers:

    void push_back (const value_type& X) const { _l.push_back(X); }
    void push_front(const value_type& X) const { _l.push_front(X); }

// data:
    // as mult_list is a unique temporary, avoid list copy by allowing modifs
    mutable std::list<space_basic<T,M> > _l;
};
template <class T, class M>
class space_mult_list : public smart_pointer<space_mult_list_rep<T,M> > {
public:

    typedef space_mult_list_rep<T,M>      rep;
    typedef smart_pointer<rep>            base;
    typedef space_basic<T,M>              value_type;
    typedef typename rep::size_type       size_type;
    typedef typename rep::const_iterator  const_iterator;

// allocators:

    space_mult_list() : smart_pointer<rep>(new_macro(rep)) {}

// accessors:

    size_type      size()  const { return base::data().size(); }
    const_iterator begin() const { return base::data().begin(); }
    const_iterator end()   const { return base::data().end(); }

// modifiers:

    void push_back (const value_type& X) const { base::data().push_back(X); }
    void push_front(const value_type& X) const { base::data().push_front(X); }

};
// -------------------------------------------------------------
// space expression: multiplication
// -------------------------------------------------------------
template <class T, class M>
inline
space_mult_list<T,M>
operator* (const space_basic<T,M>& X, const space_basic<T,M>& Y) 
{
  space_mult_list<T,M> Zm;
  Zm.push_back (X);
  Zm.push_back (Y);
  return Zm;
}
template <class T, class M>
inline
space_mult_list<T,M>
operator* (const space_mult_list<T,M>& Xm, const space_basic<T,M>& Y) 
{
  space_mult_list<T,M> Zm = Xm;
  Zm.push_back (Y);
  return Zm;
}
template <class T, class M>
inline
space_mult_list<T,M>&
operator*= (space_mult_list<T,M>& Xm, const space_basic<T,M>& Y) 
{
  Xm.push_back (Y);
  return Xm;
}
template <class T, class M>
inline
space_mult_list<T,M>
operator* (const space_basic<T,M>& X, const space_mult_list<T,M>& Ym) 
{
  space_mult_list<T,M> Zm = Ym;
  Zm.push_front (X);
  return Zm;
}
template <class T, class M>
space_mult_list<T,M>
pow (const space_basic<T,M>& X, size_t n) 
{
  space_mult_list<T,M> Ym;
  for (size_t i = 0; i < n; i++) {
    Ym.push_back (X);
  }
  return Ym;
}

} // namespace rheolef
#endif // _RHEOLEF_SPACE_MULT_H
