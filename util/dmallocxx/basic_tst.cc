///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*
  setenv DMALLOC_OPTIONS "debug=0x14f47d83,log=a.log"
  c++ basic_test.cc -ldmallocxx -o basic_test
  ./basic_test
  more a.log
 */

#include <dmalloc.h>

int main()
{
  char *fred;
  char *test;

  test = (char*)dmalloc_malloc(__FILE__, __LINE__, sizeof(char), DMALLOC_FUNC_NEW, 0 /* no alignment */, 0 /* no xalloc messages */);
  test = (char*)dmalloc_malloc(__FILE__, __LINE__, sizeof(char), DMALLOC_FUNC_NEW, 0 /* no alignment */, 0 /* no xalloc messages */);
  dmalloc_free(__FILE__, __LINE__, test, DMALLOC_FUNC_DELETE);

  fred = (char*)dmalloc_malloc(__FILE__, __LINE__, 20*sizeof(char), DMALLOC_FUNC_NEW_ARRAY, 0 /* no alignment */, 0 /* no xalloc messages */);
  fred = (char*)dmalloc_malloc(__FILE__, __LINE__, 20*sizeof(char), DMALLOC_FUNC_NEW_ARRAY, 0 /* no alignment */, 0 /* no xalloc messages */);
  dmalloc_free(__FILE__, __LINE__, fred, DMALLOC_FUNC_DELETE_ARRAY);
  return 0;
}
