#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${GEODIR-"$TOP_SRCDIR/main/tst"}"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

#Pk     err_linf
L="
P1	1e-3
P2	6e-5
P3	4e-6
"
while test "$L" != ""; do
  Pk=`echo $L | gawk '{print $1}'`
  tol=`echo $L | gawk '{print $2}'`
  L=`echo $L | gawk '{for (i=3; i <= NF; i++) print $i}'`
  loop_mpirun "./p_laplacian_damped_newton $GEODIR/circle-10-fix-$Pk $Pk 1.5 2>/dev/null | RHEOPATH=$GEODIR \$RUN ./p_laplacian_error $tol 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
