#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
NPROC_MIN=${NPROC_MIN-"1"}
silent_run () {
  run_status=0
  command="$*"
  echo "      $command"
  eval "$command"
  if test $? -ne 0; then run_status=1; fi
  return $run_status
}
run () {
  run_status=0
  if test x"${NPROC_MAX}" != x"0"; then
    command="$*"
    echo "      $command"
    eval "$command"
    if test $? -ne 0; then run_status=1; echo "        => *NO*"; fi
  fi
  return $run_status
}
loop_mpirun () {
  mpi_status=0
  mpi_command="$*"
  if test "${MPIRUN}" = "" -o "${NPROC_MAX}" = ""; then
    export RUN=""
    nompi_command=`echo ${mpi_command} | sed -e "s%\\\$RUN%${RUN}%g"`
    silent_run ${nompi_command}
    if test $? -ne 0; then mpi_status=1; echo "        => *NO*"; fi
  else
    nproc=${NPROC_MIN}
    while test ${nproc} -le ${NPROC_MAX}; do
      export RUN="${MPIRUN} -np ${nproc}"
      #echo "@@mpi1_command=\"$mpi_command\""
      mpi3_command=`echo ${mpi_command} | sed -e "s%\\\$RUN%${RUN}%g"`
      #echo "@@mpi3_command=\"$mpi3_command\""
      silent_run "${RUN} ${mpi3_command}"
      if test $? -ne 0; then mpi_status=1; echo "        => *NO*"; fi
      #sleep 1
      nproc=`expr ${nproc} + 1`
    done
  fi
  return $mpi_status
}
# the same, but do no prefix with $RUN
# just replace \$RUN by $RUN
# useful when mpirun is not the first word,
# e.g. when setting an environment variable as RHEOPATH
loop_mpirun_raw () {
  mpi_status=0
  mpi_command="$*"
  if test "${MPIRUN}" = "" -o "${NPROC_MAX}" = ""; then
    export RUN=""
    nompi_command=`echo ${mpi_command} | sed -e "s%\\\$RUN%${RUN}%g"`
    silent_run ${nompi_command}
    if test $? -ne 0; then mpi_status=1; echo "        => *NO*"; fi
  else
    nproc=${NPROC_MIN}
    while test ${nproc} -le ${NPROC_MAX}; do
      export RUN="${MPIRUN} -np ${nproc}"
      #echo "@@mpi1_command=\"$mpi_command\""
      mpi3_command=`echo ${mpi_command} | sed -e "s%\\\$RUN%${RUN}%g"`
      #echo "@@mpi3_command=\"$mpi3_command\""
      silent_run "${mpi3_command}"
      if test $? -ne 0; then mpi_status=1; echo "        => *NO*"; fi
      #sleep 1
      nproc=`expr ${nproc} + 1`
    done
  fi
  return $mpi_status
}
