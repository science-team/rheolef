#ifndef _RHEO_FIELD_ELEMENT_H
#define _RHEO_FIELD_ELEMENT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// for checking the spectral convergence exp(-ak)
// on the reference element
//
// author: Pierre.Saramito@imag.fr
//
// date: 1 october 2017
//
#include "rheolef/quadrature.h"
#include "rheolef/basis.h"
#include "rheolef/field_expr_utilities.h"
#include "equispaced.icc"

namespace rheolef {
// ---------------------------------------------------
struct space_element {
// ---------------------------------------------------
  typedef space_constant::valued_type  valued_type;
  space_element (std::string name, reference_element hat_K)
   : _b(name), _hat_K(hat_K) {}
  space_element (const basis& b, reference_element hat_K)
   : _b(b), _hat_K(hat_K) {}
  valued_type valued_tag() const { return _b.valued_tag(); }
// data:
  basis             _b;
  reference_element _hat_K;
};
// ---------------------------------------------------
struct field_element {
// ---------------------------------------------------
  field_element (const space_element& V)
   : _V(V), _dof()
  {
    _dof.resize (_V._b.ndof (_V._hat_K));
  }
  template <class Function>
  void interpolate (const Function& f)
  {
    _V._b.compute_dof (_V._hat_K, f, _dof);
  }
  void interpolate (const field_element& uk);

  template<class Value>
  Value evaluate (const point& hat_x) const
  {
    Eigen::Matrix<Value,Eigen::Dynamic,1>  phi;
    _V._b.evaluate (_V._hat_K, hat_x, phi);
    check_macro (phi.size() == _dof.size(), "invalid sizes");
    Value result = Value(0);
    for (size_t loc_idof = 0, loc_ndof = _dof.size(); loc_idof < loc_ndof; ++loc_idof) {
      result = result + _dof[loc_idof]*phi[loc_idof];
    }
    return result;
  }
  template<class Value>
  Value grad_evaluate (const point& hat_x) const
  {
    Eigen::Matrix<Value,Eigen::Dynamic,1>  grad_phi;
    _V._b.grad_evaluate (_V._hat_K, hat_x, grad_phi);
    check_macro (grad_phi.size() == _dof.size(), "invalid sizes");
    Value result = Value();
    for (size_t loc_idof = 0, loc_ndof = _dof.size(); loc_idof < loc_ndof; ++loc_idof) {
      result = result + _dof[loc_idof]*grad_phi[loc_idof];
    }
    return result;
  }
  Float operator() (const point& hat_x) const
  {
    return (*this).template evaluate<Float> (hat_x);
  }
  size_t ndof() const { return _dof.size(); }
  Float dof(size_t i) const { return _dof(i); }
// data:
  space_element                                  _V;
  Eigen::Matrix<Float,Eigen::Dynamic,1>          _dof;
};
// re-interpolate a field_element, for commutation checks
template<class Value>
struct field_element_function_wrapper {
    Value operator() (const point& hat_x) const
  	{ return _uk.template evaluate<Value> (hat_x); }
    field_element_function_wrapper (const field_element& uk) : _uk(uk) {}
    field_element _uk;
};
inline
void
field_element::interpolate (const field_element& fk)
{
  _V = fk._V;
  switch (fk._V.valued_tag()) {
    case space_constant::scalar: {
      field_element_function_wrapper<Float> f (fk);
      (*this).interpolate (f);
      break;
    } 
    case space_constant::vector: {
      field_element_function_wrapper<point> f (fk);
      (*this).interpolate (f);
      break;
    } 
    default: error_macro ("unsupported valued field_element::interpolate(field_element)");
  }
}
// ---------------------------------------------------
// error_l2
// ---------------------------------------------------
Float generic_norm2 (const Float&  x) { return sqr(x); }
Float generic_norm2 (const point&  x) { return norm2(x); }
Float generic_norm2 (const tensor& x) { return norm2(x); }

template <class Function>
Float
error_l2 (const field_element& uk, const Function& u, size_t qorder = size_t(0)) {
  using result_type = typename details::function_traits<Function>::result_type;
  if (qorder == size_t(0)) qorder = max(size_t(5), 2*uk._V._b.degree() + 2);
  quadrature_option qopt;
  qopt.set_order (qorder);;
  quadrature<Float> quad (qopt);
  Float err2 = 0;
  for (typename quadrature<Float>::const_iterator iter_q = quad.begin(uk._V._hat_K),
        last_q = quad.end(uk._V._hat_K); iter_q != last_q; iter_q++) {
    const point& hat_xq = (*iter_q).x;
    Float        hat_wq = (*iter_q).w;
    err2 += generic_norm2(u(hat_xq) - uk.template evaluate<result_type>(hat_xq))*hat_wq;
  }
  return sqrt(err2/measure(uk._V._hat_K));
}
// ---------------------------------------------------
// error_linf
// ---------------------------------------------------
template <class Function>
Float
error_linf (const field_element& uk, const Function& u, size_t nsub = 0) {
  using result_type = typename details::function_traits<Function>::result_type;
  if (nsub == 0) nsub = max(size_t(5), 2*uk._V._b.degree() + 2);
  Eigen::Matrix<point_basic<Float>,Eigen::Dynamic,1> hat_xnod;
  pointset_lagrange_equispaced (uk._V._hat_K, nsub, hat_xnod);
  Float err2 = 0;
  for (size_t loc_inod = 0, loc_nnod = hat_xnod.size(); loc_inod < loc_nnod; ++loc_inod) {
    err2 = max (err2, generic_norm2(uk.template evaluate<result_type>(hat_xnod[loc_inod]) - u(hat_xnod[loc_inod])));
  }
  return sqrt(err2);
}
// ---------------------------------------------------
// error_div_l2
// ---------------------------------------------------
template <class Function>
Float
error_div_l2 (const field_element& uk, const Function& u, size_t qorder = size_t(0)) {
  using result_type = typename details::function_traits<Function>::result_type;
  using   grad_type = typename space_constant::rank_up<result_type>::type;
  using    div_type = typename space_constant::rank_down<result_type>::type;
  if (qorder == size_t(0)) qorder = max(size_t(5), 2*uk._V._b.degree() + 2);
  quadrature_option qopt;
  qopt.set_order (qorder);
  quadrature<Float> quad (qopt);
  Float err2 = 0;
  for (typename quadrature<Float>::const_iterator iter_q = quad.begin(uk._V._hat_K),
        last_q = quad.end(uk._V._hat_K); iter_q != last_q; iter_q++) {
    const point& hat_xq = (*iter_q).x;
    Float        hat_wq = (*iter_q).w;
    grad_type grad_u_xq = uk.template grad_evaluate<grad_type>(hat_xq);
    div_type   div_u_xq = tr(grad_u_xq);
    err2 += generic_norm2(u.div(hat_xq) - div_u_xq)*hat_wq;
  }
  return sqrt(err2/measure(uk._V._hat_K));
}
// ---------------------------------------------------
// error_div_linf
// ---------------------------------------------------
template <class Function>
Float
error_div_linf (const field_element& uk, const Function& u, size_t qorder = size_t(0)) {
  using result_type = typename details::function_traits<Function>::result_type;
  using   grad_type = typename space_constant::rank_up<result_type>::type;
  using    div_type = typename space_constant::rank_down<result_type>::type;
  if (qorder == size_t(0)) qorder = max(size_t(5), 2*uk._V._b.degree() + 2);
  quadrature_option qopt;
  qopt.set_order (qorder);
  quadrature<Float> quad (qopt);
  Float err2 = 0;
  for (typename quadrature<Float>::const_iterator iter_q = quad.begin(uk._V._hat_K),
        last_q = quad.end(uk._V._hat_K); iter_q != last_q; iter_q++) {
    const point& hat_xq = (*iter_q).x;
    Float        hat_wq = (*iter_q).w;
    grad_type grad_u_xq = uk.template grad_evaluate<grad_type>(hat_xq);
    div_type   div_u_xq = tr(grad_u_xq);
    err2 = max (err2, generic_norm2(u.div(hat_xq) - div_u_xq));
  }
  return sqrt(err2);
}

} // namespace rheolef
#endif // _RHEO_FIELD_ELEMENT_H
