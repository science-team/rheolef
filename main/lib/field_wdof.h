# ifndef _RHEOLEF_FIELD_WDOF_H
# define _RHEOLEF_FIELD_WDOF_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// field_wdof: field with read & write accessors at the dof level
// terminals: field_basic, field_wdof_sliced, field_indirect
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   21 april 2020

#include "rheolef/field_rdof.h"

namespace rheolef { namespace details {

template<class FieldWdof> class field_wdof_sliced; // forward declaration
template<class FieldWdof> class field_wdof_indirect;

template<class Derived>
class field_wdof_base: public field_rdof_base<Derived> {
public:
// definitions:

  using size_type   = typename field_traits<Derived>::size_type;
  using scalar_type = typename field_traits<Derived>::scalar_type;
  using memory_type = typename field_traits<Derived>::memory_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using geo_type    = geo_basic  <float_type,memory_type>;
  using space_type  = space_basic<float_type,memory_type>;

// assignements:

  // TODO: also +-= for value, rdof, lazy
  // TODO: also */= and all from field_expr.h
  // TODO: DVT_EXPR_CTE_VEC : Value as point or tensor
  template<class Value>
  typename std::enable_if<
    details::is_rheolef_arithmetic<Value>::value
   ,field_wdof_base<Derived>&
  >::type
  operator= (const Value&);

  template<class FieldRdof>
  typename std::enable_if<
    has_field_rdof_interface<FieldRdof>::value
   ,field_wdof_base<Derived>&
  >::type
  operator= (const FieldRdof&);

  template<class FieldLazy>
  typename std::enable_if<
         has_field_lazy_interface<FieldLazy>::value
    && ! has_field_rdof_interface<FieldLazy>::value
   ,field_wdof_base<Derived>&
  >::type
  operator= (const FieldLazy&);

// accessors:

  field_wdof_indirect<Derived> operator[] (const std::string& dom_name);
  field_wdof_indirect<Derived> operator[] (const geo_type& dom);
  field_wdof_sliced  <Derived> operator[] (size_type i_comp);
  field_wdof_sliced  <Derived> operator() (size_type i_comp, size_type j_comp);

protected:
  Derived&       derived()       { return *static_cast<      Derived*>(this); }
  const Derived& derived() const { return *static_cast<const Derived*>(this); }
};

}}// namespace rheolef::details
# endif /* _RHEOLEF_FIELD_WDOF_H */
