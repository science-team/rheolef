///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/disarray.h"
#include "rheolef/environment.h"
using namespace rheolef;
int main(int argc, char**argv) {
    environment rheolef(argc, argv);
    size_t dis_n = 101;
    distributor ownership(dis_n, communicator(), distributor::decide);
    disarray<Float> x(ownership);
    size_t first_dis_i = x.ownership().first_index();
    size_t  last_dis_i = x.ownership().last_index();
    for (size_t dis_i = 0; dis_i < dis_n; dis_i++) {
    	if (dis_i >= first_dis_i && dis_i < last_dis_i) {
	    x.dis_entry (dis_i) = 1000+dis_i+0.5;
        }
    }
    x.dis_entry_assembly(); // not used, since there are no comms
    dout << x.dis_size();
    dout << std::endl;
    dout << x;
    dout << std::endl;
}
