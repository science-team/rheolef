set terminal cairolatex pdf color standalone
set output "transmission-1d-fig.tex"

set colors classic
set xtics (0, 0.25, 0.5, 0.75, 1)
set ytics (0, 1, 2, 3)
epsilon = 0.01

a1 = - 0.25*(1.0 + 3.0*epsilon)/(1.0 + epsilon)
b1 = a1
b2 = -b1 - 0.5

u1(x) = -(x*x/2 + a1*x)/epsilon
u2(x) = -(x*x/2 + b1*x + b2)

u(x) = ((x < 0.5) ? u1(x) : u2(x))

plot [0:1][0:3.5]  \
	u(x) title '[r]{exact}' lw 2 lc 0 dt 1, \
	"transmission-line-6.gdat"  title '[r]{$h=1/6\ \,$}' w lp dt 2 lw 2 lc 1 pt 1, \
	"transmission-line-10.gdat" title '[r]{$h=1/10$}'  w lp dt 3 lw 2 lc rgb '#008800' pt 2, \
	"transmission-line-14.gdat" title '[r]{$h=1/14$}'  w lp dt 4 lw 2 lc 3 pt 3

# pause -1 "<return>"

