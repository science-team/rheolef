#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
geo="mesh-2d.geo"
#geo="square-20.geo"
eps=1e-14
p=1.8
pf=1.9
np=${1-"10"}
w0=0.01
wf=2.0; # wf=1.0 pour p > 2, sinon wf=2.0
nw=${2-"40"}
dp=`echo $p  $pf $np | gawk '{print ($2-$1)/(1.0*($3-1)) }'`
dw=`echo $w0 $wf $nw | gawk '{print ($2-$1)/(1.0*($3-1)) }'`
ip=1
echo "# p relax_opt v_opt"
while test $ip -le $np; do
  w=$w0
  w_opt=0
  v_opt=0
  iw=1
  while test $iw -le $nw; do
    ./p_laplacian_fixed_point $geo P1 $p $w $eps >/dev/null 2> cv-$w.gdat
    v=`grep -v warn cv-$w.gdat | tail -1 | gawk '{print $3}'`
    if test $v != "inf"; then
      wv_opt=`echo $w $v $w_opt $v_opt | gawk '{if ($2 > $4) print $1,$2; else print $3,$4; }'`
      w_opt=`echo $wv_opt | gawk '{print $1}'`
      v_opt=`echo $wv_opt | gawk '{print $2}'`
    fi
    iw=`expr $iw + 1`
    w=`echo $w $dw | gawk '{print $1+$2}'`
  done
  echo $p $w_opt $v_opt
  ip=`expr $ip + 1`
  p=`echo $p $dp | gawk '{print $1+$2}'`
done
