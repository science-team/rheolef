///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/geo_domain.h"

namespace rheolef {

// ------------------------------------------------------------------------
// build edge and face connectivity from domain
// ------------------------------------------------------------------------
template <class T>
void
geo_rep<T,sequential>::domain_set_side_part1 (
    const domain_indirect_rep<sequential>&    indirect, 
    const geo_abstract_rep<T,sequential>&     bgd_omega,
    size_type                                 sid_dim,
    disarray<size_type,sequential>&           bgd_isid2dom_dis_isid,
    disarray<size_type,sequential>&           dom_isid2bgd_isid,
    disarray<size_type,sequential>&           dom_isid2dom_ios_dis_isid,
    size_type                                 size_by_variant [reference_element::max_variant])
{
  if (sid_dim != 0 && base::_gs._map_dimension <= sid_dim) return;
  communicator comm = bgd_omega.geo_element_ownership(sid_dim).comm();
  // ------------------------------------------------------------------------
  // 1) side compact re-numbering
  // ------------------------------------------------------------------------
  // 1.1 loop on elements and mark used sides
  disarray<size_type,sequential> bgd_isid_is_on_domain     (bgd_omega.geo_element_ownership(sid_dim), 0); // logical, init to "false"
  for (size_type ioige = 0, noige = indirect.size(); ioige < noige; ioige++) {
    size_type ige = indirect.oige (ioige).index();
    const geo_element& bgd_K = bgd_omega.get_geo_element (base::_gs._map_dimension, ige);
    for (size_type loc_isid = 0, loc_nsid = bgd_K.n_subgeo(sid_dim); loc_isid < loc_nsid; loc_isid++) {
      size_type bgd_dis_isid = 0; // TODO: = bgd_K.subgeo (sid_dim, loc_isid);
      switch (sid_dim) {
        case 0: {
	        size_type bgd_dis_inod = bgd_K[loc_isid];
	        bgd_dis_isid = bgd_omega.dis_inod2dis_iv (bgd_dis_inod);
	        break;
        }
        case 1: bgd_dis_isid = bgd_K.edge(loc_isid); break;
        case 2: bgd_dis_isid = bgd_K.face(loc_isid); break;
	default: error_macro ("domain: unexpected side dimension " << sid_dim);
      }
      bgd_isid_is_on_domain[bgd_dis_isid] += 1;
    }
  }
  // 1.2 counting & distribution for dom_isid 
  size_type dom_nsid = 0;
  for (size_type bgd_isid = 0, bgd_nsid = bgd_omega.geo_element_ownership(sid_dim).size(); bgd_isid < bgd_nsid; bgd_isid++) {
    if (bgd_isid_is_on_domain[bgd_isid] != 0) dom_nsid++ ;
  }
  // 1.4 numbering dom_isid & permutation: bgd_isid <--> dom_isid
  for (size_type variant = reference_element::first_variant_by_dimension(sid_dim);
                 variant < reference_element:: last_variant_by_dimension(sid_dim); variant++) {
    size_by_variant [variant] = 0;
  }
  distributor dom_isid_ownership (distributor::decide, comm, dom_nsid);
  bgd_isid2dom_dis_isid.resize (bgd_omega.geo_element_ownership(sid_dim), std::numeric_limits<size_type>::max());
  dom_isid2bgd_isid.resize     (dom_isid_ownership,                       std::numeric_limits<size_type>::max());
  for (size_type dom_isid = 0, bgd_isid = 0, bgd_nsid = bgd_omega.geo_element_ownership(sid_dim).size(); bgd_isid < bgd_nsid; bgd_isid++) {
    if (bgd_isid_is_on_domain[bgd_isid] == 0) continue;
    size_type dom_dis_isid = dom_isid; // sequential case !
    bgd_isid2dom_dis_isid [bgd_isid] = dom_dis_isid;
    dom_isid2bgd_isid     [dom_isid] = bgd_isid;
    const geo_element& bgd_S = bgd_omega.get_geo_element(sid_dim,bgd_isid);
    size_by_variant [bgd_S.variant()]++;
    dom_isid++;
  }
  // ------------------------------------------------------------------------
  // 2. compute sizes and resize _geo_element[variant]
  // ------------------------------------------------------------------------
  size_type dom_nge = 0;
  size_type dom_dis_nge = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(sid_dim);
                 variant < reference_element:: last_variant_by_dimension(sid_dim); variant++) {

     distributor dom_igev_ownership (distributor::decide, comm, size_by_variant [variant]);	
     geo_element::parameter_type param (variant, 1);
     base::_geo_element[variant].resize (dom_igev_ownership, param);
     base::_gs.ownership_by_variant[variant] = dom_igev_ownership;
     dom_nge     += dom_igev_ownership.size();
     dom_dis_nge += dom_igev_ownership.dis_size();
  }
  base::_gs.ownership_by_dimension[sid_dim] = distributor (dom_dis_nge, comm, dom_nge);
}
template <class T>
void
geo_rep<T,sequential>::domain_set_side_part2 (
    const domain_indirect_rep<sequential>&    indirect, 
    const geo_abstract_rep<T,sequential>&     bgd_omega,
    disarray<size_type,sequential>&              bgd_iv2dom_dis_iv,
    size_type                                 sid_dim,
    disarray<size_type,sequential>&           bgd_isid2dom_dis_isid,
    disarray<size_type,sequential>&           dom_isid2bgd_isid,
    disarray<size_type,sequential>&           dom_isid2dom_ios_dis_isid,
    size_type                                 size_by_variant [reference_element::max_variant])
{
  if (sid_dim != 0 && base::_gs._map_dimension <= sid_dim) return;
  communicator comm = bgd_omega.geo_element_ownership(sid_dim).comm();
  // ------------------------------------------------------------------------
  // 2) set _geo_element[variant] and S.set_ios_dis_ie
  //    also reperate external vertices
  // ------------------------------------------------------------------------
  distributor dom_isid_ownership = dom_isid2bgd_isid.ownership();
  for (size_type dom_isid = 0, dom_nsid = dom_isid_ownership.size(); dom_isid < dom_nsid; dom_isid++) {
    size_type bgd_isid = dom_isid2bgd_isid [dom_isid];
    size_type dom_dis_isid = dom_isid;
    size_type dom_ios_dis_isid = dom_dis_isid;
    const geo_element& bgd_S = bgd_omega.get_geo_element(sid_dim,bgd_isid);
    geo_element& dom_S = get_geo_element(sid_dim,dom_isid);
    dom_S = bgd_S;
    dom_S.set_dis_ie     (dom_dis_isid);
    dom_S.set_ios_dis_ie (dom_ios_dis_isid);
    // set S face & edge : index, orientation & rotation will be set by propagate_numbering later
  }
  // ------------------------------------------------------------------------
  // 3) propagate new vertex numbering in all `dom_S' new sides
  // ------------------------------------------------------------------------
  if (sid_dim > 0) {
    for (size_type dom_isid = 0, dom_nsid = dom_isid_ownership.size(); dom_isid < dom_nsid; dom_isid++) {
      geo_element& dom_S = get_geo_element(sid_dim,dom_isid);
      if (dom_S.dimension() == 0) continue;
      for (size_type iloc = 0, nloc = dom_S.size(); iloc < nloc; iloc++) {
        size_type bgd_dis_inod = dom_S[iloc];
        size_type bgd_dis_iv   = bgd_omega.dis_inod2dis_iv (bgd_dis_inod);
        size_type dom_dis_iv   = bgd_iv2dom_dis_iv.dis_at (bgd_dis_iv);
        size_type dom_dis_inod = base::dis_iv2dis_inod (dom_dis_iv);
        dom_S[iloc] = dom_dis_inod;
      }
    }
  }
}
// ------------------------------------------------------------------------
// geo construtor: build from domain
// ------------------------------------------------------------------------
template <class T>
void
geo_rep<T,sequential>::build_from_domain (
        const domain_indirect_rep<sequential>&              indirect,
        const geo_abstract_rep<T,sequential>&               bgd_omega,
              std::map<size_type,size_type>&                bgd_ie2dom_ie,
              std::map<size_type,size_type>&                dis_bgd_ie2dis_dom_ie)
{
trace_macro ("build("<<bgd_omega.name()<<","<<indirect.name()<<")...");
  base::_name = bgd_omega.name() + "[" + indirect.name() + "]";
  base::_version = 4;
  base::_sys_coord = bgd_omega.coordinate_system();
  base::_dimension     = bgd_omega.dimension();
  base::_piola_basis   = bgd_omega.get_piola_basis();
  base::_gs._map_dimension = indirect.map_dimension();
  base::_have_connectivity = 1;
  size_type map_dim = base::_gs._map_dimension;
  size_type size_by_variant [reference_element::max_variant];
  std::fill (size_by_variant, size_by_variant+reference_element::max_variant, 0);
  // ----------------------------------------------------
  // 1) _geo_element[0]: compact re-numbering
  // ----------------------------------------------------
  disarray<size_type,sequential> dom_isid2dom_ios_dis_isid [4];
  std::array<disarray<size_type,sequential>,4>  bgd_ige2dom_dis_ige;
  std::array<disarray<size_type,sequential>,4>  dom_ige2bgd_ige;
  domain_set_side_part1 (indirect, bgd_omega, 0,
        bgd_ige2dom_dis_ige[0], dom_ige2bgd_ige[0],
        dom_isid2dom_ios_dis_isid [0], size_by_variant);
  domain_set_side_part2 (indirect, bgd_omega, bgd_ige2dom_dis_ige[0], 0,
        bgd_ige2dom_dis_ige[0], dom_ige2bgd_ige[0],
        dom_isid2dom_ios_dis_isid [0], size_by_variant);
  // ------------------------------------------------------------------------
  // 2) count elements by variants
  // ------------------------------------------------------------------------
  for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                 variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
    size_by_variant [variant] = 0;
  }
  for (size_type ioige = 0, noige = indirect.size(); ioige < noige; ioige++) {
    size_type ige = indirect.oige (ioige).index();
    bgd_ie2dom_ie [ige] = ioige;
    const geo_element& bgd_K = bgd_omega.get_geo_element (map_dim, ige);
    size_by_variant [bgd_K.variant()]++;
  }
  // -----------------------------------------------
  // 3) _geo_element[map_dim]: compact vertex numbering
  // -----------------------------------------------
  size_type dis_nge = 0;
  size_type nge = 0;
  for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                 variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
     distributor dom_igev_ownership (distributor::decide, base::comm(), size_by_variant [variant]);
     geo_element::parameter_type param (variant, 1);
     base::_geo_element[variant].resize (dom_igev_ownership, param);
     base::_gs.ownership_by_variant [variant] = dom_igev_ownership;
     dis_nge += dom_igev_ownership.dis_size();
     nge     += dom_igev_ownership.size();
  }
  base::_gs.ownership_by_dimension [map_dim] = distributor (dis_nge, base::comm(), nge);
  // ------------------------------------------------------------------------
  // 4) count all geo_element[] and set   base::_gs.ownership_by_variant[]
  //   => then can determine the node count (order > 1)
  // ------------------------------------------------------------------------
  for (size_type sid_dim = 1; sid_dim < base::_gs._map_dimension; sid_dim++) {
    domain_set_side_part1 (indirect, bgd_omega, sid_dim,
        bgd_ige2dom_dis_ige[sid_dim], dom_ige2bgd_ige[sid_dim],
        dom_isid2dom_ios_dis_isid [sid_dim], size_by_variant);
  }
  // ------------------------------------------------------------------------
  // 5) count nodes & set   base::_gs.node_ownership
  //  => then dis_iv2dis_inod works (used at step 6)
  // ------------------------------------------------------------------------
  std::array<size_type,reference_element::max_variant> loc_nnod_by_variant ;
  reference_element::init_local_nnode_by_variant (base::order(), loc_nnod_by_variant);
  size_type nnod = 0;
  for (size_type variant = 0;
                 variant < reference_element::last_variant_by_dimension(base::map_dimension());
                 variant++) {
    nnod += base::_gs.ownership_by_variant [variant].size() * loc_nnod_by_variant [variant];
  }
  distributor dom_node_ownership (distributor::decide, base::comm(), nnod);
  base::_gs.node_ownership = dom_node_ownership;
  // ------------------------------------------------------------------------
  // 6) set _geo_element[] values
  // ------------------------------------------------------------------------
  size_type first_dis_ioige  = indirect.ownership().first_index();
  for (size_type ioige = 0, noige = indirect.size(); ioige < noige; ioige++) {
    size_type dis_ioige = first_dis_ioige + ioige;
    size_type ige = indirect.oige (ioige).index();
          geo_element& dom_K =           get_geo_element (map_dim, ioige);
    const geo_element& bgd_K = bgd_omega.get_geo_element (map_dim,   ige);
    dom_K = bgd_K;
    dom_K.set_dis_ie (dis_ioige);
    size_type ini_dis_ioige = ioige;
    dom_K.set_ios_dis_ie (ini_dis_ioige);
    // set K face & edge : index, orientation & rotation will be set by propagate_numbering later
    for (size_type iloc = 0, nloc = dom_K.size(); iloc < nloc; iloc++) {
      size_type bgd_dis_inod = bgd_K[iloc];
      size_type bgd_dis_iv   = bgd_omega.dis_inod2dis_iv (bgd_dis_inod);
      size_type dom_dis_iv   = bgd_ige2dom_dis_ige[0].dis_at (bgd_dis_iv);
      size_type dom_dis_inod = base::dis_iv2dis_inod (dom_dis_iv);
      dom_K[iloc] = dom_dis_inod;
    }
  }
  // reset also dom_ige2bgd_ige[map_dim] : used by _node[] for order > 1 geometries
  if (base::_gs._map_dimension > 0) {
    dom_ige2bgd_ige    [base::_gs._map_dimension].resize(indirect.ownership());
    bgd_ige2dom_dis_ige[base::_gs._map_dimension].resize(bgd_omega.geo_element_ownership(base::_gs._map_dimension), std::numeric_limits<size_type>::max());
    for (size_type ioige = 0, noige = indirect.size(); ioige < noige; ioige++) {
      size_type bgd_ige = indirect.oige (ioige).index();
      dom_ige2bgd_ige    [base::_gs._map_dimension] [ioige]   = bgd_ige;
      bgd_ige2dom_dis_ige[base::_gs._map_dimension] [bgd_ige] = ioige; // sequential!
    }
  }
  // ------------------------------------------------------------------------
  // 7) _geo_element[1&2]: compact renumbering
  //    _ios_ige2dis_ige[1&2]: idem
  // ------------------------------------------------------------------------
  for (size_type sid_dim = 1; sid_dim < base::_gs._map_dimension; sid_dim++) {
    domain_set_side_part2 (indirect, bgd_omega, bgd_ige2dom_dis_ige[0], sid_dim,
        bgd_ige2dom_dis_ige[sid_dim], dom_ige2bgd_ige[sid_dim],
        dom_isid2dom_ios_dis_isid [sid_dim], size_by_variant);
  }
  // ------------------------------------------------------------------------
  // 11) raw copy _node
  // TODO: use shallow copy & indirection instead (memory usage reduction):
  //    node(inod) { return bgd_omega.node(inod2bgd_inod(inod))}
  // ------------------------------------------------------------------------
  base::_node.resize (dom_node_ownership);
  disarray<size_type,sequential> dom_inod2bgd_inod (dom_node_ownership, std::numeric_limits<size_type>::max());
  size_type dom_inod = 0;
  size_type first_bgd_inod_v = 0;
  for (size_type dim = 0; dim <= base::_gs._map_dimension; dim++) {
    size_type dom_ige = 0;
    for (size_type variant = reference_element::first_variant_by_dimension(dim);
                   variant < reference_element:: last_variant_by_dimension(dim);
                   variant++) {
      if (loc_nnod_by_variant [variant] == 0) continue;
      size_type first_bgd_v = bgd_omega.sizes().first_by_variant [variant].size();
      for (size_type dom_igev = 0, dom_ngev = base::_geo_element [variant].size(); dom_igev < dom_ngev; dom_igev++, dom_ige++) {
        const geo_element& K = base::_geo_element [variant][dom_igev];
        size_type bgd_ige = dom_ige2bgd_ige [dim][dom_ige];
        assert_macro (bgd_ige >= first_bgd_v, "invalid index");
        size_type bgd_igev = bgd_ige - first_bgd_v;
        for (size_type loc_inod = 0, loc_nnod = loc_nnod_by_variant [variant]; loc_inod < loc_nnod; loc_inod++, dom_inod++) {
          size_type bgd_inod = first_bgd_inod_v + bgd_igev * loc_nnod_by_variant [variant] + loc_inod;
          dom_inod2bgd_inod [dom_inod] = bgd_inod;
          base::_node [dom_inod] = bgd_omega.node (bgd_inod);
        }
      }
      first_bgd_inod_v += bgd_omega.sizes().ownership_by_variant [variant].size() * loc_nnod_by_variant [variant];
    }
  }
  base::compute_bbox();
  // ------------------------------------------------------------------------
  // 12) propagate new edge & face numbering to all `dom_S' elements
  // ------------------------------------------------------------------------
  for (size_type dim = 1; dim < base::_gs._map_dimension; dim++) {
    set_element_side_index (dim);
  }
  // ------------------------------------------------------------------------
  // 13) reset vertex P[0] value (useful for band zero vertex domain) 
  // ------------------------------------------------------------------------
  for (size_type dom_iv = 0, dom_nv = base::_geo_element[reference_element::p].size(); dom_iv < dom_nv; dom_iv++) {
    geo_element& P = base::_geo_element[reference_element::p][dom_iv];
    size_type dom_inod = base::dis_iv2dis_inod (dom_iv);
    P[0] = dom_inod;
  }
  // ------------------------------------------------------------------------
  // 14) domains : intersection with the current domain
  // ------------------------------------------------------------------------
  const geo_base_rep<T,sequential> *ptr = dynamic_cast<const geo_base_rep<T,sequential>*> (&bgd_omega);
  check_macro (ptr != 0, "cannot build domains on \""<<base::_name<<"\"");
  const geo_base_rep<T,sequential>& bgd_omega2 = *ptr;
  for (size_type idom = 0, ndom = bgd_omega2.n_domain_indirect(); idom < ndom; ++idom) {
    const domain_indirect_basic<sequential>& bgd_dom = bgd_omega2.get_domain_indirect(idom);
trace_macro ("build("<<bgd_omega.name()<<"["<<indirect.name()<<"] with " <<bgd_dom.name()<<"...");
    if (bgd_dom.name() == indirect.name()) continue; // myself
    size_type dom_map_dim = bgd_dom.map_dimension();
    if (dom_map_dim > base::_gs._map_dimension) continue; // dom has upper dimension
    std::vector<size_type> ie_list;
    for (domain_indirect_basic<sequential>::const_iterator_ioige iter = bgd_dom.ioige_begin(), last = bgd_dom.ioige_end(); iter != last; ++iter) {
      const geo_element_indirect& ioige = *iter;
      size_type bgd_ige = ioige.index();
      size_type dom_dis_ige = bgd_ige2dom_dis_ige [dom_map_dim][bgd_ige];
      if (dom_dis_ige == std::numeric_limits<size_type>::max()) continue; // side do not belongs to dom
      size_type dom_ige = dom_dis_ige; // sequential !
      ie_list.push_back(dom_ige);
    }
    if (ie_list.size() == 0) continue; // empty intersection
    domain_indirect_basic<sequential> dom (*this, bgd_dom.name(), bgd_dom.map_dimension(), communicator(), ie_list);
    base::_domains.push_back (dom);
trace_macro ("geo("<<base::name()<<") += domain("<<dom.name()<<") map_d="<<dom.map_dimension());
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_rep<Float,sequential>;

} // namespace rheolef
