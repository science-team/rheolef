#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/linalg/tst"}
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

# full check too slow: use "runtime"
export DMALLOC_OPTIONS="debug=0x4000503,log=a.log"
echo "      export DMALLOC_OPTIONS=$DMALLOC_OPTIONS"


status=0

for algo in noprecond precond eigen suitesparse mumps; do

    loop_mpirun "./solver_tst -$algo -sdp < $SRCDIR/solver_tst.mtx 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi
  
    loop_mpirun "./solver_tst -$algo -nosdp < $SRCDIR/usym-indefinite.mtx 2>/dev/null >/dev/null"
    if test $? -ne 0; then status=1; fi

    if test $algo != "noprecond" -a $algo != "precond"; then
      # more hard, gmres failed direct are ok
      command="./solver_tst -$algo -nosdp < $SRCDIR/gre__115.mtx 2>/dev/null >/dev/null"
      loop_mpirun $command
      if test $? -ne 0; then status=1; fi
    fi
    i=5;
    while test $i -ge 0; do
        run "echo | LANG=C gawk -v n=$i -f ${SRCDIR}/gen_solver_mtx.awk > solver-$i.mtx"
        if test $? -ne 0; then status=1; break; fi
    
        loop_mpirun "./solver_tst -$algo -sdp < solver-$i.mtx 2>/dev/null >/dev/null"
        if test $? -ne 0; then status=1; fi
    
        run "rm -f solver-$i.mtx"
        if test $? -ne 0; then status=1; break; fi
    
        i=`expr $i - 1`
    done
done

exit $status
