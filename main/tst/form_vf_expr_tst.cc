
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
template<class E> void show(E) { derr << "E="<<pretty_typename_macro(E)<<endl;}
struct f1 {
  Float operator() (const point& x) const { return sqrt(fabs(x[0])); }
};
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega(argv[1]);
  string approx = argv[2];
  Float tol = (argc > 3) ? atof(argv[3]) : 1e-10;
  string sys_coord = (argc > 4) ? argv[4] : "cartesian";
  omega.set_coordinate_system (sys_coord);
  omega.boundary(); // add boundary...
  size_t d = omega.dimension();
  space Xh  (omega, approx);
  space Xvh (omega, approx, "vector");
  space Xth (omega, approx, "tensor");
  trial u (Xh);   test v (Xh);
  trial uu(Xvh);  test vv(Xvh);
  trial uuu(Xth); test vvv(Xth);
  quadrature_option qopt;
  qopt.set_family (quadrature_option::gauss);
  qopt.set_order  (2*Xh.degree()-1);
  Float err = 0;

  // scalar mass:
  form m1_old (Xh, Xh, "mass", qopt);
  form m1_new = integrate (omega, u*v, qopt);
#ifdef TO_CLEAN
  derr << "m1(old)="<<m1_old.uu();
  derr << "m1(new)="<<m1_new.uu();
#endif // TO_CLEAN
  form e1 = m1_new-m1_old;
  Float err1 = e1.uu().max_abs();
  derr << "err1 = " << err1 << endl;
  err = std::max(err,err1);

  // vector mass:
  form m2_old (Xvh, Xvh, "mass", qopt);
  form m2_new = integrate (omega, dot(uu,vv), qopt);
#ifdef TO_CLEAN
  derr << "m2(old)="<<m2_old.uu();
  derr << "m2(new)="<<m2_new.uu();
#endif // TO_CLEAN
  form e2 = m2_new-m2_old;
  Float err2 = e2.uu().max_abs();
  derr << "err2 = " << err2 << endl;
  err = std::max(err,err2);

  // scalar grad_grad:
  form m3_old (Xh, Xh, "grad_grad", qopt);
  form m3_new = integrate (omega, dot(grad(u),grad(v)), qopt);
#ifdef TO_CLEAN
  derr << "m3(old)="<<m3_old.uu();
  derr << "m3(new)="<<m3_new.uu();
#endif // TO_CLEAN
  form e3 = m3_new-m3_old;
  Float err3 = e3.uu().max_abs();
  derr << "err3 = " << err3 << endl;
  err = std::max(err,err3);

  // alpha*u*v
  Float alpha = 1/3.;
  form m4_old = alpha*form(Xh, Xh, "mass", qopt);
  // show(alpha*(u*v));
  form m4_new = integrate (omega, (alpha*u)*v, qopt);
#ifdef TO_CLEAN
  derr << "m4(old)="<<m4_old.uu();
  derr << "m4(new)="<<m4_new.uu();
#endif // TO_CLEAN
  form e4 = m4_new-m4_old;
  Float err4 = e4.uu().max_abs();
  derr << "err4 = " << err4 << endl;
  err = std::max(err,err4);

  // weighted scalar grad_grad:
  field eta_h = lazy_interpolate (Xh, f1());
  form m5_old (Xh, Xh, "grad_grad", eta_h, qopt);
  //show (eta_h*grad(u));
  //show (grad(v));
  form m5_new1 = integrate (eta_h*dot(grad(u),grad(v)), qopt);
  form m5_new2 = integrate (dot(eta_h*grad(u),grad(v)), qopt);
#ifdef TO_CLEAN
  derr << "m5(old)="<<m5_old.uu();
  derr << "m5(new)="<<m5_new.uu();
#endif // TO_CLEAN
  form e51 = m5_new1-m5_old;
  form e52 = m5_new2-m5_old;
  Float err51 = e51.uu().max_abs();
  Float err52 = e52.uu().max_abs();
  derr << "err51 = " << err51 << endl;
  derr << "err52 = " << err52 << endl;
  err = std::max(err,err51);
  err = std::max(err,err52);

  // grad: vectorial field
  form m6_old (Xh, Xvh, "grad", qopt);
  form m6_new1 = integrate (dot(grad(u),vv), qopt);
  form m6_new2 = integrate (dot(vv,grad(u)), qopt);
#ifdef TO_CLEAN
  derr << "m6(old)="<<m6_old.uu();
  derr << "m6(new1)="<<m6_new1.uu();
  derr << "m6(new2)="<<m6_new2.uu();
#endif // TO_CLEAN
  form e61 = m6_new1-m6_old;
  form e62 = m6_new2-m6_old;
  Float err61 = e61.uu().max_abs();
  Float err62 = e62.uu().max_abs();
  derr << "err61 = " << err61 << endl;
  derr << "err62 = " << err62 << endl;
  err = std::max(err,err61);
  err = std::max(err,err62);

  // symmetric tensor mass
  form m7_old (Xth, Xth, "mass", qopt);
  form m7_new = integrate (ddot(uuu,vvv), qopt);
  form e7 = m7_new-m7_old;
  Float err7 = e7.uu().max_abs();
  derr << "err7 = " << err7 << endl;
  err = std::max(err,err7);

  // D(u): gradient of vector, symmetrized tensor
  form m8_old = form(Xvh, Xth, "2D", qopt);
  form m8_new = integrate (2*ddot(D(uu),vvv), qopt);
  form e8 = m8_new-m8_old;
  Float err8 = e8.uu().max_abs();
  derr << "err8 = " << err8 << endl;
  err = std::max(err,err8);

  // 2*D(u):D(v)
  form m9_old = form(Xvh, Xvh, "2D_D", qopt);
  form m9_new = integrate (2*ddot(D(uu),D(vv)), qopt);
  form e9 = m9_new-m9_old;
  Float err9 = e9.uu().max_abs();
  derr << "err9 = " << err9 << endl;
  err = std::max(err,err9);

  // div(u)
  form m10_old = form(Xvh, Xh, "div", qopt);
  form m10_new = integrate (div(uu)*v, qopt);
#ifdef TO_CLEAN
  derr << "m10(old)="<<m10_old.uu();
  derr << "m10(new)="<<m10_new.uu();
#endif // TO_CLEAN
  form e10 = m10_new-m10_old;
  Float err10 = e10.uu().max_abs();
  derr << "err10 = " << err10 << endl;
  err = std::max(err,err10);

  // div(u)*div(v)
  if (sys_coord == "cartesian") { // old axi "div_div" was not supported
    form m11_old = form(Xvh, Xvh, "div_div", qopt);
    form m11_new = integrate (div(uu)*div(vv), qopt);
#ifdef TO_CLEAN
    derr << "m11(old)="<<m11_old.uu();
    derr << "m11(new)="<<m11_new.uu();
#endif // TO_CLEAN
    form e11 = m11_new-m11_old;
    Float err11 = e11.uu().max_abs();
    derr << "err11 = " << err11 << endl;
    err = std::max(err,err11);
  }
  // bubble & 2D_D
  bool do_bubble = (d == 2 && omega.sizes().ownership_by_variant[reference_element::q].dis_size() == 0);
  if (do_bubble) {
    quadrature_option bqopt;
    bqopt.set_family (quadrature_option::gauss);
    bqopt.set_order  (7);
    space Bvh  (omega, "bubble", "vector");
    trial uub(Bvh);  test vvb(Bvh);
    form m12_old (Bvh, Bvh, "2D_D", bqopt);
    form m12_new = integrate (2*ddot(D(uub),D(vvb)), bqopt);
    form e12 = m12_new-m12_old;
    Float err12 = e12.uu().max_abs();
    derr << "err12 = " << err12 << endl;
    err = std::max(err,err12);
  }
  // inv mass:
  space Xdh  (omega, "P"+std::to_string(Xh.degree())+"d");
  integrate_option fopt (qopt);
  fopt.invert = true;
  fopt.set_family (quadrature_option::gauss);
  fopt.set_order  (2);
  fopt.set_order  (2*Xdh.degree());
  trial ud (Xdh);   test vd (Xdh);
  form m13_old (Xdh, Xdh, "inv_mass", quadrature_option(fopt));
  form m13_new = integrate (omega, ud*vd, fopt);
#ifdef TO_CLEAN
  derr << "m13(old)="<<m13_old.uu();
  derr << "m13(new)="<<m13_new.uu();
#endif // TO_CLEAN
  form e13 = m13_new-m13_old;
  Float err13 = e13.uu().max_abs();
  derr << "err13 = " << err13 << endl;
  err = std::max(err,err13);

  if (sys_coord != "cartesian") { // batchelor trick

#ifdef NO_MORE_MAINTAINED
    // axi: s_grad_grad
    integrate_option rzfopt;
    rzfopt.ignore_sys_coord = true;
    rzfopt.set_family (quadrature_option::gauss);
    fopt.set_order  (2*Xh.degree()-1);
    form m14_new = integrate (dot(grad(u), grad(v)), rzfopt);
    form m14_old (Xh, Xh, "s_grad_grad", quadrature_option(rzfopt));
#ifdef TO_CLEAN
    derr << "m14(old)="<<m14_old.uu();
    derr << "m14(new)="<<m14_new.uu();
#endif // TO_CLEAN
    form e14 = m14_new-m14_old;
    Float err14 = e14.uu().max_abs();
    derr << "err14 = " << err14 << endl;
    err = std::max(err,err14);
  
    // axi: bcurl
    rzfopt.ignore_sys_coord = false;
    Float sgn = (sys_coord == "zr") ? 1 : -1;
    form m15_new = integrate (sgn*dot(bcurl(u), vv), rzfopt);
    form m15_old (Xh, Xvh, "s_curl", quadrature_option(rzfopt));
#ifdef TO_CLEAN
    derr << "m15(old)="<<m15_old.uu();
    derr << "m15(new)="<<m15_new.uu();
#endif // TO_CLEAN
    form e15 = m15_new-m15_old;
    Float err15 = e15.uu().max_abs();
    derr << "err15 = " << err15 << endl;
    err = std::max(err,err15);
#endif // NO_MORE_MAINTAINED

#ifdef TODO
    // axi: curl
    rzfopt.ignore_sys_coord = false;
    form m16_new = integrate (sgn*dot(curl(u), vv), rzfopt);
    form m16_old2 (Xvh, Xh, "curl", quadrature_option(rzfopt));
    form m16_old = trans(m16_old2);
#ifdef TO_CLEAN
    derr << "m16(old)="<<m16_old.uu();
    derr << "m16(new)="<<m16_new.uu();
#endif // TO_CLEAN
    form e16 = m16_new-m16_old;
    Float err16 = e16.uu().max_abs();
    derr << "err16 = " << err16 << endl;
    err = std::max(err,err16);
#endif // TODO
  }
  derr << "err = " << err << endl;
  return (err < tol) ? 0 : 1;
}
