///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile laplace_band.cc The Poisson problem on a surface by the banded level set method
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "torus.icc"
int main (int argc, char**argv) {
  environment rheolef(argc, argv);
  geo lambda (argv[1]);
  size_t d = lambda.dimension();
  space Xh (lambda, "P1");
  field phi_h = lazy_interpolate(Xh, phi);
  band gamma_h (phi_h);
  field phi_h_band = phi_h [gamma_h.band()];
  space Bh (gamma_h.band(), "P1");
  Bh.block ("isolated");
  Bh.unblock ("zero");
  trial u (Bh); test v (Bh);
  form  a  = lazy_integrate (gamma_h, dot(grad_s(u),grad_s(v)));
  field c  = lazy_integrate (gamma_h, v);
  field lh = lazy_integrate (gamma_h, f(d)*v);
  vector<field> b (gamma_h.n_connected_component());
  vector<Float> z (gamma_h.n_connected_component(), 0);
  for (size_t i = 0; i < b.size(); i++) {
    const domain& cci = gamma_h.band() ["cc"+to_string(i)];
    field phi_h_cci (Bh, 0);
    phi_h_cci [cci] = phi_h_band [cci];
    b[i] = phi_h_cci;
  }
  form  A = {{    a,  trans(b), c },
             {    b,     0,     0 },
             { trans(c), 0,     0 }};
  field Fh = {    lh,    z,     0 };
  field Uh (Fh.get_space(), 0); 
  A.set_symmetry(true);
  problem pa (A);
  pa.solve (Fh, Uh);
  dout << catchmark("phi") << phi_h
       << catchmark("u")   << Uh[0];
}
