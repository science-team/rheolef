///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile leveque.h The Leveque benchmark -- function definition
struct u {
  point operator() (const point& x) const {
    return cos(pi*t/tf)
          *point (-sqr(sin(pi*x[0]))*sin(2*pi*x[1]),
                   sqr(sin(pi*x[1]))*sin(2*pi*x[0]));
  }
  u (size_t d, Float t1) : t(t1), pi(acos(Float(-1))) {}
  static Float period() { return tf; }
protected:
  Float t, pi;
  static constexpr Float tf = 8; // time period
};
struct phi0 {
  Float operator() (const point& x) const {
    return sqrt(sqr(x[0]-x0) + sqr(x[1]-y0)) - r;
  }
  phi0(size_t d) { check_macro(d==2, "3D: not yet, sorry"); }
protected:
  static constexpr Float
    r  = 0.15, // circle radius
    x0 = 0.5,  // circle center
    y0 = 0.75; 
};
