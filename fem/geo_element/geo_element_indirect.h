#ifndef _RHEOLEF_GEO_ELEMENT_INDIRECT_H
#define _RHEOLEF_GEO_ELEMENT_INDIRECT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/compiler_mpi.h"

namespace rheolef {

// =====================================================================
// geo_element_indirect = index + orient + face shift
// =====================================================================
// TODO: compact the sign bit and size_t in a long int
class geo_element_indirect {
public:

// typedefs:

  typedef size_t           size_type;
  typedef short int        orientation_type; // for sign (+1,-1)
  typedef short int        shift_type;       // for face [0:4[ shift

// allocators:

  geo_element_indirect ()
    : _all(std::numeric_limits<size_type>::max()) {}
  geo_element_indirect (orientation_type orient, size_type ige, size_type shift = 0)  
    : _all(std::numeric_limits<size_type>::max()) 
    { set (orient, ige, shift); }

// accessors:

  size_type index() const { return _all & index_mask; }
  orientation_type orientation() const { return (_all & orient_mask) ?  -1 : 1; }
  shift_type shift() const { return (_all & shift_mask) >> shift_position; }

// modifiers:

  void set_orientation (orientation_type orient) { 
    _all = (_all & (~orient_mask));
    if (orient < 0) _all = _all | orient_mask;
  }
  void set_shift (size_type shift) {
    _all = (_all & (~shift_mask));
    _all = _all | (shift_mask & (shift << shift_position));
  }
  void set_index (size_type ige) {
    _all = (_all & (~index_mask));
    _all = _all | (index_mask & ige);
  }
  void set (orientation_type orient, size_type ige, size_type shift = 0) {
    set_orientation (orient);
    set_index       (ige);
    set_shift       (shift);
  }

// i/o:

    void dump(std::ostream& out = std::clog) const {
      for (int i = 8*sizeof(size_type) - 1; i >= 0; i--) {
        size_type bit = (_all & (size_type(1) << i)) >> i;
        out << bit;
        if (i % 8 == 0 && i != 0) out << " ";
      }
    }
    template<class Archive>
    void serialize (Archive& ar, const unsigned int version) { ar & _all; }

protected:

// implementation:

  // left bit(0) = first left bit: orientation
  static const size_type orient_position = 8*sizeof(size_type)-1;
  static const size_type orient_mask     = (size_type(1) << orient_position);

  // left bits(1:3) = face shift (for 3d faces)
  static const size_type shift_position = 8*sizeof(size_type)-4;
  static const size_type shift_mask     = (size_type(1) << shift_position) |
                                          (size_type(1) << (shift_position+1)) |
                                          (size_type(1) << (shift_position+2));


  // others right bits = index of the subgeo (face or edge)
  static const size_type index_mask     = ~(orient_mask | shift_mask);

// data:

    size_type _all;
};
// i/o:
std::istream&  operator>> (std::istream& is, geo_element_indirect& x);
std::ostream&  operator<< (std::ostream& os, const geo_element_indirect& x);

} // namespace rheolef

#ifdef _RHEOLEF_HAVE_MPI
#include "rheolef/compiler.h"
// =====================================================================
// Some serializable types, have a fixed amount of data stored at fixed field positions.
// When this is the case, boost::mpi can optimize their serialization and transmission to avoid extraneous 
// copy operations.
// To enable this optimization, we specialize the type trait is_mpi_datatype, e.g.:
namespace boost {
 namespace mpi {
  template <> struct is_mpi_datatype<rheolef::geo_element_indirect> : mpl::true_ { };
 } // namespace mpi
} // namespace boost
#endif // _RHEOLEF_HAVE_MPI

#endif // _RHEOLEF_GEO_ELEMENT_INDIRECT_H
