///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compose supports both true functions
// and class-functions (pointed out by A. Laadhary)
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct H_eps : std::binary_function<Float,Float,Float> {
  Float operator() (const Float& phi,const Float& eps) const {
    if ( phi >= eps ) return 1;
    if ( phi <= -eps ) return 0;
    return 0.5*( 1 + phi/eps + sin(Pi*phi/eps)/Pi );}
  H_eps () : Pi(acos(-1.)) {}
  Float Pi;
};
Float Pi = acos(-1.);
Float H_eps_f (const Float& phi, const Float& eps) {
    if ( phi >= eps ) return 1;
    if ( phi <= -eps ) return 0;
    return 0.5*( 1 + phi/eps + sin(Pi*phi/eps)/Pi );
}
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  Float tol = 1e-10;
  space Vh (omega, "P1");
  field phih (Vh, 1);
  field epsh (Vh, 0.1);
  Float err_max = 0;

  // compose: both args are fields
  field result1 = lazy_interpolate (Vh, compose(H_eps_f,phih,epsh));
  field result2 = lazy_interpolate (Vh, compose(H_eps(),phih,epsh));
  Float err1 = dual (result1,result1)/omega.dis_n_vertex() - 1;
  Float err2 = dual (result2,result2)/omega.dis_n_vertex() - 1;
  derr << "err_a1 = " << err1 << endl;
  derr << "err_a2 = " << err2 << endl;
  err_max = max(fabs(err1), err_max);
  err_max = max(fabs(err2), err_max);
#ifdef TO_CLEAN
  dout << "result_a1="<<result1<<endl;
  dout << "result_a2="<<result2<<endl;
#endif // TO_CLEAN

  // compose: 1st arg is constant
  result1 = lazy_interpolate (Vh, compose(H_eps_f, 1, epsh));
  result2 = lazy_interpolate (Vh, compose(H_eps(), 1, epsh));
  err1 = dual (result1,result1)/omega.dis_n_vertex() - 1;
  err2 = dual (result2,result2)/omega.dis_n_vertex() - 1;
  derr << "err_b1 = " << err1 << endl;
  derr << "err_b2 = " << err2 << endl;
  err_max = max(fabs(err1), err_max);
  err_max = max(fabs(err2), err_max);
#ifdef TO_CLEAN
  dout << "result_b1="<<result1<<endl;
  dout << "result_b2="<<result2<<endl;
#endif // TO_CLEAN

  // compose: 2nd arg is constant
  result1 = lazy_interpolate(Vh, compose(H_eps_f, phih, 0.1));
  result2 = lazy_interpolate(Vh, compose(H_eps(), phih, 0.1));
  err1 = dual (result1,result1)/omega.dis_n_vertex() - 1;
  err2 = dual (result2,result2)/omega.dis_n_vertex() - 1;
  derr << "err_c1 = " << err1 << endl;
  derr << "err_c2 = " << err2 << endl;
  err_max = max(fabs(err1), err_max);
  err_max = max(fabs(err2), err_max);

  return (err_max < tol) ? 0 : 1;
}
