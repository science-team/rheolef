#ifndef _RHEOLEF_PIOLA_FEM_HDIV_H
#define _RHEOLEF_PIOLA_FEM_HDIV_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/piola_fem.h"
namespace rheolef { 
/*Class:piola_fem_hdiv
NAME: @code{piola_fem_hdiv} - maps a H(div) finite element method
@cindex  piola transformation
@cindex  finite element method
SYNOPSIS:
  @noindent
  The @code{piola_fem_hdiv} defines how a vector-valued finite element
  method of H(div) type maps from a corresponding method defined on a reference element:
@iftex
@tex
  $$
      \boldsymbol{u}(F(\widehat{\boldsymbol{x}}))
      =
      \mathrm{det}(DF)^{-1}
      (\widehat{\boldsymbol{x}})
      DF
      (\widehat{\boldsymbol{x}})
      \widehat{\boldsymbol{u}}
      (\widehat{\boldsymbol{x}})
  $$
  for all $\widehat{\boldsymbol{x}}$ in the reference element $\widehat{K}$.
  here, $F$ denotes the Piola transformation that maps the
  the reference element $\widehat{K}$
  into the element $\widehat{K}$
  and $DF$ its Jacobian matrix.
@end tex
@end iftex
@ifnottex
  @example
    u = (1/det(DF))*(DF*hat_u);
  @end example
  for all hat_x in the reference element hat_K.
  Here, F denotes the Piola transformation that maps the
  the reference element hat_K
  into the element K
  and DF its Jacobian matrix.
@end ifnottex
  See also the
  @code{piola_fem} abstract class
  and the @code{basis_fem_RTk} class for a vector-valued
  finite element methods definied on a reference element.
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
   | Pierre.Saramito@imag.fr
DATE: 26 january 2019
End:
*/

template<class T>
class piola_fem_hdiv: public piola_fem_rep<T> {
public:
  typedef piola_fem_rep<T>          base;
  typedef typename base::value_type value_type;
  piola_fem_hdiv() : base() {}
  std::string name() const { return "Hdiv"; }
  void transform         (const piola<T>& p, const point_basic<T>& hat_u, point_basic<T>&     u) const;
  void inv_transform     (const piola<T>& p, const point_basic<T>&     u, point_basic<T>& hat_u) const;
  void grad_transform    (
	const piola<T>&                           p,
	const point_basic<T>&                 hat_u,
	const tensor_basic<T>&           hat_grad_u,
        const details::differentiate_option&       gopt, 
	      tensor_basic<T>&               grad_u) const;
};

}// namespace rheolef
#endif // _RHEOLEF_PIOLA_FEM_HDIV_H
