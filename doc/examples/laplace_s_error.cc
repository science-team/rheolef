///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "torus.icc"
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-7;
  field uh;
  din >> uh;
  const space& Wh = uh.get_space();
  field pi_h_u = lazy_interpolate(Wh, u_exact());
  field eh = uh-pi_h_u;
  trial u (Wh); test v (Wh);
  form m = integrate (u*v);
  form a = integrate (dot(grad_s(u),grad_s(v)));
  field one (Wh, 1);
  Float meas_gamma = m(one,one);
  Float lambda_e = m(one,eh)/meas_gamma;
  eh = eh - lambda_e*one;
  Float err_l2 = sqrt(m(eh,eh))/meas_gamma;
  Float err_h1 = sqrt(a(eh,eh))/meas_gamma;
  Float err_linf = eh.max_abs();
  derr << "lambda_e = " << lambda_e << endl;
  derr << "err_l2   = " << err_l2 << endl
       << "err_h1   = " << err_h1 << endl
       << "err_linf = " << err_linf << endl;
  return (err_linf < tol) ? 0 : 1;
}
