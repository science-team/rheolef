///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// build field from initializer lists
//
#include "rheolef/field_concat.h"
#include "rheolef/field.h"
#include "rheolef/field_wdof_sliced.h"
#include "rheolef/field_expr.h"
#include "rheolef/space_mult.h"

namespace rheolef { namespace details {

template <class T, class M>
field_basic<T,M>
field_concat<T,M>::build_field() const
{
  // ------------------------------------
  // first pass: compute the field size
  // ------------------------------------
  space_basic<T,M> IR = space_basic<T,M>::real();
  space_mult_list<T,M> sml;
  for (typename std::list<value_type>::const_iterator iter = _l.begin(); iter != _l.end(); ++iter) {
    const field_concat_value<T,M>& x = *iter;
    switch (x.variant) {
      case value_type::scalar: {
        sml *= IR;
        break;
      }
      case value_type::vector_scalar: {
        size_t n = x.vs.size();
        space_basic<T,M> IRn = pow(IR,n);
        sml *= IRn;
        break;
      }
      case value_type::field: {
        sml *= x.f.get_space();
        break;
      }
      default: error_macro("field initializer list: unexpected element");
    }
  }
  space_basic<T,M> Yh (sml);
  // ------------------------
  // second pass: copy values
  // ------------------------
  field_basic<T,M> yh (Yh);
  size_type i_comp = 0;
  for(typename std::list<value_type>::const_iterator iter = _l.begin(); iter != _l.end(); ++iter, i_comp++) {
    const field_concat_value<T,M>& x = *iter;
    switch (x.variant) {
      case value_type::scalar: {
        field_basic<T,M> zh (IR, 0); // TODO: accept/check non-zero values ?
        yh [i_comp] = zh;
        break;
      }
      case value_type::vector_scalar: {
        size_t n = x.vs.size();
        space_basic<T,M> IRn = pow(IR,n);
        field_basic<T,M> zh (IRn, 0); // TODO: accept/check non-zero values ?
        yh [i_comp] = zh;
        break;
      }
      case value_type::field: {
        yh [i_comp] = x.f;
        break;
      }
      default: error_macro("field initializer list: unexpected element");
    }
  }
  return yh;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M) 				\
template class field_concat<T,M>;				

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

}} // namespace rheolef::details
