///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// constants

#include "rheolef/space_constant.h"

namespace rheolef { namespace space_constant {

// ---------------------------------------------------------------------------
// valued: for multi-component field support
// ---------------------------------------------------------------------------
static
const std::string
valued_id [last_valued+1] = {
        "scalar",
        "vector",
        "tensor",
        "unsymmetric_tensor",
        "tensor3",
        "tensor4",
        "mixed",
        "undefined"
};
const std::string&
valued_name (valued_type valued_tag)
{
  if (valued_tag <= last_valued) return valued_id [valued_tag];
  error_macro ("invalid valued tag = " << valued_tag); 
  return valued_id [last_valued]; // not reached
}
valued_type
valued_tag (const std::string& name)
{
  for (size_t valued_tag = 0; valued_tag < last_valued; valued_tag++) {
    if (valued_id[valued_tag] == name) return valued_type(valued_tag);
  }
  error_macro ("invalid valued `" << name << "'"); 
  return scalar; // not reached
}
// ---------------------------------------------------------------------------
//  coordinate system helper
// ---------------------------------------------------------------------------
static
const char*const
coord_sys_table [last_coord_sys] = {
    "cartesian",
    "rz",
    "zr"
};
std::string 
coordinate_system_name (coordinate_type i)
{
    assert_macro (i < last_coord_sys, "invalid coordinate_type " << i);
    return coord_sys_table [i];
}
coordinate_type
coordinate_system (std::string sys_coord)
{
    for (size_type i = 0; i < last_coord_sys; i++)
        if (sys_coord == coord_sys_table[i]) return coordinate_type(i);
    error_macro ("unexpected coordinate system `" << sys_coord << "'");
    return last_coord_sys;
}
void
check_coord_sys_and_dimension (coordinate_type i, size_type d)
{
    assert_macro (i < last_coord_sys, "invalid coordinate_type " << i);
    check_macro (!((i == axisymmetric_rz || i == axisymmetric_zr) && d > 2),
	"inconsistent `" << coord_sys_table[i] 
	 << "' coordinate system for " << d << "D geometry"); 
}
// ---------------------------------------------------------------------------------
// symmetric & unsymmetric 2-tensors field-valued support
// ---------------------------------------------------------------------------------
static
const size_type
symmetric_tensor_index [3][3] = {
	{ 0, 1, 3},
	{ 1, 2, 4},
	{ 3, 4, 5}
};
static
const char*const
symmetric_tensor_subscript_name [6] = {
	"00", 
        "01", 
        "11",
        "02", 
        "12",
        "22"
};
static
const
size_type
unsymmetric_tensor_index [3][3] = {
        { 0, 1, 4},
        { 2, 3, 5},
        { 6, 7, 8}
};
static
const char*const
unsymmetric_tensor_subscript_name [9] = {
        "00",
        "01",  	
	"10",  	
	"11",  	
	"02",  	
	"12",  	
	"20",  	
	"21",  	
	"22"
};
// intel C++ v12 cannot support class initializers:
static std::pair<size_type,size_type>   symmetric_tensor_subscript [6];
static std::pair<size_type,size_type> unsymmetric_tensor_subscript [9];

size_type
n_component (
    valued_type        valued_tag,
    size_type          d,
    coordinate_type    sys_coord)
{ 
  switch (valued_tag) {
    case vector: return d;
    case scalar: return 0;
    case tensor4: { // A_ijkl with A_ijkl=A_jikl, A_ijkl=A_ijlk and A_ijkl=A_klij
      if ((sys_coord == axisymmetric_rz)||(sys_coord == axisymmetric_zr)) {
        return 10; // add all the theta,theta components
      } 
      switch (d) {
        case 1:  return 1;
        case 2:  return 6; 
        default: return 21;
      }
    }
    case tensor: {
      if ((sys_coord == axisymmetric_rz)||(sys_coord == axisymmetric_zr)) {
        return 4; // add the \tau_{\theta,\theta}" component
      }
      return d*(d+1)/2;
    }
    case unsymmetric_tensor: {
      if ((sys_coord == axisymmetric_rz)||(sys_coord == axisymmetric_zr)) {
        return 5;
      }
      return d*d;
    }
    default: {
      error_macro ("unsupported valued space `" << valued_name(valued_tag) << "'");
      return 0; // not reached
    }
  }
}
size_type
n_component (
    const std::string& valued,
    size_type          d,
    coordinate_type    sys_coord)
{
  return n_component (valued_tag(valued), d, sys_coord);
}
size_type
tensor_index (
    valued_type       valued_tag,
    coordinate_type   sys_coord,
    size_type         i,
    size_type         j)
{
    if (valued_tag == unsymmetric_tensor) {
      if ((sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr)
	  && i == 2 && j == 2)
	  return 4;
      return unsymmetric_tensor_index [i][j];
    } else {
      if ((sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr)
	  && i == 2 && j == 2)
	  return 3;
      return   symmetric_tensor_index [i][j];
    }
}
size_type
tensor_index (
    std::string      valued,
    std::string      sys_coord, 
    size_type   i, 
    size_type   j)
{
    return tensor_index (valued_tag(valued), coordinate_system(sys_coord), i, j);
}
std::pair<size_type,size_type>
tensor_subscript (
    valued_type       valued_tag,
    coordinate_type   sys_coord,
    size_type         i_comp)
{
    if (valued_tag == unsymmetric_tensor) {
      if ((sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr)
	  && i_comp == 4)
	 return std::pair<size_type,size_type>(2,2);
      return unsymmetric_tensor_subscript [i_comp];
    } else {
      if ((sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr)
	 && i_comp == 3)
	 return std::pair<size_type,size_type>(2,2);
      return   symmetric_tensor_subscript [i_comp];
    }
}
std::string
tensor_subscript_name (
    valued_type       valued_tag,
    coordinate_type   sys_coord,
    size_type         i_comp)
{
    if (valued_tag == unsymmetric_tensor) {
      if ((sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr)
          && i_comp == 4)
	return "22";
      return unsymmetric_tensor_subscript_name [i_comp];
    } else {
      if ((sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr)
	&& i_comp == 3) 
        return "22";
      return   symmetric_tensor_subscript_name [i_comp];
    }
}
std::pair<size_type,size_type>
tensor_subscript (
    std::string     valued,
    std::string     sys_coord,
    size_type  i_comp)
{
    return tensor_subscript (valued_tag(valued),
	         coordinate_system(sys_coord), i_comp);
}
std::string
tensor_subscript_name (
    std::string     valued,
    std::string     sys_coord,
    size_type  i_comp)
{
    return tensor_subscript_name (valued_tag(valued),
	         coordinate_system(sys_coord), i_comp);
}
// =================================================================================
// symmetric 4-tensors
// Note: 3 symmetries for 4-order tensors
//     A_ijkl=A_ijlk, A_ijkl=A_jikl & A_ijkl=Aklij
// => 6 components in 2d, 21 in 3d and 10 in 2d-axisymmetric(rz)
// =================================================================================
static
const
size_type
symmetric_tensor4_index [6][6] = {
	{  0,  1,  3,  6, 10, 15},
	{  1,  2,  4,  7, 11, 16},
	{  3,  4,  5,  8, 12, 17},
	{  6,  7,  8,  9, 13, 18},
	{ 10, 11, 12, 13, 14, 19},
	{ 15, 16, 17, 18, 19, 20}
};
// intel C++ v12 cannot support class initializers:
static
std::pair<std::pair<size_type,size_type>, std::pair<size_type,size_type> >
symmetric_tensor4_subscript [21];

static
const char* const
symmetric_tensor4_subscript_name [21] = {
	"00_00",
	"00_01",
	"01_01",
	"00_11",
	"01_11",
	"11_11",
	"00_02",
	"01_02",
	"11_02",
	"02_02",
	"00_12",
	"01_12",
	"11_12",
	"02_12",
	"12_12",
	"00_22",
	"01_22",
	"11_22",
	"02_22",
	"12_22",
	"22_22" };

// special axisymmetic case
// intel C++ v12 cannot support class initializers:
static
std::pair<std::pair<size_type,size_type>, std::pair<size_type,size_type> >
symmetric_tensor4_subscript_rz [10];

static
const char* const
symmetric_tensor4_subscript_rz_name [10] = {
	"00_00",
	"00_01",
	"01_01",
	"00_11",
	"01_11",
	"11_11",
	"00_22",
	"01_22",
	"11_22",
	"22_22" };

// -----------------------------------------------------------------------
// 4-tensor interface
// -----------------------------------------------------------------------
size_type
tensor4_index (
    valued_type       valued,
    coordinate_type   sys_coord,
    size_type         i,
    size_type         j,
    size_type         k,
    size_type         l)
{
    size_type ij = tensor_index (tensor, sys_coord, i, j);
    size_type kl = tensor_index (tensor, sys_coord, k, l);
    return symmetric_tensor4_index[ij][kl];
}
size_type
tensor4_index (
    std::string valued,
    std::string sys_coord, 
    size_type   i,
    size_type   j,
    size_type   k,
    size_type   l)
{
    return tensor4_index (valued_tag(valued), coordinate_system(sys_coord), i, j, k, l);
}
std::pair<std::pair<size_type,size_type>, std::pair<size_type,size_type> >
tensor4_subscript (
    valued_type       valued,
    coordinate_type   sys_coord,
    size_type         i_comp)
{
    if (sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr) {
      return symmetric_tensor4_subscript_rz [i_comp];
    } else {
      return symmetric_tensor4_subscript    [i_comp];
    }
}
std::string
tensor4_subscript_name (
    valued_type       valued,
    coordinate_type   sys_coord,
    size_type         i_comp)
{
    if (sys_coord == axisymmetric_rz || sys_coord == axisymmetric_zr) {
      return symmetric_tensor4_subscript_rz_name [i_comp];
    } else {
      return symmetric_tensor4_subscript_name    [i_comp];
    }
}
std::pair<std::pair<size_type,size_type>, std::pair<size_type,size_type> >
tensor4_subscript (
    std::string     valued,
    std::string     sys_coord,
    size_type       i_comp)
{
    return tensor4_subscript (valued_tag(valued),
	         coordinate_system(sys_coord), i_comp);
}
std::string
tensor4_subscript_name (
    std::string     valued,
    std::string     sys_coord,
    size_type       i_comp)
{
    return tensor4_subscript_name (valued_tag(valued),
	         coordinate_system(sys_coord), i_comp);
}
// -----------------------------------------------------------------------
// field*field : compute the value_type result at run time
// -----------------------------------------------------------------------
// TODO: move it in operators.cc
static const valued_type multiplies_result_tag_table [tensor3+1][tensor3+1] = {
   {scalar,             vector,             tensor,             unsymmetric_tensor, tensor3},
   {vector,             last_valued,        last_valued,        last_valued,        last_valued},
   {tensor,             vector,             unsymmetric_tensor, unsymmetric_tensor, last_valued},
   {unsymmetric_tensor, vector,             unsymmetric_tensor, unsymmetric_tensor, last_valued},
   {tensor3,            unsymmetric_tensor, tensor3,            tensor3,            last_valued}  
};
valued_type
multiplies_result_tag (space_constant::valued_type tag1, space_constant::valued_type tag2)
{
  if (tag1 > tensor3 || tag2 > tensor3) return last_valued;
  return multiplies_result_tag_table [tag1][tag2];
}
static const valued_type divides_result_tag_table [tensor+1][tensor+1] = {
   {scalar,      last_valued, last_valued},
   {vector,      last_valued, last_valued},
   {tensor,      last_valued, last_valued}
};
valued_type
divides_result_tag (space_constant::valued_type tag1, space_constant::valued_type tag2)
{
  if (tag1 > tensor || tag2 > tensor) return last_valued;
  return divides_result_tag_table [tag1][tag2];
}
// ---------------------------------------------------------------------------
// init arrays of pairs:
// ---------------------------------------------------------------------------
// intel C++ v12 cannot support class initializers:
struct static_initializer_t {
  static_initializer_t();
};
static_initializer_t::static_initializer_t() {
  typedef std::pair<size_type,size_type> p;
    symmetric_tensor_subscript[0] = p(0,0);
    symmetric_tensor_subscript[1] = p(0,1);
    symmetric_tensor_subscript[2] = p(1,1);
    symmetric_tensor_subscript[3] = p(0,2);
    symmetric_tensor_subscript[4] = p(1,2);
    symmetric_tensor_subscript[5] = p(2,2);

  unsymmetric_tensor_subscript[0] = p(0,0);
  unsymmetric_tensor_subscript[1] = p(0,1);
  unsymmetric_tensor_subscript[2] = p(1,0);
  unsymmetric_tensor_subscript[3] = p(1,1);
  unsymmetric_tensor_subscript[4] = p(0,2);
  unsymmetric_tensor_subscript[5] = p(1,2);
  unsymmetric_tensor_subscript[6] = p(2,0);
  unsymmetric_tensor_subscript[7] = p(2,1);
  unsymmetric_tensor_subscript[8] = p(2,2);

  typedef std::pair<p,p> pp;
  symmetric_tensor4_subscript [ 0] = pp(p(0,0), p(0,0));
  symmetric_tensor4_subscript [ 1] = pp(p(0,0), p(0,1));
  symmetric_tensor4_subscript [ 2] = pp(p(0,1), p(0,1));
  symmetric_tensor4_subscript [ 3] = pp(p(0,0), p(1,1));
  symmetric_tensor4_subscript [ 4] = pp(p(0,1), p(1,1));
  symmetric_tensor4_subscript [ 5] = pp(p(1,1), p(1,1));
  symmetric_tensor4_subscript [ 6] = pp(p(0,0), p(0,2));
  symmetric_tensor4_subscript [ 7] = pp(p(0,1), p(0,2));
  symmetric_tensor4_subscript [ 8] = pp(p(1,1), p(0,2));
  symmetric_tensor4_subscript [ 9] = pp(p(0,2), p(0,2));
  symmetric_tensor4_subscript [10] = pp(p(0,0), p(1,2));
  symmetric_tensor4_subscript [11] = pp(p(0,1), p(1,2));
  symmetric_tensor4_subscript [12] = pp(p(1,1), p(1,2));
  symmetric_tensor4_subscript [13] = pp(p(0,2), p(1,2));
  symmetric_tensor4_subscript [14] = pp(p(1,2), p(1,2));
  symmetric_tensor4_subscript [15] = pp(p(0,0), p(2,2));
  symmetric_tensor4_subscript [16] = pp(p(0,1), p(2,2));
  symmetric_tensor4_subscript [17] = pp(p(1,1), p(2,2));
  symmetric_tensor4_subscript [18] = pp(p(0,2), p(2,2));
  symmetric_tensor4_subscript [19] = pp(p(1,2), p(2,2));
  symmetric_tensor4_subscript [20] = pp(p(2,2), p(2,2));

  symmetric_tensor4_subscript_rz [0] = pp(p(0,0), p(0,0));
  symmetric_tensor4_subscript_rz [0] = pp(p(0,0), p(0,1));
  symmetric_tensor4_subscript_rz [0] = pp(p(0,1), p(0,1));
  symmetric_tensor4_subscript_rz [0] = pp(p(0,0), p(1,1));
  symmetric_tensor4_subscript_rz [0] = pp(p(0,1), p(1,1));
  symmetric_tensor4_subscript_rz [0] = pp(p(1,1), p(1,1));
  symmetric_tensor4_subscript_rz [0] = pp(p(0,0), p(2,2));
  symmetric_tensor4_subscript_rz [0] = pp(p(0,1), p(2,2));
  symmetric_tensor4_subscript_rz [0] = pp(p(1,1), p(2,2));
  symmetric_tensor4_subscript_rz [0] = pp(p(2,2), p(2,2));
}
static static_initializer_t dummy;

}} // namespace rheolef::space_constant
