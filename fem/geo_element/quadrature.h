#ifndef _RHEO_QUADRATURE_H
#define _RHEO_QUADRATURE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/smart_pointer.h"
#include "rheolef/persistent_table.h"
#include "rheolef/reference_element.h"
#include "rheolef/point.h"
#include "rheolef/integrate_option.h"
#include "rheolef/reference_element_face_transformation.h"
#include "rheolef/compiler_eigen.h"

namespace rheolef { 

/*Class:quadrature
NAME: @code{quadrature} - quadrature formulae on the reference lement
@cindex  quadrature formulae
@clindex quadrature
@clindex reference_element
SYNOPSIS:
@noindent
The @code{quadrature} class defines a container for a quadrature
formulae on the reference element (@pxref{reference_element iclass}).
This container stores the nodes coordinates and the weights.
The constructor takes two arguments: the reference element 
@math{K}
and the order @math{r} of the quadrature formulae.
The formulae is exact when computing the integral
of a polynom @math{p} that degree is less or equal to order @math{r}.
@example
                  n
    /            ___
    | p(x) dx =  \    p(x_q) w_q
    / K          /__
                 q=1
@end example
LIMITATIONS:
@noindent
The formulae is optimal when it uses a minimal number
of nodes @math{n}.
Optimal quadrature formula are hard-coded in this class.
Not all reference elements and orders are yet 
implemented. This class will be completed in the future.

AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
   | Pierre.Saramito@imag.fr
DATE:   30 november 2003
End:
*/

template<class T>
struct weighted_point {
  weighted_point () : x(), w(0) {}
  weighted_point (const point_basic<T>& x1, const T& w1) : x(x1), w(w1) {}
  point_basic<T> x;
  T              w;
};

// ----------------------------------------------------------------------------
// quadrature on a specific ref element
// ----------------------------------------------------------------------------
template<class T>
class quadrature_on_geo : public std::vector<weighted_point<T> > {
public:

// typedefs:

    typedef std::vector<weighted_point<T> > base;
    typedef typename base::size_type        size_type;
    typedef point_basic<T>                  x;

// alocators/deallocators:

    quadrature_on_geo ();

    quadrature_on_geo (const quadrature_on_geo& q) : base(q) {}
    quadrature_on_geo& operator= (const quadrature_on_geo& q) {
	base::operator= (q);
        return *this;
    }

// accessor:

    void get_nodes (Eigen::Matrix<point_basic<T>, Eigen::Dynamic, 1>& node) const;

// modifier:

    void initialize       (reference_element hat_K, quadrature_option opt);
    void init_point       (quadrature_option opt);
    void init_edge        (quadrature_option opt);
    void init_triangle    (quadrature_option opt);
    void init_square      (quadrature_option opt);
    void init_tetrahedron (quadrature_option opt);
    void init_prism       (quadrature_option opt);
    void init_hexahedron  (quadrature_option opt);

    void wx (const point_basic<T>& x, const T& w) {
	    base::push_back (weighted_point<T>(x,w)); }

    static size_type n_node_gauss (size_type r);
    
    template<class U>
    friend std::ostream& operator<< (std::ostream&, const quadrature_on_geo<U>&);
};
// ----------------------------------------------------------------------------
// quadrature representation
// ----------------------------------------------------------------------------
template<class T>
class quadrature_rep {
public:

// typedefs:

    typedef typename quadrature_on_geo<T>::size_type                 size_type;
    typedef quadrature_option::family_type                      family_type;
    typedef typename std::vector<weighted_point<T> >::const_iterator const_iterator;
    typedef geo_element_indirect::orientation_type 		     orientation_type;

// allocators:

    ~quadrature_rep();
    quadrature_rep (quadrature_option opt = quadrature_option());
    quadrature_rep (const std::string& name);
    quadrature_rep (const quadrature_rep<T>& q);
    const quadrature_rep& operator= (const quadrature_rep<T>& q);

#ifdef TO_CLEAN
// modifiers:

    void set_order  (size_type order);
    void set_family (family_type ft);
#endif // TO_CLEAN

// accessors:

    std::string    name() const;
    size_type      get_order() const;
    family_type    get_family() const;
    std::string    get_family_name() const;
    const quadrature_option& get_options() const;
    size_type      size  (reference_element hat_K) const;
    const_iterator begin (reference_element hat_K) const;
    const_iterator end   (reference_element hat_K) const;
    const weighted_point<T>& operator() (reference_element hat_K, size_type q) const;
    void get_nodes (reference_element hat_K, Eigen::Matrix<point_basic<T>, Eigen::Dynamic, 1>& node) const;

    template<class U>
    friend std::ostream& operator<< (std::ostream&, const quadrature_rep<U>&);

protected:
// internal:
    void _initialize (reference_element hat_K) const;
// data:
    quadrature_option        _options;
    mutable std::array<quadrature_on_geo<T>,reference_element::max_variant>
				  _quad;
    mutable std::vector<bool>     _initialized;
public:
    static quadrature_rep* make_ptr (const std::string& name) { return new_macro(quadrature_rep(name)); }
};
// ----------------------------------------------------------------------------
// quadrature class
// ----------------------------------------------------------------------------
//<quadrature:
template<class T>
class quadrature : public smart_pointer<quadrature_rep<T> >,
                   public persistent_table<quadrature<T>> {
public:

// typedefs:

    typedef quadrature_rep<T>              rep;
    typedef smart_pointer<rep>             base;
    typedef typename rep::size_type        size_type;
    typedef typename rep::family_type      family_type;
    typedef typename rep::const_iterator   const_iterator;
    typedef typename rep::orientation_type orientation_type;

// allocators:


    quadrature (const std::string& name = "");
    quadrature (quadrature_option opt);

// modifiers:

    void set_order  (size_type order);
    void set_family (family_type ft);
    void reset (const std::string& name);

// accessors:

    const quadrature_option& get_options() const;
    std::string    name() const            { return get_options().name(); }
    size_type      get_order() const       { return get_options().get_order();}
    family_type    get_family() const      { return get_options().get_family();}
    std::string    get_family_name() const { return get_options().get_family_name();}
    size_type      size  (reference_element hat_K) const { return base::data().size(hat_K); }
    const_iterator begin (reference_element hat_K) const { return base::data().begin(hat_K); }
    const_iterator end   (reference_element hat_K) const { return base::data().end(hat_K); }
    const weighted_point<T>& operator() (reference_element hat_K, size_type q) const
    							 { return base::data().operator() (hat_K,q); }
    void get_nodes (reference_element hat_K, Eigen::Matrix<point_basic<T>, Eigen::Dynamic, 1>& node) const
    							 { return base::data().get_nodes(hat_K,node); }
};
//>quadrature:
template<class T>
inline
std::ostream& operator<< (std::ostream& os, const quadrature<T>& q)
{
  return os << q.data();
}

// ------------------------------------------------------------
// inlined
// ------------------------------------------------------------
template<class T>
inline 
quadrature_on_geo<T>::quadrature_on_geo ()
 : std::vector<weighted_point<T> >()
{
}
template<class T>
inline
typename quadrature_on_geo<T>::size_type
quadrature_on_geo<T>::n_node_gauss (size_type r)
{
    // when using n nodes : gauss quadrature formulae order is r=2*n-1
    if (r == 0) return 1;
    size_type n = (r % 2 == 0) ? r/2+1 : (r+1)/2;
    return std::max(size_t(1), n);
}
template<class T>
inline 
typename quadrature_rep<T>::size_type
quadrature_rep<T>::get_order () const
{
    return _options.get_order();
}
template<class T>
inline 
typename quadrature_rep<T>::family_type
quadrature_rep<T>::get_family () const
{
    return _options.get_family();
}
template<class T>
inline 
std::string
quadrature_rep<T>::get_family_name () const
{
    return _options.get_family_name();
}
template<class T>
inline 
const quadrature_option&
quadrature_rep<T>::get_options () const
{
    return _options;
}
#ifdef TO_CLEAN
template<class T>
inline
void
quadrature_rep<T>::set_order (size_type r)
{
  if (get_order() != r) {
    // do not re-initialize nodes-weights if unchanged
    _options.set_order(r);
    std::fill (_initialized.begin(), _initialized.end(), false);
  }
}
template<class T>
inline
void
quadrature_rep<T>::set_family (family_type ft)
{
  if (get_family() != ft) {
    // do not re-initialize nodes-weights if unchanged
    _options.set_family(ft);
    std::fill (_initialized.begin(), _initialized.end(), false);
  }
}
#endif // TO_CLEAN

}// namespace rheolef
#endif // _RHEO_QUADRATURE_H
