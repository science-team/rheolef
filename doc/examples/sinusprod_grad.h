///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusprod_grad.h The sinus product function -- its gradient
struct grad_u {
  point operator() (const point& x) const { 
    switch (d) {
     case 0:  return point();
     case 1:  return pi*point(cos(pi*x[0]));
     case 2:  return pi*point(cos(pi*x[0])*sin(pi*x[1]),
                              sin(pi*x[0])*cos(pi*x[1])); 
     default: return pi*point(cos(pi*x[0])*sin(pi*x[1])*sin(pi*x[2]),
                              sin(pi*x[0])*cos(pi*x[1])*sin(pi*x[2]), 
                              sin(pi*x[0])*sin(pi*x[1])*cos(pi*x[2]));
    }
  }
  grad_u (size_t d1) : d(d1), pi(acos(Float(-1.0))) {}
  size_t d; Float pi;
};
