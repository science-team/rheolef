#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
geo="../../main/tst/carre-bamg-splitbedge-v2.geo"

w=1;
#w="-"; # w_opt

n=${2-"200"}
p=1.0
pf=3.0
i=1
dp=`echo $p $pf $n | gawk '{print ($2-$1)/(1.0*($3-1)) }'`
echo "# p  v_linf v_l2"
while test $i -le $n; do
 ./p_laplacian_fixed_point $geo P1 $p $w 1e-10 >/dev/null 2> cv-$w.gdat
 v_linf=`grep -v warn cv-$w.gdat | tail -1 | gawk '{print $3}'`
 v_l2=`grep -v warn cv-$w.gdat | tail -1 | gawk '{print $5}'`
 echo $p $v_linf $v_l2
 p=`echo $p $dp | gawk '{print $1+$2}'`
 i=`expr $i + 1`
done
