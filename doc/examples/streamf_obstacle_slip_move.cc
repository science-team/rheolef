///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile streamf_obstacle_slip_move.cc The stream function for the obstacle benchmark with slip boundary condition
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main (int argc, char** argv) {
  environment rheolef (argc, argv);
  field uh;
  din >> uh;
  point e0 (1,0);
  uh = lazy_interpolate (uh.get_space(), e0 - uh);
  string approx = "P" + to_string(uh.get_space().degree());
  const geo& omega = uh.get_geo();
  space Ph (omega, approx);
  Ph.block ("wall");
  Ph.block ("axis");
  Ph.block ("downstream");
  field psi_h (Ph,0.);
  trial psi (Ph);
  test   xi (Ph);
  integrate_option iopt;
  iopt.ignore_sys_coord = true;
  form a = integrate ( dot(grad(psi), grad(xi)), iopt);
  field lh = integrate (dot(uh,bcurl(xi)));
  problem p (a);
  p.solve (lh, psi_h);
  dout << catchmark("psi") << psi_h;
}
