%option noyywrap
%{
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// =========================================================================
// lexer for space_constitution header field files
//
// author: Pierre.Saramito@imag.fr
//
// date: 19 decembre 2002
//
%}
%%
[\n]                      { /* newline */ ++space_constitution_old_line_no; }
[ \t]                     { /* space  */; }
"vector"		  { yylval.string_value = insert(yytext); return VALUED; }
"tensor"		  { yylval.string_value = insert(yytext); return VALUED; }
"unsymmetric_tensor"	  { yylval.string_value = insert(yytext); return VALUED; }
[a-zA-Z0-9\-_=@$;\.\[\]]+ { yylval.string_value = insert(yytext); return IDENTIFIER; }
.			  { return *yytext ;  /* default */ }
%%

