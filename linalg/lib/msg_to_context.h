#ifndef RHEO_MSG_TO_CONTEXT_H
#define RHEO_MSG_TO_CONTEXT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

# include "rheolef/msg_util.h"
namespace rheolef {

/*F:
NAME: msg_to_context -- receive pattern (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Computes the receive compresed message pattern for gather
  and scatter.
  The local message part is computed by a separate algorithm
  (see "msg_to_local_context"(5)).
ALGORITHM:
  msg_to_context

  "input": the receive pattern and the permutation
  |   perm(0:receive_nproc-1)
  |   r_iproc(0:receive_nproc-1), r_size(0:receive_nproc-1), 
  |   r_idx(0:receive_nproc*receive_max_size-1)
  "output": the receive context (to)
  |   to_proc(0:receive_nproc-1), to_ptr(0:receive_nproc), 
  |   to_idx(0:receive_total_size-1)
  begin
  |     to_ptr(0) := 0
  |     for j := 0 to receive_nproc-1 do
  |       j1 := perm(j)
  |       to_proc(j) := r_iproc(j1)
  |       to_ptr(j+1) := r_ptr(j) + rsize(j1)
  |       for q := to_ptr(j) to to_tr(j+1)-1 do
  |         to_idx(q) := r_idx(j1, q-to_ptr(j)) - istart
  |       endfor
  |     endfor
  end

COMPLEXITY:
  Memory and time complexity is O(receive_total_size).

METHODS: @msg_to_context 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   23 march 1999
END:
*/

//<msg_to_context:
template <
    class InputIterator1,
    class InputRandomIterator2,
    class InputRandomIterator3,
    class InputRandomIterator4,
    class Size,
    class OutputIterator1,
    class OutputIterator2,
    class OutputIterator3>
void
msg_to_context (
    InputIterator1 		perm,		// receive_nproc
    InputIterator1 		last_perm,	
    InputRandomIterator2 	r_iproc,	// receive_nproc
    InputRandomIterator3 	r_size,		// receive_nproc
    InputRandomIterator4 	r_idx,		// receive_nproc*receive_max_size
    Size      			receive_max_size,
    Size      			istart,
    OutputIterator1    		to_proc,	// receive_nproc
    OutputIterator2    		to_ptr,		// receive_nproc+1
    OutputIterator3    		to_idx)		// receive_total_size
{
    OutputIterator2 prec_ptr = to_ptr;
    (*to_ptr++) = 0;
    while (perm != last_perm) {
	Size j1 = (*perm++);
	(*to_proc++) = r_iproc[j1];
	Size size = r_size[j1];
	(*to_ptr++) = (*prec_ptr++) + size;
	InputRandomIterator4 iter_idx = r_idx + j1*receive_max_size;
	InputRandomIterator4 last_idx = iter_idx + size;
	while (iter_idx != last_idx)
	    (*to_idx++) = (*iter_idx++) - istart;
    }
}

//>msg_to_context:
} // namespace rheolef
#endif // RHEO_MSG_TO_CONTEXT_H
