///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/rheostream.h"
using namespace rheolef;
using namespace std;

void
usage() {
   cerr << "orheostream_append_tst: usage: orheostream_append_tst "
	<< " filename suffix [app]"
	<< endl;
   
   cerr << " ex: orheostream_tst toto txt\n";
   exit (1);
}
int main(int argc, char** argv)
{
    if (argc < 3) usage();
    io::mode_type mode = (argc < 4) ? io::out : io::app;
    orheostream data(argv[1], argv[2], mode);
    if (!data) 
      fatal_macro("orheostream_tst: \"" 
	<< argv[1] << "." << argv[2] << ".gz\" creation failed.");

    while (cin) {
        int c = cin.get();
        if (c != EOF) data.put(c);
    }
    data.close();
}
