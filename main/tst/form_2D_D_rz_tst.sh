#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
GEODIR=$SRCDIR
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

run "../sbin/mkgeo_grid_2d -v4 -t 10 2>/dev/null | ../bin/geo -upgrade - > mesh-2d.geo 2>/dev/null"

APP="P1 P2"
Cz="0 1"
C="0"
for app in $APP; do
 case $app in
 P1) P="1 r z";;
 P2) P="1 r z r2 rz z2";;
 esac
 for i in $C; do
  for j in $Cz; do
   for p in $P; do
    for q in $P; do
     loop_mpirun "./form_2D_D_rz_tst mesh-2d -approx $app -u-monom $p -v-monom $q -u-component $i -v-component $j >/dev/null 2>/dev/null"
     if test $? -ne 0; then status=1; fi
    done
   done
  done
 done
done

run "/bin/rm -f mesh-2d.geo"

exit $status

