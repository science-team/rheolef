#!geo

mesh
4
header
 dimension 3
 nodes	4
 tetrahedra	1
 triangles	4
 edges	6
end header

0 0 0
1 0 0
0 1 0
0 0 1

T	0 1 2 3

t	0 2 1
t	0 3 2
t	0 1 3
t	1 2 3
e	0 1
e	1 2
e	2 0
e	0 3
e	1 3
e	2 3

domain
boundary
2 2 4
0
1
2
3
