# ---  Rheolef ci templates ---
# 
# This file contains common definitions and templates for
# continuous integration job of rheolef project.
#
# Usage :
# add in .gitlab-ci.yml :
# include: <path-to-this-file>/ci-rheolef-templates.yml


variables:
  # Path to dockerfiles (relative to siconos repo)
  docker_path: ci_gitlab/dockerfiles
  # Default tag for docker images
  docker_tag: latest
  # By default we allow builds to run in parallel, but certain
  # configurations may require non-parallel builds
  allow_parallel_build: 1

stages:
  # --- Docker build stage ---
  # The first stage contains jobs used to build
  # docker images 'ready to use' for a Rheolef build/install.
  # Requirement for jobs in this stage :
  # - should build and push a docker image to rheolef project registry
  # - should be allowed to failed (in order to avoid blocking of last stage jobs)
  # - should run only when commit message contains [kaniko-build]
  # - use Dockerfile from ci_gitlab/dockerfiles/<image-name>
  #
  # Templates for these jobs : .docker-build (see below).
  - kaniko-build
  # --- Build stage ---
  # jobs run on images generated in previous stage, available in rheolef registry:
  # https://gricad-gitlab.univ-grenoble-alpes.fr/rheolef/rheolef/container_registry
  # - configure and build rheolef
  # - run tests
  # -install rheolef
  #
  # Templates for these jobs : .rheolef-build (see below).
  - configure
  - build
  - test
  - install


# --- Templates definitions ---
# Build a docker image, with all required dependencies for rheolef.
# Then, push this image to the registry.
.kaniko-build:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: kaniko-build
 
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/ci_gitlab/dockerfiles/$IMAGE_NAME/Dockerfile --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest 
  only:
    variables: # Run this job only when commit message starts with [docker-build]
      - $CI_COMMIT_MESSAGE =~ /^\[kaniko-build\].*/i      

# -- Template used to define jobs to build rheolef --
# - Pull an image (possibly from rheolef registry)
#   named  IMAGE_NAME
# - Run for all branches and for all push
# - bootstrap/configure rheolef.
# - use artifacts to keep results for next stage.
.rheolef-configure:
  image: $IMAGE_NAME
  stage: configure
  script:
    - "sh ci_gitlab/configure_rheolef.sh"
  artifacts: # keep build dir for next jobs
    paths:
      - build
    expire_in: 2 days  

# -- Template used to define jobs to build rheolef --
# - Pull an image (possibly from rheolef registry)
#   named  IMAGE_NAME
# - Run for all branches and for all push
# - Use 'configure' stage artifacts
# - Build rheolef and run make check
.rheolef-build:
  image: $IMAGE_NAME
  variables:
    GIT_STRATEGY: none # do not clean git repo to keep configure results !
  stage: build
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 4
    - make check
  artifacts: # keep build dir for next jobs
    paths:
      - build
    expire_in: 2 days  

# 
.rheolef-ready:
  stage: install
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    #GIT_STRATEGY: none
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/ci_gitlab/dockerfiles/$DOCKERFILE_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE/rheolef-$IMAGE_NAME:$CI_COMMIT_SHORT_SHA --destination $CI_REGISTRY_IMAGE/rheolef-$IMAGE_NAME:latest
  when: manual

