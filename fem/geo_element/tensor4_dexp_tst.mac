/*
 * This file is part of Rheolef.
 *
 * Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
 *
 * Rheolef is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rheolef is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rheolef; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * =========================================================================
 */
/*
 * http://www.ma.utexas.edu/pipermail/maxima/2006/003031.html
 * /usr/share/maxima/5.27.0/share/linearalgebra/matrixexp.usage
*/
load(linearalgebra)$
load(f90)$
assume(z>0);
/*
z:1/exp(n);
z:0;
*/
mv: matrix([y,z],[z,y]);
m : matrix([a,b],[b,c]);
domxexpt:false;
cc:factor(matrixexp(m));
dcc_00_ij : matrix([0,0],[0,0]);
dcc_01_ij : matrix([0,0],[0,0]);
dcc_11_ij : matrix([0,0],[0,0]);

my_filter(arg) := limit(arg,z,0);

for i from 1 thru 2 do
for j from 1 thru 2 do
block(
  dcc_00_ij[i][j] : my_filter(
      subst(mv[1][1],m[1][1],
      subst(mv[2][2],m[2][2],
      subst(mv[1][2],m[1][2], diff(cc[1][1],m[i][j]))))),
  dcc_01_ij[i][j] : my_filter(
      subst(mv[1][1],m[1][1],
      subst(mv[2][2],m[2][2],
      subst(mv[1][2],m[1][2], diff(cc[1][2],m[i][j]))))),
  dcc_11_ij[i][j] : my_filter(
      subst(mv[1][1],m[1][1],
      subst(mv[2][2],m[2][2],
      subst(mv[1][2],m[1][2], diff(cc[2][2],m[i][j])))))
);
mm: mv;
cc_00_ij; dcc_00_ij;
cc_01_ij; dcc_01_ij;
cc_11_ij; dcc_11_ij;

