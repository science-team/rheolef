///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "piola_fem_lagrange.h"
#include "piola_fem_grad_post.icc"
namespace rheolef { 

// --------------------------------------------------------------------------
// scalar-valued case
// --------------------------------------------------------------------------
template<class T>
void
piola_fem_lagrange<T>::transform (
  const piola<T>& p,
  const T&        hat_u,
        T&            u) const
{
  u = hat_u;
}
template<class T>
void
piola_fem_lagrange<T>::inv_transform (
  const piola<T>& p,
  const T&            u,
        T&        hat_u) const
{
  hat_u = u;
}
template<class T>
void
piola_fem_lagrange<T>::grad_transform (
  const piola<T>&                           p,
  const T&                              hat_u,
  const point_basic<T>&            hat_grad_u,
  const details::differentiate_option&       gopt,     
        point_basic<T>&                grad_u) const
{
  // grad_u = DF^{-T}*grad_hat_u
  grad_u = p.invDF.trans_mult (hat_grad_u); // TODO: DVT_OPTIM_2D
  const T& u = hat_u;
  grad_post (p, gopt, u, grad_u);
}
// --------------------------------------------------------------------------
// vector-valued case
// --------------------------------------------------------------------------
template<class T>
void
piola_fem_lagrange<T>::transform (
  const piola<T>&         p,
  const point_basic<T>&   hat_u,
        point_basic<T>&       u) const
{
  u = hat_u;
}
template<class T>
void
piola_fem_lagrange<T>::inv_transform (
  const piola<T>&         p,
  const point_basic<T>&       u,
        point_basic<T>&   hat_u) const
{
  hat_u = u;
}
template<class T>
void
piola_fem_lagrange<T>::grad_transform (
  const piola<T>&                           p,
  const point_basic<T>&                 hat_u,
  const tensor_basic<T>&           hat_grad_u,
  const details::differentiate_option&       gopt,     
        tensor_basic<T>&               grad_u) const
{
  // grad_u = grad_hat_u*DF^{-1}
  grad_u = hat_grad_u*p.invDF; // TODO: DVT_OPTIM_2D
  const point_basic<T>& u = hat_u;
  grad_post (p, gopt, u, grad_u);
}
// --------------------------------------------------------------------------
// tensor-valued case
// --------------------------------------------------------------------------
template<class T>
void
piola_fem_lagrange<T>::transform (
  const piola<T>&         p,
  const tensor_basic<T>&   hat_u,
        tensor_basic<T>&       u) const
{
  u = hat_u;
}
template<class T>
void
piola_fem_lagrange<T>::inv_transform (
  const piola<T>&         p,
  const tensor_basic<T>&       u,
        tensor_basic<T>&   hat_u) const
{
  hat_u = u;
}
template<class T>
void
piola_fem_lagrange<T>::grad_transform (
  const piola<T>&                           p,
  const tensor_basic<T>&                 hat_u,
  const tensor3_basic<T>&           hat_grad_u,
  const details::differentiate_option&        gopt,     
        tensor3_basic<T>&               grad_u) const
{
  // grad_u_{ijk} = grad_hat_u_{ijl}*DF^{-1}_{lk}
  grad_u = hat_grad_u*p.invDF; // TODO: DVT_OPTIM_2D
  const tensor_basic<T>& u = hat_u;
  grad_post (p, gopt, u, grad_u);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                               \
template class piola_fem_lagrange<T>;						\

_RHEOLEF_instanciation(Float)

}// namespace rheolef
