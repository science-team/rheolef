#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
geo="mesh-2d.geo"
geo="square-20.geo"
p=${1-"3"}
n=${2-"20"}
w=0.01
wf=2.0
i=1
dw=`echo $w $wf $n | gawk '{print ($2-$1)/(1.0*($3-1)) }'`
echo "# p = $p"
echo "# relax  v"
while test $i -le $n; do
 ./p_laplacian_fixed_point $geo P1 $p $w 1e-10 >/dev/null 2> cv-$w.gdat
 v=`grep -v warn cv-$w.gdat | tail -1 | gawk '{print $3}'`
 echo $w $v
 w=`echo $w $dw | gawk '{print $1+$2}'`
 i=`expr $i + 1`
done
