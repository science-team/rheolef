## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ${top_builddir}/config/config.mk

# -----------------------------------------------------------------------------
# documentation
# -----------------------------------------------------------------------------

DOC_3_LIST = 							\
	catchmark

DOC_7_LIST = 							\
	smart_pointer						\
	persistent_table					\
	rheostream						

# TODO_DOX: all manN
DOC_TODO = 							\
	heap_allocator.7					\
	stack_allocator.7					\
	Vector.7 						\
	iorheo.7 						\
	pretty_name.7

if HAVE_DOX_DOCUMENTATION
man_MANS = 							\
	$(DOC_3_LIST:=.3rheolef)				\
	$(DOC_7_LIST:=.7rheolef)

DOC_DOX  = 							\
	$(DOC_3_LIST:=.man3)					\
	$(DOC_7_LIST:=.man7)
else
man_MANS =
DOC_DOX  =
endif
# -----------------------------------------------------------------------------
# the file set
# -----------------------------------------------------------------------------


pkginclude_HEADERS = 						\
	smart_pointer.h						\
	persistent_table.h					\
	pretty_name.h 						\
	promote.h 						\
	heap_object.h						\
	heap_allocator.h					\
	stack_allocator.h					\
	Vector.h 						\
	iorheo.h 						\
	iorheo-members.h 					\
	catchmark.h 						\
	iorheobase.h 						\
	rheostream.h 						\
	misc_algo.h 						\
	pair_util.h 						\
	rounder.h

noinst_HEADERS =						\
	scatch.icc 						

noinst_LTLIBRARIES = libutil.la
libutil_la_SOURCES = 						\
	iorheo.cc 						\
	rheostream.cc 						\
	pretty_name.cc

libutil_la_LIBADD = 						\
        $(LDADD_BOOST_IO)					\
        $(LDADD_DMALLOC)					\
	$(LDADD_FLOAT)

EXTRA_DIST = Makefile.am 

CLEANFILES = *.ii *.ii_pic *.da *.gcov *.[oa1359] *.o.d		\
	cxx_repository/* KCC_files/* KCC_pic/*  		\
	*.[1-9]rheolef *.man[1-9]				\
	stamp-symlink.in 

CVSIGNORE = Makefile.in stamp-symlink.in $(srcdir)/Makefile.in
WCIGNORE  = 

# -----------------------------------------------------------------------------
# extra rules
# -----------------------------------------------------------------------------

all-local: dvi-local
dvi-local: $(man_MANS) $(DOC_DOX)

check-local: all


clean-local:
	rm -rf cxx_repository
	for f in $(DEPDIR)/*; do		\
	    rm -f $$f;			\
	    touch $$f;				\
	done


AM_CPPFLAGS = 					\
	-I${top_builddir}/include 		\
	$(INCLUDES_BOOST)			\
	$(INCLUDES_FLOAT)

smart_pointer.3: smart_pointer.h ${top_builddir}/config/doc2man
	cat ${srcdir}/smart_pointer.h | \
	  ${SRC2MAN} -name smart_pointer -section 3 \
	  >  smart_pointer.3
smart_pointer.3texi: smart_pointer.h ${top_builddir}/config/doc2texi
	cat $(srcdir)/smart_pointer.h | \
	  ${SRC2TEXI} -file smart_pointer.h -node 'smart_pointer internal,,, Classes' \
	  > smart_pointer.3texi

BUILT_SOURCES =								\
	stamp-symlink.in

stamp-symlink.in: Makefile.am
	@$(MKSYMLINK) $(pkginclude_HEADERS)
	@touch stamp-symlink.in

