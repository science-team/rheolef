///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// input space_constitution files:
//   idiststream& operator >> (idiststream&, space_constitution&);
//
// author: Pierre.Saramito@imag.fr
//
// date: 19 dec 2011
//
#include "space_constitution_get.icc"

// ================================================================================
// part 1 : read from idiststeam and build as tree_type* result_ptr
// ================================================================================
/* AIX requires this to be the first thing in the file.  */
#ifndef __GNUC__
# if _RHEOLEF_HAVE_ALLOCA_H
#  include <alloca.h>
# else
#  ifdef _AIX
#pragma alloca
#  else
#   ifndef alloca /* predefined by HP cc +Olibcalls */
char *alloca ();
#   endif
#  endif
# endif
#endif

namespace rheolef {

using namespace std;

typedef size_t size_type;

static size_type space_constitution_line_no = 1;
static size_type space_constitution_n_error = 0;

extern int space_constitution_lex();
void space_constitution_error (const char* msg) {
  std::string near;
  error_macro("space constitution input:" << space_constitution_line_no << ": " << msg);
  space_constitution_n_error++;
}
int space_constitution_wrap () { return 1; }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#define YYMALLOC ::malloc
#define YYFREE   ::free
#include "space_constitution_yacc.cc"
// avoid re-definition of YY_NULL within flex
#ifdef YY_NULL
#undef YY_NULL
#endif
#include "space_constitution_lex.cc"
#pragma GCC diagnostic pop

static yyFlexLexer input_space_constitution;

int space_constitution_lex() { return input_space_constitution.yylex(); }

// ================================================================================
// part 2 : main call
// ================================================================================
template<class T, class M>
idiststream&
operator>> (idiststream& ids, space_constitution<T,M>& constit)
{
  space_constitution_get_pass_1_2 (ids, space_constitution_parse,
    input_space_constitution, space_constitution_line_no, space_constitution_n_error); 
  // convert tree_type result_ptr to space_constitution
  const tree_type* ptr = result_ptr;
  constit = build_from_tree<T,M> (*ptr);
  delete_macro (result_ptr); result_ptr = 0;
  return ids;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template idiststream& operator>> (idiststream&, space_constitution<Float,sequential>&);

#ifdef _RHEOLEF_HAVE_MPI
template idiststream& operator>> (idiststream&, space_constitution<Float,distributed>&);
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
