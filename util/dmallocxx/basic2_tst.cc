///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*
  setenv DMALLOC_OPTIONS "debug=0x14f47d83,log=a.log"
  c++ basic_test.cc -ldmallocxx -o basic_test
  ./basic_test
  more a.log
 */

#include <dmalloc.h>
#include "return.h"

inline
void *operator new(size_t size, const char *const file, int line)
{
  if (size == 0) return (void*)0;
  return dmalloc_malloc(file, line, size, DMALLOC_FUNC_NEW,
			0 /* no alignment */, 0 /* no xalloc messages */);
}
inline
void *operator new[](size_t size,  const char *const file, int line)
{
  if (size == 0) return (void*)0;
  return dmalloc_malloc(file, line, size, DMALLOC_FUNC_NEW_ARRAY,
			0 /* no alignment */, 0 /* no xalloc messages */);
}
inline
void
operator delete(void *pnt)
{
  char	*file;
  GET_RET_ADDR(file);
  dmalloc_free(file, 0, pnt, DMALLOC_FUNC_DELETE);
}
inline
void
operator delete[](void *pnt)
{
  char	*file;
  GET_RET_ADDR(file);
  dmalloc_free(file, 0, pnt, DMALLOC_FUNC_DELETE_ARRAY);
}
# define 	new_macro(obj)            new (__FILE__ , __LINE__) obj
# define 	new_tab_macro(typ, n)     (new (__FILE__ , __LINE__) typ [(n)])
# define        delete_macro(ptr)         { if (ptr) delete (ptr); }
# define        delete_tab_macro(ptr)     { if (ptr) delete [] (ptr); }

int main()
{
  char *fred;
  char *test;

  test = new_macro(char);
  test = new_macro(char);
  delete test;

  fred = new_tab_macro(char,20);
  fred = new_tab_macro(char,20);
  delete [] fred;
  return 0;
}
