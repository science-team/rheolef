//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// from a bug was detected when nproc >= 2
// in segalman_cavity_log_statio_continuation_g.cc
// => fixed now in rheolef-7.1
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Th (omega, "P1d", "tensor");
  space Xh (omega, "P2", "vector");
  space Qh (omega, "P1");
  space IR = space::real();
  Xh.block("top");  Xh.block("bottom");
  Xh.block("left"); Xh.block("right");
  space Yh = Th*Xh*Qh*IR;
  field uh (Xh, 1);
  field yh (Yh, 1);
  field yh1 = yh[1];
  check_macro (yh1.b().size() == uh.b().size(), "invalid sizes");
  Float e1b = norm (vec<Float>(yh1.b() - uh.b()));
  derr << "e1b="<<e1b<<endl;
  return e1b < 1e-7 ? 0 : 1;
}
