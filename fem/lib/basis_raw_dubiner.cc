///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "basis_raw_dubiner.h"
#include "basis_ordering.icc"
#include "dubiner.icc"

namespace rheolef {
using namespace std;

// =========================================================================
// basis raw dubiner members
// =========================================================================
template<class T>
basis_raw_dubiner<T>::~basis_raw_dubiner()
{
}
template<class T>
basis_raw_dubiner<T>::basis_raw_dubiner (std::string name)
  : basis_raw_rep<T> (name),
    _inod2ideg(),
    _value_ad()
{
  if ((name.length()) > 0 && (name[0] == 'D')) {
    // TODO: check also that name fits "Dk" where is an k integer
    base::_degree = atoi(name.c_str()+1);
  } else if (name.length() > 0) { // missing 'D' !
    error_macro ("invalid polynomial name `"<<name<<"' for the Dk raw polynomial set");
  } else {
    // empty name : default cstor
    base::_degree = 0;
  }
}
template<class T>
typename basis_raw_dubiner<T>::size_type
basis_raw_dubiner<T>::ndof (reference_element hat_K) const
{
  return reference_element::n_node (hat_K.variant(), base::_degree);
}
template<class T>
void
basis_raw_dubiner<T>::_initialize (reference_element hat_K) const
{
  build_inod2ideg (hat_K, base::_degree, _inod2ideg[hat_K.variant()]);
  _value_ad [hat_K.variant()].resize (ndof(hat_K));
}
// evaluation of all basis functions at hat_x:
template<class T>
void
basis_raw_dubiner<T>::evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const
{
  base::_initialize_guard (hat_K);
  eval_dubiner_basis (hat_x, hat_K, base::_degree, _inod2ideg[hat_K.variant()], value);
}
template<class T>
void
basis_raw_dubiner<T>::grad_evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&     value) const
{
  base::_initialize_guard (hat_K);
  std::vector<ad3_basic<T> >& value_ad = _value_ad [hat_K.variant()];
  point_basic<ad3_basic<T> > hat_x_ad = ad3::point (hat_x);
  eval_dubiner_basis (hat_x_ad, hat_K, base::_degree, _inod2ideg[hat_K.variant()], value_ad);
  size_t loc_ndof = value_ad.size();
  value.resize(loc_ndof);
  for (size_t loc_idof = 0; loc_idof < loc_ndof; loc_idof++) {
    value[loc_idof] = value_ad[loc_idof].grad();
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template class basis_raw_dubiner<T>;

_RHEOLEF_instanciation(Float)

}// namespace rheolef
