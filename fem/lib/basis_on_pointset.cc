///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/basis_on_pointset.h"
#include "rheolef/memorized_value.h"
#include "rheolef/pretty_name.h"
#include "basis_on_pointset_evaluate.icc"
namespace rheolef {

// ----------------------------------------------------------
// name
// ----------------------------------------------------------
template<class T>
std::string
basis_on_pointset_rep<T>::_make_name(
  mode_type          mode,
  const std::string& basis_name,
  const std::string& pointset_name)
{
  if (mode == max_mode) {
    return "";
  } else if (mode == quad_mode) {
    return basis_name + "@q(" + pointset_name + ")";
  } else {
    return basis_name + "@b(" + pointset_name + ")";
  }
}
template<class T>
std::string
basis_on_pointset_rep<T>::name() const
{
  if (_mode == max_mode) {
    return "";
  } else if (_mode == quad_mode) {
    return _make_name (_mode, _b.name(), _quad.name());
  } else {
    return _make_name (_mode, _b.name(), _nb.name());
  }
}
template<class T>
typename basis_on_pointset_rep<T>::mode_type
basis_on_pointset_rep<T>::_parse_name(
  const std::string& name,
        std::string& basis_name,
        std::string& pointset_name)
{
  long i_pos_at = name.find ('@');
  check_macro (i_pos_at > 0 && i_pos_at+1 < long(name.size()),
	"invalid basis_on_pointset name, expect e.g. \"P1@b(gauss(3))\"");
  basis_name = name.substr (0, i_pos_at);
  check_macro (name[i_pos_at+1] == 'q' || name[i_pos_at+1] == 'b',
	"invalid basis_on_pointset name, expect e.g. \"P1@b(gauss(3))\"");
  mode_type mode = name[i_pos_at+1] == 'q' ? quad_mode : nodal_mode;
  long l = name.size() - (i_pos_at+4);
  check_macro (l > 0 && i_pos_at+3 < long(name.size()),
	"invalid basis_on_pointset name, expect e.g. \"P1@b(gauss(3))\"");
  pointset_name = name.substr (i_pos_at+3, l);
  return mode;
}
// ----------------------------------------------------------
// persistent table
// ----------------------------------------------------------
template<class T>
basis_on_pointset_rep<T>*
basis_on_pointset_rep<T>::make_ptr (const std::string& name)
{
  return new_macro(basis_on_pointset_rep<T>(name));
}
template <class T>
basis_on_pointset_rep<T>::~basis_on_pointset_rep()
{
  if (name() != "") {
    persistent_table<basis_on_pointset<T>>::unload (name());
  }
}
template<class T>
void
basis_on_pointset<T>::reset (const std::string& name)
{
  if (name == "") {
    base::operator= (new_macro(rep()));
  } else {
    base::operator= (persistent_table<basis_on_pointset<T>>::load (name));
  }
}
template<class T>
basis_on_pointset<T>::basis_on_pointset (const std::string& name)
 : base(new_macro(rep)),
   persistent_table<basis_on_pointset<T>>()
{
  reset (name);
}
template<class T>
basis_on_pointset<T>::basis_on_pointset (const quadrature<T>&  quad, const basis_basic<T>& b)
 : base(),
   persistent_table<basis_on_pointset<T>>()
{
  std::string name = basis_on_pointset_rep<T>::_make_name (basis_on_pointset_rep<T>::quad_mode, b.name(), quad.name());
  reset (name);
}
template<class T>
basis_on_pointset<T>::basis_on_pointset (const basis_basic<T>& nb,   const basis_basic<T>& b)
 : base(),
   persistent_table<basis_on_pointset<T>>()
{
  std::string name = basis_on_pointset_rep<T>::_make_name (basis_on_pointset_rep<T>::nodal_mode, b.name(), nb.name());
  reset (name);
}
template<class T>
void
basis_on_pointset<T>::set (const quadrature<T>&  quad, const basis_basic<T>& b)
{
  std::string name = basis_on_pointset_rep<T>::_make_name (basis_on_pointset_rep<T>::quad_mode, b.name(), quad.name());
  reset (name);
}
template<class T>
void
basis_on_pointset<T>::set (const basis_basic<T>& nb,   const basis_basic<T>& b)
{
  std::string name = basis_on_pointset_rep<T>::_make_name (basis_on_pointset_rep<T>::nodal_mode, b.name(), nb.name());
  reset (name);
}
// ----------------------------------------------------------
// cstors
// ----------------------------------------------------------
template<class T>
basis_on_pointset_rep<T>::basis_on_pointset_rep(const std::string& name)
 : _b(),
   _mode(max_mode),
   _quad(),
   _nb(),
   _scalar_val(),
   _vector_val(),
   _tensor_val(),
   _tensor3_val(),
   _tensor4_val(),
   _sid_scalar_val(),
   _sid_vector_val(),
   _sid_tensor_val(),
   _sid_tensor3_val(),
   _sid_tensor4_val(),
   _initialized(),
   _grad_initialized(),
   _sid_initialized()
{
  reset (name);
}
template<class T>
void
basis_on_pointset_rep<T>::reset (const std::string& name)
{
        _initialized.fill(false);
  _grad_initialized.fill(false);
   _sid_initialized.fill(false);
  if (name == "") return;
  std::string basis_name, pointset_name;
  _mode = _parse_name (name, basis_name, pointset_name);
  _b = basis_basic<T> (basis_name);
  if (_mode == quad_mode) {
    _quad = quadrature<T> (pointset_name);
  } else {
    _nb = basis_basic<T> (pointset_name);
  }
}
template<class T>
basis_on_pointset_rep<T>::basis_on_pointset_rep (const basis_on_pointset_rep<T>& x)
 : _b(x._b),
   _mode(x._mode),
   _quad(x._quad),
   _nb(x._nb),
   _scalar_val(x._scalar_val),
   _vector_val(x._vector_val),
   _tensor_val(x._tensor_val),
   _tensor3_val(x._tensor3_val),
   _tensor4_val(x._tensor4_val),
   _sid_scalar_val(x._sid_scalar_val),
   _sid_vector_val(x._sid_vector_val),
   _sid_tensor_val(x._sid_tensor_val),
   _sid_tensor3_val(x._sid_tensor3_val),
   _sid_tensor4_val(x._sid_tensor4_val),
   _initialized     (x._initialized),
   _grad_initialized(x._grad_initialized),
   _sid_initialized (x._sid_initialized)
{
  trace_macro("PHYSICAL COPY***************");
}
template<class T>
basis_on_pointset_rep<T>&
basis_on_pointset_rep<T>::operator= (const basis_on_pointset_rep<T>& x)
{
  trace_macro("ASSIGN**************");
   _b = x._b;
   _mode = x._mode;
   _quad = x._quad;
   _nb = x._nb;
   _scalar_val = x._scalar_val;
   _vector_val = x._vector_val;
   _tensor_val = x._tensor_val;
   _tensor3_val = x._tensor3_val;
   _tensor4_val = x._tensor4_val;
   _sid_scalar_val = x._sid_scalar_val;
   _sid_vector_val = x._sid_vector_val;
   _sid_tensor_val = x._sid_tensor_val;
   _sid_tensor3_val = x._sid_tensor3_val;
   _sid_tensor4_val = x._sid_tensor4_val;
   _initialized      = x._initialized;
   _grad_initialized = x._grad_initialized;
   _sid_initialized  = x._sid_initialized;
  return *this;
}
template<class T>
typename basis_on_pointset_rep<T>::size_type
basis_on_pointset_rep<T>::nnod (reference_element hat_K) const
{
  if (_mode == quad_mode) {
    return _quad.size (hat_K);
  } else {
    return _nb.nnod   (hat_K);
  }
}
template<class T>
typename basis_on_pointset_rep<T>::size_type
basis_on_pointset_rep<T>::ndof (reference_element hat_K) const
{
  return _b.ndof (hat_K);
}
template<class T>
const quadrature<T>&
basis_on_pointset_rep<T>::get_quadrature() const
{
  check_macro (_mode == quad_mode, "get_quadrature: pointset mode is not quadrature");
  return _quad;
}
template<class T>
const basis_basic<T>&
basis_on_pointset_rep<T>::get_nodal_basis() const
{
  check_macro (_mode == nodal_mode, "get_nodal_basis: pointset mode is not nodal");
  return _nb;
}
// -----------------------------------------------------------------------
// basis evaluated on lattice of quadrature formulae
// -----------------------------------------------------------------------
template<class T>
void 
basis_on_pointset_rep<T>::_initialize_continued (
  reference_element                                     hat_K,
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const
{
  switch (_b.valued_tag()) {
    case space_constant::scalar: {
      details::basis_on_pointset_evaluate (_b, hat_K, hat_node, _scalar_val[hat_K.variant()]);
      break;
    }
    case space_constant::vector: {
      details::basis_on_pointset_evaluate (_b, hat_K, hat_node, _vector_val[hat_K.variant()]);
      break;
    }
    case space_constant::tensor: {
      // note: not yet available in basis ; for specific tensor basis, e.g. winter
      details::basis_on_pointset_evaluate (_b, hat_K, hat_node, _tensor_val[hat_K.variant()]);
      break;
    }
    default: error_macro("evaluate: unexpected "<<space_constant::valued_name(_b.valued_tag())
	<< "-valued basis \"" << _b.name() << "\"");
  }
}
template<class T>
void 
basis_on_pointset_rep<T>::_grad_initialize_continued (
  reference_element                                     hat_K,
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const
{
  switch (_b.valued_tag()) {
    case space_constant::scalar:
      details::basis_on_pointset_grad_evaluate (_b, hat_K, hat_node, _vector_val[hat_K.variant()]);
      break;
    case space_constant::vector:
      details::basis_on_pointset_grad_evaluate (_b, hat_K, hat_node, _tensor_val[hat_K.variant()]);
      break;
    case space_constant::tensor:
      details::basis_on_pointset_grad_evaluate (_b, hat_K, hat_node, _tensor3_val[hat_K.variant()]);
      break;
    default: error_macro("grad_evaluate: unexpected "<<space_constant::valued_name(_b.valued_tag())
	<< "-valued basis \"" << _b.name() << "\"");
  }
}
// TODO: avoid code repetition between initialize & grad_initialize
template<class T>
void 
basis_on_pointset_rep<T>::_initialize (reference_element hat_K) const
{
  if (_mode == quad_mode) {
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> hat_node;
    _quad.get_nodes (hat_K, hat_node);
    _initialize_continued (hat_K, hat_node);
  } else {
    const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node = _nb.hat_node (hat_K);
    _initialize_continued (hat_K, hat_node);
  }
  _initialized [hat_K.variant()] = true;
}
template<class T>
void 
basis_on_pointset_rep<T>::_grad_initialize (reference_element hat_K) const
{
  reference_element::variant_type K_variant = hat_K.variant();
  if (_mode == quad_mode) {
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> hat_node;
    _quad.get_nodes (hat_K, hat_node);
    _grad_initialize_continued (hat_K, hat_node);
  } else {
    const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node = _nb.hat_node (hat_K);
    _grad_initialize_continued (hat_K, hat_node);
  }
  _grad_initialized [K_variant] = true;
}
// ----------------------------------------------------------
// side restriction (for DG)
// ----------------------------------------------------------
// We have a side tilde_K on an element tilde_L
// and a Piola transform from hat_K to tilde_L
// We have a quadrature formula or a point-set on hat_K:
//	(hat_xq) q=0..nq
// and we are able to transform it on tilde_K subset partial tilde_L:
//	(tilde_xq) q=0..nq
// We want to evaluate on this pointset a basis "b" of tilde_L.
// 
// basis_on_pointset<T>::init_all_sides (reference_element tilde_L)
//   => loop on all possible tilde_K side of tilde_L 
//     build the (tilde_xq)q set and evaluate on it the basis "b"
//     (possibly, have to reverse or reorder (ie rotate in 3d) the tilde_xq set)
//     store all results contiguously, side by side, in the same array
//
// basis_on_pointset<T>::evaluate_on_side (reference_element tilde_L, const side_information_type& sid)
//   => if not initialized, call init_all_sides
//      then, from the current side index, compute begin and end pointers
//      that will be used by accessors
// 
// AVANTAGES:
//  - transparent : no changes in downstream codes (field_evaluate.cc etc)
//  - fast : the basis is evaluate only once for each reference element
// TECHNIQUE:
//  possibly, have to reverse or reorder (ie rotate in 3d) the tilde_xq set
//  - first, do it in 2d, with only an optional reverse ordering
//    when the reference side has the opposite orientation.
//    This is not known at "init_all_sides()" call, as it is given later
//    by the "side_information_type" argument of "restrict_on_side()" call.
//    So, we cannot use begin() and end() iterators.
//    Thus, the random accessor is preferable: "value()"
//    and any call to iterators should check that we are not in the "side" mode.
//  - second, extend it to 3d: the reordering is more complex when there is shift
//    * nodes on the vertices are rotated
//    * nodes on the boundary edges of a face are shifted: iedge=(iedge0+shift)%nedge
//    * nodes inside the face are completely reordered
//    => a table of reordering is required, it should be build by restrict_on_side()
//    Note that there is a finite number of possible shift+reverse combinations
//    and we can enumerate it and compute one time for all the all reordering
//    at init_all_sides(). Then, at restrict_on_side(), we have just to compute
//    the index associated to the reordering.
// IMPLEMENTATION:
//  - may have a different data structure for eval on volume or side 
//         _scalar_val(),     _vector_val(),
//     _sid_scalar_val(), _sid_vector_val(),
//    or with a vol/side flag:
//      mutable std::array<2, array<Eigen::Matrix<T,Eigen::Dynamic,1>,
//        reference_element::max_variant> >        _scalar_val;
//    =>
//        _scalar_val [vol_side_flag] [hat_K] [idof]
//    motivation: when the test::_bops is used for volume, then for side,
//    and then volume again, we do not have to reinitialize.
//    perhaps, it should solve the BUG_TEST_CONST ?

template<class T>
void
basis_on_pointset_rep<T>::_sid_initialize_continued (
	reference_element                                     tilde_L,
	const side_information_type&                          sid,
	const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_node) const
{
  reference_element::variant_type L_variant = tilde_L.variant();
  size_type ori_idx = (sid.orient == 1) ? 0 : 1;
  switch (_b.valued_tag()) {
    case space_constant::scalar: {
      if (! _b.option().is_trace_n()) {
        details::basis_on_pointset_evaluate      (_b, tilde_L, hat_node, _sid_scalar_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
        details::basis_on_pointset_grad_evaluate (_b, tilde_L, hat_node, _sid_vector_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
      } else { // e.g. "trace_n(RTkd)"
        details::basis_on_pointset_evaluate_on_side (_b, tilde_L, sid, hat_node, _sid_scalar_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
      }
      break;
    }
    case space_constant::vector: {
      if (! _b.option().is_trace_n()) {
        details::basis_on_pointset_evaluate      (_b, tilde_L, hat_node, _sid_vector_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
        details::basis_on_pointset_grad_evaluate (_b, tilde_L, hat_node, _sid_tensor_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
      } else {
	fatal_macro ("basis_trace_n(vector): not yet");
#ifdef TODO
        details::basis_on_pointset_evaluate_on_sides (_b, tilde_L, hat_node, _sid_vector_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
#endif // TODO
      }
      break;
    }
    case space_constant::tensor: {
      if (! _b.option().is_trace_n()) {
        details::basis_on_pointset_evaluate      (_b, tilde_L, hat_node,  _sid_tensor_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
        details::basis_on_pointset_grad_evaluate (_b, tilde_L, hat_node, _sid_tensor3_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
      } else {
	fatal_macro ("basis_trace_n(tensor): not yet");
#ifdef TODO
        details::basis_on_pointset_evaluate_on_sides (_b, tilde_L, hat_node, _sid_tensor_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
#endif // TODO
      }
      break;
    }
    case space_constant::tensor3: {
      if (! _b.option().is_trace_n()) {
        details::basis_on_pointset_evaluate      (_b, tilde_L, hat_node, _sid_tensor3_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
        details::basis_on_pointset_grad_evaluate (_b, tilde_L, hat_node, _sid_tensor4_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
      } else {
	fatal_macro ("basis_trace_n(tensor3): not yet");
#ifdef TODO
        details::basis_on_pointset_evaluate_on_sides (_b, tilde_L, hat_node, _sid_tensor_val[L_variant][sid.loc_isid][ori_idx][sid.shift]);
#endif // TODO
      }
      break;
    }
    default: error_macro("evaluate_on_sides: unexpected "<<space_constant::valued_name(_b.valued_tag())
	  << "-valued basis \"" << _b.name() << "\"");
  }
}
// TODO: avoid code repetition between initialize & grad_initialize
template<class T>
void
basis_on_pointset_rep<T>::_sid_initialize (reference_element tilde_L, const side_information_type& sid) const
{
  // transform side hat_node on hat_K into tilde_node on tilde_K, the side of tilde_L:
  // => orient and shift varies for each side 
  // the transformed nodes from hat_K to tilde_K depends upon (loc_isid,orient,shift)
  reference_element::variant_type L_variant = tilde_L.variant();
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> hat_node, tilde_node;
  if (_mode == quad_mode) {
    _quad.get_nodes (sid.hat, hat_node);
  } else {
    hat_node = _nb.hat_node (sid.hat); // ... on its Lagrange nodes.
  }
  tilde_node.resize (hat_node.size());
  if (! _b.option().is_trace_n()) {
    // transform hat_xq on side hat_S to tilde_xq on partial tilde_L
    for (size_type q = 0, nq = hat_node.size(); q < nq; ++q) {
      tilde_node[q] = reference_element_face_transformation (tilde_L, sid, hat_node[q]);
    }
  } else {
    // else e.g. "trace_n(RTkd)": hat_nodes remains on side hat_S
    tilde_node = hat_node;
  } 
  _sid_initialize_continued (tilde_L, sid, tilde_node);
}
template<class T>
void
basis_on_pointset_rep<T>::_sid_initialize (reference_element tilde_L) const
{
  size_type d = tilde_L.dimension();
  size_type nsid = tilde_L.n_subgeo (d-1);
  side_information_type sid;
  sid.dim = d-1;
  for (sid.loc_isid = 0; sid.loc_isid < nsid; ++sid.loc_isid) {
    sid.n_vertex = tilde_L.subgeo_size (sid.dim, sid.loc_isid);
    sid.hat.set_variant (sid.n_vertex, sid.dim);
    int last_orient = (sid.dim == 0) ? 1 : -1; // no negative orient for 0d side
    for (sid.orient = 1; sid.orient >= last_orient; sid.orient -= 2) {
      size_type n_shift = (d == 3) ? nsid : 1;
      for (sid.shift = 0; sid.shift < n_shift; ++sid.shift) {
        _sid_initialize (tilde_L, sid);
      }
    }
  }
  _sid_initialized [tilde_L.variant()] = true;
}
template<class T>
void
basis_on_pointset_rep<T>::_sid_grad_initialize (reference_element tilde_L) const
{
  fatal_macro ("_sid_grad_initialize: not yet");
}
// ----------------------------------------------------------
// accessors
// ----------------------------------------------------------
template<class T>
template<class Value>
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset_rep<T>::evaluate (reference_element hat_K) const
{
  if (!_initialized [hat_K.variant()]) _initialize  (hat_K);
  return details::memorized_matrix<T,Value>().get (*this, hat_K);
}
template<class T>
template<class Value>
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset_rep<T>::grad_evaluate (reference_element hat_K) const
{
  if (!_grad_initialized [hat_K.variant()])  _grad_initialize  (hat_K);
  return details::memorized_matrix<T,Value>().get (*this, hat_K);
}
template<class T>
template<class Value>
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset_rep<T>::evaluate_on_side (
    reference_element            hat_K,
    const side_information_type& sid) const
{
  if (!_sid_initialized [hat_K.variant()]) _sid_initialize  (hat_K);
  return details::memorized_side_value<T,Value>().get (*this, hat_K, sid);
}
template<class T>
template<class Value>
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&
basis_on_pointset_rep<T>::grad_evaluate_on_side (
  reference_element		hat_K,
  const side_information_type&	sid) const
{
  if (!_sid_initialized [hat_K.variant()]) _sid_initialize  (hat_K);
  return details::memorized_side_value<T,Value>().get (*this, hat_K, sid);
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation_value(T,Value)		 			\
template 									\
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& 			\
basis_on_pointset_rep<T>::evaluate (reference_element hat_K) const;		\
template 									\
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&			\
basis_on_pointset_rep<T>::evaluate_on_side (					\
    reference_element            hat_K,						\
    const side_information_type& sid) const;					\
template 									\
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& 			\
basis_on_pointset_rep<T>::grad_evaluate (reference_element hat_K) const;	\
template 									\
const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&			\
basis_on_pointset_rep<T>::grad_evaluate_on_side (				\
  reference_element	       hat_K,						\
  const side_information_type& sid) const;					\

#define _RHEOLEF_instanciation(T)      					 	\
template class basis_on_pointset<T>;						\
template class basis_on_pointset_rep<T>;					\
_RHEOLEF_instanciation_value(T,T)			 			\
_RHEOLEF_instanciation_value(T,point_basic<T>)		 			\
_RHEOLEF_instanciation_value(T,tensor_basic<T>)		 			\
_RHEOLEF_instanciation_value(T,tensor3_basic<T>)	 			\
_RHEOLEF_instanciation_value(T,tensor4_basic<T>)	 			\

_RHEOLEF_instanciation(Float)

} // namespace rheolef
