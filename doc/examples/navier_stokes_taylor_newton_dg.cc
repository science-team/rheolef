///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_taylor_newton_dg.cc The Navier-Stokes equations for the Taylor benchmark by Newton and discontinuous Galerkin methods
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "taylor.h"
#include "stokes_dirichlet_dg.icc"
#include "inertia.h"
#include "navier_stokes_dg.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float eps = numeric_limits<Float>::epsilon();
  geo omega (argv[1]);
  string approx   = (argc > 2) ?      argv[2]  : "P1d";
  Float  Re       = (argc > 3) ? atof(argv[3]) : 100;
  Float  tol      = (argc > 4) ? atof(argv[4]) : eps;
  size_t max_iter = (argc > 5) ? atoi(argv[5]) : 100; 
  string restart  = (argc > 6) ?      argv[6]  : ""; 
  navier_stokes_dg F (Re, omega, approx);
  navier_stokes_dg::value_type xh = F.initial (restart);
  int status = damped_newton (F, xh, tol, max_iter, &derr);
  dout << catchmark("Re") << Re << endl
       << catchmark("u") << xh[0]
       << catchmark("p") << xh[1];
  return status;
}
