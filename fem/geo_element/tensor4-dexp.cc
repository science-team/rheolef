///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// dexp(tensor) = d_exp(a)/d a : 2D is explicit while 3D is not yet available
// 
#include "rheolef/tensor4.h"
// =========================================================================
// 2D case: explicit
// =========================================================================
//
// see tensor_exp_tst.mac
// see also maxima: matrixexp.usage allows explicit expm() computation in 2D
//    http://www.ma.utexas.edu/pipermail/maxima/2006/003031.html
//    /usr/share/maxima/5.27.0/share/linearalgebra/matrixexp.usage
// refs. KanGueFor-2009, p. 50 (is buggy: missing 2 factor in A00 & A11...)
namespace rheolef { 

template <class T>
tensor4_basic<T>
dexp (const tensor_basic<T>& chi, size_t dim) {
  check_macro (dim==2, "dexp(d="<<dim<<"): only d=2 case available yet, sorry");
  static Float eps = 1e3*std::numeric_limits<Float>::epsilon();
    tensor4 E;
    Float a = chi(0,0), b = chi(1,0), c = chi(1,1);
    T a2=sqr(a),
      b2=sqr(b),
      c2=sqr(c);
    Float d2 = sqr(a-c)+4*b2;
    Float d = sqrt(d2);
    if (abs(d) < eps) { // chi = a*I, degenerate case, see tensor4_dexp_tst.mac
      E(0,0, 0,0) =
      E(1,1, 1,1) =
      E(0,1, 0,1) =
      E(0,1, 1,0) =
      E(1,0, 0,1) =
      E(1,0, 1,0) = exp(a);
      return E;
    }
#ifdef TO_CLEAN
    // --------------------------------------
    // refs. KanGueFor-2009, p. 50 : bugs...
    // --------------------------------------
    Float d3 = d*d2;
    Float eac = exp((a+c)/2);
    Float a3 = sinh(d/2), a4 = cosh(d/2);
    E(0,0, 0,0) = (eac/d3)
                 *((pow(a-c,3) + 4*sqr(b)*(a-c+1))*a3
                   + d*(sqr(a-c) + 2*sqr(b))*a4); 
    E(0,0, 0,1) = E(0,0, 1,0)
                = ((2*b*eac)/d3)
                 *((sqr(d) + 2*(c-a))*a3 + d*(a-c)*a4);
    E(0,0, 1,1) = ((2*sqr(b)*eac)/d3)
                 *(-2*a3 + a4*d);
    E(0,1, 0,0) = E(1,0, 0,0)
                = ((b*eac)/d3)
                 *((sqr(d) + 2*(c-a))*a3 + d*(a-c)*a4);
    E(0,1, 0,1) = E(1,0, 0,1) = E(0,1, 1,0) = E(1,0, 1,0)
                = ((2*eac)/d3)
                 *(sqr(a-c)*a3 + 2*sqr(b)*d*a4);
    E(0,1, 1,1) = E(1,0, 1,1)
                = ((b*eac)/d3)
                 *((sqr(d) + 2*(a-c))*a3 + d*(c-a)*a4);
    E(1,1, 0,0) = ((2*sqr(b)*eac)/d3)
                 *(-2*a3 + d*a4);
    E(1,1, 0,1) = E(1,1, 1,0)
                = ((2*b*eac)/d3)
                 *((sqr(d) + 2*(a-c))*a3 + d*(c-a)*a4);
    E(1,1, 1,1) = ((2*eac)/d3)
                 *((pow(a-c,3) + 4*sqr(b)*(a-c-1))*a3
                   - d*(sqr(a-c) + 2*sqr(b))*a4);
#endif // TO_CLEAN
    // ----------------------------------------
    // geneated by maxima: see tensor4-exp.mac
    // ----------------------------------------
    // TODO: symmetriser !!!!!!!!!!!!!!!!!!!!
    // E00ij
    // -----
    {
      T k5=1/d2,
        k7=exp(-d/2.0+c/2.0+a/2.0),
        k8=2*a-2*c,
        k9=1/d,
        k10=exp(d),
        k11=c*d*k10-a*d*k10-c2*k10+2*a*c*k10-4*b2*k10-a2*k10-c*d+a*d-c2+2*a*c-4*b2-a2;
      E(0,0,0,0) 
        = -k5*(1.0/2.0-k8*k9/4.0)*k7*k11/2.0+k8*k7*k11/sqr(d2)/2.0-k5*k7*(-d*k10-k8*c2*k9*k10/2.0+a*k8*c*k9*k10+k8*c*k9*k10/2.0-2*b2*k8*k9*k10-a2*k8*k9*k10/2.0-a*k8*k9*k10/2.0+k8*c*k10/2.0+2*c*k10-a*k8*k10/2.0-2*a*k10+d-k8*c*k9/2.0+a*k8*k9/2.0+2*c-2*a)/2.0;
    }
    {
      T k6=exp(-d/2.0+c/2.0+a/2.0),
        k7=1/d,
        k8=exp(d),
        k9=c*d*k8-a*d*k8-c2*k8+2*a*c*k8-4*b2*k8-a2*k8-c*d+a*d-c2+2*a*c-4*b2-a2;
      E(0,0, 0,1) = E(0,0, 1,0)
        = b*k6*k9/pow(d,3)+4*b*k6*k9/sqr(d2)-k6*(-4*b*c2*k7*k8+8*a*b*c*k7*k8+4*b*c*k7*k8-16*pow(b,3)*k7*k8-4*a2*b*k7*k8-4*a*b*k7*k8+4*b*c*k8-4*a*b*k8-8*b*k8-4*b*c*k7+4*a*b*k7-8*b)/d2/2.0; 
    }
    {
      T k5=1/d2,
        k7=exp(-d/2.0+c/2.0+a/2.0),
        k8=2*c-2*a,
        k9=1/d,
        k10=exp(d),
        k11=c*d*k10-a*d*k10-c2*k10+2*a*c*k10-4*b2*k10-a2*k10-c*d+a*d-c2+2*a*c-4*b2-a2;
      E(0,0,1,1)
        = -k5*(1.0/2.0-k8*k9/4.0)*k7*k11/2.0+k8*k7*k11/sqr(d2)/2.0-k5*k7*(d*k10-c2*k8*k9*k10/2.0+a*c*k8*k9*k10+c*k8*k9*k10/2.0-2*b2*k8*k9*k10-a2*k8*k9*k10/2.0-a*k8*k9*k10/2.0+c*k8*k10/2.0-a*k8*k10/2.0-2*c*k10+2*a*k10-d-c*k8*k9/2.0+a*k8*k9/2.0-2*c+2*a)/2.0;
    }
    // ----------------------------------------
    // E01ij
    // ----------------------------------------
    {
      T k1=2*a-2*c, 
        k3=a/2.0, 
        k4=c/2.0, 
        k6=exp(-d/2.0+k4+k3), 
        k7=exp(d)-1, 
        k8=1/d;
      E(0,1, 0,0) = E(1,0, 0,0)
       = b*k8*(1.0/2.0-k1*k8/4.0)*k6*k7-b*k1*k6*k7/pow(d,3)/2.0+b*k1*exp(d/2.0+k4+k3)/d2/2.0;
    }
    {
      T k3=1/d2, 
        k4=a/2.0, 
        k5=c/2.0, 
        k7=exp(-d/2.0+k5+k4), 
        k8=exp(d)-1;
      E(0,1, 0,1) = E(1,0, 0,1) = E(0,1, 1,0) = E(1,0, 1,0)
        = k7*k8/d-2*b2*k3*k7*k8-4*b2*k7*k8/pow(d,3)+4*b2*k3*exp(d/2.0+k5+k4);
    }
    {
      T k1=2*c-2*a, 
        k3=a/2.0, 
        k4=c/2.0, 
        d=sqrt(d2), 
        k6=exp(-d/2.0+k4+k3), 
        k7=exp(d)-1, 
        k8=1/d;
      E(0,1, 1,1) = E(1,0, 1,1)
        = b*k8*(1.0/2.0-k1*k8/4.0)*k6*k7-b*k1*k6*k7/pow(d,3)/2.0+b*k1*exp(d/2.0+k4+k3)/d2/2.0;
    }
    // ----------------------------------------
    // E11ij
    // ----------------------------------------
    {
      T k3=4*b2, 
        k4=-2*a*c, 
        k7=1/d2, 
        k9=exp(-d/2.0+c/2.0+a/2.0), 
        k10=2*a, 
        k11=-2*c, 
        k12=k11+k10, 
        k13=1/d, 
        k14=exp(d), 
        k15=c*d*k14-a*d*k14+c2*k14-2*a*c*k14+4*b2*k14+a2*k14-c*d+a*d+c2+k4+k3+a2;
      E(1,1,0,0) = k7*(1.0/2.0-k12*k13/4.0)*k9*k15/2.0-k12*k9*k15/sqr(d2)/2.0+k7*k9*(-d*k14+k12*c2*k13*k14/2.0-a*k12*c*k13*k14+k12*c*k13*k14/2.0+2*b2*k12*k13*k14+a2*k12*k13*k14/2.0-a*k12*k13*k14/2.0+k12*c*k14/2.0-2*c*k14-a*k12*k14/2.0+2*a*k14+d-k12*c*k13/2.0+a*k12*k13/2.0+k11+k10)/2.0;
    }
    {
      T k3=4*b2, 
        k4=-2*a*c, 
        k8=exp(-d/2.0+c/2.0+a/2.0), 
        k9=1/d, 
        k10=exp(d), 
        k11=c*d*k10-a*d*k10+c2*k10-2*a*c*k10+4*b2*k10+a2*k10-c*d+a*d+c2+k4+k3+a2;
      E(1,1,0,1) = E(1,1, 1,0)
        = -b*k8*k11/pow(d,3)-4*b*k8*k11/sqr(d2)+k8*(4*b*c2*k9*k10-8*a*b*c*k9*k10+4*b*c*k9*k10+16*pow(b,3)*k9*k10+4*a2*b*k9*k10-4*a*b*k9*k10+4*b*c*k10-4*a*b*k10+8*b*k10-4*b*c*k9+4*a*b*k9+8*b)/d2/2.0;
    }
    {
      T k3=4*b2,
        k4=-2*a*c,
        k7=1/d2,
        k9=exp(-d/2.0+c/2.0+a/2.0),
        k10=-2*a,
        k11=2*c,
        k12=k11+k10,
        k13=1/d,
        k14=exp(d),
        k15=c*d*k14-a*d*k14+c2*k14-2*a*c*k14+4*b2*k14+a2*k14-c*d+a*d+c2+k4+k3+a2;
      E(1,1,1,1) = k7*(1.0/2.0-k12*k13/4.0)*k9*k15/2.0-k12*k9*k15/sqr(d2)/2.0+k7*k9*(d*k14+c2*k12*k13*k14/2.0-a*c*k12*k13*k14+c*k12*k13*k14/2.0+2*b2*k12*k13*k14+a2*k12*k13*k14/2.0-a*k12*k13*k14/2.0+c*k12*k14/2.0-a*k12*k14/2.0+2*c*k14-2*a*k14-d-c*k12*k13/2.0+a*k12*k13/2.0+k11+k10)/2.0;
    }
    return E;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template tensor4_basic<T> dexp (const tensor_basic<T>& a, size_t d);		\

_RHEOLEF_instanciation(Float)

}// namespace rheolef
