///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile lambda_c.h The combustion problem -- the critical parameter value as a function
struct alpha_c_fun {
  typedef Float value_type;
  typedef Float float_type;
  alpha_c_fun() : _f1(0) {}
  Float residue (const Float& a) const { return tanh(a) - 1/a; }
  void update_derivative (const Float& a) const { 
				_f1 = 1/sqr(cosh(a)) + 1/sqr(a); }
  Float derivative_solve (const Float& r) const { return r/_f1; }
  Float dual_space_norm  (const Float& r) const { return abs(r); }
  mutable Float _f1;
};
Float alpha_c() {
  Float tol = numeric_limits<Float>::epsilon();
  size_t max_iter = 100;
  alpha_c_fun f;
  Float ac = 1;
  newton (alpha_c_fun(), ac, tol, max_iter);
  return ac;
}
Float lambda_c() {
  Float ac = alpha_c();
  return 8*sqr(ac/cosh(ac));
}
