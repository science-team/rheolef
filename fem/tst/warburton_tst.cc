///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// print the Warburton point set
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 september 2017
//
#include "warburton.icc"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  char   t = (argc > 1) ? argv[1][0]    : 't';
  size_t k = (argc > 2) ? atoi(argv[2]) : 10;
  bool map_on_reference_element = !(argc > 3);
  reference_element hat_K;
  hat_K.set_name(t);
  Eigen::Matrix<point_basic<Float>,Eigen::Dynamic,1> hat_xnod;
  pointset_lagrange_warburton (hat_K, k, hat_xnod, map_on_reference_element);
  cout << setprecision(16)
            << "# element " << t << std::endl
            << "# degree  " << k << std::endl
            << "# size    " << hat_xnod.size() << std::endl;
  for (size_t i = 0, n = hat_xnod.size(); i < n; i++) {
    hat_xnod[i].put (cout, std::max(size_t(1),hat_K.dimension()));
    cout << endl;
  }
}
