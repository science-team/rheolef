#ifndef _RHEOLEF_GEO_HEADER_H
#define _RHEOLEF_GEO_HEADER_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// i/o for geo header file format version 3
//
#include "rheolef/diststream.h"
#include "rheolef/reference_element.h"
#include "rheolef/space_constant.h"

namespace rheolef {

struct geo_header {
  typedef size_t                          size_type;
  typedef space_constant::coordinate_type coordinate_type;
  geo_header();
// accessor:
  bool need_upgrade() const;
// data:
  size_type dimension;
  size_type map_dimension;
  coordinate_type sys_coord;
  size_type order;
  size_type dis_size_by_variant   [reference_element::max_variant];
  size_type dis_size_by_dimension [4];
};
inline
geo_header::geo_header()
  : dimension(0),
    map_dimension(0),
    sys_coord(space_constant::cartesian),
    order(1),
    dis_size_by_variant(),
    dis_size_by_dimension()
{
  std::fill (dis_size_by_variant,   dis_size_by_variant + reference_element::max_variant, 0);
  std::fill (dis_size_by_dimension, dis_size_by_dimension + 4, 0);
}
idiststream& operator>> (idiststream& ips, geo_header& h);
odiststream& operator<< (odiststream& ops, const geo_header& h);

} // namespace rheolef {
#endif // _RHEOLEF_GEO_HEADER_H
