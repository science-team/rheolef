///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute the element "grad_grad" matrix condition number
// for non-regression test purpose
//
// author: Pierre.Saramito@imag.fr
//
// date: 27 september 2017
//
#include "rheolef/basis.h"
#include "rheolef/quadrature.h"
#include "rheolef/eigen_util.h"
#include "form_element.h"
using namespace rheolef;
using namespace std;
using namespace Eigen;

template <class Basis>
void 
show_grad_grad_base (
  const Basis&                   b,
  reference_element              hat_K,
  SparseMatrix<Float,RowMajor>&  grad_grad,
  bool                           dump)
{
  build_grad_grad (b, hat_K, grad_grad);
  Matrix<Float,Dynamic,Dynamic> full_grad_grad = grad_grad;
  cout << setprecision(numeric_limits<Float>::digits10)
       << "approx   " << b.name() << endl
       << "element  " << hat_K.name() << endl
       << "det      " << full_grad_grad.determinant() << endl
       << "cond     " << cond(full_grad_grad) << endl
       << "size     " << grad_grad.rows() << endl
       << "nnz      " << grad_grad.nonZeros()<< endl
       << "fill     " << 1.*grad_grad.nonZeros()/sqr(grad_grad.rows())<< endl
    ;
  if (!dump) return;
  ofstream out ("grad_grad.mtx");
  put_matrix_market (out, grad_grad);
}
void 
show_grad_grad (
  const basis&   b,
  reference_element hat_K,
  bool              dump)
{
  SparseMatrix<Float,RowMajor> grad_grad;
  show_grad_grad_base (b, hat_K, grad_grad, dump);
  cout << setprecision(numeric_limits<Float>::digits10)
       << "node     " << b.option().get_node_name() << endl
       << "raw_poly " << b.option().get_raw_polynomial_name() << endl
    ;
}
int main(int argc, char**argv) {
  basis_option sopt;
  string approx= (argc > 1) ? argv[1]    : "P3";
  char   t     = (argc > 2) ? argv[2][0] : 't';
  string node  = (argc > 3) ? argv[3]    : sopt.get_node_name();
  string poly  = (argc > 4) ? argv[4]    : sopt.get_raw_polynomial_name();
  bool   dump  = (argc > 5);
  sopt.set_node (node);
  sopt.set_raw_polynomial (poly);
  reference_element hat_K;
  hat_K.set_name(t);
  if (approx[0] == 'D') {
    basis_raw b (approx);
    SparseMatrix<Float,RowMajor> grad_grad;
    show_grad_grad_base (b, hat_K, grad_grad, dump);
  } else {
    string name = approx + sopt.stamp();
    basis b (name);
    show_grad_grad (b, hat_K, dump);
 }
}
