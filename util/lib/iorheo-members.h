///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// ------------------------------------------------------
// booleans
// ------------------------------------------------------
boolean (istream, verbose)
boolean (ostream, verbose)
boolean (ostream, execute)
boolean (ostream, clean)

boolean (istream, transpose)
boolean (ostream, transpose)

boolean (istream, upgrade)

boolean (ostream, logscale)
boolean (ostream, fill)
boolean (ostream, grid)
boolean (ostream, domains)
boolean (ostream, shrink)
boolean (ostream, tube)
boolean (ostream, ball)
boolean (ostream, full)
boolean (ostream, stereo)
boolean (ostream, cut)
boolean (ostream, iso)
boolean (ostream, split)
boolean (ostream, volume)
boolean (ostream, autovscale)
boolean (ostream, skipvtk)

boolean (ostream, velocity)
boolean (ostream, deformation)
boolean (ostream, elevation)
boolean (istream, fastfieldload)
boolean (ostream, bezieradapt)
boolean (ostream, lattice)
boolean (ostream, showlabel)
boolean (ostream, reader_on_stdin)
// ------------------------------------------------------
// members
// ------------------------------------------------------
member (ostream, black_and_white, color)
member (ostream, gray,            color)
member (ostream, color,           color)

member (istream, rheo, 	       format)
member (ostream, rheo, 	       format)

member (istream, peschetola,      format)
member (ostream, peschetola,      format)

member (istream, bamg,            format)
member (ostream, bamg,            format)

member (istream, grummp,          format)
member (ostream, grummp,          format)

member (istream, mmg3d,           format)
member (ostream, mmg3d,           format)

member (istream, tetgen,          format)
member (ostream, tetgen,          format)

member (istream, gmsh,            format)
member (ostream, gmsh,            format)
member (ostream, gmsh_pos,        format)

member (istream, qmg,             format)
member (ostream, qmg,	       format)

member (istream, vtkdata,         format)
member (ostream, vtkdata,         format)

member (istream, vtkpolydata,     format)
member (ostream, vtkpolydata,     format)

member (istream, cemagref,        format)
member (ostream, cemagref,        format)

member (istream, hb,              format)
member (ostream, hb,              format)

member (istream, matrix_market,   format)
member (ostream, matrix_market,   format)

member (istream, ml,              format) 
member (ostream, ml,              format) 

member (istream, matlab,          format) 
member (ostream, matlab,          format) 

member (ostream, sparse_matlab,   format)

member (istream, vtk, 	       format)
member (ostream, vtk, 	       format)

member (istream, geomview,     format)
member (ostream, geomview,     format)

member (ostream, dump,         format)
member (ostream, ps,           format) 
member (ostream, gnuplot,      format)
member (ostream, plotmtv,      format)
member (ostream, x3d, 	       format)
member (ostream, atom, 	       format)
member (ostream, paraview,     format)

// ------------------------------------------------------
//
// 2) options associated to a value
//
//      cout << x3d << n_color(10) << g;
//
// ------------------------------------------------------
io_scalar (std::string,  rhstype)                          
io_scalar (std::string,  rhsfmt)	
io_scalar (size_t,       nrhs)	
io_scalar (size_t,       ivec)	
io_scalar (size_t,       nptr)	
io_scalar (size_t,       nidx)	
io_scalar (size_t,       line_no)	
o_scalar  (size_t,       subdivide)

io_scalar (unsigned int, ncolor)
io_scalar (std::string,  basename)
io_scalar (std::string,  image_format)
io_scalar (std::string,  mark)
o_scalar  (std::string,  label)
o_scalar  (Float,        isovalue)
o_scalar  (size_t,       n_isovalue)
o_scalar  (size_t,       n_isovalue_negative)
o_scalar  (Float,        vectorscale)
o_scalar  (size_t,       anglecorner)
o_scalar  (Float,        rounding_precision)
io_scalar (size_t,       branch_counter)
