# ifndef _IORHEOBASE_H
# define _IORHEOBASE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/compiler.h"
namespace rheolef { 

// --------------------------------------------------------------
// header macro definitions, for code reduction purpose
// --------------------------------------------------------------

#define iorheobase_def_boolean_accessor_macro(ioclass,stream,name) \
	static bool get##name (stream& s)              		\
	{                                                       \
            return ioclass::flags(s) [name];		        \
	}
#define iorheobase_manip_boolean_accessor_macro(ioclass,stream,name) \
	inline stream& name (stream& s) 			\
	{ 							\
	    ioclass::setf (s, ioclass::name);			\
	    return s;						\
	}							\
	inline stream& no##name (stream& s) 			\
	{ 							\
	    ioclass::unsetf (s, ioclass::name);			\
	    return s;						\
	}
	// ------------------------------------------------------
#define iorheobase_def_member_macro(ioclass,stream,name,groupe)	\
	iorheobase_def_boolean_accessor_macro(ioclass,stream,name)

#define iorheobase_manip_member_macro(ioclass,stream,name,groupe)	\
	inline stream& name (stream& s)				\
	{ 							\
	  ioclass::setf(s, ioclass::name, ioclass::groupe##_field);	\
	  return s;						\
	}
	// ------------------------------------------------------
#define iorheobase_def_scalar_macro(ioclass,t, a) 		\
    protected:							\
	t  a ## _;						\
	const t&  a () const;					\
	void      a (const t& x);				\
    public:							\
	static const t& get##a (std::ios& s);			\
	static std::ios& set##a (std::ios& s, const t& x);

#define iorheobase_manip_scalar_macro(ioclass,t, a, io)		\
	inline io##rheomanip1<t> set##a (const t& x)	\
	{							\
	    return io##rheomanip1<t>(&(ioclass::set##a), x);	\
	}

// --------------------------------------------------------------
// body macro definitions, for code reduction purpose
// --------------------------------------------------------------
# define iorheobase_io_scalar_body_macro(ioclass,t,a)           \
const t&                                                        \
ioclass::a () const                                             \
{                                                               \
        return a##_;                                            \
}                                                               \
void                                                            \
ioclass::a (const t& x)                                         \
{                                                               \
        a##_ = x;                                               \
}                                                               \
const t&                                                        \
ioclass::get##a (std::ios& s)                                        \
{                                                               \
        return (ioclass::get_pointer(s) -> a());                \
}                                                               \
std::ios&                                                            \
ioclass::set##a (std::ios& s, const t& x)                            \
{                                                               \
        (ioclass::get_pointer(s) -> a(x));                      \
        return s;                                               \
}
// --------------------------------------------------------------
// io manipulators
// --------------------------------------------------------------
template <class T>
struct irheomanip1 {
  irheomanip1 (std::ios& (*ff)(std::ios&, const T&), const T& ii)
   : f(ff), i(ii) {}
// data:
  std::ios& (*f) (std::ios&, const T&);
  T i;
};
template <class T>
inline
std::istream&
operator>> (std::istream& is, const irheomanip1<T>& m)
{
  m.f (is, m.i);
  return is;
}
template <class T>
struct orheomanip1 {
  orheomanip1 (std::ios& (*ff)(std::ios&, const T&), const T& ii)
   : f(ff), i(ii) {}
// data:
  std::ios& (*f) (std::ios&, const T&);
  T i;
};
template <class T>
inline
std::ostream&
operator<< (std::ostream& os, const orheomanip1<T>& m)
{
  m.f (os, m.i);
  return os;
}
template <class T>
struct iorheomanip1 {
  iorheomanip1 (std::ios& (*ff)(std::ios&, const T&), const T& ii)
    : f(ff), i(ii) {}
// data:
  std::ios& (*f) (std::ios&, const T&);
  T i;
};
template <class T>
inline
std::istream&
operator>> (std::istream& is, const iorheomanip1<T>& m)
{
  m.f (is, m.i);
  return is;
}
template <class T>
inline
std::ostream&
operator<< (std::ostream& os, const iorheomanip1<T>& m)
{
  m.f (os, m.i);
  return os;
}
// ---------------------------------------------------------------------
// iofem trivial memory handler
// TODO: a global destructor that delete the list
//  when global cstor/dstor works...
// ---------------------------------------------------------------------


template <class T>
class iorheobase_memory_handler {
public:
    static std::list<T*> *pointer_list;
    static inline T* iorheobase_alloc () {
        static bool init = false;
        if (!init) {
	    pointer_list = new_macro(std::list<T*>);
            init = true;
        }
        T *p = new_macro(T); 
        (*pointer_list).push_back(p); 
        return p;
    }
    //
    // io-manips / memory interface
    //
    static inline T* get_pointer (std::ios& io) {
        static int iobaseidx = 0;
        static bool iobaseidx_initialized = false;
        if (!iobaseidx_initialized) {
            iobaseidx = std::ios::xalloc();
	    iobaseidx_initialized = true;
	}
	T* p = (T*)io.pword(iobaseidx);
	
#ifdef _RHEOLEF_HAVE_IOS_BITALLOC
	// io.pword(iobaseidx) n'est pas forcement nul la premiere fois
	// e.g. CRAY C++ ne l'initialise pas
	//  => peut valoir une valeur arbitraire...
	//     pas moyen de savoir quand il faut l'initialiser
	// std::ios::bitalloc: un bit va permettre de dire
	//     si on a deja initialise'
	//     bitalloc = pas standard (e.g. pas avec KAI C++)
	//     mais permet de contourner le bug du 
	//     compilateur cray c++
	//     le script configure teste si bitalloc() existe
	static long iobase_bitidx;
        static bool iobase_bitidx_initialized = false;
	long bit;
        if (!iobase_bitidx_initialized) {
	    iobase_bitidx = std::ios::bitalloc();
	    iobase_bitidx_initialized = true;
	}
	long bit_init;
	if (iobase_bitidx == 0) {
	    // no bits are still available in std::ios...
	    bit_init = (p != 0);
	} else {
	    // get iobase_bitidx flag in io.flags:
	    long io_flags = io.flags();
	    bit_init = io_flags & iobase_bitidx;
	    // set iobase_bitidx flag to true in io.flags:
	    io.flags(io_flags | iobase_bitidx);
	}
#else  // ! _RHEOLEF_HAVE_IOS_BITALLOC
	long bit_init = (p != 0);
#endif // _RHEOLEF_HAVE_IOS_BITALLOC

	if (bit_init == 0) {
	  if (p != 0) { 
	    warning_macro ("ios::pwords(int) return value not initialized (fixed)");
	  }
	  io.pword(iobaseidx) = p = iorheobase_alloc();
	} else { 
	  if (p == 0) { 
	    fatal_macro ("ios::pwords(int) return null value: memory corrupted");
	  }
	}
	return p;
    }
};
}// namespace rheolef
# endif /* _IORHEOBASE_H */
