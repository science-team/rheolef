///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile streamf_contraction.cc The stream function for the contraction benchmark
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "contraction.h"
int main (int argc, char** argv) {
  environment rheolef (argc, argv);
  field uh;
  din >> uh;
  const geo& omega = uh.get_geo();
  size_t d = omega.dimension();
  Float c = omega.xmax()[1];
  string approx = "P" + to_string(uh.get_space().degree());
  space Ph = contraction::streamf_space (omega, approx);
  field psi_h = contraction::streamf_field (Ph);
  integrate_option iopt;
  iopt.ignore_sys_coord = true;
  const space& Xh = uh.get_space();
  trial psi (Ph), u (Xh);
  test   xi (Ph), v (Xh);
  form a = (d == 3) ? integrate (ddot(grad(psi), grad(xi)))
                    : integrate ( dot(grad(psi), grad(xi)), iopt);
  field lh = integrate (dot(uh,bcurl(xi)));
  problem p (a);
  p.solve (lh, psi_h);
  dout << catchmark("psi") << psi_h;
}
