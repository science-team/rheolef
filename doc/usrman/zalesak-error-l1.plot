set terminal cairolatex pdf color standalone
set output "zalesak-error-l1.tex"

set size square 
set log xy
set key bottom
set xrange [1e-2:1e-1]
set yrange [1e-4:1e-1]
graph_ratio_xy=1.0/3.0
set xtics ( \
	'[c]{$10^{-1}$}' 1e-1, \
	'[c]{$10^{-2}$}' 1e-2)
set ytics ( \
	'[r]{$10^{-1}$}' 1e-1, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-4}$}' 1e-4)
set xlabel '[c]{$h$}'
set  label '[l]{$E_h^{(n_{max})}\approx\mathcal{O}(h^2)$}' at graph 0.03,0.93

# triangle a droite
alpha = 2
slope_A = graph_ratio_xy*alpha
xA =  0.27
yA =  0.15
dxA = 0.10
dyA = dxA*slope_A
set label sprintf('[l]{\scriptsize $\alpha=%.3g$}',alpha) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

plot \
'zalesak-error-l1.gdat' \
  i 0 u 2:8 \
  t '[r]{$k=1$}' \
  w lp lw 2 dt 2 lc rgb '#ff0000', \
'zalesak-error-l1.gdat' \
  i 1 u 2:8 \
  t '[r]{$2$}' \
  w lp lw 2 dt 2 lc rgb '#008800', \
'zalesak-error-l1.gdat' \
  i 2 u 2:8 \
  t '[r]{$3$}' \
  w lp lw 2 dt 2 lc rgb '#0000ff', \
'zalesak-error-l1.gdat' \
  i 3 u 2:8 \
  t '[r]{$4$}' \
  w lp lw 2 dt 2 lc rgb '#ff00ff'

#pause -1
