///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile diffusion_tensor_error.cc The tensor diffusion benchmark -- error computation
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "diffusion_tensor_exact.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-10;
  field sigma_h;
  din >> catchmark("sigma") >> sigma_h;
  const geo& omega = sigma_h.get_geo();
  size_t k = sigma_h.get_space().degree();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(3*(k+1));
  Float err_l2 = sqrt(integrate (omega, norm2(sigma_h - sigma_exact()), iopt));
  space Th1 (omega, "P"+itos(3*(k+1)), "tensor");
  field eh = interpolate(Th1, sigma_h - sigma_exact());
  Float err_linf = eh.max_abs();
  derr << "err_l2   = " << err_l2   << endl
       << "err_linf = " << err_linf << endl;
  return (err_l2 < tol) ? 0 : 1;
}
