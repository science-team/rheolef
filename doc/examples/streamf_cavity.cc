///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile streamf_cavity.cc The stream function for the driven cavity benchmark
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cavity.h"
int main (int argc, char** argv) {
  environment rheolef (argc, argv);
  field uh;
  din >> uh;
  string approx = "P" + to_string(uh.get_space().degree());
  space Ph = cavity::streamf_space (uh.get_geo(), approx);
  space Xh = uh.get_space();
  size_t d = uh.get_geo().dimension();
  trial u (Xh), psi (Ph); test phi (Ph);
  form a = (d == 3) ? integrate (ddot(grad(psi),grad(phi)))
                    : integrate ( dot(grad(psi),grad(phi)));
  form b = (d==3) ? integrate (dot(curl(u),phi))
                  : integrate (curl(u)*phi);
  field psi_h = cavity::streamf_field (Ph);
  field lh = b*uh;
  problem p (a);
  p.solve (lh, psi_h);
  dout << catchmark("psi") << psi_h;
}
