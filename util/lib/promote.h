#ifndef _RHEOLEF_PROMOTE_H
#define _RHEOLEF_PROMOTE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/compiler.h"
namespace rheolef {
template<class T1, class T2>
struct promote_not_specialized_for_this_case {};

template<class T1, class T2>
struct promote { 
  typedef promote_not_specialized_for_this_case<T1,T2> type;
};
#define skit_promote(T1,T2) typename promote<T1,T2>::type 

template <> struct promote<char, char > { typedef int type; };
template <> struct promote<char, unsigned char > { typedef int type; };
template <> struct promote<char, short int > { typedef int type; };
template <> struct promote<char, short unsigned int > { typedef unsigned int type; };
template <> struct promote<char, int > { typedef int type; };
template <> struct promote<char, unsigned int > { typedef unsigned int type; };
template <> struct promote<char, long > { typedef long type; };
template <> struct promote<char, unsigned long > { typedef unsigned long type; };
template <> struct promote<char, float > { typedef float type; };
template <> struct promote<char, double > { typedef double type; };
template <> struct promote<char, long double > { typedef long double type; };
template <> struct promote<unsigned char, char > { typedef int type; };
template <> struct promote<unsigned char, unsigned char > { typedef int type; };
template <> struct promote<unsigned char, short int > { typedef int type; };
template <> struct promote<unsigned char, short unsigned int > { typedef unsigned int type; };
template <> struct promote<unsigned char, int > { typedef int type; };
template <> struct promote<unsigned char, unsigned int > { typedef unsigned int type; };
template <> struct promote<unsigned char, long > { typedef long type; };
template <> struct promote<unsigned char, unsigned long > { typedef unsigned long type; };
template <> struct promote<unsigned char, float > { typedef float type; };
template <> struct promote<unsigned char, double > { typedef double type; };
template <> struct promote<unsigned char, long double > { typedef long double type; };
template <> struct promote<short int, char > { typedef int type; };
template <> struct promote<short int, unsigned char > { typedef int type; };
template <> struct promote<short int, short int > { typedef int type; };
template <> struct promote<short int, short unsigned int > { typedef unsigned int type; };
template <> struct promote<short int, int > { typedef int type; };
template <> struct promote<short int, unsigned int > { typedef unsigned int type; };
template <> struct promote<short int, long > { typedef long type; };
template <> struct promote<short int, unsigned long > { typedef unsigned long type; };
template <> struct promote<short int, float > { typedef float type; };
template <> struct promote<short int, double > { typedef double type; };
template <> struct promote<short unsigned int, char > { typedef unsigned int type; };
template <> struct promote<short unsigned int, unsigned char > { typedef unsigned int type; };
template <> struct promote<short unsigned int, short int > { typedef unsigned int type; };
template <> struct promote<short unsigned int, short unsigned int > { typedef unsigned int type; };
template <> struct promote<short unsigned int, int > { typedef unsigned int type; };
template <> struct promote<short unsigned int, unsigned int > { typedef unsigned int type; };
template <> struct promote<short unsigned int, long > { typedef long type; };
template <> struct promote<short unsigned int, unsigned long > { typedef unsigned long type; };
template <> struct promote<short unsigned int, float > { typedef float type; };
template <> struct promote<short unsigned int, double > { typedef double type; };
template <> struct promote<short unsigned int, long double > { typedef long double type; };
template <> struct promote<int, char > { typedef int type; };
template <> struct promote<int, unsigned char > { typedef int type; };
template <> struct promote<int, short int > { typedef int type; };
template <> struct promote<int, short unsigned int > { typedef unsigned int type; };
template <> struct promote<int, int > { typedef int type; };
template <> struct promote<int, unsigned int > { typedef unsigned int type; };
template <> struct promote<int, long > { typedef long type; };
template <> struct promote<int, unsigned long > { typedef unsigned long type; };
template <> struct promote<int, float > { typedef float type; };
template <> struct promote<int, double > { typedef double type; };
template <> struct promote<int, long double > { typedef long double type; };
template <> struct promote<unsigned int, char > { typedef unsigned int type; };
template <> struct promote<unsigned int, unsigned char > { typedef unsigned int type; };
template <> struct promote<unsigned int, short int > { typedef unsigned int type; };
template <> struct promote<unsigned int, short unsigned int > { typedef unsigned int type; };
template <> struct promote<unsigned int, int > { typedef unsigned int type; };
template <> struct promote<unsigned int, unsigned int > { typedef unsigned int type; };
template <> struct promote<unsigned int, long > { typedef long type; };
template <> struct promote<unsigned int, unsigned long > { typedef unsigned long type; };
template <> struct promote<unsigned int, float > { typedef float type; };
template <> struct promote<unsigned int, double > { typedef double type; };
template <> struct promote<unsigned int, long double > { typedef long double type; };
template <> struct promote<long, char > { typedef long type; };
template <> struct promote<long, unsigned char > { typedef long type; };
template <> struct promote<long, short int > { typedef long type; };
template <> struct promote<long, short unsigned int > { typedef long type; };
template <> struct promote<long, int > { typedef long type; };
template <> struct promote<long, unsigned int > { typedef long type; };
template <> struct promote<long, long > { typedef long type; };
template <> struct promote<long, unsigned long > { typedef unsigned long type; };
template <> struct promote<long, float > { typedef float type; };
template <> struct promote<long, double > { typedef double type; };
template <> struct promote<long, long double > { typedef long double type; };
template <> struct promote<unsigned long, char > { typedef unsigned long type; };
template <> struct promote<unsigned long, unsigned char > { typedef unsigned long type; };
template <> struct promote<unsigned long, short int > { typedef unsigned long type; };
template <> struct promote<unsigned long, short unsigned int > { typedef unsigned long type; };
template <> struct promote<unsigned long, int > { typedef unsigned long type; };
template <> struct promote<unsigned long, unsigned int > { typedef unsigned long type; };
template <> struct promote<unsigned long, long > { typedef unsigned long type; };
template <> struct promote<unsigned long, unsigned long > { typedef unsigned long type; };
template <> struct promote<unsigned long, float > { typedef float type; };
template <> struct promote<unsigned long, double > { typedef double type; };
template <> struct promote<unsigned long, long double > { typedef long double type; };
template <> struct promote<float, char > { typedef float type; };
template <> struct promote<float, unsigned char > { typedef float type; };
template <> struct promote<float, short int > { typedef float type; };
template <> struct promote<float, short unsigned int > { typedef float type; };
template <> struct promote<float, int > { typedef float type; };
template <> struct promote<float, unsigned int > { typedef float type; };
template <> struct promote<float, long > { typedef float type; };
template <> struct promote<float, unsigned long > { typedef float type; };
template <> struct promote<float, float > { typedef float type; };
template <> struct promote<float, double > { typedef double type; };
template <> struct promote<float, long double > { typedef long double type; };
template <> struct promote<double, char > { typedef double type; };
template <> struct promote<double, unsigned char > { typedef double type; };
template <> struct promote<double, short int > { typedef double type; };
template <> struct promote<double, short unsigned int > { typedef double type; };
template <> struct promote<double, int > { typedef double type; };
template <> struct promote<double, unsigned int > { typedef double type; };
template <> struct promote<double, long > { typedef double type; };
template <> struct promote<double, unsigned long > { typedef double type; };
template <> struct promote<double, float > { typedef double type; };
template <> struct promote<double, double > { typedef double type; };
template <> struct promote<double, long double > { typedef long double type; };
template <> struct promote<long double, char > { typedef long double type; };
template <> struct promote<long double, unsigned char > { typedef long double type; };
template <> struct promote<long double, short int > { typedef long double type; };
template <> struct promote<long double, short unsigned int > { typedef long double type; };
template <> struct promote<long double, int > { typedef long double type; };
template <> struct promote<long double, unsigned int > { typedef long double type; };
template <> struct promote<long double, long > { typedef long double type; };
template <> struct promote<long double, unsigned long > { typedef long double type; };
template <> struct promote<long double, float > { typedef long double type; };
template <> struct promote<long double, double > { typedef long double type; };
template <> struct promote<long double, long double > { typedef long double type; };

#ifdef _RHEOLEF_HAVE_FLOAT128
  template <> struct promote<char, float128 > { typedef float128 type; };
  template <> struct promote<unsigned char, float128 > { typedef float128 type; };
  template <> struct promote<short unsigned int, float128 > { typedef float128 type; };
  template <> struct promote<int, float128 > { typedef float128 type; };
  template <> struct promote<unsigned int, float128 > { typedef float128 type; };
  template <> struct promote<long, float128 > { typedef float128 type; };
  template <> struct promote<unsigned long, float128 > { typedef float128 type; };
  template <> struct promote<float, float128 > { typedef float128 type; };
  template <> struct promote<double, float128 > { typedef float128 type; };
  template <> struct promote<float128, char > { typedef float128 type; };
  template <> struct promote<float128, unsigned char > { typedef float128 type; };
  template <> struct promote<float128, short int > { typedef float128 type; };
  template <> struct promote<float128, short unsigned int > { typedef float128 type; };
  template <> struct promote<float128, int > { typedef float128 type; };
  template <> struct promote<float128, unsigned int > { typedef float128 type; };
  template <> struct promote<float128, long > { typedef float128 type; };
  template <> struct promote<float128, unsigned long > { typedef float128 type; };
  template <> struct promote<float128, float > { typedef float128 type; };
  template <> struct promote<float128, double > { typedef float128 type; };
  template <> struct promote<float128, float128 > { typedef float128 type; };
#endif // _RHEOLEF_HAVE_FLOAT128

} // namespace rheolef
#endif /* _RHEOLEF_PROMOTE_H */
