#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
# -------------------------------------------------------------------
# check environment variables in an installed Rheolef
#
# author: Pierre.Saramito@imag.fr
#
# date: 6 january 2003
# -------------------------------------------------------------------
# TODO:
# - check MANPATH for good version and no mistakes
# -------------------------------------------------------------------
#set -x
if test $# -lt 3; then
    echo "usage: $0 <shlibpath_var> <libdir> <bindir>" >&2; exit 1
fi
shlibpath_var=$1
libdir=$2
bindir=$3
# -------------------------------------------------------------------
# get basics from environment
# -------------------------------------------------------------------
if test -x /bin/pwd; then
    # solve symlinks
    BINPWD=/bin/pwd
else
    # built-in pwd
    BINPWD=pwd
fi
# -------------------------------------------------------------------
# check PATH
# -------------------------------------------------------------------
if test x$PATH = x,; then
    echo "    *** INCREDIBLE ***"
    echo "        Your PATH environment variable is not set !"
    echo $LINE; exit 1
fi
path_list=`echo $PATH | sed -e 's/:/ /g'`
new_path_list=""
for d in $path_list; do	
    #echo "DIR           $d"
    if test -d $d && cd $d 2>/dev/null; then
        d_solved=`$BINPWD 2>/dev/null`
	cd $here
	new_path_list="${new_path_list} ${d_solved}"
    fi
done
path_list=${new_path_list}
# -------------------------------------------------------------------
# check ypcat
# -------------------------------------------------------------------
ypcat=""
for d in $path_list; do	
    if test -x $d/ypcat; then
	ypcat=$d/ypcat
        break
    fi
done
# -------------------------------------------------------------------
# check SHELL
# -------------------------------------------------------------------
if test "$USER" != "" -a "$ypcat" != ""; then
    USER_SHELL=`$ypcat passwd 2>/dev/null | grep "$USER" | awk -F ":" '{print $7}'`
elif test "$USER" != "" -a -r /etc/passwd; then
    USER_SHELL=`grep "$USER" /etc/passwd | awk -F ":" '{print $7}'`
fi
case "$USER_SHELL" in
    */csh | */tcsh) 
    	shell_path=path
    	shell_style=c
    	shell_rc='~/.cshrc'
	;;
    */bash)
    	shell_path=PATH
    	shell_style=b
    	shell_rc='~/.bashrc'
        ;;
    */sh)
    	shell_path=PATH
    	shell_style=b
    	shell_rc='$HOME/.profile'
        ;;
    *)
    	# unknown shell ???
    	shell_style=b
    	shell_rc='~/.profile'
        ;;
esac
here=`$BINPWD`
if test -d $libdir && cd $libdir 2>/dev/null; then
    libdir_solved=`$BINPWD 2>/dev/null`
else
    libdir_solved=$libdir;
fi
cd $here
if test -d $bindir && cd $bindir 2>/dev/null; then
    bindir_solved=`$BINPWD 2>/dev/null`
else
    bindir_solved=$bindir
fi
cd $here

LINE='================================================================================'
echo $LINE
# -------------------------------------------------------------------
# check if installed
# -------------------------------------------------------------------
if test ! -x $bindir/rheolef-config; then
    echo "    WARNING: The file $bindir/rheolef-config is not present."
    echo "    HINT:    Enter 'make install'."
    echo $LINE; exit 1
fi
if test ! -f $libdir/librheolef.la; then
    echo "    WARNING: The file $libdir/librheolef.la is not present."
    echo "    HINT:    Enter 'make install'."
    echo $LINE; exit 1
fi
VERSION=`$bindir/rheolef-config --version`
# -------------------------------------------------------------------
# check PATH
# -------------------------------------------------------------------
#echo "bindir_solved $bindir_solved"
n=0
list=""
registered=false
# d1 = fisrt dir containing rheolef-config in PATH
d1=""
d1_solved=""
for d in $path_list; do	
    d_solved=$d
    #echo "d_solved $d_solved"
    #echo "DIR_SOLVED    $d_solved"
    #echo "BINDIR_SOLVED $bindir_solved"
    if test "$d_solved" = "$bindir_solved"; then
    	registered=true
        #echo FOUNDED_HERE
    fi
    if test -x $d/rheolef-config; then
    	if test "$d1" = ""; then
	    d1=$d
	    d1_solved=$d_solved
	fi
        list="$list $d"
        n=`expr $n + 1`
    fi
done
#echo N $n
#echo LIST $list
#echo FOUNDED $registered
if test "$registered" = false; then
    echo
    echo "    WARNING:"
    echo "        The directory $bindir may be"
    echo "        added to the  PATH environment variable."
    echo
    echo "    HINT: Add at the end of your $shell_rc file:"
    echo
    if test $shell_style = c; then
        echo "        set path = ($bindir \$path)"
    else
        echo "        export PATH=\"$bindir:\$PATH\""
    fi
    echo 
    echo "    and then enter:"
    echo 
    echo "        source $shell_rc"
    echo
    echo $LINE; exit 1
fi
# here, bindir is in PATH
# -----------------------
if test $n -eq 0; then
    echo "    WARNING:"
    echo "    Rheolef is not yet installed"
    echo "    HINT: Run 'make install'"
    echo
    echo $LINE; exit 1
elif test $n -ne 1; then
    # d1 = fisrt dir containing rheolef-config in PATH
    if test "$d1_solved" != "$bindir_solved"; then
	d1_version=`$d1/rheolef-config --version`
        echo
        echo "    WARNING:"
        echo "      Installed Rheolef commands"
        echo "          $bindir/rheolef-config (version $VERSION)"
        echo "      is hidden by another installation:"
        echo "          $d1/rheolef-config (version $d1_version)"
        echo
        echo "    HINT: Edit $shell_rc and then"
	echo "      swap $bindir and $d1 in"
	echo "      the $shell_path environment variable. Then enter:"
        echo
        echo "        source $shell_rc"
        echo
        echo $LINE; exit 1
    fi
fi
## # -------------------------------------------------------------------
## # check SHLIBPATH variable: now obsolete, since hard-linked
## # -------------------------------------------------------------------
## if test $shlibpath_var = xunsupported,; then
## 	# shlib not supported
## 	echo $LINE; exit 0
## fi
## cmd1="echo \\\$\$shlibpath_var"
## cmd2=`eval $cmd1`
## shlibpath=`eval "echo $cmd2"`
## libdir_list=`echo $shlibpath | sed -e 's/:/ /g'`
## if test -f /etc/ld.so.conf; then
##     libdir_list="$libdir_list `cat /etc/ld.so.conf`"
## fi
## libdir_list="$libdir_list /lib /usr/lib"
## registered=false
## n=0
## list=""
## # d1 = fisrt dir containing librheolef.la in SHLIBPATH
## d1=""
## d1_solved=""
## #echo shlibpath $shlibpath
## for d in $libdir_list; do	
##     if test -d $d && cd $d 2>/dev/null; then
##         d_solved=`$BINPWD 2>/dev/null`
## 	cd $here
##     else
## 	cd $here
## 	continue
##     fi
##     #echo DIR $d
##     #echo LIBDIR $libdir
##     if test "$d_solved" = "$libdir_solved"; then
##     	registered=true
##     fi
##     if test -f $d/librheolef.la; then
##         list="$list $d"
## 	if test "$d1" = ""; then
## 	    d1=$d
## 	    d1_solved=$d_solved
## 	fi
##         n=`expr $n + 1`
##     fi
## done
## def_libdir='`rheolef-config --libdir`'
## # here, there is some librheolef.la in shlibpath ($n -ge 1)
## # ---------------------------------------------------------
## #   1) $n -eq 0                           => add libdir
## #   2) test $registered = true  -a $n -ge 2       => possible conflict (depend on order)
## #   3) test $registered = false -a $n -ge 1  => add libdir before
## #   4) test $registered = true  -a $n -eq 1          => ok
## #
## if  test $n -eq 0 || \
##     test $registered = true -a $n -ge 2 -a "$d1_solved" != "$libdir_solved" || \
##     test $registered = false; then
## 
##     if test $n -eq 0; then
##         echo
##         echo "    WARNING:"
##         echo "        The directory $libdir may be"
##         echo "        added to the  ${shlibpath_var} environment variable."
## 
##     elif test $registered -a $n -ge 2 -a "$d1_solved" != "$libdir_solved"; then
## 
##         # libdir is not the first in shlibpath
##         d1_version=`grep current $d1/librheolef.la | sed -e 's/current=//'`
##         version=`grep current $libdir/librheolef.la | sed -e 's/current=//'`
##         echo
##         echo "    WARNING:"
##         echo "      Installed Rheolef library"
##         echo
##         echo "        $libdir/librheolef.la (version $version)"
##         echo
##         echo "      is hidden by another installation:"
##         echo
##         echo "        $d1/librheolef.la (version $d1_version)"
## 
##     else 
##     	# test $registered = false -a $n -ge 1
## 
##         d1_version=`grep current $d1/librheolef.la | sed -e 's/current=//'`
##         version=`grep current $libdir/librheolef.la | sed -e 's/current=//'`
##         echo
##         echo "    WARNING:"
##         echo "      Current Rheolef library in"
##         echo
##         echo "        $libdir/librheolef.la (version $version)"
##         echo
##         echo "      is not registered. Instead, another Rheolef library is actually registered:"
##         echo
##         echo "        $d1/librheolef.la (version $d1_version)"
##     fi
##     echo
##     if test x"$shlibpath" = x""; then
## 
##         echo "        The variable  $shlibpath_var is not set."
##         echo
##         echo "    HINT: Edit $shell_rc and insert:"
##         echo
## 	if test $shell_style = c; then
##             echo "            setenv $shlibpath_var $def_libdir"
##         else
##             echo "            export $shlibpath_var=$def_libdir"
##         fi
##     else
##         echo
##         echo "    HINT: Edit $shell_rc and insert:"
##         echo
## 	if test $shell_style = c; then
##             echo "            setenv $shlibpath_var \"$def_libdir:\${$shlibpath_var}\""
##         else
##             echo "            export $shlibpath_var=\"$def_libdir:\${$shlibpath_var}\""
##         fi
##     fi
##     echo
##     echo "        and then enter:"
##     echo 
##     echo "            source $shell_rc"
##     echo
##     echo $LINE; exit 1
## fi
echo "    Rheolef $VERSION successfully installed."
echo "    Have fun with Rheolef !"
echo $LINE; exit 0
