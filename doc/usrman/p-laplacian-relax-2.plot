set terminal cairolatex pdf color standalone
set output "p-laplacian-relax-2.tex"

set size square 
set colors classic
set key left at graph 0.1,0.95
set xrange [0:1]
set yrange [0:0.6]
set xtics 0.25
set ytics (0,0.1,0.2,0.3,0.4)

set xlabel '[c]{\large $\omega$}'
set  label '[r]{\Large $\bar{v}$}' at graph -0.05,0.95

plot \
'p-laplacian-relax-p=3.gdat' t '[r]{$p=3$}' w lp lc 4 lw 2, \
'p-laplacian-relax-p=4.gdat' t '[r]{$p=4$}' w lp lc 3 lw 2, \
'p-laplacian-relax-p=5.gdat' t '[r]{$p=5$}' w lp lc rgb '#008800' lw 2, \
'p-laplacian-relax-p=6.gdat' t '[r]{$p=6$}' w lp lc 1 lw 2

#pause -1 "<retour>"
