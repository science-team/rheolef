#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATADIR=$SRCDIR
BINDIR="../bin"
SBINDIR="../sbin"
NPROC_MAX=${NPROC_MAX-"7"}
ROUNDER="$BINDIR/field - -field -round -I$DATADIR"
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0


L="
triangle_p1-v2
"

ROUNDER="$BINDIR/field -I$SRCDIR -field -round 1e-7 -"
for geo in $L; do
  loop_mpirun "./integrate_dg_tst $DATADIR/$geo.geo P1d 2>/dev/null | $ROUNDER 2>/dev/null | diff - $DATADIR/integrate_dg_tst-$geo-P1d.valid >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
