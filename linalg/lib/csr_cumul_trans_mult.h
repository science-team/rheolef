# ifndef _SKIT_CSR_CUMUL_TRANS_MULT_H
# define _SKIT_CSR_CUMUL_TRANS_MULT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// CSR: Compressed Sparse Row format
//
// algorithm-oriented generic library
// inspired from sparskit2 fortran library
//  
// author: Pierre.Saramito@imag.fr
//  
// date: 12 november 1997
//  
//@!\vfill\listofalgorithms
/*@! 
 \vfill \pagebreak \mbox{} \vfill \begin{algorithm}[h]
  \caption{{\tt trans\_mult}: sparse matrix $y += a^T*x$, where $x,y$ are dense vectors.}
  \begin{algorithmic}
    \INPUT {sparse matrix a and dense vector x}
      ia(0:nrowa), ja(0:nnza-1), a(0:nnza-1),
      x(0:nrowa)
    \ENDINPUT
    \OUTPUT {number of non-null elements in $z=x\pm y$}
      y(0:ncola)
    \ENDOUTPUT
    \NOTE {}
      The $y$ vector may be set to zero before the call in order to 
      compute $y := a^T*x$
    \ENDNOTE
    \BEGIN 
      \FORTO {i := 0}{nrowa-1}
          \FORTO {p := ia(i)}{ia(i+1)-1}
	      y(ja(p)) += a(p) * x(i)
          \ENDFOR
      \ENDFOR
    \END
 \end{algorithmic} \end{algorithm}
 \vfill \pagebreak \mbox{} \vfill
*/
namespace rheolef { 

template <
    class InputIterator1,
    class InputIterator3,
    class SetOperator,
    class RandomAccessMutableIterator>
void
csr_cumul_trans_mult (
    InputIterator1 ia,
    InputIterator1 last_ia,
    InputIterator3 x,
    SetOperator set_op, // set_op: += or -= but not = because may cumul
    RandomAccessMutableIterator y)
{
    typedef typename std::iterator_traits<InputIterator1>::value_type InputIterator2;
    typedef typename std::iterator_traits<RandomAccessMutableIterator>::value_type T;
    InputIterator2 a = (*ia++);
    while (ia != last_ia) {
        T xi = *x++;
        InputIterator2 last_a = (*ia++);
	while (a != last_a) {
	    set_op (y [(*a).first], (*a).second * xi);
	    ++a;
        }
    }
}
//@!\vfill
}// namespace rheolef
# endif // _SKIT_CSR_CUMUL_TRANS_MULT_H
