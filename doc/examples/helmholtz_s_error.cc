///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile helmholtz_s_error.cc The Helmholtz problem on a surface -- error analysis
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "sphere.icc"
int main (int argc, char**argv) {
  environment rheolef(argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e+38;
  field uh; din >> uh;
  const space& Wh = uh.get_space();
  trial u (Wh); test v (Wh);
  form m = integrate (u*v);
  form a = integrate (dot(grad_s(u),grad_s(v)));
  size_t d = Wh.get_geo().dimension();
  field pi_h_u = lazy_interpolate(Wh, u_exact(d));
  field eh = uh - pi_h_u;
  dout << "err_l2   " << sqrt(m(eh,eh)) << endl
       << "err_h1   " << sqrt(a(eh,eh)) << endl
       << "err_linf " << eh.max_abs()   << endl;
  return (eh.max_abs() < tol) ? 0 : 1;
}
