///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo.h"
#include "rheolef/geo_domain.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/space_numbering.h"

#include "geo_header.h"

namespace rheolef {

// ----------------------------------------------------------------------------
// build connectivity: for input files such as gmsh that do not have
// a global numbering of edges and faces
// ----------------------------------------------------------------------------

template <class T>
void
geo_rep<T,sequential>::build_connectivity(
  std::array<disarray<geo_element_auto<>,sequential>, reference_element::max_variant>& tmp_geo_element)
{
  typedef geo_element_auto<> element_t;
  typedef disarray<element_t,sequential> disarray_t;
  typedef typename disarray_t::const_iterator const_iterator_by_variant_tmp;
  size_type map_dim = geo_base_rep<T,sequential>::_gs._map_dimension;
  if (map_dim <= 1) return;
  build_connectivity_sides (map_dim-1, tmp_geo_element); 
  build_connectivity_sides (map_dim-2, tmp_geo_element); 
}
template <class T>
void
geo_rep<T,sequential>::build_connectivity_sides (
  size_type                             side_dim,
  std::array<disarray<geo_element_auto<>,sequential>, reference_element::max_variant>& tmp_geo_element)
{
  typedef geo_element_auto<> element_t;
  typedef disarray<element_t,sequential> disarray_t;
  typedef typename disarray_t::const_iterator const_iterator_by_variant_tmp;
  if (side_dim == 0) return;
  // ------------------------------------------------------------------------
  // 1) count sides
  // ------------------------------------------------------------------------
  // difficulte : on ne connait pas la taille de la table des aretes et faces par variante...
  // first pass: count by variant and use a nsid global counter as pseudo side index
  //  this index is not usable since sides indexes may be ordered by increasing variants
  //  and here, during the first pass, sides appears by unordered variant
  size_type size_by_variant [reference_element::max_variant];
  std::fill (size_by_variant, size_by_variant+reference_element::max_variant, 0);
  index_set empty_set; // TODO: add a global allocator to empty_set
  size_type map_dim = geo_base_rep<T,sequential>::_gs._map_dimension;
  size_type nsid = 0;
  {
   disarray<index_set,sequential> ball_S (geo_element_ownership(0), empty_set);
   for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                 variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
    for (const_iterator_by_variant_tmp iter = tmp_geo_element [variant].begin(),
                                       last = tmp_geo_element [variant].end();
                                       iter != last; iter++) {
  
      const geo_element& K = *iter;
      for (size_type loc_isid = 0, loc_nsid = K.n_subgeo(side_dim); loc_isid < loc_nsid; loc_isid++) {
        size_type S_variant = reference_element::variant (K.subgeo_size (side_dim, loc_isid), side_dim);
        size_type loc_jv0 = K.subgeo_local_vertex (side_dim, loc_isid, 0);
        size_type     jv0 = K[loc_jv0];
        index_set isid_set = ball_S [jv0]; // copy: will be intersected
        for (size_type sid_jloc = 1, sid_nloc = K.subgeo_size (side_dim, loc_isid); sid_jloc < sid_nloc; sid_jloc++) { 
          size_type loc_jv = K.subgeo_local_vertex (side_dim, loc_isid, sid_jloc);
          size_type     jv = K[loc_jv];
          const index_set& ball_jv = ball_S [jv];
          isid_set.inplace_intersection (ball_jv);
        }
        check_macro (isid_set.size() <= 1, "connectivity problem");
        if (isid_set.size() == 1) {
  	  continue; // side already exists
        }
        // then insert it in ball_S
        for (size_type sid_jloc = 0, sid_nloc = K.subgeo_size (side_dim, loc_isid); sid_jloc < sid_nloc; sid_jloc++) { 
          size_type loc_jv = K.subgeo_local_vertex (side_dim, loc_isid, sid_jloc);
          size_type     jv = K[loc_jv];
          ball_S[jv] += nsid;
        }
        size_by_variant [S_variant]++;
        nsid++;
      }
    }
   }
  }
  // ------------------------------------------------------------------------
  // 2) allocate tmp_geo_element[]
  // ------------------------------------------------------------------------
  size_type first_by_variant [reference_element::max_variant+1];
  std::fill (first_by_variant, first_by_variant+reference_element::max_variant+1, 0);
  for (size_type side_variant = reference_element::first_variant_by_dimension(side_dim);
                 side_variant < reference_element:: last_variant_by_dimension(side_dim); side_variant++) {
    geo_element_auto<> init_val (side_variant, base::order());
    tmp_geo_element [side_variant].resize (size_by_variant [side_variant], init_val);
    base::_gs.ownership_by_variant [side_variant] = tmp_geo_element [side_variant].ownership();
    first_by_variant [side_variant+1] = first_by_variant [side_variant] + size_by_variant [side_variant];
  }
  // ownership_by_dimension[side_dim]: used by connectivity
  base::_gs.ownership_by_dimension [side_dim] = distributor (nsid, base::comm(), nsid);

  // ------------------------------------------------------------------------
  // 3) set numbering (ordered by variant) & create sides
  // ------------------------------------------------------------------------
  geo_element_auto<> S;
  {
   size_type count_by_variant [reference_element::max_variant];
   std::fill (count_by_variant, count_by_variant+reference_element::max_variant, 0);
   disarray<index_set,sequential> ball_S (geo_element_ownership(0), empty_set);
   nsid = 0;
   for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                  variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
    for (const_iterator_by_variant_tmp iter = tmp_geo_element [variant].begin(),
                                       last = tmp_geo_element [variant].end();
                                       iter != last; iter++) {
  
      const geo_element& K = *iter;
      for (size_type loc_isid = 0, loc_nsid = K.n_subgeo(side_dim); loc_isid < loc_nsid; loc_isid++) {
        size_type S_variant = reference_element::variant (K.subgeo_size (side_dim, loc_isid), side_dim);
        size_type loc_jv0 = K.subgeo_local_vertex (side_dim, loc_isid, 0);
        size_type     jv0 = K[loc_jv0];
        index_set isid_set = ball_S [jv0]; // copy: will be intersected
        for (size_type sid_jloc = 1, sid_nloc = K.subgeo_size (side_dim, loc_isid); sid_jloc < sid_nloc; sid_jloc++) { 
          size_type loc_jv = K.subgeo_local_vertex (side_dim, loc_isid, sid_jloc);
          size_type     jv = K[loc_jv];
          const index_set& ball_jv = ball_S [jv];
          isid_set.inplace_intersection (ball_jv);
        }
        check_macro (isid_set.size() <= 1, "connectivity problem");
        if (isid_set.size() == 1) {
  	  continue; // side already exists
        }
        // side does not exists yet: then insert it in ball_S
        for (size_type sid_jloc = 0, sid_nloc = K.subgeo_size (side_dim, loc_isid); sid_jloc < sid_nloc; sid_jloc++) { 
          size_type loc_jv = K.subgeo_local_vertex (side_dim, loc_isid, sid_jloc);
          size_type     jv = K[loc_jv];
          ball_S[jv] += nsid;
        }
        size_type isid_by_variant = count_by_variant [S_variant];
        count_by_variant [S_variant]++;
        nsid++;
        // create S
        S.reset (S_variant, base::order());
        // set also indexes
        size_type ige = first_by_variant[S_variant] + isid_by_variant;
  	S.set_ios_dis_ie (ige);
  	S.set_dis_ie (ige);
	// copy all nodes from K.subgeo into S that goes to tmp_gheo_element (will be conserved)
        size_type S_n_node = K.subgeo_n_node (side_dim, loc_isid); // TODO: use ref_elem
        for (size_type sid_jlocnod = 0, sid_nlocnod = S_n_node; sid_jlocnod < sid_nlocnod; sid_jlocnod++) { 
          size_type loc_jnod = K.subgeo_local_node (side_dim, loc_isid, sid_jlocnod); // TODO: use ref_elem
          size_type     jnod = K.node(loc_jnod);
          S[sid_jlocnod] = jnod;
        }
        // then copy the side S in the tmp mesh:
        tmp_geo_element [S_variant][isid_by_variant] = S;
      }
    }
   }
  }
}
// =========================================================================
// get upgrade
// =========================================================================
/// @brief upgrade for geo
template <class T>
idiststream&
geo_rep<T,sequential>::get_upgrade (
  idiststream&									      ips,
  const geo_header&                                                                   hdr)
{
  using namespace std;
  check_macro (ips.good(), "bad input stream for geo.");
  istream& is = ips.is();
  // ------------------------------------------------------------------------
  // 1) get nodes
  // ------------------------------------------------------------------------
  size_type nnod = hdr.dis_size_by_dimension [0];
  disarray<node_type, sequential> node (nnod);
  if (hdr.dimension > 0) {
    node.get_values (ips, _point_get<T>(hdr.dimension));
    check_macro (ips.good(), "bad input stream for geo.");
  }
  // ------------------------------------------------------------------------
  // 2) get elements
  // ------------------------------------------------------------------------
  typedef geo_element_auto<> element_t;
  typedef disarray<element_t, sequential> disarray_t;
  std::array<disarray_t, reference_element::max_variant>  tmp_geo_element;
  if (hdr.map_dimension > 0) {
    for (size_type variant = reference_element::first_variant_by_dimension(hdr.map_dimension);
                   variant < reference_element:: last_variant_by_dimension(hdr.map_dimension); variant++) {
      geo_element_auto<> init_val (variant, hdr.order);
      tmp_geo_element [variant] = disarray_t (hdr.dis_size_by_variant [variant], init_val);
      tmp_geo_element [variant].get_values (ips);
      check_macro (ips.good(), "bad input stream for geo.");
    }
  }
  // ------------------------------------------------------------------------
  // 3) build & upgrade
  // ------------------------------------------------------------------------
  bool do_upgrade = iorheo::getupgrade(is);
  build_from_data (hdr, node, tmp_geo_element, do_upgrade);
  // ------------------------------------------------------------------------
  // 4) get domain, until end-of-file
  // ------------------------------------------------------------------------
  vector<index_set> ball [4];
  domain_indirect_basic<sequential> dom;
  while (dom.get (ips, *this, ball)) {
     base::_domains.push_back (dom);
  }
  return ips;
}
template <class T>
void
geo_rep<T,sequential>::build_from_data (
  const geo_header&                   hdr,
  const disarray<node_type, sequential>& node,
        std::array<disarray<geo_element_auto<>,sequential>, reference_element::max_variant>&
				      tmp_geo_element,
  bool  			      do_upgrade)
{
  using namespace std;
  typedef geo_element_auto<> element_t;
  typedef disarray<element_t, sequential> disarray_t;
  typedef typename disarray_t::const_iterator const_iterator_by_variant_tmp;
  typedef typename disarray_t::iterator       iterator_by_variant_tmp;
  // ------------------------------------------------------------------------
  // 1) get header
  // ------------------------------------------------------------------------
  base::_have_connectivity = false;
  base::_version       = 4;
  base::_name          = "unnamed";
  base::_dimension     = hdr.dimension;
  base::_gs._map_dimension = hdr.map_dimension;
  base::_sys_coord     = hdr.sys_coord;
  base::_piola_basis.reset_family_index (hdr.order);
  size_type nnod   = hdr.dis_size_by_dimension [0];
  size_type n_edg  = hdr.dis_size_by_dimension [1];
  size_type n_fac  = hdr.dis_size_by_dimension [2];
  size_type n_elt  = hdr.dis_size_by_dimension [base::_gs._map_dimension];
  // ------------------------------------------------------------------------
  // 2) get coordinates
  // ------------------------------------------------------------------------
  base::_node = node; 
  base::_gs.node_ownership = base::_node.ownership();
  // ------------------------------------------------------------------------
  // 3) bounding box: _xmin, _xmax
  // ------------------------------------------------------------------------
  base::compute_bbox();
  // ------------------------------------------------------------------------
  // 4) get elements
  // ------------------------------------------------------------------------
  if (base::_gs._map_dimension > 0) {
    for (size_type variant = reference_element::first_variant_by_dimension(base::_gs._map_dimension);
                   variant < reference_element:: last_variant_by_dimension(base::_gs._map_dimension); variant++) {
      base::_gs.ownership_by_variant [variant] = tmp_geo_element [variant].ownership();
    }
    base::_gs.ownership_by_dimension [base::_gs._map_dimension] = distributor (n_elt, base::comm(), n_elt);
  }
  // check that nodes are numbered by increasing node_subgeo_dim
  {
    std::vector<size_type> node_subgeo_dim (nnod, size_type(-1));
    size_type prev_variant = 0;
    for (size_type variant = reference_element::first_variant_by_dimension(base::_gs._map_dimension);
                   variant < reference_element:: last_variant_by_dimension(base::_gs._map_dimension); variant++) {
      for (const_iterator_by_variant_tmp iter = tmp_geo_element [variant].begin(),
                                         last = tmp_geo_element [variant].end();
                                         iter != last; iter++) {
  
        const geo_element& K = *iter;
        check_macro (prev_variant <= K.variant(), "elements should be numbered by increasing variants (petqTPH)");
        prev_variant = K.variant();
        for (size_type d = 0; d <= base::_gs._map_dimension; d++) { 
          for (size_type loc_inod = K.first_inod(d), loc_nnod = K.last_inod(d); loc_inod < loc_nnod; loc_inod++) {
            check_macro (K[loc_inod] < nnod, "inod K["<<loc_inod<<"]="<<K[loc_inod]
		<<" is out of range [0:"<<nnod<<"[; K.dis_ie="<<K.dis_ie());
            node_subgeo_dim [K[loc_inod]] = d;
          }
        }
      }
    }
    size_type prev_node_dim = 0;
    size_type i_curr_node = 0;
    for (typename std::vector<size_type>::const_iterator iter = node_subgeo_dim.begin(), last = node_subgeo_dim.end();
		iter != last; iter++, i_curr_node++) {
      check_macro (prev_node_dim <= *iter, "nodes should be numbered by increasing subgeo dimension (prev="
		<< prev_node_dim << " > subgeo_dim(node("<<i_curr_node<<"))=" << *iter << ")");
      prev_node_dim = *iter;
    }
  }
  // compute n_vert (n_vert < nnod when order > 1) and set element indexes (K.dis_ie & K.ios_dis_ie)
  size_type n_vert = 0;
  if (base::_gs._map_dimension == 0) {
    n_vert = nnod;
  } else {
    vector<size_t> is_vertex (nnod, 0);
    size_type ie = 0;
    for (size_type variant = reference_element::first_variant_by_dimension(base::_gs._map_dimension);
                   variant < reference_element:: last_variant_by_dimension(base::_gs._map_dimension); variant++) {
      for (iterator_by_variant_tmp iter = tmp_geo_element [variant].begin(),
                                   last = tmp_geo_element [variant].end();
                                   iter != last; iter++, ie++) {
  
  	geo_element& K = *iter;
  	K.set_ios_dis_ie (ie);
  	K.set_dis_ie (ie);
        if (base::order() > 1) {
          for (size_type iloc = 0, nloc = K.size(); iloc < nloc; iloc++) {
            is_vertex [K[iloc]] = 1;
          }
        }
      }
    }
    if (base::order() == 1) {
      n_vert = nnod;
    } else {
      n_vert = accumulate (is_vertex.begin(), is_vertex.end(), 0);
    }
  }
  // ------------------------------------------------------------------------
  // 5) create vertex-element (0d elements)
  // ------------------------------------------------------------------------
  {
    geo_element_auto<> init_val (reference_element::p, base::order());
    tmp_geo_element [reference_element::p].resize (n_vert, init_val);
    size_type first_iv = base::_node.ownership().first_index();
    size_type iv = 0;
    for (iterator_by_variant_tmp iter = tmp_geo_element [reference_element::p].begin(),
                                 last = tmp_geo_element [reference_element::p].end();
                                 iter != last; iter++, iv++) {
      geo_element& P = *iter;
      P[0]            = first_iv + iv;
      P.set_dis_ie     (first_iv + iv); // TODO: P[0] & dis_ie redundant for `p'
      P.set_ios_dis_ie (first_iv + iv);
    }
  }
  // ownership_by_dimension[0]: used by connectivity
  base::_gs.ownership_by_variant   [reference_element::p] = tmp_geo_element [reference_element::p].ownership();
  base::_gs.ownership_by_dimension [0] = tmp_geo_element [reference_element::p].ownership();
  // ------------------------------------------------------------------------
  // 6) build connectivity
  // ------------------------------------------------------------------------
  build_connectivity (tmp_geo_element);
  // ------------------------------------------------------------------------
  // 7) copy tmp_geo_element into _geo_element
  // ------------------------------------------------------------------------
  for (size_type dim = 0; dim <= base::_gs._map_dimension; dim++) {

    for (size_type variant = reference_element::first_variant_by_dimension(dim);
                   variant < reference_element:: last_variant_by_dimension(dim); variant++) {

      geo_element::parameter_type param (variant, 1);
      base::_geo_element [variant].resize (tmp_geo_element [variant].size(), param);
      size_type igev = 0;
      iterator_by_variant iter2 = base::_geo_element [variant].begin();
      for (const_iterator_by_variant_tmp iter = tmp_geo_element [variant].begin(),
                                         last = tmp_geo_element [variant].end();
                                         iter != last; iter++, iter2++) {
  
        *iter2 = *iter;
      }
    }
  }
  // ------------------------------------------------------------------------
  // 8) set indexes on faces and edges of elements, for P2 approx
  // ------------------------------------------------------------------------
  set_element_side_index (1);
  set_element_side_index (2);
  // ------------------------------------------------------------------------
  // 9) node renumbering, when v1 -> v2 aka !have_connectivity
  // ------------------------------------------------------------------------
  if (do_upgrade) {
    // 9.1. compute the new node numbering
    std::vector<size_type> inod2new_inod (base::_node.size(), std::numeric_limits<size_type>::max());
    std::vector<size_t> K_new_inod;
    for (size_type dim = 0; dim < base::_gs._map_dimension; dim++) {
      for (size_type variant = reference_element::first_variant_by_dimension(base::_gs._map_dimension);
                     variant < reference_element:: last_variant_by_dimension(base::_gs._map_dimension); variant++) {
        const_iterator_by_variant_tmp tmp_iter = tmp_geo_element [variant].begin();
        for (const_iterator_by_variant iter = base::_geo_element [variant].begin(),
                                       last = base::_geo_element [variant].end();
                                       iter != last; iter++, tmp_iter++) {
  
          const geo_element& K = *iter;
          const geo_element& tmp_K = *tmp_iter;
          space_numbering::dis_idof (base::_piola_basis, base::_gs, K, K_new_inod);
          for (size_type loc_inod = 0; loc_inod < tmp_K.n_node(); loc_inod++) {
            size_type     inod = tmp_K.node (loc_inod);
            size_type new_inod = K_new_inod [loc_inod];
            if (inod2new_inod [inod] != std::numeric_limits<size_type>::max()) continue; // already done
            inod2new_inod [inod] = new_inod;
          }
        }
      }
    }
#ifdef _RHEOLEF_PARANO
    // 9.1.b check permutation
    std::vector<size_type> new_inod2inod (base::_node.size(), std::numeric_limits<size_type>::max());
    for (size_type inod = 0, nnod = base::_node.size(); inod < nnod; inod++) {
      check_macro (inod2new_inod [inod] < nnod, "invalid renumbering: inod2new_inod ["<<inod<<"] = "
      			<< inod2new_inod [inod] << " is out of range [0:"<<nnod<<"[");
      new_inod2inod [inod2new_inod [inod]] = inod;
    }
    for (size_type inod = 0, nnod = base::_node.size(); inod < nnod; inod++) {
      check_macro (new_inod2inod [inod2new_inod [inod]] == inod, "invalid permutation");
    }
#endif // _RHEOLEF_PARANO
    // 9.2. apply the new node numbering to the node table
    disarray<point_basic<T>,sequential> new_node (base::_node.ownership());
    for (size_type inod = 0, nnod = base::_node.size(); inod < nnod; inod++) {
      size_type new_inod = inod2new_inod [inod];
      new_node [new_inod] = base::_node [inod];
    }
    base::_node = new_node;
  }
  // ------------------------------------------------------------------------
  // 11) epilogue
  // ------------------------------------------------------------------------
  if (! base::_have_connectivity && ! do_upgrade) {
    warning_macro ("mesh without connectivity is obsolete (HINT: see geo -upgrade)");
  }
  if (do_upgrade) {
    base::_have_connectivity = true;
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_rep<Float,sequential>;

} // namespace rheolef
