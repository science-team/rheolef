///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// TODO: merge with nfem/plib/rounder.h
template <class T>
struct rounder_type : std::unary_function<T,T> {
  rounder_type (const T& prec) : _prec(prec) {}
  T operator() (const T& x) const {
     // use floor : std::round() is non-standard (INTEL C++)
     T value = _prec*floor(x/_prec + 0.5);
     if (1+value == 1) value = T(0);
     return value;
  }
  T _prec;
};
