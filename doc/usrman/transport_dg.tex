The aim of this chapter is to introduce to discontinuous Galerkin methods
within the \Rheolef\  environment.
For some recent presentations of discontinuous Galerkin methods,
see~\citet{PieErn-2012} for theoretical aspects and
\citet{HesWar-2008} for algorithmic and implementation.
% The discontinuous Galerkin methods are in active development
% in \Rheolef, and new features will appear soon.

% ------------------------------
\subsection{The stationary transport equation}
% ------------------------------
\label{sec-transport-dg}%
\apindex{discontinuous}%
\cindex{problem!transport equation!steady}
The steady scalar transport problem writes:

\ \ $(P)$: \emph{find $\phi$, defined in $\Omega$, such that}
\begin{eqnarray*}
  {\bf u}.\nabla \phi + \sigma\phi &=& f \ \mbox{ in } \Omega \\
  \phi &=& \phi_\Gamma \ \mbox{ on } \partial\Omega_{-}
\end{eqnarray*}
where ${\bf u}$, $\sigma>0$, $f$ and $\phi_\Gamma$ being known.
Note that this is the steady version of 
the unsteady diffusion-convection problem previously
introduced in section~\ref{pb-transport-unsteady}, page~\pageref{pb-transport-unsteady}
and when the diffusion coefficient $\nu$ vanishes.
\cindex{upstream boundary}%
Here, the $\partial\Omega_{-}$ notation is the \emph{upstream} boundary part, defined by
\[
   \partial\Omega_{-}
   = \{ x \in\partial\Omega; \ {\bf u}(x).{\bf n}(x) < 0 \}
\]
Let us suppose that ${\bf u}\in W^{1,\infty}(\Omega)^d$ and introduce
the space:
\[ 
   X = \{ \varphi \in L^2(\Omega); \ ({\bf u}.\nabla) \varphi \in L^2(\Omega)^d \}
\]
and, for all $\phi,\varphi\in X$ 
\begin{equation*}
  \begin{array}{rcccl}
  a(\phi,\varphi) &=& 
	{\displaystyle
          \int_\Omega 
	    \left(
              {\bf u}.\nabla \phi \, \varphi
	      +
	      \sigma \,
              \phi \, \varphi
	    \right)
	    \, {\rm d}x
        }
	&+&
	{\displaystyle
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n}\right)
	    \phi \  \varphi \, {\rm d}s
        }
	\\
  l(\varphi) &=& 
	{\displaystyle
  	  \int_\Omega 
	    f \, \varphi \, {\rm d}x
        }
        &+&
	{\displaystyle
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n}\right)
	    \phi_\Gamma \, \varphi \, {\rm d}s
        }
 \end{array}
\end{equation*}
Then, the variational formulation writes:

\ \ $(FV)$: \emph{find $\phi\in X$ such that}
\begin{eqnarray*}
  a(\phi,\varphi) &=& l(\varphi), \ \forall \varphi\in X
\end{eqnarray*}
Note that the term 
\mbox{$
  \max(0,-{\bf u}.{\bf n})
  =
  (|{\bf u}.{\bf n}|-{\bf u}.{\bf n})/2
$}
is positive and vanishes everywhere except on $\partial\Omega_{-}$.
\cindex{boundary condition!weakly imposed}%
Thus, the boundary condition $\phi=\phi_\Gamma$ is weakly imposed 
on $\partial\Omega_{-}$ via the integrals on the boundary.
%
The \emph{discontinuous} finite element space is defined by:
\[ 
   X_h = \{ \varphi_h \in L^2(\Omega); \varphi_{h|K}\in P_k, \ \forall K \in \mathcal{T}_h \}
\]
where $k\geq 0$ is the polynomial degree.
Note that $X_h \not\subset X$
and that the $\nabla\phi_h$ term has no more sense for discontinuous functions $\phi_h \in X_h$.
Following~\citet[p.~14]{PieErn-2012}, we introduce the
\emph{broken gradient} $\nabla_h$ as a convenient notation:
\[
	(\nabla_h \phi_h)_{|K} = \nabla (\phi_{h|K})
   	, \ \forall K \in \mathcal{T}_h
\]
Thus
\[
        \int_\Omega
	    {\bf u}.\nabla_h \phi_h \, \varphi_h
	    \, {\rm d}x
	=
        \sum_{K \in \mathcal{T}_h}
          \int_K
	    {\bf u}.\nabla \phi_h \, \varphi_h
	    \, {\rm d}x
   	, \ \forall \phi_h, \varphi_h \in X_h
\]
This leads to a discrete version $a_h$ of the bilinear form $a$,
defined for all $\phi_h,\varphi_h\in X_h$ by
(see e.g.~\citealp[p.~57]{PieErn-2012}, eqn. (2.34)):
\begin{eqnarray*}
  a_h(\phi_h,\varphi_h) &=& 
        \int_\Omega
	    \left(
              {\bf u}.\nabla_h \phi_h \, \varphi_h
	      +
	      \sigma \phi_h \ \varphi_h
	    \right)
	    \, {\rm d}x
        +
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n} \right)
	    \phi_h \, \varphi_h
	    \, {\rm d}s
	\\
	&& + \ 
        \sum_{S \in \mathscr{S}_h^{(i)}}
          \int_S
	    \left(
	      -\ 
	      {\bf u}.{\bf n} \,
	      \jump{\phi_h} \, \average{\varphi_h}
              +
	      \Frac{\alpha}{2}
	      |{\bf u}.{\bf n}| \,
	      \jump{\phi_h} \, \jump{\varphi_h}
            \right)
	    \, {\rm d}s
\end{eqnarray*}
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{upwinding-triangle-2-fig.pdf}
  \end{center}
  \caption{Discontinuous Galerkin method:
	an internal side,
        its two neighbor elements and their opposite normals.}
  \label{fig-dg-neighbor}
\end{figure}
\cindex{internal sides of a mesh}%
The last term involves a sum over $\mathscr{S}_h^{(i)}$, the set
of \emph{internal sides} of the mesh $\mathcal{T}_h$.
Each internal side $S\in \mathscr{S}_h^{(i)}$ has two possible orientations: one is 
choosen definitively. In practice, this orientation is defined
in the \file{.geo} file containing the mesh,
where all sides are listed, together with their orientation.
Let ${\bf n}$ the normal to the oriented side $S$:
as $S$ is an internal side, there exists two elements $K_-$ and $K_+$
such that $S=\partial K_- \cap \partial K_+$ and ${\bf n}$ is the
outward unit normal of $K_-$ on $\partial K_- \cap S$ and the
 inward unit normal of $K_+$ on $\partial K_+ \cap S$,
as shown on Fig.~\ref{fig-dg-neighbor}.
For all $\phi_h\in X_h$, recall that $\phi_h$ is in general discontinuous
across the internal side $S$.
We define on $S$ the {\em inner value} 
\mbox{$\phi_h^- = \phi_{h|K_-}$}
of $\phi_h$ as the restriction $\phi_{h|K_-}$
of $\phi_h$ in $K_-$ along $\partial K_-\cap S$.
Conversely, we define the {\em outer value}
\mbox{$\phi_h^ + = \phi_{h|K_+}$}.
\cindex{operator!\code{jump}, across sides}%
\cindex{operator!\code{average}, across sides}%
\cindex{form!{$\jump{u}\jump{v}$}}%
\cindex{form!{$\jump{u}\average{v}$}}%
We also denote on $S$ the \emph{jump}
	$\jump{\phi_h}=\phi_h^--\phi_h^+$
and the \emph{average}
	$\average{\phi_h}=(\phi_h^-+\phi_h^+)/2$.
\cindex{upwinding}%
The last term in the definition of $a_h$ is pondered by 
a coefficient $\alpha\geq 0$.
Choosing $\alpha=0$ correspond to the so-called \emph{centered flux} approximation,
while $\alpha=1$ is the \emph{upwinding flux} approximation.
\apindex{P0}%
The case $\alpha=1$ and $k=0$ (piecewise constant approximation)
leads to the popular upwinding finite volume scheme.
Finally, the discrete variational formulation writes:

\ \ $(FV)_h$: \emph{find $\phi_h\in X_h$ such that}
\begin{eqnarray*}
  a_h(\phi_h,\varphi_h) &=& l(\varphi_h), \ \forall \varphi_h\in X_h
\end{eqnarray*}

The following code implement this problem in the \Rheolef\  environment.
% ---------------------------------------
\myexamplelicense{transport_dg.cc}
% ---------------------------------------

% ------------------------------
\subsubsection*{Comments}
% ------------------------------
The data are $f=0$, $\phi_\gamma=1$ and ${\bf u}=(1,0,0)$,
and then the exact solution is known:
$\phi(x)=\exp(-\sigma x_0)$.
The numerical tests are running with $\sigma=3$ by default.
The one-dimensional case writes:
\begin{verbatim}
  make transport_dg
  mkgeo_grid -e 10 > line.geo
  ./transport_dg line P0  | field -
  ./transport_dg line P1d | field -
  ./transport_dg line P2d | field -
\end{verbatim}
Observe the jumps across elements: these jumps 
decreases with mesh refinement or when polynomial approximation increases.
The two-dimensional case writes:
\begin{verbatim}
  mkgeo_grid -t 10 > square.geo
  ./transport_dg square P0  | field -
  ./transport_dg square P1d | field -elevation -
  ./transport_dg square P2d | field -elevation -
\end{verbatim}
The elevation view shows details on inter-element jumps.
Finaly, the three-dimensional case writes:
\begin{verbatim}
  mkgeo_grid -T 5 > cube.geo
  ./transport_dg cube P0  | field -
  ./transport_dg cube P1d | field -
  ./transport_dg cube P2d | field -
\end{verbatim}

  \begin{figure}[htb]
    \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=5.5cm]{transport_dg-1d-elevation.pdf} &
          \includegraphics[height=5.5cm]{transport_dg-1d-elevation2.pdf}
       \end{tabular}
    \end{center}
    \caption{The discontinuous Galerkin method
	for the transport problem when $k=0$ and $d=1$.}
    \label{fig-transport-dg-view}
  \end{figure}

  \begin{figure}[htb]
    \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=5.5cm]{transport_dg-cv-l2-2d-t-grid.pdf} &
          \includegraphics[height=5.5cm]{transport_dg-cv-linf-2d-t-grid.pdf}
       \end{tabular}
    \end{center}
    \caption{The discontinuous Galerkin method for the transport problem: convergence when $d=2$.}
    \label{fig-transport-dg-cv}
  \end{figure}

Fig.~\ref{fig-transport-dg-view} plots
the solution when $d=1$ and $k=0$: observe that
the boundary condition $\phi=1$ at $x_0=0$ is only weakly satisfied.
It means that the approximation $\phi_h(0)$ tends to $1$ when $h$ tends to zero.
%
\cindex{convergence!error!versus mesh}%
Fig.~\ref{fig-transport-dg-cv} plots the error $\phi-\phi_h$ in
$L^2$ and $L^\infty$ norms: these errors behave as 
$\mathcal{O}\left(h^{k+1}\right)$ for all $k\geq 0$, which is optimal.
%
A theoretical 
$\mathcal{O}\left(h^{k+1/2}\right)$
error bound was shown by \citet{JohPit-1986}.
The present numerical results confirm that these theoretical
error bounds can be improved for some families of meshes,
as pointed out by \citet{Ric-1988},
that showed a $\mathcal{O}\left(h^{k+1}\right)$ optimal bound for
the transport problem.
This result was recently extended by \citet{CocDonGuz-2010},
while \citet{Pet-1991-dg} showed
that the estimate $\mathcal{O}\left(h^{k+1/2}\right)$ is sharp
for general families of quasi-uniform meshes.
