///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the basis orderings
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 january 2018
//
#include "basis.h"
using namespace rheolef;
using namespace std;

int main(int argc, char**argv) {
  side_information_type sid;
  string approx = (argc > 1) ?      argv[1]    : "P4";
  char   t      = (argc > 2) ?      argv[2][0] : 't';
  sid.shift     = (argc > 3) ? atoi(argv[3])   : 0; // for 3d side
  reference_element tilde_K;
  tilde_K.set_name(t);
  sid.dim = tilde_K.dimension() - 1;
  basis b (approx);
  Eigen::Matrix<size_t,Eigen::Dynamic,1> perm;
  for (sid.orient = -1; sid.orient <= 1; sid.orient += 2) {
    cout << "orient " << sid.orient << endl;
    for (sid.loc_isid = 0; sid.loc_isid < tilde_K.n_subgeo (tilde_K.dimension()-1); ++sid.loc_isid) {
      sid.n_vertex = tilde_K.subgeo_size (sid.dim, sid.loc_isid);
      sid.hat.set_variant (sid.n_vertex, sid.dim);
      b.local_idof_on_side (tilde_K, sid, perm);
      cout << "side " << sid.loc_isid << endl;
      for (size_t i = 0; i < size_t(perm.size()); ++i) cout << perm[i] << endl;
    }
  }
}
