///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile level_set_sphere.cc Extraction as a surface mesh of the zero level set -- spherical geometry
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sphere.icc"
int main (int argc, char**argv) {
  environment rheolef (argc,argv);
  geo lambda (argv[1]);
  level_set_option opts;
  opts.split_to_triangle
    = (argc > 2 && argv[2] == std::string("-tq")) ? false : true;
  space Xh (lambda, "P1");
  field phi_h = lazy_interpolate(Xh, phi);
  geo gamma = level_set (phi_h, opts);
  dout << gamma;
}
