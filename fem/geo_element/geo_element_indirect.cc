///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo_element_indirect.h"

namespace rheolef {

std::istream&
operator>> (std::istream& is, geo_element_indirect& x)
{
    char c;
    is >> std::ws >> c;
    x.set_shift(0);
    if (isdigit(c)) { // positive orientation:
      x.set_orientation(1);
      is.unget();
    } else { // negative orientation:
      x.set_orientation(-1);
    }
    geo_element_indirect::size_type idx;
    is >> idx;
    x.set_index (idx);
    return is;
}
std::ostream&
operator<< (std::ostream& os, const geo_element_indirect& x)
{
    if (x.orientation() < 0) os << '-';
    return os << x.index();
}

} // namespace rheolef
