///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/tensor.h"
using namespace rheolef;
using namespace std;

int main () {
    Float aa[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    tensor a (aa);
    cout << a << endl;
    cin >> a;
    cout << a << endl;
    point x (1,2,3);
    point y = a*x;
    cout << y << endl;
    tensor b;
    cin >> b;
    tensor c = a*b;
    cout << c << endl;
    cout << trans(c) << endl;
    tensor m = (c+trans(c))/2.;
    cout << m << endl;
#ifdef TODO 
    point lambda = eigsym(m);
    cout << lambda << endl;
    // eigenvectors are still buggy
    tensor v;
    point lambda = eigsym(m,v);
    tensor d;
    d(0,0) = lambda[0];
    d(1,1) = lambda[1];
    d(2,2) = lambda[2];
    cout << d << endl;
    cout << v << endl;
    cout << m-v*d*trans(v) << endl;
#endif // TODO 
}
