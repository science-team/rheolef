///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// draft-1 version of the geo_element class with arbitrarily order
// solution with std::vector inside each geo_element
//
// avantage : 
//  - souplesse : gere la memoire en tas dans geo (efficace)
//    mais aussi peut creer des elements temporaires, si besoin
//  - inconvenient : tableau de geo_element, qui contiennent des tableaux
//    alloues dynamiquement => la memoire n'est pas contigue globalement
//
#include "rheolef/hack_array.h"
#include "rheolef/geo_element.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;

int main(int argc, char**argv)
{
  environment env (argc, argv);
  check_macro (communicator().size() == 2, "expect nproc = 2");
  size_t dis_size = 50;
  distributor ownership (dis_size, communicator(), distributor::decide);
  check_macro (sizeof(geo_element_hack) == sizeof(geo_element::size_type), "portability problem");
  size_t order = 1;
  geo_element::parameter_type param (reference_element::e, order); // edge, order 1
  hack_array<geo_element_hack> ge_e (ownership,param);
  geo_element_auto<> E (reference_element::e, order);
  size_t jproc = (communicator().rank() == 0) ? 1 : 0;
  if (communicator().rank() == 0) {
    E[0] = 5;  
    E[1] = 18;
  } else {
    E[0] = 6;  
    E[1] = 19;  
  }
  for (size_t dis_i = ownership.first_index(jproc), dis_n = ownership.last_index(jproc); dis_i < dis_n; dis_i++) {
    ge_e.dis_entry(dis_i) = E;
  }
  ge_e.dis_entry_assembly();
  ge_e.put_values (dout);
}
