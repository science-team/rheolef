///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet_hdg.cc The Poisson problem by the hybrid discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod_dirichlet.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ?      argv[2]  : "P1d";
  Float  n      = (argc > 3) ? atof(argv[3]) : 1;
  Float  beta   = (argc > 4) ? atof(argv[4]) : 1;
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["sides"], approx);
  Mh.block("boundary");
  space Wh(Mh.get_geo()["boundary"],approx);
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  trial x(Yh), lambda(Mh);
  test  y(Yh), mu(Mh);
  auto sigma = x[0], u = x[1];
  auto tau   = y[0], v = y[1];
  integrate_option iopt;
  iopt.invert = true;
  auto coef = beta*pow(h_local(),n);
  form inv_a = integrate(dot(sigma,tau) + u*div_h(tau) + v*div_h(sigma)
                         - on_local_sides(coef*u*v), iopt);
  form b = integrate("internal_sides",
             (-dot(jump(sigma),normal()) + 2*coef*average(u))*mu)
         + integrate("boundary", (-dot(sigma,normal()) + coef*u)*mu);
  form c = integrate("internal_sides", 2*coef*lambda*mu)
         + integrate("boundary",         coef*lambda*mu);
  field lh = integrate (-f(d)*v);
  field kh(Mh,0), lambda_h(Mh,0);
  lambda_h ["boundary"] = lazy_interpolate (Wh, g(d));
  form s = c + b*inv_a*trans(b);
  field rh = b*(inv_a*lh) - kh;
  problem p (s);
  p.solve (rh, lambda_h);
  field xh = inv_a*(lh - b.trans_mult(lambda_h));
  dout << catchmark("n")      << n << endl
       << catchmark("beta")   << beta << endl
       << catchmark("u")      << xh[1]
       << catchmark("lambda") << lambda_h
       << catchmark("sigma")  << xh[0];
}
